// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkContinuousGeometryCoreExport.h>

#include <unordered_map>
#include <cstddef>
#include <functional>
#include <array>
#include <cassert>

#include <list>
#include <vector>
#include <qdebug.h>

// ///////////////////////////////////////////////////////////////////
// Geometry enums
// ///////////////////////////////////////////////////////////////////
namespace dtkContinuousGeometryEnums {
    enum TrimLoopType : unsigned short {
        outer = 0,
        inner = 1,
        unknown = 2
    };
}

// /////////////////////////////////////////////////////////////////
// Geometry primitives
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometryPrimitives {
    class Array_2 {
    public:
        Array_2(double d_0, double d_1);
        virtual ~Array_2();
        Array_2 operator*(double d) const;
        Array_2 operator/(double d) const;
        virtual Array_2 operator-(const Array_2& other) const;
        virtual Array_2 operator+(const Array_2& other) const;
        Array_2& operator=(const Array_2& other);
        bool operator==(const Array_2& other) const;
        bool operator!=(const Array_2& other) const;
        const double& operator[](std::size_t idx) const;
        double& operator[](std::size_t idx);
        double* data(void);

    protected:
        double m_data[2];
    };
    std::ostream& operator<<(std::ostream& stream, const Array_2& array);
    QDebug operator<<(QDebug stream, const Array_2& array);

    class Vector_2;
    class Point_2 : public Array_2 {
    public:
        Point_2(double d_0, double d_1);
        Point_2(const Point_2& p);
        virtual Point_2 operator*(double d) const;
        virtual Point_2 operator/(double d) const;
        virtual Point_2 operator-(const Point_2& other) const;
        virtual Point_2 operator+(const Point_2& other) const;
        virtual Point_2 operator+(const Vector_2& other) const;
    };

    class Vector_2 : public Array_2 {
    public:
        Vector_2(double d_0, double d_1);
        Vector_2(const Vector_2& p);
        virtual Vector_2 operator*(double d) const;
    };

    class Segment_2 {
    public:
        Segment_2(const Point_2& p_0, const Point_2& p_1);
        Segment_2& operator=(const Segment_2& other);
        const dtkContinuousGeometryPrimitives::Point_2& operator[](std::size_t idx) const;
        dtkContinuousGeometryPrimitives::Point_2& operator[](std::size_t idx);

    protected:
		std::array< Point_2, 2> m_ps;
    };

    class Ray_2
    {
    public:
        Ray_2(const Point_2& p, const Vector_2& v);
        const dtkContinuousGeometryPrimitives::Point_2& source(void) const;
        dtkContinuousGeometryPrimitives::Point_2& source(void);
        const dtkContinuousGeometryPrimitives::Vector_2& direction(void) const;
        dtkContinuousGeometryPrimitives::Vector_2& direction(void);

    protected:
        Point_2 m_p;
        Vector_2 m_v;
    };

    class Line_2 {
    public:
        explicit Line_2(const Point_2& p, const Vector_2& v);
        Line_2& operator=(const Line_2& other);
        const dtkContinuousGeometryPrimitives::Point_2& point(void) const;
        dtkContinuousGeometryPrimitives::Point_2& point(void);
        const dtkContinuousGeometryPrimitives::Vector_2& direction(void) const;
        dtkContinuousGeometryPrimitives::Vector_2& direction(void);

    protected:
        Point_2 m_p;
        Vector_2 m_v;
    };

    class Circle_2 {
    public:
        Circle_2(const Point_2& center, double radius);
        const dtkContinuousGeometryPrimitives::Point_2& center(void) const;
        dtkContinuousGeometryPrimitives::Point_2& center(void);
        double radius() const;

    protected:
        Point_2 m_center;
        double m_radius;
    };


    class Array_3
    {
    public:
        Array_3(double d_0, double d_1, double d_2);
        Array_3 operator*(double d) const;
        Array_3 operator/(double d) const;
        Array_3 operator-(const Array_3& other) const;
        Array_3 operator+(const Array_3& other) const;
        Array_3& operator=(const Array_3& other);
        bool operator==(const Array_3& other) const;
        bool operator!=(const Array_3& other) const;
        const double& operator[](std::size_t idx) const;
        double& operator[](std::size_t idx);
        double* data(void);

    protected:
        double m_data[3];
    };
    std::ostream& operator<<(std::ostream& stream, const Array_3& array);
    QDebug operator<<(QDebug stream, const Array_3& array);
    class Point_3 : public Array_3
    {
    public:
        Point_3(double d_0, double d_1, double d_2);
        Point_3(const Array_3& a);
        Point_3(const Point_3& p);
        Point_3& operator=(const Point_3& other);
    };
    class Vector_3 : public Array_3 {
    public:
        Vector_3(double d_0, double d_1, double d_2);
        Vector_3(const Array_3& a);
        Vector_3(const Vector_3& p);
        Vector_3& operator=(const Vector_3& other);
    };

    class Segment_3 {
    public:
        Segment_3(const Point_3& p_0, const Point_3& p_1);
        Segment_3& operator=(const Segment_3& other);
        bool operator==(const Segment_3& other) const;
        const dtkContinuousGeometryPrimitives::Point_3& operator[](std::size_t idx) const;
        dtkContinuousGeometryPrimitives::Point_3& operator[](std::size_t idx);

    protected:
		std::array< Point_3, 2> m_ps;
    };

    class Ray_3 {
    public:
        Ray_3(const Point_3& p, const Vector_3& v);
        const dtkContinuousGeometryPrimitives::Point_3& source(void) const;
        dtkContinuousGeometryPrimitives::Point_3& source(void);
        const dtkContinuousGeometryPrimitives::Vector_3& direction(void) const;
        dtkContinuousGeometryPrimitives::Vector_3& direction(void);

    protected:
        Point_3 m_p;
        Vector_3 m_v;
    };

    class Line_3 {
    public:
        explicit Line_3(const Point_3& p, const Vector_3& v);
        const dtkContinuousGeometryPrimitives::Point_3& point(void) const;
        dtkContinuousGeometryPrimitives::Point_3& point(void);
        const dtkContinuousGeometryPrimitives::Vector_3& direction(void) const;
        dtkContinuousGeometryPrimitives::Vector_3& direction(void);

    protected:
        Point_3 m_p;
        Vector_3 m_v;
    };

    class Sphere_3 {
    public:
        explicit Sphere_3(const Point_3& center, double radius);
        Sphere_3(const Sphere_3& other);
        const dtkContinuousGeometryPrimitives::Point_3& center(void) const;
        dtkContinuousGeometryPrimitives::Point_3& center(void);
        double radius(void) const;
        double& radius(void);

    protected:
        Point_3 m_center;
        double m_radius;
    };

    class Array_4
    {
    public:
        Array_4(double d_0, double d_1, double d_2, double d_3);
        Array_4 operator*(double d) const;
        Array_4 operator-(const Array_4& other) const;
        Array_4& operator=(const Array_4& other);
        bool operator==(const Array_4& other) const;
        const double& operator[](std::size_t idx) const;
        double& operator[](std::size_t idx);
        double* data(void);

    protected:
        double m_data[4];
    };
    std::ostream& operator<<(std::ostream& stream, const Array_4& array);
    QDebug operator<<(QDebug stream, const Array_4& array);

    class Point_4 : public Array_4
    {
    public:
        Point_4(double d_0, double d_1, double d_2, double d_3);
        Point_4(const Array_4& a);
        Point_4(const Point_4& p);
    };

    class AABB_2 : public Array_4
    {
    public:
        AABB_2(double x_min, double y_min, double x_max, double y_max);
        AABB_2(const Array_4& a);
        AABB_2(const AABB_2& p);
        double xmin(void);
        double ymin(void);
        double xmax(void);
        double ymax(void);
    };

    class Array_6
    {
    public:
        Array_6(double d_0, double d_1, double d_2, double d_3, double d_4, double d_5);
        Array_6 operator*(double d) const;
        Array_6 operator-(const Array_6& other) const;
        Array_6& operator=(const Array_6& other);
        bool operator==(const Array_6& other) const;
        const double& operator[](std::size_t idx) const;
        double& operator[](std::size_t idx);
        double* data(void);

    protected:
        double m_data[6];
    };
    std::ostream& operator<<(std::ostream& stream, const Array_6& array);
    QDebug operator<<(QDebug stream, const Array_6& array);

    class AABB_3 : public Array_6
    {
    public:
        AABB_3(double x_min, double y_min, double z_min, double x_max, double y_max, double z_max);
        AABB_3(const Array_6& a);
        AABB_3(const AABB_3& p);
        double xmin(void);
        double ymin(void);
        double zmin(void);
        double xmax(void);
        double ymax(void);
        double zmax(void);
    };

    class PointImage : public Point_3
    {
    public:
        PointImage(Point_3 point, bool liability);
        PointImage(const PointImage& other);

        const Point_3& point() const;
        bool liability() const;

    private :
        bool p_liability;
    };

    class PointPreImage : public Point_2
    {
    public:
        PointPreImage(Point_2 point, bool liability);
        PointPreImage(const PointPreImage& other);
        PointPreImage& operator=(const PointPreImage& other);
        const Point_2& point() const;
        bool liability() const;

    private:
        bool p_liability;
    };

    //Not for surfaces degenerated cases
    class PointIntersection
    {
    public:
        PointIntersection();
        PointIntersection(const PointImage& image);
        PointIntersection(const PointIntersection& other);

        void pushBackPreImage(const PointPreImage& pre_image);

        void popBackPreImage(void);

        const PointImage& image(void) const;
        const std::list< PointPreImage >& preImages(void) const;

    private:
        PointImage p_image;
        /* The preimage could possibly be a segment, on a plane*/
        std::list< PointPreImage > p_preImages;
    };

    struct SegmentIntersection {
        SegmentIntersection();
        SegmentIntersection(const Segment_3& image);
        SegmentIntersection(const SegmentIntersection& other);

        SegmentIntersection& operator=(const SegmentIntersection& other);

        void pushBackPreImage(const Segment_2& point);

        void popBackPreImage(void);

        const Segment_3& image(void) const;
        const std::list< Segment_2 >& preImages(void) const;

    private:
        Segment_3 p_image;
        /* The preimage could possibly be a segment, on a plane*/
        std::list< Segment_2 > p_preImages;
    };

    struct LineIntersection {
        LineIntersection();
        LineIntersection(const Line_3& image);
        LineIntersection(const LineIntersection& other);

        void pushBackPreImage(const Line_2& point);

        void popBackPreImage(void);

        const Line_3& image(void) const;
        const std::list< Line_2 >& preImages(void) const;

    private:
        Line_3 p_image;
        /* The preimage could possibly be a segment, or a plane*/
        std::list< Line_2 > p_preImages;
    };

    class IntersectionObject {
    public:
        IntersectionObject(const PointIntersection& p_int);
        IntersectionObject(const SegmentIntersection& s_int);
        IntersectionObject(const LineIntersection& l_int);

        IntersectionObject& operator=(const IntersectionObject& other);

        IntersectionObject(const IntersectionObject& other);

        bool isPoint(void) const;
        bool isSegment(void) const;
        bool isLine(void) const;

        const PointIntersection& pointIntersection() const;
        const SegmentIntersection& segmentIntersection() const;
        const LineIntersection& lineIntersection() const;

        ~IntersectionObject() {}

    private:
        PointIntersection m_p_int;
        SegmentIntersection m_s_int;
        LineIntersection m_l_int;

        bool m_is_p;
        bool m_is_s;
        bool m_is_l;
    };

    class PointImage_2 : public Point_2 {
    public:
        PointImage_2(Point_2 point, bool liability);
        PointImage_2(const PointImage_2& other);

        const Point_2& point() const;
        bool liability() const;

    private :
        bool p_liability;
    };

    class PointPreImage_1 {
    public:
        PointPreImage_1(double point, bool liability);
        PointPreImage_1(const PointPreImage_1& other);

        double point() const;
        bool liability() const;

    private:
        double m_point;
        bool m_liability;
    };

    class PointIntersection_2 {

    public:
        PointIntersection_2();
        PointIntersection_2(const PointImage_2& image);
        PointIntersection_2(const PointIntersection_2& other);

        void pushBackPreImage(const PointPreImage_1& pre_image);

        void popBackPreImage(void);

        const PointImage_2& image(void) const;
        const std::list< PointPreImage_1 >& preImages(void) const;

    private:
        PointImage_2 p_image;
        /* The preimage could possibly be a segment, on a plane*/
        std::list< PointPreImage_1 > p_preImages;
    };


    class IntersectionObject_2 {
    public:
        IntersectionObject_2(const PointIntersection_2& p_int);

        IntersectionObject_2& operator=(const IntersectionObject_2& other);

        IntersectionObject_2(const IntersectionObject_2& other);

        bool isPoint(void) const;

        const PointIntersection_2& pointIntersection() const;

        ~IntersectionObject_2();

    private:
        PointIntersection_2 m_p_int;

        bool m_is_p;
    };
}

// /////////////////////////////////////////////////////////////////
// Geometry Tools
// /////////////////////////////////////////////////////////////////
class dtkRationalBezierCurve;
class dtkRationalBezierCurve2D;
class dtkClippedNurbsSurface;
class dtkRationalBezierSurface;
class dtkNurbsCurve;
class dtkNurbsCurve2D;
class dtkNurbsSurface;

namespace  dtkContinuousGeometryTools {

    typedef dtkContinuousGeometryPrimitives::Point_2 Point_2;
    typedef dtkContinuousGeometryPrimitives::Vector_2 Vector_2;
    typedef dtkContinuousGeometryPrimitives::Circle_2 Circle_2;

    typedef dtkContinuousGeometryPrimitives::Array_3 Array_3;
    typedef dtkContinuousGeometryPrimitives::Point_3 Point_3;
    typedef dtkContinuousGeometryPrimitives::Vector_3 Vector_3;
    typedef dtkContinuousGeometryPrimitives::Sphere_3 Sphere_3;

    // /////////////////////////////////////////////////////////////////
    // Hash for dtkMatrixMaps
    // /////////////////////////////////////////////////////////////////

    template <typename SizeT> inline void dtkHashCombineImpl(SizeT& seed, SizeT value)
    {
        seed ^= value + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    }

    template < typename T > inline void dtkHashCombine(std::size_t& seed, const T& v)
    {
        std::hash<T> hasher;
        dtkHashCombineImpl(seed, hasher(v));
    }

    struct dtkHash {
        template < typename T, typename U > std::size_t operator()(const std::pair<T, U>& c) const;
        template < typename T, std::size_t A > std::size_t operator()(const std::array<T, A>& c) const;
    };

    template < typename T, std::size_t A > inline std::size_t dtkHash::operator()(const std::array<T, A>& c) const
    {
        return dtkHashRange(c.begin(), c.end());
    }

    template < typename T, typename U > inline std::size_t dtkHash::operator()(const std::pair<T, U>& c) const
    {
        std::size_t seed = 0;
        dtkHashCombine(seed, c.first);
        dtkHashCombine(seed, c.second);
        return seed;
    }

    template< typename K > using dtkMatrixMap = std::unordered_map< std::pair< std::size_t, std::size_t>, K, dtkHash >;
    template< typename K > using dtkMap = std::unordered_map< double, K >;

    // ///////////////////////////////////////////////////////////
    void computeBinomialCoefficients(dtkMatrixMap< double >& r_binomial_coefficients, std::size_t n, std::size_t k);

    double squaredDistance(const Point_3& p_a, const Point_3& p_b);
    double squaredDistance(const Point_2& p_a, const Point_2& p_b);

    Vector_3 crossProduct(const Array_3& p_a, const Array_3& p_b);
    void crossProduct(Vector_3& r_vector, const Array_3& p_a, const Array_3& p_b);

    double dotProduct(const Array_3& p_a, const Array_3& p_b);

    void normalize(Vector_2& r_vector);
    void normalize(Vector_3& r_vector);

    double norm(const Vector_3& p_vector);
    double norm(const Vector_2& p_vector);

    bool isNull(const Vector_2& p_vector, double p_tolerance_to_zero);
    bool isNull(const Vector_3& p_vector, double p_tolerance_to_zero);

    bool isInAAB(const Point_2& p_point, double xmin, double ymin, double xmax, double ymax);
    bool isInAAB(const Point_3& p_point, double xmin, double ymin, double zmin, double xmax, double ymax, double zmax);

    bool isInCircle(const Circle_2& p_circle, const Point_2& p_point);

    double distance(const Sphere_3& p_sphere_A, const Sphere_3& p_sphere_B);

    bool intersect(const Sphere_3& p_sphere_A, const Sphere_3& p_sphere_B);

    bool deepCover(const Sphere_3& p_sphere_A, const Sphere_3& p_sphere_B);

    void boundingSphere(dtkContinuousGeometryPrimitives::Sphere_3& r_sphere, const dtkContinuousGeometryPrimitives::Sphere_3& p_sphere_a, const dtkContinuousGeometryPrimitives::Sphere_3& p_sphere_b);

    double convexHullApproximationError(const dtkRationalBezierCurve& p_rational_bezier_curve);
    double convexHullApproximationError1(const dtkRationalBezierCurve& p_rational_bezier_curve);
    double convexHullApproximationError1(const dtkRationalBezierCurve2D& p_rational_bezier_curve);

    // /////////////////////////////////////////////////////////////////// 2D
    void convexHull2DApproximation(std::list< Point_2 >& r_polyline, const dtkRationalBezierCurve2D& p_rational_bezier_curve, double p_error);
    void convexHull2DApproximation(std::list< Point_2 >& r_polyline, const dtkRationalBezierCurve2D& p_rational_bezier_curve, double p_error, double p_0, double p_1);
    void convexHull2DApproximation(std::list< Point_2 >& r_polyline, const dtkNurbsCurve2D& p_nurbs_curve, double p_error);
    void convexHull2DApproximation(std::list< Point_2 >& r_polyline, const dtkNurbsCurve2D& p_nurbs_curve, double p_error, double p_0, double p_1);
    void convexHull3DApproximation(std::list< Point_2 >& r_polyline, const dtkRationalBezierCurve2D& p_rational_bezier_curve, const dtkRationalBezierSurface& p_rational_bezier_surface, double p_3D_error);
    void convexHull3DApproximation(std::list< Point_2 >& r_polyline, const dtkRationalBezierCurve2D& p_rational_bezier_curve, const dtkRationalBezierSurface& p_rational_bezier_surface, double p_3D_error, double p_0, double p_1);

    // /////////////////////////////////////////////////////////////////// 3D
    void convexHullApproximation(std::list< Point_3 >& r_polyline, const dtkNurbsCurve& p_nurbs_curve, double p_error);
    void convexHullApproximation(std::list< Point_3 >& r_polyline, const dtkRationalBezierCurve& p_rational_bezier_curve, double p_error);

    // void convexHullApproximationWithParameters(std::list< std::pair< Point_3, double  >& r_polyline, const dtkNurbsCurve& p_nurbs_curve, double p_error);
    // void convexHullApproximationWithParameters(std::list< std::pair< Point_3, double > >& r_polyline, const std::pair< dtkRationalBezierCurve *, double * >& p_rational_bezier_curve, double p_error);

    void recomposeNurbsCurve(dtkNurbsCurve& r_nurbs_curve, const std::vector< dtkRationalBezierCurve * >& p_splits);

    void decomposeToSplitRationalBezierCurves(std::vector< std::tuple< dtkRationalBezierCurve2D *, double *, const dtkRationalBezierSurface *> >& r_splits, const dtkNurbsCurve2D& p_nurbs_curve,  const dtkClippedNurbsSurface& p_clipped_nurbs_surface);

    void splitBezierCurveWithClippedNurbsSurface(std::vector< std::pair< dtkRationalBezierCurve2D *, const dtkRationalBezierSurface *> >& r_split_bezier_curves, const dtkRationalBezierCurve2D& p_bezier_curve, const dtkClippedNurbsSurface& p_clipped_nurbs_surface);

    bool isAPlane(const dtkRationalBezierSurface& p_rational_bezier_surface, double p_tolerance = 1e-10);
    bool isAPlane(const dtkNurbsSurface& p_nurbs_surface, double p_tolerance = 1e-10);

    template <typename T> int sign(T val);

    // ////////////////////////////////////////////////////////////////
    void integerPartitioning(std::uint_least16_t p_integer, std::list< std::vector< uint_least16_t > >& r_partitions)

}

#include <dtkContinuousGeometryUtils.tpp>

//
// dtkContinuousGeometryUtils.h ends here
