// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

/*!
    \page dtkcontinuousgeometry-index.html
    \title dtkContinuousGeometry Module

    \brief The dtkContinuousGeometry module is part of dtk's applicative \l {All Modules}{modules}.


    \section1 Prerequisites

    dtkContinuousGeometry is based on \l{https://github.com/d-tk/dtk}{dtk} and \l{https://www.qt.io/}{Qt}, and can not be built without these.
    \l{https://cmake.org/}{CMake} directives are shipped with the library to build it with ease.

    Most of the functionnalities are provided by plugins, distributed separately: \l{https://github.com/d-tk/dtk-plugins-continuous-geometry}{dtkContinuousGeometry plugins}

    Currently all the objects defined in the layer are implemented as plugins with \l{https://www.rhino3d.com/fr/opennurbs}{openNURBS}.

    An attempt to provide the similar coverage with \l{https://github.com/SINTEF-Geometry/SISL}{SISL} is currently on the go.

    Lastly a default implementation is available for some objects.

    \section1 Overview

    The dtkContinuousGeometry layer provides a high level API for creating and modifying parametric objects such as NURBS curves and surfaces.

	This section gives a brief overview of the steps required to add a new process (concept and concrete implementation). These topics are covered more in-depth on the dedicated pages.

    A process is defined in dtkContinuousGeometry as an implementation of a
    function (arbitrarily complex), taking a given set of inputs, and
    returning a given set of outputs.

	All processes must be implemented in plugins, thus avoiding adding
	new dependencies to the core platform. This implies that all
	processes must derive from a concept available in the core.

	\section2 Defining a concept

	see \l{Defining-a-new-concept}{How to define a new concept} for more details

	Concepts give a standard API to the function implemented by
	the process, as described above. The only features supported by
	dtkContinuousGeometry are those that have a concept in the core.

	\section2 Implementing a concept

	Some concepts are already defined in the core platform. To add an implementation for these, the recommanded way is to create a new plugin. This usually require six files:
	\list
    \li CMakeLists.txt
    \li plugin.json
	\li plugin.cpp
    \li plugin.h
    \li implementation.h
    \li implementation.cpp
	\endlist

	For the first four files, it most of the time comes down to replacing the name of an existing implementation by the new one. Implementation files have to override all the pure virtual methods defined by the concept. Most often, it can be achieved easily by storing the input and outputs in a d-pointer, leaving only the "run()" method to be written.

    \section1 Reference
    These are links to the API reference materials and some HowTo guides.
    \list
    \li \l{dtkContinuousGeometry C++ Classes}{C++ Classes API}
    \li \l{Defining-a-new-concept}{How to define a new concept}
    \li \l{Visual-programming}{The visual programming tool}
    \li \l{Adding-widgets-to-plugins}{Associate a widget to a plugin}
    \endlist

    \sa {All Modules}
*/


//
// dtkContinuousGeometry-index.qdoc ends here
