// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkContinuousGeometryUtils.h"

#include <cmath>
#include <cassert>

#include "dtkRationalBezierCurve"
#include "dtkRationalBezierCurve2D"
#include "dtkRationalBezierSurface"
#include "dtkNurbsCurve"
#include "dtkNurbsCurve2D"
#include "dtkNurbsSurface"
#include "dtkClippedNurbsSurface"
#include "dtkRationalBezierCurve2DLineIntersector"
#include "dtkRationalBezierCurve2DElevator.h"

//For debbuging purpose only
#include <iostream>

// ///////////////////////////////////////////////////////////////////
// Geometry enums
// ///////////////////////////////////////////////////////////////////

/*!
  \namespace dtkContinuousGeometryEnums
  \inmodule dtkContinuousGeometry

  \brief Contains all the enums of \l dtkContinuousGeometry layer.
*/

/*!
  \enum dtkContinuousGeometryEnums::TrimLoopType
  Describes which part of the surface is to be kept.

  \value outer
         The part of the surface outside of the trim loop is to be kept.
  \value inner
         The part of the surface inside the trim loop is to be kept.
  \value unknown
         If the type of the trim loop isn't known. This state should not be used.
*/

// /////////////////////////////////////////////////////////////////
// Geometry primitives
// /////////////////////////////////////////////////////////////////
/*!
  \namespace dtkContinuousGeometryPrimitives
  \inmodule dtkContinuousGeometry

  \brief Contains all the primitives that that can be used within this framework.
*/
namespace dtkContinuousGeometryPrimitives {
    Array_2::Array_2(double d_0, double d_1)
    {
        m_data[0] = d_0;
        m_data[1] = d_1;
    }
    Array_2::~Array_2() {}
    Array_2 Array_2::operator*(double d) const
    {
        Array_2 r((*this).m_data[0] * d, (*this).m_data[1] * d);
        return r;
    }
    Array_2 Array_2::operator/(double d) const
    {
        Array_2 r((*this).m_data[0] / d, (*this).m_data[1] / d);
        return r;
    }
    Array_2 Array_2::operator+(const Array_2& other) const
    {
        Array_2 r((*this).m_data[0] + other.m_data[0], (*this).m_data[1] + other.m_data[1]);
        return r;
    }
    Array_2 Array_2::operator-(const Array_2& other) const
    {
        Array_2 r((*this).m_data[0] - other.m_data[0], (*this).m_data[1] - other.m_data[1]);
        return r;
    }
    Array_2& Array_2::operator=(const Array_2& other)
    {
        (*this).m_data[0] = other.m_data[0];
        (*this).m_data[1] = other.m_data[1];

        return (*this);
    }
    bool Array_2::operator==(const Array_2& other) const {
        if ((*this).m_data[0] == other.m_data[0] && (*this).m_data[1] == other.m_data[1]) {
            return true;
        }
        return false;
    }
    bool Array_2::operator!=(const Array_2& other) const {
        if ((*this).m_data[0] != other.m_data[0] || (*this).m_data[1] != other.m_data[1]) {
            return false;
        }
        return true;
    }
    const double& Array_2::operator[](std::size_t idx) const
    {
        assert(idx < 2);
        return m_data[idx];
    }
    double& Array_2::operator[](std::size_t idx)
    {
        assert(idx < 2);
        return m_data[idx];
    }
    double* Array_2::data(void)
    {
        return &m_data[0];
    }
    std::ostream& operator<<(std::ostream& stream, const Array_2& array)
    {
        stream << array[0] << " " << array[1];
        return stream;
    }
    QDebug operator<<(QDebug stream, const Array_2& array)
    {
        stream << array[0] << " " << array[1];
        return stream;
    }
    // ///////////////////////////////////////////////////////////////////

    Point_2::Point_2(double d_0, double d_1) : Array_2(d_0, d_1) {}
    Point_2::Point_2(const Point_2& p) : Array_2(p.m_data[0], p.m_data[1]) {}
    Point_2 Point_2::operator*(double d) const
    {
        Point_2 r((*this).m_data[0] * d, (*this).m_data[1] * d);
        return r;
    }
    Point_2 Point_2::operator/(double d) const
    {
        Point_2 r((*this).m_data[0] / d, (*this).m_data[1] / d);
        return r;
    }
    Point_2 Point_2::operator-(const Point_2& other) const
    {
        return Point_2((*this).m_data[0] - other.m_data[0], (*this).m_data[1] - other.m_data[1]);
    }
    Point_2 Point_2::operator+(const Point_2& other) const
    {
        return Point_2((*this).m_data[0] + other.m_data[0], (*this).m_data[1] + other.m_data[1]);
    }
    Point_2 Point_2::operator+(const Vector_2& other) const
    {
        return Point_2((*this).m_data[0] + other[0], (*this).m_data[1] + other[1]);
    }
    // ///////////////////////////////////////////////////////////////////

    Vector_2::Vector_2(double d_0, double d_1) : Array_2(d_0, d_1) {}
    Vector_2::Vector_2(const Vector_2& v) : Array_2(v.m_data[0],  v.m_data[1]) {}
    Vector_2 Vector_2::operator*(double d) const
    {
        Vector_2 r((*this).m_data[0] * d, (*this).m_data[1] * d);
        return r;
    }
    // ///////////////////////////////////////////////////////////////////

	Segment_2::Segment_2(const Point_2& p_0, const Point_2& p_1) : m_ps({{ p_0, p_1 }}) {}

    Segment_2& Segment_2::operator=(const Segment_2& other)
    {
        (*this).m_ps[0] = other.m_ps[0];
        (*this).m_ps[1] = other.m_ps[1];

        return (*this);
    }
    const dtkContinuousGeometryPrimitives::Point_2& Segment_2::operator[](std::size_t idx) const
    {
        assert(idx < 2);
        return m_ps[idx];
    }
    dtkContinuousGeometryPrimitives::Point_2& Segment_2::operator[](std::size_t idx)
    {
        assert(idx < 2);
        return m_ps[idx];
    }

    // ///////////////////////////////////////////////////////////////////

    Ray_2::Ray_2(const Point_2& p, const Vector_2& v) : m_p(p), m_v(v) {}
    const dtkContinuousGeometryPrimitives::Point_2& Ray_2::source(void) const {return m_p;}
    dtkContinuousGeometryPrimitives::Point_2& Ray_2::source(void) {return m_p;}
    const dtkContinuousGeometryPrimitives::Vector_2& Ray_2::direction(void) const {return m_v;}
    dtkContinuousGeometryPrimitives::Vector_2& Ray_2::direction(void) {return m_v;}

    // ///////////////////////////////////////////////////////////////////

    Line_2::Line_2(const Point_2& p, const Vector_2& v) : m_p(p), m_v(v) {}
    Line_2& Line_2::operator=(const Line_2& other) {
        (*this).m_p = other.m_p;
        (*this).m_v = other.m_v;

        return (*this);
    }
    const dtkContinuousGeometryPrimitives::Point_2& Line_2::point(void) const {return m_p;}
    dtkContinuousGeometryPrimitives::Point_2& Line_2::point(void) {return m_p;}
    const dtkContinuousGeometryPrimitives::Vector_2& Line_2::direction(void) const {return m_v;}
    dtkContinuousGeometryPrimitives::Vector_2& Line_2::direction(void) {return m_v;}

    // ///////////////////////////////////////////////////////////////

    Circle_2::Circle_2(const Point_2& center, double radius) : m_center(center), m_radius(radius) {};
    const dtkContinuousGeometryPrimitives::Point_2& Circle_2::center(void) const {return m_center;}
    dtkContinuousGeometryPrimitives::Point_2& Circle_2::center(void) {return m_center;}
    double Circle_2::radius(void) const {return m_radius;}

    // ///////////////////////////////////////////////////////////////////
    Array_3::Array_3(double d_0, double d_1, double d_2)
    {
        m_data[0] = d_0;
        m_data[1] = d_1;
        m_data[2] = d_2;
    }
    Array_3 Array_3::operator*(double d) const
    {
        Array_3 r((*this).m_data[0] * d, (*this).m_data[1] * d, (*this).m_data[2] * d);
        return r;
    }
    Array_3 Array_3::operator/(double d) const
    {
        Array_3 r((*this).m_data[0] / d, (*this).m_data[1] / d, (*this).m_data[2] / d);
        return r;
    }
    Array_3 Array_3::operator+(const Array_3& other) const
    {
        Array_3 r((*this).m_data[0] + other.m_data[0], (*this).m_data[1] + other.m_data[1], (*this).m_data[2] + other.m_data[2]);
        return r;
    }
    Array_3 Array_3::operator-(const Array_3& other) const
    {
        Array_3 r((*this).m_data[0] - other.m_data[0], (*this).m_data[1] - other.m_data[1], (*this).m_data[2] - other.m_data[2]);
        return r;
    }
    Array_3& Array_3::operator=(const Array_3& other)
    {
        (*this).m_data[0] = other.m_data[0];
        (*this).m_data[1] = other.m_data[1];
        (*this).m_data[2] = other.m_data[2];

        return (*this);
    }
    bool Array_3::operator==(const Array_3& other) const {
        if ((*this).m_data[0] == other.m_data[0] && (*this).m_data[1] == other.m_data[1] && (*this).m_data[2] == other.m_data[2]) {
            return true;
        }
        return false;
    }
    bool Array_3::operator!=(const Array_3& other) const {
        if ((*this).m_data[0] != other.m_data[0] || (*this).m_data[1] != other.m_data[1] || (*this).m_data[2] != other.m_data[2]) {
            return true;
        }
        return false;
    }
    const double& Array_3::operator[](std::size_t idx) const
    {
        assert(idx < 3);
        return m_data[idx];
    }
    double& Array_3::operator[](std::size_t idx)
    {
        assert(idx < 3);
        return m_data[idx];
    }
    double* Array_3::data(void)
    {
        return &m_data[0];
    }
    std::ostream& operator<<(std::ostream& stream, const Array_3& array)
    {
        stream << array[0] << " " << array[1] << " " << array[2];
        return stream;
    }
    QDebug operator<<(QDebug stream, const Array_3& array)
    {
        stream << array[0] << " " << array[1] << " " << array[2];
        return stream;
    }

    // ///////////////////////////////////////////////////////////////////

    Point_3::Point_3(const Array_3& a) : Array_3(a) {}
    Point_3::Point_3(double d_0, double d_1, double d_2) : Array_3(d_0, d_1, d_2) {}
    Point_3::Point_3(const Point_3& p) : Array_3(p.m_data[0], p.m_data[1], p.m_data[2]) {}
    Point_3& Point_3::operator=(const Point_3& other)
    {
        (*this).m_data[0] = other.m_data[0];
        (*this).m_data[1] = other.m_data[1];
        (*this).m_data[2] = other.m_data[2];
    }
    // ///////////////////////////////////////////////////////////////

    Vector_3::Vector_3(const Array_3& a) : Array_3(a) {}
    Vector_3::Vector_3(double d_0, double d_1, double d_2) : Array_3(d_0, d_1, d_2) {}
    Vector_3::Vector_3(const Vector_3& p) : Array_3(p.m_data[0], p.m_data[1], p.m_data[2]) {}
    Vector_3& Vector_3::operator=(const Vector_3& other)
    {
        (*this).m_data[0] = other.m_data[0];
        (*this).m_data[1] = other.m_data[1];
        (*this).m_data[2] = other.m_data[2];
    }
// ///////////////////////////////////////////////////////////////////
	Segment_3::Segment_3(const Point_3& p_0, const Point_3& p_1) : m_ps({{p_0, p_1}})
	{}

    Segment_3& Segment_3::operator=(const Segment_3& other)
    {
        (*this).m_ps[0] = other.m_ps[0];
        (*this).m_ps[1] = other.m_ps[1];

        return (*this);
    }
    bool Segment_3::operator==(const Segment_3& other) const
    {
        if ((*this).m_ps[0] == other.m_ps[0] && (*this).m_ps[0] == other.m_ps[0]) {
            return true;
        }
        return false;
    }
    const dtkContinuousGeometryPrimitives::Point_3& Segment_3::operator[](std::size_t idx) const {
        assert(idx < 2);
        return m_ps[idx];
    }
    dtkContinuousGeometryPrimitives::Point_3& Segment_3::operator[](std::size_t idx)
    {
        assert(idx < 2);
        return m_ps[idx];
    }

    // ///////////////////////////////////////////////////////////////

    Ray_3::Ray_3(const Point_3& p, const Vector_3& v) : m_p(p), m_v(v) {}
    const dtkContinuousGeometryPrimitives::Point_3& Ray_3::source(void) const {return m_p;}
    dtkContinuousGeometryPrimitives::Point_3& Ray_3::source(void) {return m_p;}
    const dtkContinuousGeometryPrimitives::Vector_3& Ray_3::direction(void) const {return m_v;}
    dtkContinuousGeometryPrimitives::Vector_3& Ray_3::direction(void) {return m_v;}

    // ///////////////////////////////////////////////////////////////////

    Line_3::Line_3(const Point_3& p, const Vector_3& v) : m_p(p), m_v(v) {}
    const dtkContinuousGeometryPrimitives::Point_3& Line_3::point(void) const {return m_p;}
    dtkContinuousGeometryPrimitives::Point_3& Line_3::point(void) {return m_p;}
    const dtkContinuousGeometryPrimitives::Vector_3& Line_3::direction(void) const {return m_v;}
    dtkContinuousGeometryPrimitives::Vector_3& Line_3::direction(void) {return m_v;}

    Sphere_3::Sphere_3(const Point_3& center, double radius) : m_center(center), m_radius(radius) {}
    Sphere_3::Sphere_3(const Sphere_3& other) : m_center(other.m_center), m_radius(other.m_radius) {}
    const dtkContinuousGeometryPrimitives::Point_3& Sphere_3::center(void) const {return m_center;}
    dtkContinuousGeometryPrimitives::Point_3& Sphere_3::center(void) {return m_center;}
    double Sphere_3::radius(void) const {return m_radius;}
    double& Sphere_3::radius(void) {return m_radius;}

    // ///////////////////////////////////////////////////////////////////
    Array_4::Array_4(double d_0, double d_1, double d_2, double d_3)
    {
        m_data[0] = d_0;
        m_data[1] = d_1;
        m_data[2] = d_2;
        m_data[3] = d_3;
    }
    Array_4 Array_4::operator*(double d) const
    {
        Array_4 r((*this).m_data[0] * d, (*this).m_data[1] * d, (*this).m_data[2] * d, (*this).m_data[3] * d);
        return r;
    }
    Array_4 Array_4::operator-(const Array_4& other) const
    {
        Array_4 r((*this).m_data[0] - other.m_data[0], (*this).m_data[1] - other.m_data[1], (*this).m_data[2] - other.m_data[2], (*this).m_data[3] - other.m_data[3]);
        return r;
    }
    Array_4& Array_4::operator=(const Array_4& other)
    {
        (*this).m_data[0] = other.m_data[0];
        (*this).m_data[1] = other.m_data[1];
        (*this).m_data[2] = other.m_data[2];
        (*this).m_data[3] = other.m_data[3];

        return (*this);
    }
    bool Array_4::operator==(const Array_4& other) const {
        if ((*this).m_data[0] == other.m_data[0] && (*this).m_data[1] == other.m_data[1] && (*this).m_data[2] == other.m_data[2] && (*this).m_data[2] == other.m_data[3]) {
            return true;
        }
        return false;
    }
    const double& Array_4::operator[](std::size_t idx) const
    {
        assert(idx < 4);
        return m_data[idx];
    }
    double& Array_4::operator[](std::size_t idx)
    {
        assert(idx < 4);
        return m_data[idx];
    }
    double* Array_4::data(void)
    {
        return &m_data[0];
    }
    std::ostream& operator<<(std::ostream& stream, const Array_4& array)
    {
        stream << array[0] << " " << array[1] << " " << array[2] << " " << array[3];
        return stream;
    }
    QDebug operator<<(QDebug stream, const Array_4& array)
    {
        stream << array[0] << " " << array[1] << " " << array[2] << " " << array[3];
        return stream;
    }
    // ///////////////////////////////////////////////////////////////////

    Point_4::Point_4(const Array_4& a) : Array_4(a) {}
    Point_4::Point_4(double d_0, double d_1, double d_2, double d_3) : Array_4(d_0, d_1, d_2, d_3) {}
    Point_4::Point_4(const Point_4& p) : Array_4(p.m_data[0], p.m_data[1], p.m_data[2], p.m_data[3]) {}

    // ///////////////////////////////////////////////////////////////

    AABB_2::AABB_2(double x_min, double y_min, double x_max, double y_max) : Array_4(x_min, y_min, x_max, y_max)  {
        assert(x_min <= x_max);
        assert(y_min <= y_max);
    }
    AABB_2::AABB_2(const Array_4& a)  : Array_4(a) {
        assert(a[0] <= a[2]);
        assert(a[1] <= a[3]);
    }

    AABB_2::AABB_2(const AABB_2& p) : Array_4(p.m_data[0], p.m_data[1], p.m_data[2], p.m_data[3]) {}
    double AABB_2::xmin(void) {return m_data[0];}
    double AABB_2::ymin(void) {return m_data[1];}
    double AABB_2::xmax(void) {return m_data[2];}
    double AABB_2::ymax(void) {return m_data[3];}

    // ///////////////////////////////////////////////////////////////

    Array_6::Array_6(double d_0, double d_1, double d_2, double d_3, double d_4, double d_5)
    {
        m_data[0] = d_0;
        m_data[1] = d_1;
        m_data[2] = d_2;
        m_data[3] = d_3;
        m_data[4] = d_4;
        m_data[5] = d_5;
    }
    Array_6 Array_6::operator*(double d) const
    {
        Array_6 r((*this).m_data[0] * d, (*this).m_data[1] * d, (*this).m_data[2] * d, (*this).m_data[3] * d, (*this).m_data[4] * d, (*this).m_data[5] * d);
        return r;
    }
    Array_6 Array_6::operator-(const Array_6& other) const
    {
        Array_6 r((*this).m_data[0] - other.m_data[0], (*this).m_data[1] - other.m_data[1], (*this).m_data[2] - other.m_data[2], (*this).m_data[3] - other.m_data[3], (*this).m_data[4] - other.m_data[4], (*this).m_data[5] - other.m_data[5]);
        return r;
    }
    Array_6& Array_6::operator=(const Array_6& other)
    {
        (*this).m_data[0] = other.m_data[0];
        (*this).m_data[1] = other.m_data[1];
        (*this).m_data[2] = other.m_data[2];
        (*this).m_data[3] = other.m_data[3];
        (*this).m_data[4] = other.m_data[4];
        (*this).m_data[5] = other.m_data[5];

        return (*this);
    }
    bool Array_6::operator==(const Array_6& other) const {
        if ((*this).m_data[0] == other.m_data[0] && (*this).m_data[1] == other.m_data[1] && (*this).m_data[2] == other.m_data[2] && (*this).m_data[2] == other.m_data[3] && (*this).m_data[4] == other.m_data[4] && (*this).m_data[5] == other.m_data[5]) {
            return true;
        }
        return false;
    }
    const double& Array_6::operator[](std::size_t idx) const
    {
        assert(idx < 6);
        return m_data[idx];
    }
    double& Array_6::operator[](std::size_t idx)
    {
        assert(idx < 6);
        return m_data[idx];
    }
    double* Array_6::data(void)
    {
        return &m_data[0];
    }
    std::ostream& operator<<(std::ostream& stream, const Array_6& array)
    {
        stream << array[0] << " " << array[1] << " " << array[2] << " " << array[3] << " " << array[4] << " " << array[5];
        return stream;
    }
    QDebug operator<<(QDebug stream, const Array_6& array)
    {
        stream << array[0] << " " << array[1] << " " << array[2] << " " << array[3] << " " << array[4] << " " << array[5];
        return stream;
    }
    // ///////////////////////////////////////////////////////////////////

    AABB_3::AABB_3(double x_min, double y_min, double z_min, double x_max, double y_max, double z_max) : Array_6(x_min, y_min, z_min, x_max, y_max, z_max)  {
        assert(x_min <= x_max);
        assert(y_min <= y_max);
        assert(z_min <= z_max);
    }
    AABB_3::AABB_3(const Array_6& a)  : Array_6(a) {
        assert(a[0] <= a[3]);
        assert(a[1] <= a[4]);
        assert(a[2] <= a[5]);
    }

    AABB_3::AABB_3(const AABB_3& p) : Array_6(p.m_data[0], p.m_data[1], p.m_data[2], p.m_data[3], p.m_data[4], p.m_data[5]) {}
    double AABB_3::xmin(void) {return m_data[0];}
    double AABB_3::ymin(void) {return m_data[1];}
    double AABB_3::zmin(void) {return m_data[2];}
    double AABB_3::xmax(void) {return m_data[3];}
    double AABB_3::ymax(void) {return m_data[4];}
    double AABB_3::zmax(void) {return m_data[5];}

    // ///////////////////////////////////////////////////////////////

    PointImage::PointImage(Point_3 point, bool liability) : Point_3(point), p_liability(liability){}
    PointImage::PointImage(const PointImage& other) : Point_3(other), p_liability(other.p_liability) {}

    const Point_3& PointImage::point() const {return *(static_cast<const Point_3 *>(this));}
    bool PointImage::liability() const { return p_liability;}

    // ///////////////////////////////////////////////////////////////////

    PointPreImage::PointPreImage(Point_2 point, bool liability) : Point_2(point), p_liability(liability){}
    PointPreImage::PointPreImage(const PointPreImage& other) : Point_2(other), p_liability(other.p_liability) {}
    PointPreImage& PointPreImage::operator=(const PointPreImage& other)
    {
        (*this)[0] = other.point()[0];
        (*this)[1] = other.point()[1];

        p_liability = other.liability();

        return (*this);
    }
    const Point_2& PointPreImage::point() const {return *(static_cast<const Point_2 *>(this));}
    bool PointPreImage::liability() const { return p_liability;}

    // ///////////////////////////////////////////////////////////////

    PointIntersection::PointIntersection() : p_image(PointImage(Point_3(0., 0., 0.), false)) {};
    PointIntersection::PointIntersection(const PointImage& image) : p_image(image) {};
    PointIntersection::PointIntersection(const PointIntersection& other) : p_image(other.p_image), p_preImages(other.p_preImages) {};

    void PointIntersection::pushBackPreImage(const PointPreImage& pre_image)
    {
        p_preImages.push_back(pre_image);
    }

    void PointIntersection::popBackPreImage(void)
    {
        p_preImages.pop_back();
    }

    const PointImage& PointIntersection::image(void) const {return p_image;}
    const std::list< PointPreImage >& PointIntersection::preImages(void) const {return p_preImages;}

   // ////////////////////////////////////////////////////////////////

    SegmentIntersection::SegmentIntersection() : p_image(Segment_3(Point_3(0., 0., 0.), Point_3(0., 0., 0.))) {};
    SegmentIntersection::SegmentIntersection(const Segment_3& image) : p_image(image) {};
    SegmentIntersection::SegmentIntersection(const SegmentIntersection& other) : p_image(other.p_image), p_preImages(other.p_preImages) {};

    SegmentIntersection& SegmentIntersection::operator=(const SegmentIntersection& other)
    {
        p_image = other.p_image;
        p_preImages = other.p_preImages;
        return (*this);
    }

    void SegmentIntersection::pushBackPreImage(const Segment_2& point)
    {
        p_preImages.push_back(point);
    }

    void SegmentIntersection::popBackPreImage(void)
    {
        p_preImages.pop_back();
    }

    const Segment_3& SegmentIntersection::image(void) const {return p_image;}
    const std::list< Segment_2 >& SegmentIntersection::preImages(void) const {return p_preImages;}

    // ///////////////////////////////////////////////////////////////////

    LineIntersection::LineIntersection() : p_image(Line_3(Point_3(0., 0., 0.), Point_3(0., 0., 0.))) {};
    LineIntersection::LineIntersection(const Line_3& image) : p_image(image) {};
    LineIntersection::LineIntersection(const LineIntersection& other) : p_image(other.p_image), p_preImages(other.p_preImages) {};

    void LineIntersection::pushBackPreImage(const Line_2& point)
    {
        p_preImages.push_back(point);
    }

    void LineIntersection::popBackPreImage(void)
    {
        p_preImages.pop_back();
    }

    const Line_3& LineIntersection::image(void) const {return p_image;}
    const std::list< Line_2 >& LineIntersection::preImages(void) const {return p_preImages;}

    // ///////////////////////////////////////////////////////////////

    IntersectionObject::IntersectionObject(const PointIntersection& p_int) : m_p_int(p_int), m_is_p(true), m_is_s(false), m_is_l(false) {};
    IntersectionObject::IntersectionObject(const SegmentIntersection& s_int) : m_s_int(s_int), m_is_p(false), m_is_s(true), m_is_l(false) {};
    IntersectionObject::IntersectionObject(const LineIntersection& l_int) : m_l_int(l_int), m_is_p(false), m_is_s(false), m_is_l(true) {};

    IntersectionObject& IntersectionObject::operator=(const IntersectionObject& other)
    {
        m_p_int = PointIntersection(other.m_p_int);
        m_s_int = SegmentIntersection(other.m_s_int);
        m_l_int = LineIntersection(other.m_l_int);

        m_is_p = other.m_is_p;
        m_is_s = other.m_is_s;
        m_is_l = other.m_is_l;
        return (*this);
    }

    IntersectionObject::IntersectionObject(const IntersectionObject& other)
    {
        m_p_int = other.m_p_int;
        m_s_int = other.m_s_int;
        m_l_int = other.m_l_int;

        m_is_p = other.m_is_p;
        m_is_s = other.m_is_s;
        m_is_l = other.m_is_l;
    }

    bool IntersectionObject::isPoint(void) const {return m_is_p;}
    bool IntersectionObject::isSegment(void) const {return m_is_s;}
    bool IntersectionObject::isLine(void) const {return m_is_l;}

    const PointIntersection& IntersectionObject::pointIntersection() const {
        if (!this->isPoint()) {
            dtkWarn() << "This intersection is not a point";
        }
        return m_p_int;
    }

    const SegmentIntersection& IntersectionObject::segmentIntersection() const {
        if (!this->isSegment()) {
            dtkWarn() << "This intersection is not a segment";
        }
        return m_s_int;
    }
    const LineIntersection& IntersectionObject::lineIntersection() const {
        if (!this->isLine()) {
            dtkWarn() << "This intersection is not a line";
        }
        return m_l_int;
    }

    // ///////////////////////////////////////////////////////////////

    PointImage_2::PointImage_2(Point_2 point, bool liability) : Point_2(point), p_liability(liability){}
    PointImage_2::PointImage_2(const PointImage_2& other) : Point_2(other), p_liability(other.p_liability) {}

    const Point_2& PointImage_2::point() const {return *(static_cast<const Point_2 *>(this));}
    bool PointImage_2::liability() const { return p_liability;}

    // ///////////////////////////////////////////////////////////////

    PointPreImage_1::PointPreImage_1(double point, bool liability) : m_point(point), m_liability(liability){}
    PointPreImage_1::PointPreImage_1(const PointPreImage_1& other) : m_point(other.m_point), m_liability(other.m_liability) {}

    double PointPreImage_1::point() const {return m_point;}
    bool PointPreImage_1::liability() const { return m_liability;}

    // ///////////////////////////////////////////////////////////////

    PointIntersection_2::PointIntersection_2() : p_image(PointImage_2(Point_2(0., 0.), false)) {};
    PointIntersection_2::PointIntersection_2(const PointImage_2& image) : p_image(image) {};
    PointIntersection_2::PointIntersection_2(const PointIntersection_2& other) : p_image(other.p_image), p_preImages(other.p_preImages) {};

    void PointIntersection_2::pushBackPreImage(const PointPreImage_1& pre_image)
    {
        p_preImages.push_back(pre_image);
    }

    void PointIntersection_2::popBackPreImage(void)
    {
        p_preImages.pop_back();
    }

    const PointImage_2& PointIntersection_2::image(void) const {return p_image;}
    const std::list< PointPreImage_1 >& PointIntersection_2::preImages(void) const {return p_preImages;}

    // ///////////////////////////////////////////////////////////////

    IntersectionObject_2::IntersectionObject_2(const PointIntersection_2& p_int) : m_p_int(p_int), m_is_p(true) {};

    IntersectionObject_2& IntersectionObject_2::operator=(const IntersectionObject_2& other)
    {
        m_p_int = PointIntersection_2(other.m_p_int);

        m_is_p = other.m_is_p;

        return (*this);
    }

    IntersectionObject_2::IntersectionObject_2(const IntersectionObject_2& other)
    {
        m_p_int = other.m_p_int;

        m_is_p = other.m_is_p;
    }

    bool IntersectionObject_2::isPoint(void) const {return m_is_p;}

    const PointIntersection_2& IntersectionObject_2::pointIntersection() const {
        if (!this->isPoint()) {
            dtkWarn() << "This intersection is not a point";
        }
        return m_p_int;
    }
    IntersectionObject_2::~IntersectionObject_2() {}

}

// /////////////////////////////////////////////////////////////////
// Geometry Tools
// /////////////////////////////////////////////////////////////////

/*!
  \namespace dtkContinuousGeometryTools
  \inmodule dtkContinuousGeometry

  \brief Contains all tools that can be applied on the different primitives available under dtkContinuousGeometryPrimitives.
*/
namespace dtkContinuousGeometryTools {
    /*! \fn dtkContinuousGeometryTools::computeBinomialCoefficients(dtkMatrixMap< double >& r_binomial_coefficients, std::size_t n, std::size_t k)
      Computes all the binomial coefficients B(n, k) : \a n! / (\a k! (\a n - \a k)!) from 0 -> \a n and 0 -> \a k. \a k must be lower than \a n. It uses the recursive formula.

      \a r_binomial_coefficients : is a dtkMatrixMap which will be cleared, and reconstructed by setting the binomal coefficients up to \a k and \a n. The coefficients can then be accessed r_binomial_coefficients.at({l, m}), where l < \a k and m < \a n.
    */
    void computeBinomialCoefficients(dtkMatrixMap< double >& r_binomial_coefficients, std::size_t n, std::size_t k)
    {
        if(k >= n) { dtkFatal() << "k must be lower than n"; return;}
        r_binomial_coefficients.clear();
        // Setup the first line
        r_binomial_coefficients[{0, 0}] = 1.0 ;
        for (std::size_t j = k; j > 0; --j) {
            r_binomial_coefficients[{0, j}] = 0.;
        }

        //Setup the first row
        for (std::size_t j = 0; j <= n; ++j) {
            r_binomial_coefficients[{j, 0}] = 1.;
        }

        // Setup the other lines
        for (std::size_t i = 0; i < n; ++i) {
            r_binomial_coefficients[{i + 1, 0}] = 1.;
            for(std::size_t j = 1; j <= k  ; ++j) {
                if( i + 1 < j){
                    r_binomial_coefficients[{i, j}] = 0.;
                } else{
                    r_binomial_coefficients[{i + 1, j}] = r_binomial_coefficients[{i, j}] + r_binomial_coefficients[{i, j - 1}];
                }
            }
        }
    }

    /*! \fn dtkContinuousGeometryTools::squaredDistance(const Point_3& p_a, const Point_3& p_b)
      Returns the square product of the 3D distance between \a p_a and \a p_b.

      \a p_a : a 3D point

      \a p_b : another 3D point
     */
    double squaredDistance(const Point_3& p_a, const Point_3& p_b)
    {
        return (p_b[2] - p_a[2]) * (p_b[2] - p_a[2]) + (p_b[1] - p_a[1]) * (p_b[1] - p_a[1]) + (p_b[0] - p_a[0]) * (p_b[0] - p_a[0]);
    }

    /*! \fn dtkContinuousGeometryTools::squaredDistance(const Point_2& p_a, const Point_2& p_b)
      Returns the square product of the 2D distance between \a p_a and \a p_b.

      \a p_a : a 2D point

      \a p_b : another 2D point
    */
    double squaredDistance(const Point_2& p_a, const Point_2& p_b)
    {
        return (p_b[1] - p_a[1]) * (p_b[1] - p_a[1]) + (p_b[0] - p_a[0]) * (p_b[0] - p_a[0]);
    }

    /*! \fn dtkContinuousGeometryTools::crossProduct(const Array_3& p_a, const Array_3& p_b)
      Returns the 3D vector of cross product between \a p_a and \a p_b.

      \a p_a : a 3D array

      \a p_b : another 3D array
     */
    dtkContinuousGeometryPrimitives::Vector_3 crossProduct(const Array_3& p_a, const Array_3& p_b)
    {
        Vector_3 r_vector(0., 0., 0.);
        crossProduct(r_vector, p_a, p_b);
        return r_vector;
    }

    /*! \fn dtkContinuousGeometryTools::crossProduct(Vector_3& r_vector, const Array_3& p_a, const Array_3& p_b)
      Stores in \a r_vector the 3D vector of cross product between \a p_a and \a p_b.

      \a r_vector : the result of the cross product calculus

      \a p_a : a 3D array

      \a p_b : another 3D array
    */
    void crossProduct(Vector_3& r_vector, const Array_3& p_a, const Array_3& p_b)
    {
        r_vector[0] = p_a[1] * p_b[2] - p_a[2] * p_b[1];
        r_vector[1] = p_a[2] * p_b[0] - p_a[0] * p_b[2];
        r_vector[2] = p_a[0] * p_b[1] - p_a[1] * p_b[0];
    }

    /*! \fn dtkContinuousGeometryTools::dotProduct(const Array_3& p_a, const Array_3& p_b)
      Returns the dot product between \a p_a and \a p_b.

      \a p_a : a 3D array

      \a p_b : another 3D array
    */
    double dotProduct(const Array_3& p_a, const Array_3& p_b)
    {
        return p_a[0] * p_b[0] + p_a[1] * p_b[1] + p_a[2] * p_b[2];
    }

    /*! \fn dtkContinuousGeometryTools::normalize(Vector_2& r_vector)
      Normalizes the \a r_vector. I.e., makes it norm equals one, doesn't change its direction.

      \a r_vector : a 2D vector to normalize
    */
    void normalize(Vector_2& r_vector)
    {
        double length = std::sqrt(r_vector[0] * r_vector[0] + r_vector[1] * r_vector[1]);
        r_vector[0] /= length;
        r_vector[1] /= length;
    }

    /*! \fn dtkContinuousGeometryTools::normalize(Vector_3& r_vector)
      Normalizes the \a r_vector. I.e., makes it norm equals one, doesn't change its direction.

      \a r_vector : a 3D vector to normalize
    */
    void normalize(Vector_3& r_vector)
    {
        double length = std::sqrt(r_vector[0] * r_vector[0] + r_vector[1] * r_vector[1] + r_vector[2] * r_vector[2]);
        r_vector[0] /= length;
        r_vector[1] /= length;
        r_vector[2] /= length;
    }

    /*! \fn dtkContinuousGeometryTools::norm(const Vector_3& r_vector)
      Returns the norm of \a r_vector.

      \a r_vector : a 3D vector from which the norm is computed
    */
    double norm(const Vector_3& p_vector)
    {
        return std::sqrt(p_vector[0] * p_vector[0] + p_vector[1] * p_vector[1] + p_vector[2] * p_vector[2]);
    }

    /*! \fn dtkContinuousGeometryTools::norm(const Vector_2& r_vector)
      Returns the norm of \a r_vector.

      \a r_vector : a 2D vector from which the norm is computed
    */
    double norm(const Vector_2& p_vector)
    {
        return std::sqrt(p_vector[0] * p_vector[0] + p_vector[1] * p_vector[1]);
    }

    /*! \fn dtkContinuousGeometryTools::isNull(const Vector_2& p_vector, double p_tolerance_to_zero)
      Returns true if all values of \a p_vector are inferior or equal to \a p_tolerance_to_zero.
      Returns false otherwise.

      \a p_vector : the 2D vector to check

      \a p_tolerance_to_zero : value under which a coordinate is considered null
     */
    bool isNull(const Vector_2& p_vector, double p_tolerance_to_zero)
    {
        for (std::size_t i = 0; i < 2; ++ i) {
            if (std::fabs(p_vector[i]) > p_tolerance_to_zero) {
                return false;
            }
        }
        return true;
    }

    /*! \fn dtkContinuousGeometryTools::isNull(const Vector_3& p_vector, double p_tolerance_to_zero)
      Returns true if all values of \a p_vector are inferior or equal to \a p_tolerance_to_zero.
      Returns false otherwise.

      \a p_vector : the 3D vector to check

      \a p_tolerance_to_zero : value under which a coordinate is considered null
    */
    bool isNull(const Vector_3& p_vector, double p_tolerance_to_zero)
    {
        for (std::size_t i = 0; i < 3; ++ i) {
            if (std::fabs(p_vector[i]) > p_tolerance_to_zero) {
                return false;
            }
        }
        return true;
    }

    /*! \fn dtkContinuousGeometryTools::isInAAB(const Point_3& p_point, double xmin, double ymin, double zmin, double xmax, double ymax, double zmax)
      Returns true is the \a p_point lies in or on the 3D axis aligned bounding box defined by \a xmin, \a ymin, \a zmin, \a xmax, \a ymax, \a zmax. Else returns false.

      \a p_point : the 3D point to check
     */
    bool isInAAB(const Point_3& p_point, double xmin, double ymin, double zmin, double xmax, double ymax, double zmax)
    {
        if (p_point[0] < xmin){
            return false;
        } else if (p_point[0] > xmax){
            return false;
        } else if (p_point[1] < ymin) {
            return false;
        } else if (p_point[1] > ymax) {
            return false;
        } else if (p_point[2] < zmin) {
            return false;
        } else if (p_point[2] > zmax) {
            return false;
        } else {
            return true;
        }
    }

    /*! \fn dtkContinuousGeometryTools::isInAAB(const Point_2& p_point, double xmin, double ymin, double xmax, double ymax)
      Returns true is the \a p_point lies in or on the 2D axis aligned bounding box defined by \a xmin, \a ymin, \a xmax, \a ymax. Else returns false.

      \a p_point : the 2D point to check
     */
    bool isInAAB(const Point_2& p_point, double xmin, double ymin, double xmax, double ymax)
    {
        if (p_point[0] < xmin){
            return false;
        } else if (p_point[0] > xmax){
            return false;
        } else if (p_point[1] < ymin) {
            return false;
        } else if (p_point[1] > ymax) {
            return false;
        } else {
            return true;
        }
    }

    /*! \fn dtkContinuousGeometryTools::isInCircle(const Circle_2& p_circle, const Point_2& p_point)
      Returns true is the \a p_point lies in or on \a p_circle. Else returns false.

      \a p_circle : the circle to check

      \a p_point : the 2D point to check
     */
    bool isInCircle(const Circle_2& p_circle, const Point_2& p_point)
    {
        if ((p_point[0] - p_circle.center()[0]) * (p_point[0] - p_circle.center()[0]) +
            (p_point[1] - p_circle.center()[1]) * (p_point[1] - p_circle.center()[1])
            <= p_circle.radius() * p_circle.radius()) {
            return true;
        } else {
            return false;
        }
    }

    /*! \fn dtkContinuousGeometryTools::distance(const Sphere_3& p_sphere_A, const Sphere_3& p_sphere_B)
      Returns the distance between \a p_sphere_A and \a p_sphere_B. If the distance is < 0, it means the two spheres intersect.

      \a p_sphere_A : a sphere

      \a p_sphere_B : another sphere
    */
    double distance(const Sphere_3& p_sphere_A, const Sphere_3& p_sphere_B)
    {
        return norm(Vector_3(p_sphere_B.center() - p_sphere_A.center())) - (p_sphere_A.radius() + p_sphere_B.radius());
    }

    /*! \fn dtkContinuousGeometryTools::intersect(const Sphere_3& p_sphere_A, const Sphere_3& p_sphere_B)
      Returns true if \a p_sphere_A and \a p_sphere_A intersect. Else returns false.

      \a p_sphere_A : a sphere

      \a p_sphere_B : another sphere
    */
    bool intersect(const Sphere_3& p_sphere_A, const Sphere_3& p_sphere_B)
    {
        double distance = dtkContinuousGeometryTools::distance(p_sphere_A, p_sphere_B);
        if (distance > 0.) {
            return false;
        }
        return true;
    }

    /*! \fn dtkContinuousGeometryTools::deepCover(const Sphere_3& p_sphere_A, const Sphere_3& p_sphere_B)
      Returns true if \a p_sphere_A intersects \a p_sphere_B and \a p_sphere_A does not contain \a p_sphere_B's center and p_sphere_B does not contain \a p_sphere_A's. Else returns false.

      \a p_sphere_A : a sphere

      \a p_sphere_B : another sphere
    */
    bool deepCover(const Sphere_3& p_sphere_A, const Sphere_3& p_sphere_B)
    {
        if (intersect(p_sphere_A, p_sphere_B)) {
            // ///////////////////////////////////////////////////////////////////
            // The spheres intersect
            // ///////////////////////////////////////////////////////////////////
            if (distance(p_sphere_A, p_sphere_B) < std::min(p_sphere_A.radius(), p_sphere_B.radius())) {
                // ///////////////////////////////////////////////////////////////////
                // No center is contained in the other sphere
                // ///////////////////////////////////////////////////////////////////
                return true;
            }
            // ///////////////////////////////////////////////////////////////////
            // One center is contained in the other sphere
            // ///////////////////////////////////////////////////////////////////
            return false;
        }
        return false;
    }

    /*! \fn dtkContinuousGeometryTools::boundingSphere(dtkContinuousGeometryPrimitives::Sphere_3& r_sphere, const dtkContinuousGeometryPrimitives::Sphere_3& p_sphere_a, const dtkContinuousGeometryPrimitives::Sphere_3& p_sphere_b)
      Computes and stores in \a r_sphere the smallest sphere including both \a p_sphere_a and p_sphere_b.

      \a r_sphere : modified to represent the sphere including \a p_sphere_a and \a p_sphere_b

      \a p_sphere_a : a sphere

      \a p_sphere_b : another sphere
    */
    void boundingSphere(dtkContinuousGeometryPrimitives::Sphere_3& r_sphere, const dtkContinuousGeometryPrimitives::Sphere_3& p_sphere_a, const dtkContinuousGeometryPrimitives::Sphere_3& p_sphere_b)
    {
        // ///////////////////////////////////////////////////////////////////
        // Computes the point at mid-distance of the centers
        // ///////////////////////////////////////////////////////////////////
        dtkContinuousGeometryPrimitives::Point_3 midpoint((p_sphere_a.center()[0] + p_sphere_b.center()[0]) / 2., (p_sphere_a.center()[1] + p_sphere_b.center()[1]) / 2., (p_sphere_a.center()[2] + p_sphere_b.center()[2]) / 2.);
        double dist = std::sqrt(dtkContinuousGeometryTools::squaredDistance(p_sphere_b.center(), midpoint));
        if (dist < 1e-5) {
            r_sphere.center() = midpoint;
            r_sphere.radius() = std::max(p_sphere_b.radius(), p_sphere_a.radius());
        } else {
            double a_p = - dist - p_sphere_a.radius();
            double b_p = - dist + p_sphere_a.radius();
            double c_p = dist - p_sphere_b.radius();
            double d_p = dist + p_sphere_b.radius();
            double min_p = std::min(a_p, std::min(b_p, std::min(c_p, d_p)));
            double max_p = std::max(a_p, std::max(b_p, std::max(c_p, d_p)));
            double center = (max_p + min_p) / 2.;
            r_sphere.center() = midpoint + (p_sphere_b.center() - midpoint) * (min_p + max_p) / (2 * dist);
            r_sphere.radius() = (max_p - min_p) / 2.;
        }
    }

    /*! \fn dtkContinuousGeometryTools::convexHullApproximationError(const dtkRationalBezierCurve& p_rational_bezier_curve)
      Returns an upper bound on the approximation that is realized by approximating the \a p_rational_bezier_curve by the segment joining the first and last control point of the rational Bezier curve.

      \a p_rational_bezier_curve : the 3D rational Bezier curve to analyse
    */
    double convexHullApproximationError(const dtkRationalBezierCurve& p_rational_bezier_curve)
    {
        std::size_t degree = p_rational_bezier_curve.degree();
        // ///////////////////////////////////////////////////////////////////
        // Recovers the control points in a vector
        // ///////////////////////////////////////////////////////////////////
        std::vector< Point_3 > cps;
        cps.reserve(degree + 1);
        Point_3 cp(0., 0., 0.);
        for(std::size_t i = 0; i <= degree; ++i) {
            p_rational_bezier_curve.controlPoint(i, cp.data());
            cps[i] = cp;
        }
        dtkContinuousGeometryPrimitives::Vector_3 a(0., 0., 0.);
        dtkContinuousGeometryPrimitives::Vector_3 b(0., 0., 0.);
        dtkContinuousGeometryPrimitives::Vector_3 c(0., 0., 0.);
        double lambda = 0.;
        double temp = 0.;
        double max = 0.;
        for(std::size_t i = 1; i < degree; ++i) {
            a = Vector_3(cps[i][0] - cps[0][0], cps[i][1] - cps[0][1], cps[i][2] - cps[0][2]);
            b = Vector_3(cps[degree][0] - cps[0][0], cps[degree][1] - cps[0][1], cps[degree][2] - cps[0][2]);
            lambda = std::max(0., std::min(1., dotProduct(a, b) / squaredDistance(cps[degree], cps[0])));
            temp = norm(Vector_3(a[0] - lambda * b[0], a[1] - lambda * b[1], a[2] - lambda * b[2]));
            if (temp > max) {
                max = temp;
            }
        }
        if (degree == 2) {
            return max;
        }
        return (1 - std::pow(2, 2 - int(degree))) * max;
    }


    /*! \fn dtkContinuousGeometryTools::convexHullApproximationError1(const dtkRationalBezierCurve& p_rational_bezier_curve)
      Returns an upper bound on the approximation distance that is realized by approximating the \a p_rational_bezier_curve by the segment joining the first and last control point of the rational Bezier curve.

      \a p_rational_bezier_curve : the rational Bezier curve to analyse
    */
    double convexHullApproximationError1(const dtkRationalBezierCurve& p_rational_bezier_curve)
    {
        std::size_t degree = p_rational_bezier_curve.degree();
        if (degree == 0 ) {
            return 0.;
        }
        // ///////////////////////////////////////////////////////////////////
        // Recovers the control points in a vector
        // ///////////////////////////////////////////////////////////////////
        std::vector< Point_3 > cps;
        cps.reserve(degree + 1);
        Point_3 cp(0., 0., 0.);
        for(std::size_t i = 0; i < degree + 1; ++i) {
            p_rational_bezier_curve.controlPoint(i, cp.data());
            cps.push_back(cp);
        }
        dtkContinuousGeometryPrimitives::Vector_3 a(0., 0., 0.);
        double temp = 0.;
        double max = 0.;
        double c = 0.;
        double d = 0.;
        for(std::size_t i = 1; i < degree; ++i) {
            c = (double(degree) - double(i)) / double(degree);
            d = double(i) / double(degree);
            a = Vector_3(cps[i][0] - c * cps[0][0] - d * cps[degree][0], cps[i][1] - c * cps[0][1] - d * cps[degree][1], cps[i][2] - c * cps[0][2] - d * cps[degree][2]);
            temp = norm(a);
            if (temp > max) {
                max = temp;
            }
        }
        return (1 - std::pow(2, 1 - int(degree))) * max;
    }

    /*! \fn dtkContinuousGeometryTools::convexHullApproximationError1(const dtkRationalBezierCurve2D& p_rational_bezier_curve)
      Returns an upper bound on the approximation distance that is realized by approximating the \a p_rational_bezier_curve by the segment joining the first and last control point of the rational Bezier curve.

      \a p_rational_bezier_curve : the 2D rational Bezier curve to analyse
    */
    double convexHullApproximationError1(const dtkRationalBezierCurve2D& p_rational_bezier_curve)
    {
        std::size_t degree = p_rational_bezier_curve.degree();
        if (degree == 0 ) {
            return 0.;
        }
        // ///////////////////////////////////////////////////////////////////
        // Recovers the control points in a vector
        // ///////////////////////////////////////////////////////////////////
        std::vector< Point_2 > cps;
        cps.reserve(degree + 1);
        Point_2 cp(0., 0.);
        for(std::size_t i = 0; i < degree + 1; ++i) {
            p_rational_bezier_curve.controlPoint(i, cp.data());
            cps.push_back(cp);
        }
        dtkContinuousGeometryPrimitives::Vector_2 a(0., 0.);
        double temp = 0.;
        double max = 0.;
        double c = 0.;
        double d = 0.;
        for(std::size_t i = 1; i < degree; ++i) {
            c = (double(degree) - double(i)) / double(degree);
            d = double(i) / double(degree);
            a = Vector_2(cps[i][0] - c * cps[0][0] - d * cps[degree][0], cps[i][1] - c * cps[0][1] - d * cps[degree][1]);
            temp = norm(a);
            if (temp > max) {
                max = temp;
            }
        }
        return (1 - std::pow(2, 1 - int(degree))) * max;
    }

    /*! \fn dtkContinuousGeometryTools::convexHullApproximation(std::list< Point_3 >& r_polyline, const dtkNurbsCurve& p_nurbs_curve, double p_error)
      Computes and appends to \a r_polyline the discretization of \a p_nurbs_curve, where each segment of \a r_polyline is at a maximum distance of \a p_error to the \a p_nurbs_curve.

      \a r_polyline : the polyline of 3D points to which the segments of the discretization of \a p_nurbs_curve will be added

      \a p_nurbs_curve : the 3D NURBS curve to discretize

      \a p_error : the maximum tolerated approximation distance between a segment of the polyline and \a p_nurbs_curve
    */
    void convexHullApproximation(std::list< Point_3 >& r_polyline, const dtkNurbsCurve& p_nurbs_curve, double p_error)
    {
        std::vector < std::pair< dtkRationalBezierCurve *, double * > > decomposition;
        p_nurbs_curve.decomposeToRationalBezierCurves(decomposition);

        std::list < dtkContinuousGeometryPrimitives::Point_3 > polyline;
        for (auto it = decomposition.begin(); it != std::prev(decomposition.end()); ++it) {
            convexHullApproximation(polyline, *(it->first), p_error);
            polyline.pop_back();
            r_polyline.splice(r_polyline.end(), polyline);
        }
        convexHullApproximation(polyline, *std::prev(decomposition.end())->first, p_error);
        r_polyline.splice(r_polyline.end(), polyline);
    }

    /*! \fn dtkContinuousGeometryTools::convexHullApproximation(std::list< Point_3 >& r_polyline, const dtkRationalBezierCurve& p_rational_bezier_curve, double p_error)
      Computes and appends to \a r_polyline the discretization of \a p_rational_bezier_curve, where each segment of \a r_polyline is at a maximum distance of \a p_error to the \a p_rational_bezier_curve.

      \a r_polyline : the polyline of 3D points to which the segments of the discretization of \a p_rational_bezier_curve will be added

      \a p_rational_bezier_curve : the 3D rational Bezier curve to discretize

      \a p_error : the maximum tolerated approximation distance between a segment of the polyline and \a p_rational_bezier_curve
    */
    void convexHullApproximation(std::list< Point_3 >& r_polyline, const dtkRationalBezierCurve& p_rational_bezier_curve, double p_error)
    {
        std::list < dtkRationalBezierCurve * > bezier_curves;
        dtkRationalBezierCurve * init_curve = new dtkRationalBezierCurve(p_rational_bezier_curve);
        bezier_curves.push_back(init_curve);
        auto it = bezier_curves.begin();
        while (it != bezier_curves.end()) {
            double error = convexHullApproximationError1(*(*it));
            if (error > p_error) {
                dtkRationalBezierCurve *curve_a = new dtkRationalBezierCurve();
                dtkRationalBezierCurve *curve_b = new dtkRationalBezierCurve();
                (*it)->split(curve_a, curve_b, 0.5);
                delete (*it);
                it = bezier_curves.erase(it);
                bezier_curves.insert(it, curve_b);
                --it;
                bezier_curves.insert(it, curve_a);
                --it;
            } else {
                ++it;
            }
        }
        Point_3 p(0., 0., 0.);

        // ///////////////////////////////////////////////////////////////////
        // Adds the first control point of each Bezier curve
        // ///////////////////////////////////////////////////////////////////
        for (auto curve = bezier_curves.begin(); curve != bezier_curves.end(); ++curve) {
            (*curve)->controlPoint(0, p.data());
            r_polyline.push_back(p);
        }
        // ///////////////////////////////////////////////////////////////////
        // Adds the last control point of the last Bezier curve
        // ///////////////////////////////////////////////////////////////////
        (*std::prev(bezier_curves.end()))->controlPoint((*std::prev(bezier_curves.end()))->degree(), p.data());
        r_polyline.push_back(p);

        // ///////////////////////////////////////////////////////////////////
        // Delete all curves
        // ///////////////////////////////////////////////////////////////////
        for (auto curve = bezier_curves.begin(); curve != bezier_curves.end(); ++curve) {
            delete (*curve);
        }
    }

    /*! \fn dtkContinuousGeometryTools::convexHull2DApproximation(std::list< Point_2 >& r_polyline, const dtkRationalBezierCurve2D& p_rational_bezier_curve, double p_error, double p_0, double p_1)
      Computes and appends to \a r_polyline the discretization of \a p_rational_bezier_curve, from parameter \a p_0 to \a p_1, where each segment of \a r_polyline is at a maximum distance of \a p_error to the \a p_rational_bezier_curve.

      \a r_polyline : the polyline of 2D points to which the segments of the discretization of \a p_rational_bezier_curve will be added

      \a p_rational_bezier_curve : the 2D rational Bezier curve to discretize

      \a p_error : the maximum tolerated approximation distance between a segment of the polyline and \a p_rational_bezier_curve

      \a p_0 : the parameter at which the discretization will start. Must be within[0, p_1[

      \a p_1 : the parameter at which the discretization will end. Must be within ]p_0, 1]
    */
    void convexHull2DApproximation(std::list< Point_2 >& r_polyline, const dtkRationalBezierCurve2D& p_rational_bezier_curve, double p_error, double p_0, double p_1)
    {
        Q_ASSERT_X(p_0 < p_1, Q_FUNC_INFO, "Invalid parameters : p_0 >= p_1.");

        dtkRationalBezierCurve2D *init_curve = nullptr;
        if(p_0 != 0.) {
            // ///////////////////////////////////////////////////////////////////
            // Discard first part of the curve
            // ///////////////////////////////////////////////////////////////////
            dtkRationalBezierCurve2D *curve_a = new dtkRationalBezierCurve2D();
            dtkRationalBezierCurve2D *curve_b = new dtkRationalBezierCurve2D();
            p_rational_bezier_curve.split(curve_a, curve_b, p_0);
            delete curve_a;
            dtkInfo() << __LINE__;
            if(p_1 != 1.) {
                // ///////////////////////////////////////////////////////////////////
                // Discard last part of the curve
                // ///////////////////////////////////////////////////////////////////
                dtkRationalBezierCurve2D *curve_c = new dtkRationalBezierCurve2D();
                dtkRationalBezierCurve2D *curve_d = new dtkRationalBezierCurve2D();
                curve_b->split(curve_c, curve_d, p_1);
                delete curve_d;
                delete curve_b;
                dtkInfo() << __LINE__;
                init_curve = curve_c;
            } else {
                dtkInfo() << __LINE__;
                init_curve = curve_b;
            }
        } else if(p_1 != 1.) {
            // ///////////////////////////////////////////////////////////////////
            // Discard last part of the curve
            // ///////////////////////////////////////////////////////////////////
            dtkRationalBezierCurve2D *curve_a = new dtkRationalBezierCurve2D();
            dtkRationalBezierCurve2D *curve_b = new dtkRationalBezierCurve2D();
            p_rational_bezier_curve.split(curve_a, curve_b, p_1);
            delete curve_b;
            dtkInfo() << __LINE__;
            init_curve = curve_a;
        } else {
            dtkInfo() << __LINE__;
            init_curve = new dtkRationalBezierCurve2D(p_rational_bezier_curve);
        }

        std::list < dtkRationalBezierCurve2D * > bezier_curves;
        bezier_curves.push_back(init_curve);
        auto it = bezier_curves.begin();
        while (it != bezier_curves.end()) {
            double error = convexHullApproximationError1(*(*it));
            if (error > p_error) {
                dtkRationalBezierCurve2D *curve_a = new dtkRationalBezierCurve2D();
                dtkRationalBezierCurve2D *curve_b = new dtkRationalBezierCurve2D();
                (*it)->split(curve_a, curve_b, 0.5);
                delete (*it);
                it = bezier_curves.erase(it);
                bezier_curves.insert(it, curve_b);
                --it;
                bezier_curves.insert(it, curve_a);
                --it;
            } else {
                ++it;
            }
        }
        Point_2 p(0., 0.);

        // ///////////////////////////////////////////////////////////////////
        // Adds the first control point of each Bezier curve
        // ///////////////////////////////////////////////////////////////////
        for (auto curve = bezier_curves.begin(); curve != bezier_curves.end(); ++curve) {
            (*curve)->controlPoint(0, p.data());
            r_polyline.push_back(p);
        }
        // ///////////////////////////////////////////////////////////////////
        // Adds the last control point of the last Bezier curve
        // ///////////////////////////////////////////////////////////////////
        (*std::prev(bezier_curves.end()))->controlPoint((*std::prev(bezier_curves.end()))->degree(), p.data());
        r_polyline.push_back(p);

        // ///////////////////////////////////////////////////////////////////
        // Delete all curves
        // ///////////////////////////////////////////////////////////////////
        for (auto curve = bezier_curves.begin(); curve != bezier_curves.end(); ++curve) {
            delete (*curve);
        }
    }

    /*! \fn dtkContinuousGeometryTools::convexHull2DApproximation(std::list< Point_2 >& r_polyline, const dtkRationalBezierCurve2D& p_rational_bezier_curve, double p_error)
      Computes and appends to \a r_polyline the discretization of \a p_rational_bezier_curve, where each segment of \a r_polyline is at a maximum distance of \a p_error to the \a p_rational_bezier_curve.

      \a r_polyline : the polyline of 2D points to which the segments of the discretization of \a p_rational_bezier_curve will be added

      \a p_rational_bezier_curve : the 2D rational Bezier curve to discretize

      \a p_error : the maximum tolerated approximation distance between a segment of the polyline and \a p_rational_bezier_curve
    */
    void convexHull2DApproximation(std::list< Point_2 >& r_polyline, const dtkRationalBezierCurve2D& p_rational_bezier_curve, double p_error)
    {
        std::list < dtkRationalBezierCurve2D * > bezier_curves;
        dtkRationalBezierCurve2D *init_curve = new dtkRationalBezierCurve2D(p_rational_bezier_curve);
        bezier_curves.push_back(init_curve);
        auto it = bezier_curves.begin();
        while (it != bezier_curves.end()) {
            double error = convexHullApproximationError1(*(*it));
            if (error > p_error) {
                dtkRationalBezierCurve2D *curve_a = new dtkRationalBezierCurve2D();
                dtkRationalBezierCurve2D *curve_b = new dtkRationalBezierCurve2D();
                (*it)->split(curve_a, curve_b, 0.5);
                delete (*it);
                it = bezier_curves.erase(it);
                bezier_curves.insert(it, curve_b);
                --it;
                bezier_curves.insert(it, curve_a);
                --it;
            } else {
                ++it;
            }
        }
        Point_2 p(0., 0.);

        // ///////////////////////////////////////////////////////////////////
        // Adds the first control point of each Bezier curve
        // ///////////////////////////////////////////////////////////////////
        for (auto curve = bezier_curves.begin(); curve != bezier_curves.end(); ++curve) {
            (*curve)->controlPoint(0, p.data());
            r_polyline.push_back(p);
        }
        // ///////////////////////////////////////////////////////////////////
        // Adds the last control point of the last Bezier curve
        // ///////////////////////////////////////////////////////////////////
        (*std::prev(bezier_curves.end()))->controlPoint((*std::prev(bezier_curves.end()))->degree(), p.data());
        r_polyline.push_back(p);

        // ///////////////////////////////////////////////////////////////////
        // Delete all curves
        // ///////////////////////////////////////////////////////////////////
        for (auto curve = bezier_curves.begin(); curve != bezier_curves.end(); ++curve) {
            delete (*curve);
        }
    }

    /*! \fn dtkContinuousGeometryTools::convexHull2DApproximation(std::list< Point_2 >& r_polyline, const dtkNurbsCurve2D& p_nurbs_curve, double p_error, double p_0, double p_1)
      Computes and appends to \a r_polyline the discretization of \a p_nurbs_curve, from parameter \a p_0 to \a p_1, where each segment of \a r_polyline is at a maximum distance of \a p_error to the \a p_nurbs_curve.

      \a r_polyline : the polyline of 2D points to which the segments of the discretization of \a p_nurbs_curve will be added

      \a p_nurbs_curve : the 2D NURBS curve to discretize

      \a p_error : the maximum tolerated approximation distance between a segment of the polyline and \a p_nurbs_curve

      \a p_0 : the parameter at which the discretization will start. Must be within [knots[0], p_1[

      \a p_1 : the parameter at which the discretization will end. Must be within ]p_0, knots[nb_of_knots - 1]]
    */
    void convexHull2DApproximation(std::list< Point_2 >& r_polyline, const dtkNurbsCurve2D& p_nurbs_curve, double p_error, double p_0, double p_1)
    {
        std::vector < std::pair< dtkRationalBezierCurve2D *, double * > > decomposition;
        p_nurbs_curve.decomposeToRationalBezierCurve2Ds(decomposition);
        Q_ASSERT_X(decomposition.size() == 0, Q_FUNC_INFO, "The decomposition of the NURBS curve resulted in 0 Bezier curves.");

        // ///////////////////////////////////////////////////////////////////
        // Need to discard all Bezier curves ending before parameter p_0, and starting after p_1
        // ///////////////////////////////////////////////////////////////////
        for(auto it = decomposition.begin(); it != decomposition.end();) {
            if(it->second[1] < p_0) {
                decomposition.erase(it);
            } else if(it->second[0] > p_1) {
                decomposition.erase(it);
            }
            ++it;
        }

        Q_ASSERT_X(decomposition.size() == 0, Q_FUNC_INFO, "The reduction of the number of Bezier curves resulted in 0 Bezier curves.");

        if(decomposition.size() == 1) {
            // ///////////////////////////////////////////////////////////////////
            // Clip both front and end
            // ///////////////////////////////////////////////////////////////////
            convexHull2DApproximation(r_polyline, *(decomposition.front().first), p_error, (p_0 - decomposition.front().second[0]) / (decomposition.front().second[1] - decomposition.front().second[0]), (p_1 - decomposition.front().second[0]) / (decomposition.front().second[1] - decomposition.front().second[0]));
        } else {
            // ///////////////////////////////////////////////////////////////////
            // Clip front of first curve
            // ///////////////////////////////////////////////////////////////////
            std::list < dtkContinuousGeometryPrimitives::Point_2 > polyline;
            convexHull2DApproximation(polyline, *(decomposition.front().first), p_error, (p_0 - decomposition.front().second[0]) / (decomposition.front().second[1] - decomposition.front().second[0]), 1.);
            polyline.pop_back();
            r_polyline.splice(r_polyline.end(), polyline);
            for (auto it = std::next(decomposition.begin()); it != std::prev(decomposition.end()); ++it) {
                convexHull2DApproximation(polyline, *(it->first), p_error);
                polyline.pop_back();
                r_polyline.splice(r_polyline.end(), polyline);
            }
            // ///////////////////////////////////////////////////////////////////
            // Clip end of last curve
            // ///////////////////////////////////////////////////////////////////
            convexHull2DApproximation(r_polyline, *(decomposition.front().first), p_error, 0., (p_1 - decomposition.front().second[0]) / (decomposition.front().second[1] - decomposition.front().second[0]));
            r_polyline.splice(r_polyline.end(), polyline);
        }
    }

    /*! \fn dtkContinuousGeometryTools::convexHull2DApproximation(std::list< Point_2 >& r_polyline, const dtkNurbsCurve2D& p_nurbs_curve, double p_error)
      Computes and appends to \a r_polyline the discretization of \a p_nurbs_curve, where each segment of \a r_polyline is at a maximum distance of \a p_error to the \a p_nurbs_curve.

      \a r_polyline : the polyline of 2D points to which the segments of the discretization of \a p_nurbs_curve will be added

      \a p_nurbs_curve : the 2D NURBS curve to discretize

      \a p_error : the maximum tolerated approximation distance between a segment of the polyline and \a p_nurbs_curve
    */
    void convexHull2DApproximation(std::list< Point_2 >& r_polyline, const dtkNurbsCurve2D& p_nurbs_curve, double p_error)
    {
        std::vector < std::pair< dtkRationalBezierCurve2D *, double * > > decomposition;
        p_nurbs_curve.decomposeToRationalBezierCurve2Ds(decomposition);

        std::list < dtkContinuousGeometryPrimitives::Point_2 > polyline;
        for (auto it = decomposition.begin(); it != std::prev(decomposition.end()); ++it) {
            convexHull2DApproximation(polyline, *(it->first), p_error);
            polyline.pop_back();
            r_polyline.splice(r_polyline.end(), polyline);
        }
        convexHull2DApproximation(polyline, *std::prev(decomposition.end())->first, p_error);
        r_polyline.splice(r_polyline.end(), polyline);
    }

    /*! \fn dtkContinuousGeometryTools::convexHull3DApproximation(std::list< Point_2 >& r_polyline, const dtkRationalBezierCurve2D& p_rational_bezier_curve, const dtkRationalBezierSurface& p_rational_bezier_surface, double p_3D_error)
      Computes and appends to \a r_polyline the discretization of \a p_rational_bezier_curve, where each segment of \a r_polyline is at a maximum 3D distance of \a p_3D_error to the \a p_rational_bezier_curve embedded in \a p_rational_bezier_surface (hence a 3D rational Bezier curve).

      \a r_polyline : the polyline of 2D points to which the segments of the discretization of \a p_rational_bezier_curve will be added

      \a p_rational_bezier_curve : the 2D rational Bezier curve to discretize

      \a p_rational_bezier_curve : the rational Bezier surface used to embed \a p_rational_bezier_curve

      \a p_3D_error : the maximum tolerated 3D approximation distance between a segment of the polyline and \a p_rational_bezier_curve embeded by \a p_rational_bezier_surface
    */
    void convexHull3DApproximation(std::list< Point_2 >& r_polyline, const dtkRationalBezierCurve2D& p_rational_bezier_curve, const dtkRationalBezierSurface& p_rational_bezier_surface, double p_3D_error)
    {
        std::list < dtkRationalBezierCurve2D * > bezier_curves;
        dtkRationalBezierCurve2D * init_curve = new dtkRationalBezierCurve2D(p_rational_bezier_curve);
        bezier_curves.push_back(init_curve);
        auto it = bezier_curves.begin();
        while (it != bezier_curves.end()) {
            dtkRationalBezierCurve2DElevator *embedder = dtkContinuousGeometry::rationalBezierCurve2DElevator::pluginFactory().create("defaultRationalBezierCurve2DElevator");
            embedder->setInputRationalBezierCurve2D(*it);
            embedder->setInputRationalBezierSurface(const_cast<dtkRationalBezierSurface *>(&p_rational_bezier_surface));
            embedder->run();
            dtkRationalBezierCurve *bezier_curve_3d = embedder->rationalBezierCurve();
            double error = convexHullApproximationError1(*bezier_curve_3d);
            if (error > p_3D_error) {
                dtkRationalBezierCurve2D *curve_a = new dtkRationalBezierCurve2D();
                dtkRationalBezierCurve2D *curve_b = new dtkRationalBezierCurve2D();
                (*it)->split(curve_a, curve_b, 0.5);
                delete (*it);
                it = bezier_curves.erase(it);
                bezier_curves.insert(it, curve_b);
                --it;
                bezier_curves.insert(it, curve_a);
                --it;
            } else {
                ++it;
            }
        }
        Point_2 p(0., 0.);

        // ///////////////////////////////////////////////////////////////////
        // Adds the first control point of each Bezier curve
        // ///////////////////////////////////////////////////////////////////
        for (auto curve = bezier_curves.begin(); curve != bezier_curves.end(); ++curve) {
            (*curve)->controlPoint(0, p.data());
            r_polyline.push_back(p);
        }
        // ///////////////////////////////////////////////////////////////////
        // Adds the last control point of the last Bezier curve
        // ///////////////////////////////////////////////////////////////////
        (*std::prev(bezier_curves.end()))->controlPoint((*std::prev(bezier_curves.end()))->degree(), p.data());
        r_polyline.push_back(p);

        // ///////////////////////////////////////////////////////////////////
        // Delete all curves
        // ///////////////////////////////////////////////////////////////////
        for (auto curve = bezier_curves.begin(); curve != bezier_curves.end(); ++curve) {
            delete (*curve);
        }
    }

    /*! \fn dtkContinuousGeometryTools::convexHull3DApproximation(std::list< Point_2 >& r_polyline, const dtkRationalBezierCurve2D& p_rational_bezier_curve, const dtkRationalBezierSurface& p_rational_bezier_surface, double p_3D_error, double p_0, double p_1)
      Computes and appends to \a r_polyline the discretization of \a p_rational_bezier_curve, from parameter \a p_0 to \a p_1, where each segment of \a r_polyline is at a maximum 3D distance of \a p_3D_error to the \a p_rational_bezier_curve embedded in \a p_rational_bezier_surface (hence a 3D rational Bezier curve).

      \a r_polyline : the polyline of 2D points to which the segments of the discretization of \a p_rational_bezier_curve will be added

      \a p_rational_bezier_curve : the 2D rational Bezier curve to discretize

      \a p_rational_bezier_curve : the rational Bezier surface used to embed \a p_rational_bezier_curve

      \a p_3D_error : the maximum tolerated 3D approximation distance between a segment of the polyline and \a p_rational_bezier_curve embeded by \a p_rational_bezier_surface

      \a p_0 : the parameter at which the discretization will start. Must be within [0, p_1[

      \a p_1 : the parameter at which the discretization will end. Must be within ]p_0, 1]
    */
    void convexHull3DApproximation(std::list< Point_2 >& r_polyline, const dtkRationalBezierCurve2D& p_rational_bezier_curve, const dtkRationalBezierSurface& p_rational_bezier_surface, double p_3D_error, double p_0, double p_1)
    {
        Q_ASSERT_X(p_0 < p_1, Q_FUNC_INFO, "Invalid parameters : p_0 >= p_1.");
        dtkRationalBezierCurve2D *init_curve = nullptr;
        if(p_0 != 0.) {
            // ///////////////////////////////////////////////////////////////////
            // Discard first part of the curve
            // ///////////////////////////////////////////////////////////////////
            dtkRationalBezierCurve2D *curve_a = new dtkRationalBezierCurve2D();
            dtkRationalBezierCurve2D *curve_b = new dtkRationalBezierCurve2D();
            p_rational_bezier_curve.split(curve_a, curve_b, p_0);
            delete curve_a;
            if(p_1 != 1.) {
                // ///////////////////////////////////////////////////////////////////
                // Discard last part of the curve
                // ///////////////////////////////////////////////////////////////////
                dtkRationalBezierCurve2D *curve_c = new dtkRationalBezierCurve2D();
                dtkRationalBezierCurve2D *curve_d = new dtkRationalBezierCurve2D();
                curve_b->split(curve_c, curve_d, p_1);
                delete curve_d;
                delete curve_b;
                init_curve = curve_c;
            } else {
                init_curve = curve_b;
            }
        } else if(p_1 != 1.) {
            // ///////////////////////////////////////////////////////////////////
            // Discard last part of the curve
            // ///////////////////////////////////////////////////////////////////
            dtkRationalBezierCurve2D *curve_a = new dtkRationalBezierCurve2D();
            dtkRationalBezierCurve2D *curve_b = new dtkRationalBezierCurve2D();
            p_rational_bezier_curve.split(curve_a, curve_b, p_1);
            delete curve_b;
            init_curve = curve_a;
        } else {
            init_curve = new dtkRationalBezierCurve2D(p_rational_bezier_curve);
        }

        std::list < dtkRationalBezierCurve2D * > bezier_curves;
        bezier_curves.push_back(init_curve);
        auto it = bezier_curves.begin();
        while (it != bezier_curves.end()) {
            dtkRationalBezierCurve2DElevator *embedder = dtkContinuousGeometry::rationalBezierCurve2DElevator::pluginFactory().create("defaultRationalBezierCurve2DElevator");
            embedder->setInputRationalBezierCurve2D(*it);
            embedder->setInputRationalBezierSurface(const_cast<dtkRationalBezierSurface *>(&p_rational_bezier_surface));
            embedder->run();
            dtkRationalBezierCurve *bezier_curve_3d = embedder->rationalBezierCurve();
            double error = convexHullApproximationError1(*bezier_curve_3d);
            if (error > p_3D_error) {
                dtkRationalBezierCurve2D *curve_a = new dtkRationalBezierCurve2D();
                dtkRationalBezierCurve2D *curve_b = new dtkRationalBezierCurve2D();
                (*it)->split(curve_a, curve_b, 0.5);
                delete (*it);
                it = bezier_curves.erase(it);
                bezier_curves.insert(it, curve_b);
                --it;
                bezier_curves.insert(it, curve_a);
                --it;
            } else {
                ++it;
            }
        }
        Point_2 p(0., 0.);

        // ///////////////////////////////////////////////////////////////////
        // Adds the first control point of each Bezier curve
        // ///////////////////////////////////////////////////////////////////
        for (auto curve = bezier_curves.begin(); curve != bezier_curves.end(); ++curve) {
            (*curve)->controlPoint(0, p.data());
            r_polyline.push_back(p);
        }
        // ///////////////////////////////////////////////////////////////////
        // Adds the last control point of the last Bezier curve
        // ///////////////////////////////////////////////////////////////////
        (*std::prev(bezier_curves.end()))->controlPoint((*std::prev(bezier_curves.end()))->degree(), p.data());
        r_polyline.push_back(p);

        // ///////////////////////////////////////////////////////////////////
        // Delete all curves
        // ///////////////////////////////////////////////////////////////////
        for (auto curve = bezier_curves.begin(); curve != bezier_curves.end(); ++curve) {
            delete (*curve);
        }
    }
    // void convexHullApproximationWithParameters(std::list< std::pair< Point_3, double > >& r_polyline, const dtkNurbsCurve& p_nurbs_curve, double p_error)
    // {
    //     std::vector < std::pair< dtkRationalBezierCurve *, double * > > decomposition;
    //     p_nurbs_curve.decomposeToRationalBezierCurves(decomposition);

    //     std::list < std::pair< Point_3, double > > polyline;
    //     for (auto it = decomposition.begin(); it != std::prev(decomposition.end()); ++it) {
    //         convexHullApproximationWithParameters(polyline, *it, p_error);
    //         polyline.pop_back();
    //         r_polyline.splice(r_polyline.end(), polyline);
    //     }
    //     convexHullApproximationWithParameters(polyline, *std::prev(decomposition.end()), p_error);
    //     r_polyline.splice(r_polyline.end(), polyline);
    // }

    // void convexHullApproximationWithParameters(std::list< std::pair< Point_3, double > >& r_polyline, const std::pair< dtkRationalBezierCurve *, double * >& p_rational_bezier_curve, double p_error)
    // {
    //     std::list < std::pair<dtkRationalBezierCurve *, double *> > bezier_curves;
    //     dtkRationalBezierCurve * init_curve = new dtkRationalBezierCurve(*p_rational_bezier_curve.first);
    //     double *limits = new double[2];
    //     limits[0] = p_rational_bezier_curve.second[0];
    //     limits[1] = p_rational_bezier_curve.second[1];
    //     bezier_curves.push_back(std::make_pair(init_curve, limits));
    //     auto it = bezier_curves.begin();
    //     while (it != bezier_curves.end()) {
    //         double error = convexHullApproximationError1(*(it->first));
    //         if (error > p_error) {
    //             dtkRationalBezierCurve * curve_a = new dtkRationalBezierCurve();
    //             dtkRationalBezierCurve * curve_b = new dtkRationalBezierCurve();
    //             double *limits_a = new double[2];
    //             double *limits_b = new double[2];
    //             limits_a[0] = it->second[0];
    //             limits_a[1] = it->second[0] + it->second[1] * 0.5;
    //             limits_b[0] = it->second[0] + it->second[1] * 0.5;
    //             limits_b[1] = it->second[1];
    //             it->first->split(curve_a, curve_b, 0.5);
    //             delete it->second;
    //             delete it->first;
    //             it = bezier_curves.erase(it);
    //             bezier_curves.insert(it, std::make_pair(curve_b, limits_b));
    //             --it;
    //             bezier_curves.insert(it, std::make_pair(curve_a, limits_a));
    //             --it;
    //         } else {
    //             ++it;
    //         }
    //     }
    //     Point_3 p(0., 0., 0.);

    //     // ///////////////////////////////////////////////////////////////////
    //     // Adds the first control point of each Bezier curve
    //     // ///////////////////////////////////////////////////////////////////
    //     double u = 0.;
    //     for (auto curve = bezier_curves.begin(); curve != bezier_curves.end(); ++curve) {
    //         u = curve->second[0];
    //         curve->first->controlPoint(0, p.data());
    //         r_polyline.push_back(std::make_pair(p, u));
    //     }
    //     // ///////////////////////////////////////////////////////////////////
    //     // Adds the last control point of the last Bezier curve
    //     // ///////////////////////////////////////////////////////////////////
    //     std::prev(bezier_curves.end())->first->controlPoint(std::prev(bezier_curves.end())->first->degree(), p.data());
    //     r_polyline.push_back(std::make_pair(p, std::prev(bezier_curves.end())->second[1]));

    //     // ///////////////////////////////////////////////////////////////////
    //     // Delete all curves
    //     // ///////////////////////////////////////////////////////////////////
    //     for (auto curve = bezier_curves.begin(); curve != bezier_curves.end(); ++curve) {
    //         delete curve->first;
    //         delete curve->second;
    //     }
    // }

    /*! \fn dtkContinuousGeometryTools::isAPlane(const dtkRationalBezierSurface& p_rational_bezier_surface, double p_tolerance)
      Returns true if the points do not diverge more than \a p_tolerance from a plane constructed with 3 of the control points of the \a p_rational_bezier_surface. Else returns false.

      \a p_rational_bezier_surface : the analysed rational Bezier surface

      \a p_tolerance : the maximal tolerance on the dot product between the normal of the plane and a vector from one of the 3 points and the other points
    */
    bool isAPlane(const dtkRationalBezierSurface& p_rational_bezier_surface, double p_tolerance)
    {
        std::size_t u_degree = p_rational_bezier_surface.uDegree();
        std::size_t v_degree = p_rational_bezier_surface.vDegree();

        dtkContinuousGeometryPrimitives::Point_3 o(0., 0., 0.);
        p_rational_bezier_surface.controlPoint(0, 0, o.data());
        dtkContinuousGeometryPrimitives::Point_3 a(0., 0., 0.);
        p_rational_bezier_surface.controlPoint(u_degree, 0, a.data());
        dtkContinuousGeometryPrimitives::Point_3 b(0., 0., 0.);
        p_rational_bezier_surface.controlPoint(0, v_degree, b.data());
        dtkContinuousGeometryPrimitives::Vector_3 oa = a - o;
        dtkContinuousGeometryPrimitives::Vector_3 ob = b - o;
        dtkContinuousGeometryPrimitives::Vector_3 plane_n = dtkContinuousGeometryTools::crossProduct(oa, ob);

        dtkContinuousGeometryPrimitives::Point_3 p(0., 0., 0.);
        dtkContinuousGeometryPrimitives::Vector_3 op(0., 0., 0.);
        for(std::size_t i = 1; i < u_degree; ++i) {
            p_rational_bezier_surface.controlPoint(i, 0, p.data());
            op = p - o;
            if(std::abs(dtkContinuousGeometryTools::dotProduct(plane_n, op)) > p_tolerance) {
                return false;
            }
        }
        for(std::size_t i = 1; i < u_degree; ++i) {
            p_rational_bezier_surface.controlPoint(i, v_degree, p.data());
            op = p - o;
            if(std::abs(dtkContinuousGeometryTools::dotProduct(plane_n, op)) > p_tolerance) {
                return false;
            }
        }
        for(std::size_t i = 0; i <= u_degree; ++i) {
            for(std::size_t j = 1; j < v_degree; ++j) {
                p_rational_bezier_surface.controlPoint(i, j, p.data());
                op = p - o;
                if(std::abs(dtkContinuousGeometryTools::dotProduct(plane_n, op)) > p_tolerance) {
                    return false;
                }
            }
        }
        p_rational_bezier_surface.controlPoint(u_degree, v_degree, p.data());
        op = p - o;
        if(std::abs(dtkContinuousGeometryTools::dotProduct(plane_n, op)) > p_tolerance) {
            return false;
        }
        return true;
    }

    /*! \fn dtkContinuousGeometryTools::isAPlane(const dtkNurbsSurface& p_nurbs_surface, double p_tolerance)
      Returns true if the points do not diverge more than \a p_tolerance from a plane constructed with 3 of the control points of the \a p_nurbs_surface. Else returns false.

      \a p_nurbs_surface : the analysed NURBS surface

      \a p_tolerance : the maximal tolerance on the dot product between the normal of the plane and a vector from one of the 3 points and the other points
    */
    bool isAPlane(const dtkNurbsSurface& p_nurbs_surface, double p_tolerance)
    {
        std::size_t u_nb_cps = p_nurbs_surface.uNbCps();
        std::size_t v_nb_cps = p_nurbs_surface.vNbCps();
        dtkContinuousGeometryPrimitives::Point_3 o(0., 0., 0.);
        p_nurbs_surface.controlPoint(0, 0, o.data());
        dtkContinuousGeometryPrimitives::Point_3 a(0., 0., 0.);
        p_nurbs_surface.controlPoint(u_nb_cps - 1, 0, a.data());
        dtkContinuousGeometryPrimitives::Point_3 b(0., 0., 0.);
        p_nurbs_surface.controlPoint(0, v_nb_cps - 1, b.data());
        // ///////////////////////////////////////////////////////////////////
        // Handles the "degeneracy" case of a tube
        // ///////////////////////////////////////////////////////////////////
        qDebug() << "squaredDistance(o, a)" << squaredDistance(o, a);
        if(squaredDistance(o, a) < 1e-5) {
            if(u_nb_cps - 2 > 0) {
                p_nurbs_surface.controlPoint(u_nb_cps - 2, 0, a.data());
            } else {
                p_nurbs_surface.controlPoint(0, v_nb_cps - 2, a.data());
            }
        }
        qDebug() << "squaredDistance(o, b)" << squaredDistance(o, b);
        if(squaredDistance(o, b) < 1e-5) {
            if(v_nb_cps - 2 > 0) {
                p_nurbs_surface.controlPoint(0, v_nb_cps - 2, b.data());
                std::cerr << squaredDistance(o, b) << std::endl;
            } else {
                p_nurbs_surface.controlPoint(u_nb_cps - 2, 0, b.data());
            }
        }
        dtkContinuousGeometryPrimitives::Vector_3 oa = a - o;
        dtkContinuousGeometryPrimitives::Vector_3 ob = b - o;
        dtkContinuousGeometryPrimitives::Vector_3 plane_n = dtkContinuousGeometryTools::crossProduct(oa, ob);

        dtkContinuousGeometryPrimitives::Point_3 p(0., 0., 0.);
        dtkContinuousGeometryPrimitives::Vector_3 op(0., 0., 0.);
        for(std::size_t i = 1; i < u_nb_cps - 1; ++i) {
            p_nurbs_surface.controlPoint(i, 0, p.data());
            op = p - o;
            if(std::abs(dtkContinuousGeometryTools::dotProduct(plane_n, op)) > p_tolerance) {
                return false;
            }
        }
        for(std::size_t i = 1; i < u_nb_cps - 1; ++i) {
            p_nurbs_surface.controlPoint(i, v_nb_cps - 1, p.data());
            op = p - o;
            if(std::abs(dtkContinuousGeometryTools::dotProduct(plane_n, op)) > p_tolerance) {
                return false;
            }
        }
        for(std::size_t i = 0; i <= u_nb_cps - 1; ++i) {
            for(std::size_t j = 1; j < v_nb_cps - 1; ++j) {
                p_nurbs_surface.controlPoint(i, j, p.data());
                op = p - o;
                if(std::abs(dtkContinuousGeometryTools::dotProduct(plane_n, op)) > p_tolerance) {
                    return false;
                }
            }
        }
        p_nurbs_surface.controlPoint(u_nb_cps - 1, v_nb_cps - 1, p.data());
        op = p - o;
        if(std::abs(dtkContinuousGeometryTools::dotProduct(plane_n, op)) > p_tolerance) {
            return false;
        }
        return true;
    }

    /*! \fn dtkContinuousGeometryTools::decomposeToSplitRationalBezierCurves(std::vector< std::tuple< dtkRationalBezierCurve2D *, double *, const dtkRationalBezierSurface *> >& r_splits, const dtkNurbsCurve2D& p_nurbs_curve,  const dtkClippedNurbsSurface& p_clipped_nurbs_surface)
      WORK IN PROGRESS
      Decomposes a 2D NURBS curve into rational Bezier curves using the clipping lines of the \a p_clipped_nurbs_surface to cut the NURBS curve. The Bezier curves are appended to \a r_splits, along their limits in the NURBS curve parameter space, and the rational Bezier surface on which they lie.

      \a r_splits : vector to which the 2D Bezier curves are appended, along their limits in the 2D \a p_nurbs_curve parameter space, and the rational Bezier surface on which they lie

      \a p_nurbs_curve : the 2D NURBS curve from which the split rational Bezier curves are to be recovered

      \a p_clipped_nurbs_surface : the clipped NURBS surface used to cut the 2D NURBS curve

      The 2D NURBS curve (\a p_nurbs_curve) must be enclosed in the limits of the knots vectors of the NURBS surface (\a p_clipped_nurbs_surface).
    */
    void decomposeToSplitRationalBezierCurves(std::vector< std::tuple< dtkRationalBezierCurve2D *, double *, const dtkRationalBezierSurface *> >& r_splits, const dtkNurbsCurve2D& p_nurbs_curve,  const dtkClippedNurbsSurface& p_clipped_nurbs_surface)
    {
        // ///////////////////////////////////////////////////////////////////
        // First clip the NURBS curve into Beziers curves
        // ///////////////////////////////////////////////////////////////////
        std::vector< std::pair< dtkRationalBezierCurve2D*, double*> > rational_bezier_curves;
        p_nurbs_curve.decomposeToRationalBezierCurve2Ds(rational_bezier_curves);
        // ///////////////////////////////////////////////////////////////////
        // Then split Beziers curves with the limits of the p_clipped_nurbs_surface Bezier patches
        // ///////////////////////////////////////////////////////////////////
        for(auto rbc = rational_bezier_curves.begin(); rbc != rational_bezier_curves.end(); ++rbc) {
            std::vector< std::pair< dtkRationalBezierCurve2D *, const dtkRationalBezierSurface *> > split;
            splitBezierCurveWithClippedNurbsSurface(split, *(rbc->first), p_clipped_nurbs_surface);
            // r_splits.push_back(split);
        }
    }

    /*! \fn dtkContinuousGeometryTools::splitBezierCurveWithClippedNurbsSurface(std::vector< std::pair< dtkRationalBezierCurve2D *, const dtkRationalBezierSurface *> >& r_split_bezier_curves, const dtkRationalBezierCurve2D& p_bezier_curve, const dtkClippedNurbsSurface& p_clipped_nurbs_surface)
      WORK IN PROGRESS
      Decomposes a 2D rational Bezier curve (\a p_bezier_curve) into rational Bezier curves (\a r_split_bezier_curves) using the clipping lines of the \a p_clipped_nurbs_surface to cut the 2D rational Bezier curve. The 2D Bezier curves are appended to \a r_split_bezier_curves, along the rational Bezier surface on which they lie.

      \a r_split_bezier_curves : vector to which the 2D Bezier curves are appended, along the rational Bezier surface on which they lie

      \a p_bezier_curve : the 2D rational Bezier curve from which the split rational Bezier curves are to be recovered

      \a p_clipped_nurbs_surface : the clipped NURBS surface used to cut the 2D rational Bezier curve

      The 2D rational Bezier curve (\a p_bezier_curve) must be enclosed in the limits of the knots vectors of the NURBS surface (\a p_clipped_nurbs_surface).
    */
    void splitBezierCurveWithClippedNurbsSurface(std::vector< std::pair< dtkRationalBezierCurve2D *, const dtkRationalBezierSurface *> >& r_split_bezier_curves, const dtkRationalBezierCurve2D& p_bezier_curve, const dtkClippedNurbsSurface& p_clipped_nurbs_surface)
    {
        // ///////////////////////////////////////////////////////////////////
        // Recovers the first and last knots values : in order not to check intersection with these
        // ///////////////////////////////////////////////////////////////////
        // std::size_t u_nb_knots = p_clipped_nurbs_surface.uDegree() + p_clipped_nurbs_surface.uNbCps() - 1;
        // double *u_knots = new double[u_nb_knots];
        // p_clipped_nurbs_surface.uKnots(u_knots);

        // std::size_t v_nb_knots = p_clipped_nurbs_surface.vDegree() + p_clipped_nurbs_surface.vNbCps() - 1;
        // double *v_knots = new double[v_nb_knots];
        // p_clipped_nurbs_surface.uKnots(v_knots);
        // ///////////////////////////////////////////////////////////////////
        // Iterates on the rational bezier surfaces and on their definition domain in the nurbs parameters domain
        // ///////////////////////////////////////////////////////////////////
        dtkRationalBezierCurve2DLineIntersector *intersector = dtkContinuousGeometry::rationalBezierCurve2DLineIntersector::pluginFactory().create("sislRationalBezierCurve2DLineIntersector");
        if (intersector == nullptr) {
            dtkFatal() << "To use the function " << Q_FUNC_INFO << ", sislRationalBezierCurve2DLineIntersector must be compiled and working";
        }
        // ///////////////////////////////////////////////////////////////////
        // Cut all the bezier curves into smaller "non-crossing-Bezier-patches" bezier curves
        // ///////////////////////////////////////////////////////////////////
        for(auto line = p_clipped_nurbs_surface.m_clipping_lines.begin(); line != p_clipped_nurbs_surface.m_clipping_lines.end(); ++line) {

        }
    }

    /*! \fn dtkContinuousGeometryTools::recomposeNurbsCurve(dtkNurbsCurve& r_nurbs_curve, const std::vector< dtkRationalBezierCurve * >& p_splits)
      Recomposes a 3D NURBS curve (\a r_nurbs_curve) from a set of contiguous 3D rational Bezier curves (\a p_splits).

      \a r_nurbs_curve : the 2D rational Bezier curve from which the split rational Bezier curves are to be recovered

      \a p_splits : a set of contiguous 3D rational Bezier curves

      The rational Bezier curves must be of the same degree, and the end point of any must match the starting point of the following.
    */
    void recomposeNurbsCurve(dtkNurbsCurve& r_nurbs_curve, const std::vector< dtkRationalBezierCurve * >& p_splits)
    {
        // ///////////////////////////////////////////////////////////////////
        // Checks all Bezier curve are of the same degree, and that each end control point of one bezier curve match the next first control point
        // ///////////////////////////////////////////////////////////////////
        //TODO
        if (r_nurbs_curve.data() != nullptr) {
            dtkWarn() << "r_nurbs_curve is replaced, pointed value migt be lost";
        } else {
            dtkAbstractNurbsCurveData *nurbs_curve_data = dtkContinuousGeometry::abstractNurbsCurveData::pluginFactory().create("dtkNurbsCurveDataOn");
            if (nurbs_curve_data == nullptr) {
                dtkFatal() << "To use the function " << Q_FUNC_INFO << ", dtkNurbsCurve must be implemented under the OpenNURBS plugins and working";
            }
            std::size_t degree = (*p_splits.begin())->degree();
            std::vector< double > cps;
            cps.reserve(4 * ((degree + 1) * p_splits.size() - (p_splits.size() - 1)));
            std::vector< double > knots;
            knots.reserve(2 * (degree +1) + (p_splits.size() - 1) * degree);
            dtkContinuousGeometryPrimitives::Point_3 cp(0., 0., 0.);
            double w = 0.;
            for (std::size_t d = 0; d < degree; ++d) {
                knots.push_back(0);
            }
            for(auto split = p_splits.begin(); split != p_splits.end(); ++split) {
                //Do not add the last control point as it is the same as the first control point of the next Bezier curve
                for(std::size_t i = 0; i < (*split)->degree(); ++i) {
                    (*split)->controlPoint(i, cp.data());
                    (*split)->weight(i, &w);
                    cps.push_back(cp[0]);
                    cps.push_back(cp[1]);
                    cps.push_back(cp[2]);
                    cps.push_back(w);
                }
            }
            std::size_t k = 1;
            for(auto split = p_splits.begin(); split != std::prev(p_splits.end()); ++split, ++k) {
                for(std::size_t i = 0; i < (*split)->degree(); ++i) {
                    knots.push_back(double(k) / double(p_splits.size()));
                }
            }
            //Add the last control point of the last Bezier curve
            (*std::prev(p_splits.end()))->controlPoint(degree, cp.data());
            (*std::prev(p_splits.end()))->weight(degree, &w);
            cps.push_back(cp[0]);
            cps.push_back(cp[1]);
            cps.push_back(cp[2]);
            cps.push_back(w);
            for (std::size_t d = 0; d < degree; ++d) {
                knots.push_back(1);
            }
            nurbs_curve_data->create(3, cps.size() / 4, degree + 1, knots.data(), cps.data());
            r_nurbs_curve.setData(nurbs_curve_data);
        }
    }

    /*! \fn dtkContinuousGeometryTools::integerPartitioning(std::uint_least16_t p_integer, std::list< std::vector< std::uint_least16_t > >& r_partitions)
      Computes and stores in \a r_partitions, the integer partitionning of \a p_integer.

      \a p_integer : the integer to partition

      \a r_partitions : all the possible integer partions of \a p_integer
    */
    void integerPartitioning(std::uint_least16_t p_integer, std::list< std::vector< uint_least16_t > >& r_partitions)
    {
        if(p_integer == 0) {
            r_partitions.push_back(std::vector< double >(1, 0));//Adds only 0
            return;
        }
        int k = 0; //rank of the last element strickly greater than 1
        std::size_t n = 0; //number of terms equaling 1 in the (ai)s
        r_partitions.push_back(std::vector< double >(1, p_integer));//Adds the first decomposition, i.e. the element to decompose
        bool decomposed = false;
        auto partition = r_partitions.begin();
        while (!decomposed) { //Needs to decompose once more at least
            k = -1;
            n = 0;
            for(std::size_t i = 0; i < partition->size(); ++i) {
                if ((*partition)[i] > 1) {
                    ++k;
                } else {
                    ++n;
                }
            }
            if (k == -1) {
                decomposed = true;
                break;
            }
            r_partitions.push_back(std::vector< double >()); //Adds storage for the next decomposition
            for(std::size_t j = 0; j < std::size_t(k); ++j) {
                (*std::next(partition)).push_back((*partition)[j]);
            }
            // ///////////////////////////////////////////////////////////////////
            // Euclidian division
            // ///////////////////////////////////////////////////////////////////
            std::size_t r = int(n + 1) % int((*partition)[k] - 1);
            std::size_t q = (n + 1) / ((*partition)[k] - 1);
            for(std::size_t j = k; j < k + q + 1; ++j) {
                (*std::next(partition)).push_back((*partition)[k] - 1);
            }
            if(r != 0) {
                (*std::next(partition)).push_back(r);
            }
            ++partition;
        }
    }
}

//
// dtkContinuousGeometryUtils.cpp ends here
