// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkNurbsPolyhedralSurface.h"

#include "dtkContinuousGeometry.h"

#include "dtkNurbsSurface.h"
#include "dtkRationalBezierSurface.h"
#include "dtkTrimLoop.h"

#include "dtkRationalBezierCurve2D.h"
#include "dtkClippedNurbsSurface.h"
#include "dtkClippedTrimLoop.h"
#include "dtkClippedTrim.h"

/*!
  \class dtkNurbsPolyhedralSurface
  \inmodule dtkContinuousGeometry
  \brief dtkNurbsPolyhedralSurface is the polyhedral discretization of a NURBS Surface

  The following line of code shows how to instanciate the class in pratice :
  \code
  dtkNurbsPolyhedralSurface *polyhedral_surface = dtkContinuousGeometry::nurbsPolyhedralSurface::pluginFactory().create("dtkNurbsPolyhedralSurfaceCgal");
  \endcode
*/

/*! \fn dtkNurbsPolyhedralSurface::dtkNurbsPolyhedralSurface(void)
  Instanciates a dtkNurbsPolyhedralSurface.
*/
dtkNurbsPolyhedralSurface::dtkNurbsPolyhedralSurface(void) : m_nurbs_surface(nullptr), m_polyhedral_surface(nullptr) {}

/*! \fn dtkNurbsPolyhedralSurface::dtkNurbsPolyhedralSurface(const dtkNurbsPolyhedralSurface& other)
  Instanciates a dtkNurbsPolyhedralSurface by copying \a other content.
*/
dtkNurbsPolyhedralSurface::dtkNurbsPolyhedralSurface(const dtkNurbsPolyhedralSurface& other) : m_nurbs_surface(other.m_nurbs_surface), m_polyhedral_surface(other.m_polyhedral_surface) {}

/*! \fn dtkNurbsPolyhedralSurface::~dtkNurbsPolyhedralSurface(void)
  Destructor.
*/
dtkNurbsPolyhedralSurface::~dtkNurbsPolyhedralSurface(void) {}

/*! \fn  dtkNurbsPolyhedralSurface::initialize(dtkNurbsSurface *p_nurbs_surface, double approximation)
  Initializes the dtkNurbsPolyhedralSurface with a NURBS surface (\a p_nurbs_surface).

  \a nurbs_surface : the NURBS surface to polyhedralize

  \a approximation : the tolerated approximation from the NURBS surface to its polyhedralization
*/
void dtkNurbsPolyhedralSurface::initialize(dtkNurbsSurface *nurbs_surface, double /*approximation*/, int /*surface_nb*/)
{
    m_nurbs_surface = nurbs_surface;
    m_clipped_surface = new dtkClippedNurbsSurface(*nurbs_surface);
}

/*! \fn dtkNurbsPolyhedralSurface::isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const
  Returns true if \a point is culled by the trim loops of the surface, else returns false.
*/


/*! \fn dtkNurbsPolyhedralSurface::evaluatePoint(double p_u, double p_v, double *r_point) const
  Writes in \a r_point the evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation
*/

/*! \fn dtkNurbsPolyhedralSurface::evaluateNormal(double p_u, double p_v, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/

/*! \fn dtkNurbsPolyhedralSurface::evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_point the evaluation, in \a r_u_deriv the first derivative in "u" direction, in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/

/*! \fn dtkNurbsPolyhedralSurface::evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_u_deriv the first derivative in "u" direction and in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/

/*! \fn dtkNurbsPolyhedralSurface::nurbsSurface(void) const
  Returns the NURBS surface given for initialization. See \l dtkNurbsPolyhedralSurface::initialize((dtkNurbsSurface *p_nurbs_surface).
*/
const dtkNurbsSurface& dtkNurbsPolyhedralSurface::nurbsSurface(void) const
{
    return *m_nurbs_surface;
}
/*! \fn dtkNurbsPolyhedralSurface::polyhedralSurface(void) const
  Returns a mesh approximating the NURBS surface.
*/
const dtkMesh& dtkNurbsPolyhedralSurface::polyhedralSurface(void) const
{
    return *m_polyhedral_surface;
}

const dtkClippedNurbsSurface* dtkNurbsPolyhedralSurface::clippedNurbsSurface(void) const
{
    return m_clipped_surface;
}

/*! \fn dtkNurbsPolyhedralSurface::pointsAndTriangles(std::vector< dtkContinuousGeometryPrimitives::Point_3 >& r_points, std::vector< std::size_t >& r_triangles) const
  Returns a triangulation approximating the NURBS surface as points and triangles.

  \a r_points : the geometrical points of the triangulation

  \a r_triangles : the triangles of the triangulation
*/

/*! \fn dtkNurbsPolyhedralSurface::pointsTrianglesAndNormals(std::vector< dtkContinuousGeometryPrimitives::Point_3 >& r_points, std::vector< std::size_t >& r_triangles, std::vector< dtkContinuousGeometryPrimitives::Vector_3 >& r_normals) const
  Returns a triangulation approximating the NURBS surface as points, triangles and normals.

  \a r_points : the geometrical points of the triangulation

  \a r_triangles : the triangles of the triangulation

  \a r_normal : the normals at the points returned in \a r_points
*/

/*! \fn dtkNurbsPolyhedralSurface::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
*/

/*! \fn dtkNurbsPolyhedralSurface::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/

/*! \fn dtkNurbsPolyhedralSurface::clone(void) const
  Clone
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkNurbsPolyhedralSurface, nurbsPolyhedralSurface, dtkContinuousGeometry);
}
