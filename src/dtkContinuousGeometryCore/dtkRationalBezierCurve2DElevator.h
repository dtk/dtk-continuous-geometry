// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

// /////////////////////////////////////////////////////////////////
// dtkRationalBezierCurve2DElevator
// /////////////////////////////////////////////////////////////////

class dtkRationalBezierCurve2D;
class dtkRationalBezierSurface;
class dtkRationalBezierCurve;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkRationalBezierCurve2DElevator
{
 public:
    virtual ~dtkRationalBezierCurve2DElevator(void) {};

 public:
    virtual void setInputRationalBezierCurve2D(dtkRationalBezierCurve2D *curve) = 0;
    virtual void setInputRationalBezierSurface(dtkRationalBezierSurface *surface) = 0;

    virtual void run(void) = 0;

    virtual dtkRationalBezierCurve *rationalBezierCurve(void) = 0;
};

// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkRationalBezierCurve2DElevator*)
DTK_DECLARE_PLUGIN(dtkRationalBezierCurve2DElevator, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkRationalBezierCurve2DElevator, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkRationalBezierCurve2DElevator, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkRationalBezierCurve2DElevator, DTKCONTINUOUSGEOMETRYCORE_EXPORT, rationalBezierCurve2DElevator);
}

//
// dtkRationalBezierCurve2DElevator.h ends here
