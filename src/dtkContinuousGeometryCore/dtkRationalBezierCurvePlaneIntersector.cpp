/*
 * dtkRationalBezierCurvePlaneIntersector.cpp
 *
 *  Created on: Apr 1, 2020
 *      Author: xxiao
 */

#include "dtkContinuousGeometry.h"
#include "dtkRationalBezierCurvePlaneIntersector.h"

namespace dtkContinuousGeometry {
  DTK_DEFINE_CONCEPT(dtkRationalBezierCurvePlaneIntersector, rationalBezierCurvePlaneIntersector, dtkContinuousGeometry);
}
