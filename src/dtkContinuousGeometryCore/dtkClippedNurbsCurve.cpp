// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkClippedNurbsCurve.h"

#include "dtkContinuousGeometryUtils.h"

#include "dtkNurbsCurve.h"
#include "dtkRationalBezierCurve.h"

/*!
  \class dtkClippedNurbsCurve
  \inmodule dtkContinuousGeometry
  \brief dtkClippedNurbsCurve is a clipped representation of a dtkNurbsCurve

  The following line of code shows how to instanciate the class in pratice :
  \code
  dtkNurbsCurve *nurbs_curve = new dtkNurbsCurve(dtkContinuousGeometry::abstractNurbsCurveData::pluginFactory().create("dtkNurbsCurveDataDefault");
  nurbs_curve->create(...);
  dtkClippedNurbsCurve *clipped_nurbs_curve = new dtkClippedNurbsCurve(*nurbs_curve);
  \endcode
 */


/*! \fn dtkClippedNurbsCurve::dtkClippedNurbsCurve(const dtkNurbsCurve& p_nurbs_curve)
   Instanciates a dtkClippedNurbsCurve by clipping a NURBS curve (\a p_nurbs_curve).
*/
dtkClippedNurbsCurve::dtkClippedNurbsCurve(const dtkNurbsCurve& nurbs_curve) : m_nurbs_curve(nurbs_curve)
{
    m_nurbs_curve.decomposeToRationalBezierCurves(m_rational_bezier_curves);
    // ///////////////////////////////////////////////////////////////////
    // For conveniance, recover the first and last knot
    // ///////////////////////////////////////////////////////////////////
    std::size_t nb_knots = m_nurbs_curve.nbCps() + m_nurbs_curve.degree() - 1;
    double *knots = new double[nb_knots];
    m_nurbs_curve.knots(knots);
    first_knot = knots[0];
    last_knot = knots[nb_knots - 1];
    delete [] knots;
}

/*! \fn dtkClippedNurbsCurve::~dtkClippedNurbsCurve(void)
   Destructor.
*/
dtkClippedNurbsCurve::~dtkClippedNurbsCurve(void){}

/*! \fn  dtkClippedNurbsCurve::degree(void) const
  Returns the degree of the curve.
 */
std::size_t dtkClippedNurbsCurve::degree(void) const
{
    return m_nurbs_curve.degree();
}

/*! \fn  dtkClippedNurbsCurve::nbCps(void) const
  Returns the number of weighted control points controlling the curve.
 */
std::size_t dtkClippedNurbsCurve::nbCps(void) const
{
    return m_nurbs_curve.nbCps();
}

/*! \fn  dtkClippedNurbsCurve::dim(void) const
  Returns the dimension of the space in which the curve lies.
*/
std::size_t dtkClippedNurbsCurve::dim(void) const
{
    return m_nurbs_curve.dim();
}

/*! \fn  dtkClippedNurbsCurve::controlPoint(std::size_t i, double *r_cp) const
  Writes in \a r_cp the \a i th weighted control point coordinates[xi, yi, zi].

  \a i : index of the weighted control point

  \a r_cp : array of size 3 to store the weighted control point coordinates[xi, yi, zi]
*/
void dtkClippedNurbsCurve::controlPoint(std::size_t i, double* r_cp) const
{
   m_nurbs_curve.controlPoint(i, r_cp);
}

/*! \fn  dtkClippedNurbsCurve::weight(std::size_t i, double *r_w) const
  Writes in \a r_w the \a i th weighted control point weight wi.

  \a i : index of the weighted control point

  \a r_w : array of size 1 to store the weighted control point weight wi
*/
void dtkClippedNurbsCurve::weight(std::size_t i, double* r_w) const
{
    m_nurbs_curve.weight(i, r_w);
}

/*! \fn dtkClippedNurbsCurve::knots(double *r_knots) const
  Writes in \a r_knots the knots of the curve.

  \a r_knots : array of size (nb_cp + order - 2) to store the knots
*/
void dtkClippedNurbsCurve::knots(double* r_knots) const
{
    m_nurbs_curve.knots(r_knots);
}

/*! \fn dtkClippedNurbsCurve::firstKnot(void) const
  Returns the value of the first knot of the NURBS curve.
 */
double dtkClippedNurbsCurve::firstKnot(void) const
{
    return first_knot;
}

/*! \fn dtkClippedNurbsCurve::lastKnot(void) const
  Returns the value of the last knot of the NURBS curve.
*/
double dtkClippedNurbsCurve::lastKnot(void) const
{
    return last_knot;
}

/*! \fn dtkClippedNurbsCurve::evaluatePoint(double p_u, double *r_point) const
  Writes in \a r_point the evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkClippedNurbsCurve::evaluatePoint(double p_u, double* r_point) const
{
    m_nurbs_curve.evaluatePoint(p_u, r_point);
}

/*! \fn dtkClippedNurbsCurve::evaluateNormal(double p_u, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkClippedNurbsCurve::evaluateNormal(double p_u, double* r_normal) const
{
    m_nurbs_curve.evaluateNormal(p_u, r_normal);
}

/*! \fn dtkClippedNurbsCurve::evaluateNormal(double p_u, double *r_point, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkClippedNurbsCurve::evaluateNormal(double p_u, double* r_point, double* r_normal) const
{
    m_nurbs_curve.evaluateNormal(p_u, r_point, r_normal);
}

//
// dtkClippedNurbsCurve.cpp ends here
