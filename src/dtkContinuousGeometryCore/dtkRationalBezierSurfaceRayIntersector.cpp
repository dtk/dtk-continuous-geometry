// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkContinuousGeometry.h"
#include "dtkRationalBezierSurfaceRayIntersector.h"

/*!
  \class dtkRationalBezierSurfaceRayIntersector
  \inmodule dtkContinuousGeometry
  \brief dtkRationalBezierSurfaceRayIntersector is a concept describing the intersection between a ray and a rational Bezier surface.

  It cannot be instanciated on its own.

  It is to be inherited from the classes acting as plugins. For more information : dtkPluginsContinuousGeometry.

  The following lines of code show how to instanciate and use the class in pratice :
  \code
  dtkRationalBezierSurfaceRayIntersector *intersector = dtkContinuousGeometry::rationalBezierSurfaceRayIntersector::pluginFactory().create("sislRationalBezierSurfaceRayIntersector");
  intersector->setRationalBezierSurface(...);
  intersector->setRay(...);
  intersector->run();
  const std::vector< IntersectionObject > & intersection_object = intersector->intersection();
  \endcode
  Check dtkPluginsContinuousGeometry for more information on available plugins for dtkRationalBezierSurfaceRayIntersector.
*/

/*! \fn dtkRationalBezierSurfaceRayIntersector::setRationalBezierSurface(dtkRationalBezierSurface *surface)
  Provides the rational Bezier surface to the intersector (\a surface).

  \a surface : the rational Bezier surface to intersect
*/

/*! \fn dtkRationalBezierSurfaceRayIntersector::setRay(dtkContinuousGeometryPrimitives::Ray_3 *ray)
  Provides the \a ray to intersect the surface with to the intersector.

  \a ray : the ray to intersect the rational Bezier surface with
*/

/*! \fn dtkRationalBezierSurfaceRayIntersector::run(void)
  Intersects the rational Bezier surface (provided with \l setRationalBezierSurface(dtkRationalBezierSurface *surface)) with the ray(provided with \l setRay(Ray_3 *ray)), the result can be retrieved with \l intersection(void).
*/

/*! \fn dtkRationalBezierSurfaceRayIntersector::intersection(void)
  Returns the intersection objects.

  The caller is responsible for deleting all the IntersectionObject of the returned vector, as well as the vector.
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkRationalBezierSurfaceRayIntersector, rationalBezierSurfaceRayIntersector, dtkContinuousGeometry);
}
