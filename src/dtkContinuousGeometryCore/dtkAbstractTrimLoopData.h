// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

class dtkTrim;
namespace dtkContinuousGeometryPrimitives {
    class Point_2;
}
namespace  dtkContinuousGeometryEnums {
    enum TrimLoopType : unsigned short;
}

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkAbstractTrimLoopData
{
 public:
    dtkAbstractTrimLoopData(void) {;};
    virtual ~dtkAbstractTrimLoopData(void) {;};

 public:
    virtual dtkContinuousGeometryEnums::TrimLoopType type(void) const = 0;

 public:
    virtual void setType(dtkContinuousGeometryEnums::TrimLoopType p_type) const = 0;

 public:
    virtual bool isPointCulled(dtkContinuousGeometryPrimitives::Point_2 p_point) const = 0;

    virtual bool toPolyline(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_discretisation_length) const = 0;
    virtual bool toPolylineApprox(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_approximation) const = 0;

    virtual bool isValid(void) const = 0;

 public:
   virtual void aabb(double *r_aabb) const = 0;

public:
    virtual dtkAbstractTrimLoopData *clone(void) const = 0;

public:
    /// @TODO: refactore that part!
    std::vector< dtkTrim * > m_trims;
};

// ///////////////////////////////////////////////////////////////////
DTK_DECLARE_OBJECT(dtkAbstractTrimLoopData*)
DTK_DECLARE_PLUGIN(dtkAbstractTrimLoopData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractTrimLoopData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractTrimLoopData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkAbstractTrimLoopData, DTKCONTINUOUSGEOMETRYCORE_EXPORT, abstractTrimLoopData);
}

//
// dtkAbstractTrimLoopData.h ends here
