// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkContinuousGeometryCoreExport.h>

#include <QRunnable>
#include <QString>

#include <dtkCore>

class dtkBRep;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkBRepWriter : public QRunnable
{
public:
    virtual void setInputBRep(dtkBRep*) = 0;
	virtual void setOutputBRepFile(const QString&) = 0;

    virtual void run(void) = 0;
    virtual QString outputBRepFile(void) const = 0;

};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkBRepWriter*);
DTK_DECLARE_PLUGIN(dtkBRepWriter, DTKCONTINUOUSGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_FACTORY(dtkBRepWriter, DTKCONTINUOUSGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_MANAGER(dtkBRepWriter, DTKCONTINUOUSGEOMETRYCORE_EXPORT);

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkBRepWriter, DTKCONTINUOUSGEOMETRYCORE_EXPORT, bRepWriter);
}

//
// dtkBRepWriter.h ends here
