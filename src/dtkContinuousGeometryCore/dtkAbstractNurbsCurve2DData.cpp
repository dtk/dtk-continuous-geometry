// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkAbstractNurbsCurve2DData.h"

#include "dtkContinuousGeometry.h"

/*!
  \class dtkAbstractNurbsCurve2DData
  \inmodule dtkContinuousGeometry
  \brief dtkAbstractNurbsCurve2DData is the data container for 2D Non Uniform Rational Basis Splines curve

  It cannot be instanciated on its own.

  It is to be inherited from the classes acting as plugins. For more information : dtkPluginsContinuousGeometry.

  In pratice, the client code will instanciate \l dtkNurbsCurve2D. Check \l dtkNurbsCurve2D for more information.
*/

/*! \fn dtkAbstractNurbsCurve2DData::dtkAbstractNurbsCurve2DData(void)
  Instanciates the structure.

  To add the content, see \l create(std::size_t nb_cp, std::size_t order, double *knots, double *cps).
*/

/*! \fn dtkAbstractNurbsCurve2DData::create(std::size_t nb_cp, std::size_t order, double *knots, double *cps) const
  Creates the 2D NURBS curve by providing the required content.

  \a nb_cp : number of control points

  \a order : order

  \a knots : array containing the knots

  \a cps : array containing the weighted control points, specified as : [x0, y0, w0, ...]
*/

/*! \fn  dtkAbstractNurbsCurve2DData::degree(void) const
  Returns the degree of the curve.
*/

/*! \fn  dtkAbstractNurbsCurve2DData::nbCps(void) const
  Returns the number of weighted control points controlling the curve.
*/

/*! \fn  dtkAbstractNurbsCurve2DData::controlPoint(std::size_t i, double *r_cp) const
  Writes in \a r_cp the \a i th weighted control point coordinates[xi, yi].

  \a i : index of the weighted control point

  \a r_cp : array of size 2 to store the weighted control point coordinates[xi, yi]
*/

/*! \fn  dtkAbstractNurbsCurve2DData::weight(std::size_t i, double *r_w) const
  Writes in \a r_w the \a i th weighted control point weight wi.

  \a i : index of the weighted control point

  \a r_w : array of size 1 to store the weighted control point weight wi
*/

/*! \fn dtkAbstractNurbsCurve2DData::knots(double *r_knots) const
  Writes in \a r_knots the knots of the curve.

  \a r_knots : array of size (nb_cp + order - 2) to store the knots
*/

/*! \fn dtkAbstractNurbsCurve2DData::knots(void) const
  Returns a pointer to the array storing the knots of the curve. The array is of size : (nb_cp + order - 2).
*/

/*! \fn dtkAbstractNurbsCurve2DData::evaluatePoint(double p_u, double *r_point) const
  Writes in \a r_point the evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 2 to store the result of the evaluation
*/

/*! \fn dtkAbstractNurbsCurve2DData::evaluateNormal(double p_u, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_normal : array of size 2 to store the result of the normal evaluation
*/

/*! \fn dtkAbstractNurbsCurve2DData::evaluateNormal(double p_u, double *r_point, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 2 to store the result of the evaluation

  \a r_normal : array of size 2 to store the result of the normal evaluation
*/

/*! \fn dtkAbstractNurbsCurve2DData::decomposeToRationalBezierCurve2Ds(std::vector< std::pair< dtkRationalBezierCurve2D*, double* > >& r_rational_bezier_curves ) const
  Decomposes the NURBS curve into dtkRationalBezierCurve \a r_rational_bezier_curves.

 \a r_rational_bezier_curves : vector in which the dtkRationalBezierCurve are stored, along with their limits in the NURBS curve parameter space
*/

/*! \fn dtkAbstractNurbsCurve2DData::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates.

  \a r_aabb : array of size 4 to store the limits coordinates
*/

/*! \fn dtkAbstractNurbsCurve2DData::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 4 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/

/*! \fn dtkAbstractNurbsCurve2DData::clone(void) const
   Clone.
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkAbstractNurbsCurve2DData, abstractNurbsCurve2DData, dtkContinuousGeometry);
}

//
// dtkAbstractNurbsCurve2DData.cpp ends here
