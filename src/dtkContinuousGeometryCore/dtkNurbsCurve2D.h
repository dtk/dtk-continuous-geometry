// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

#include "dtkAbstractNurbsCurve2DData.h"

class dtkRationalBezierCurve2D;
class dtkTrim;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkNurbsCurve2D
{

public:
    explicit dtkNurbsCurve2D(void) : m_data(nullptr) {;};
    explicit dtkNurbsCurve2D(dtkAbstractNurbsCurve2DData *data) : m_data(data) {;};
             dtkNurbsCurve2D(const dtkNurbsCurve2D& other) :m_data(other.m_data->clone()) {;};

public:
    virtual ~dtkNurbsCurve2D(void);

 public:
    const dtkAbstractNurbsCurve2DData* data(void) const { return m_data; };
    dtkAbstractNurbsCurve2DData *data(void)       { return m_data;  };
    void setData(dtkAbstractNurbsCurve2DData* data) { m_data = data; };

 public:
    void create(std::size_t nb_cp, std::size_t order, double* knots, double* cps) const;

 public :
    std::size_t degree(void) const;
    std::size_t nbCps(void) const;

    void controlPoint(std::size_t i, double* r_cp) const;
    void weight(std::size_t i, double* r_w) const;

    void knots(double* r_knots) const;

    const double *knots(void) const;

 public:
   void evaluatePoint(double p_u, double* r_point) const;
   void evaluateNormal(double p_u, double* r_normal) const;
   void evaluateNormal(double p_u, double* r_point, double* r_normal) const;

 public:
   void decomposeToRationalBezierCurve2Ds(std::vector< std::pair< dtkRationalBezierCurve2D *, double *> >& r_rational_bezier_curves) const;

 public:
   void aabb(double* r_aabb) const;
   void extendedAabb(double* r_aabb, double factor) const;

 protected:
    dtkAbstractNurbsCurve2DData *m_data;
};


// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkNurbsCurve2D*)
DTK_DECLARE_PLUGIN(dtkNurbsCurve2D, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkNurbsCurve2D, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkNurbsCurve2D, DTKCONTINUOUSGEOMETRYCORE_EXPORT)


namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkNurbsCurve2D, DTKCONTINUOUSGEOMETRYCORE_EXPORT, nurbsCurve2D);
}
//
// dtkNurbsCurve2D.h ends here
