// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

class dtkRationalBezierCurve;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkRationalBezierCurveLossLessDegreeReductor
{
public:
    virtual void setRationalBezierCurve(dtkRationalBezierCurve *bezier_curve) = 0;

    virtual void run(void) = 0;

    virtual dtkRationalBezierCurve *rationalBezierCurve(void) = 0;
};

// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkRationalBezierCurveLossLessDegreeReductor*)
DTK_DECLARE_PLUGIN(dtkRationalBezierCurveLossLessDegreeReductor, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkRationalBezierCurveLossLessDegreeReductor, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkRationalBezierCurveLossLessDegreeReductor, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkRationalBezierCurveLossLessDegreeReductor, DTKCONTINUOUSGEOMETRYCORE_EXPORT, rationalBezierCurveLossLessDegreeReductor);
}

//
// dtkRationalBezierCurveLossLessDegreeReductor.h ends here
