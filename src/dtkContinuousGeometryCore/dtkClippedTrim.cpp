/// @TODO: yet another constant
constexpr double tolerance_in_parameters_space = 1e-6;
// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkClippedTrim.h"

#include "dtkTrim.h"
#include "dtkContinuousGeometry.h"
#include "dtkContinuousGeometryUtils.h"
#include "dtkNurbsCurve.h"
#include "dtkNurbsCurve2D.h"
#include "dtkRationalBezierCurve2DLineIntersector.h"
#include "dtkRationalBezierCurve2D.h"
#include "dtkRationalBezierCurve.h"
#include "dtkRationalBezierSurface.h"

#include "dtkRationalBezierCurve2DElevator.h"
#include "dtkRationalBezierCurveLossLessDegreeReductor.h"

/*!
  \class dtkClippedTrim
  \inmodule dtkContinuousGeometry
  \brief The dtkClippedTrim class is a clipped representation of a trim

  The following line of code shows how to instanciate the class in pratice :
  \code
  dtkTrim *trim = new dtkTrim(...);
  dtkClippedTrim *clipped_trim = new dtkClippedTrim(*trim);
  \endcode
*/

/*! \fn dtkClippedTrim::dtkClippedTrim(const dtkTrim& p_trim)
   Instanciates a dtkClippedTrim by clipping a trim (\a p_trim).
*/
dtkClippedTrim::dtkClippedTrim(const dtkTrim& trim) : m_trim(trim), m_nurbs_curve(nullptr)
{
    std::vector< std::pair< dtkRationalBezierCurve2D*, double*> > rational_bezier_curves;
    m_trim.curve2D().decomposeToRationalBezierCurve2Ds(rational_bezier_curves);
    for (auto it = rational_bezier_curves.begin(); it != rational_bezier_curves.end(); ++it) {
        m_rational_bezier_curves_2d.push_back(it->first);
        delete[] it->second;
        it->second = nullptr;
    }
}

/*! \fn dtkClippedTrim::~dtkClippedTrim(void)
   Destructor.
*/
dtkClippedTrim::~dtkClippedTrim(void)
{
    for (auto it = m_rational_bezier_curves_2d.begin(); it != m_rational_bezier_curves_2d.end(); ++it) {
        delete (*it);
    }
    for (auto it = m_located_rational_bezier_curves.begin(); it != m_located_rational_bezier_curves.end(); ++it) {
        delete it->first;
    }
    for (auto it = m_located_rational_bezier_curves_copy.begin(); it != m_located_rational_bezier_curves_copy.end(); ++it) {
        delete it->first;
    }
    for (auto it = m_rational_bezier_curves.begin(); it != m_rational_bezier_curves.end(); ++it) {
        delete (*it);
    }
    if(m_nurbs_curve != nullptr) {
        delete m_nurbs_curve;
        m_nurbs_curve = nullptr;
    }
}

/*! \fn dtkClippedTrim::split(const std::vector< dtkContinuousGeometryPrimitives::Line_2 >& p_clipping_lines)
   Splits the trim with a set of lines (\a p_clipping_lines).
 */
bool dtkClippedTrim::split(const std::vector< dtkContinuousGeometryPrimitives::Line_2 >& p_clipping_lines)
{
    std::list< dtkRationalBezierCurve2D *> split_bezier_curves;
    std::unique_ptr<dtkRationalBezierCurve2DLineIntersector> intersector(
          dtkContinuousGeometry::rationalBezierCurve2DLineIntersector::pluginFactory().create("sislRationalBezierCurve2DLineIntersector"));
    if(intersector == nullptr) {
        dtkFatal() << "To use " << Q_FUNC_INFO << " , dtkRationalBezierCurve2DLineIntersector must be compiled under the Sisl plugins";
    }
/*
    std::cout << "clipping lines: " << std::endl;
    for (auto line : p_clipping_lines) {
        std::cout << line.point()[0] << "," << line.point()[1] << "," << 0 << std::endl;
        std::cout << line.point()[0] + line.direction()[0] << "," << line.point()[1] + line.direction()[1] << "," << 0 << std::endl;
    }

    std::cout << "2d bezier curves: " << std::endl;
    for (auto bezier_2d : m_rational_bezier_curves_2d) {
        for (double i = 0.; i < 1.01; i += 0.1) {
            double p[2];
            bezier_2d->evaluatePoint(i, p);
            std::cout << p[0] << "," << p[1] << "," << 0 << std::endl;
        }
    }
*/
    /// @TODO: ugly code, memory leaks, inefficiencies
    for(auto it = m_rational_bezier_curves_2d.begin(); it != m_rational_bezier_curves_2d.end(); ++it) {
        std::vector< double > intersection_parameters;
        intersector->setRationalBezierCurve2D((*it));
        for (auto line = p_clipping_lines.begin(); line != p_clipping_lines.end(); ++line) {
            intersector->setLine(*line);
            intersector->run();
            std::vector< double > temp_intersection_parameters = intersector->intersectionsParameters();
            // ///////////////////////////////////////////////////////////////////
            // Removes 0s and 1s
            // ///////////////////////////////////////////////////////////////////
            std::sort(temp_intersection_parameters.begin(), temp_intersection_parameters.end());
            temp_intersection_parameters.erase(temp_intersection_parameters.begin(),
                                               std::lower_bound(temp_intersection_parameters.begin(),
                                                                temp_intersection_parameters.end(),
                                                                tolerance_in_parameters_space));
            temp_intersection_parameters.erase(std::upper_bound(temp_intersection_parameters.begin(),
                                                                temp_intersection_parameters.end(),
                                                                1.-tolerance_in_parameters_space),
                                                                temp_intersection_parameters.end());
            intersection_parameters.insert(intersection_parameters.end(), temp_intersection_parameters.begin(), temp_intersection_parameters.end());
        }
        // ///////////////////////////////////////////////////////////////////
        // Removes duplicates
        // ///////////////////////////////////////////////////////////////////
        intersection_parameters.erase(std::unique(intersection_parameters.begin(), intersection_parameters.end()), intersection_parameters.end());
        std::list< dtkRationalBezierCurve2D * > temp_split_bezier_curves;
        (*it)->split(temp_split_bezier_curves, intersection_parameters);
        split_bezier_curves.splice(split_bezier_curves.end(), temp_split_bezier_curves);
    }
    for (auto it = m_rational_bezier_curves_2d.begin(); it != m_rational_bezier_curves_2d.end(); ++it) {
        delete (*it);
    }
    m_rational_bezier_curves_2d.clear();
    m_rational_bezier_curves_2d.splice(m_rational_bezier_curves_2d.end(), split_bezier_curves);
    if(m_rational_bezier_curves_2d.empty()) {
        dtkError() << "One of the trim was split into 0 rational bezier curves";
        return false;
    }
    return true;
}

/*! \fn dtkClippedTrim::locate(const std::vector< std::pair< dtkRationalBezierSurface *, double * > > p_clipping_rational_bezier_surfaces)
   Locates the clipped rational Bezier curves on the clipped rational Bezier surfaces (\a p_clipping_rational_bezier_surfaces).
 */
bool dtkClippedTrim::locate(const std::vector< std::pair< dtkRationalBezierSurface *, double * > >& p_clipping_rational_bezier_surfaces)
{
    bool ret = true;
    double x_param_delta = std::numeric_limits<double>::infinity(), y_param_delta = x_param_delta;
    double u_min = std::numeric_limits<double>::infinity(), u_max = -u_min;
    double v_min = std::numeric_limits<double>::infinity(), v_max = -v_min;
    for(auto bezier_surface = p_clipping_rational_bezier_surfaces.begin(); bezier_surface != p_clipping_rational_bezier_surfaces.end(); ++bezier_surface) {
      if (bezier_surface->second[2] - bezier_surface->second[0] < x_param_delta) x_param_delta = bezier_surface->second[2] - bezier_surface->second[0];
      if (bezier_surface->second[3] - bezier_surface->second[1] < y_param_delta) y_param_delta = bezier_surface->second[3] - bezier_surface->second[1];
      
      if (bezier_surface->second[0] < u_min) u_min = bezier_surface->second[0];
      if (bezier_surface->second[1] < v_min) v_min = bezier_surface->second[1];
      if (bezier_surface->second[2] > u_max) u_max = bezier_surface->second[2];
      if (bezier_surface->second[3] > v_max) v_max = bezier_surface->second[3];
    }
    double epsilon_x = 0.; // bounded error
    double epsilon_y = 0.;
    for (auto bezier_curve : m_rational_bezier_curves_2d) {
        // bounding box of the bezier curve
        double bbox[4];
        bezier_curve->aabb(bbox);
        if (bbox[2] - bbox[0] < x_param_delta && bbox[2] - bbox[0] > tolerance_in_parameters_space) x_param_delta = bbox[2] - bbox[0];
        if (bbox[3] - bbox[1] < y_param_delta && bbox[3] - bbox[1] > tolerance_in_parameters_space) y_param_delta = bbox[3] - bbox[1];

        if (bbox[0] < u_min && std::abs(u_min - bbox[0]) > epsilon_x) epsilon_x = std::abs(u_min - bbox[0]);
        if (bbox[1] < v_min && std::abs(v_min - bbox[1]) > epsilon_y) epsilon_y = std::abs(v_min - bbox[1]);
        if (bbox[2] > u_max && std::abs(u_max - bbox[2]) > epsilon_x) epsilon_x = std::abs(u_max - bbox[2]);
        if (bbox[3] > v_max && std::abs(v_max - bbox[3]) > epsilon_y) epsilon_y = std::abs(v_max - bbox[3]);
    }
    double delta_x = x_param_delta/10.; // the constant 10 is sufficient I think
    double delta_y = y_param_delta/10.;
    if (delta_x < epsilon_x) delta_x = 2.*epsilon_x;
    if (delta_y < epsilon_y) delta_y = 2.*epsilon_y;
    std::cout << "delta_x = " << delta_x << "; delta_y = " << delta_y << std::endl;
    for(auto bezier_curve = m_rational_bezier_curves_2d.begin(); bezier_curve != m_rational_bezier_curves_2d.end(); ++bezier_curve) {
        bool located = false;
        // ///////////////////////////////////////////////////////////////////
        // Evaluate at u = 0.5, check that the point lies in the bezier surface domain
        // ///////////////////////////////////////////////////////////////////
        dtkContinuousGeometryPrimitives::Point_2 p(0., 0.);
        // ///////////////////////////////////////////////////////////////////
        // Can set and change but everything is set with <= and >= MEMORY ISSUES
        // ///////////////////////////////////////////////////////////////////
        (*bezier_curve)->evaluatePoint(0.5, p.data());

        std::vector<std::pair< dtkRationalBezierSurface *, double * >> bezier_surfaces_candidates, bezier_surfaces_valid;
        for (auto bezier_surface : p_clipping_rational_bezier_surfaces) {
            if ( bezier_surface.second[0] - delta_x <= p[0] && p[0] <= bezier_surface.second[2] + delta_x &&
                 bezier_surface.second[1] - delta_y <= p[1] && p[1] <= bezier_surface.second[3] + delta_y) {
                bezier_surfaces_candidates.push_back(bezier_surface);
            }
        }

        if (bezier_surfaces_candidates.size() > 1) {
            for (auto bezier_surface : bezier_surfaces_candidates) {
                if (bezier_surface.second[0] <= p[0] && bezier_surface.second[2] >= p[0] && 
                    bezier_surface.second[1] <= p[1] && bezier_surface.second[3] >= p[1]) {
                    bezier_surfaces_valid.push_back(bezier_surface);
                }
            }
            if (bezier_surfaces_valid.size() == 0) { // in case the point is just outside the domain due to precision issue
                // check u & v bounds separately
                for (auto bezier_surface : bezier_surfaces_candidates) {
                    if (bezier_surface.second[0] <= p[0] && bezier_surface.second[2] >= p[0]) {
                        bezier_surfaces_valid.push_back(bezier_surface);
                        break;
                    }
                    if (bezier_surface.second[1] <= p[1] && bezier_surface.second[3] >= p[1]) {
                        bezier_surfaces_valid.push_back(bezier_surface);
                        break;
                    }
                }
            }
        }
        else if (bezier_surfaces_candidates.size() == 1) {
            bezier_surfaces_valid = bezier_surfaces_candidates;
        }

        for(auto bezier_surface = bezier_surfaces_valid.begin(); bezier_surface != bezier_surfaces_valid.end(); ++bezier_surface) {
                // ///////////////////////////////////////////////////////////////////
                // Compute the bezier curve control points in the bezier surface coordinates
                // ///////////////////////////////////////////////////////////////////
            if (bezier_surface->first->uDegree() <= 5 && bezier_surface->first->vDegree() <= 5) {
                dtkAbstractRationalBezierCurve2DData *rational_bezier_curve_2d_data = dtkContinuousGeometry::abstractRationalBezierCurve2DData::pluginFactory().create("dtkRationalBezierCurve2DDataOn");
                if(rational_bezier_curve_2d_data == nullptr) {
                    dtkFatal() << "To use " << Q_FUNC_INFO << " , dtkAbstractRationalBezierCurve2DData must be compiled under the openNURBS plugins";
                }
                std::vector<double> coef(3 * ((*bezier_curve)->degree() + 1));
                dtkContinuousGeometryPrimitives::Point_2 p(0., 0.);
                double w = 0.;
                for (std::size_t i = 0; i < (*bezier_curve)->degree() + 1; ++i) {
                    (*bezier_curve)->controlPoint(i, p.data());
                    (*bezier_curve)->weight(i, &w);
                    coef[3 * i] = (p[0] - bezier_surface->second[0]) / (bezier_surface->second[2] - bezier_surface->second[0]);
                    coef[3 * i + 1] = (p[1] - bezier_surface->second[1]) /  (bezier_surface->second[3] - bezier_surface->second[1]);
                    coef[3 * i + 2] = w;
                }
                rational_bezier_curve_2d_data->create((*bezier_curve)->degree() + 1, coef.data());
                dtkRationalBezierCurve2D *curve = new dtkRationalBezierCurve2D(rational_bezier_curve_2d_data);
                m_located_rational_bezier_curves.push_back(std::make_pair(curve, bezier_surface->first));

                // this is to deal with the "clone" issue of rationalBezierCurve2D
                // rational_bezier_curve_2d_data_copy is used for 3d approximation and will be changed in the process, but it will not be used afterwards.
                //TODO need to find a better solution for a deep clone
                dtkAbstractRationalBezierCurve2DData *rational_bezier_curve_2d_data_copy = dtkContinuousGeometry::abstractRationalBezierCurve2DData::pluginFactory().create("dtkRationalBezierCurve2DDataOn");
                rational_bezier_curve_2d_data_copy->create((*bezier_curve)->degree() + 1, coef.data());
                dtkRationalBezierCurve2D* curve_copy = new dtkRationalBezierCurve2D(rational_bezier_curve_2d_data_copy);
                m_located_rational_bezier_curves_copy.push_back(std::make_pair(curve_copy, bezier_surface->first));
            }
            else {
                // the bezier surface degrees are too high; split the curve into piecewise line segments
                double delta = 1./((*bezier_curve)->degree());
                for (std::size_t i = 0; i < (*bezier_curve)->degree(); ++i) {
                    // one line segment
                    std::vector<double> coef(6); // two control points
                    dtkContinuousGeometryPrimitives::Point_2 p1(0., 0.);
                    dtkContinuousGeometryPrimitives::Point_2 p2(0., 0.);

                    double t1 = i*delta, t2 = (i + 1)*delta;
                    if (i == 0) t1 = 0.;
                    if (i == (*bezier_curve)->degree() - 1) t2 = 1.;

                    (*bezier_curve)->evaluatePoint(t1, p1.data());
                    (*bezier_curve)->evaluatePoint(t2, p2.data());
                    
                    coef[0] = (p1[0] - bezier_surface->second[0])/(bezier_surface->second[2] - bezier_surface->second[0]);
                    coef[1] = (p1[1] - bezier_surface->second[1])/(bezier_surface->second[3] - bezier_surface->second[1]);
                    coef[2] = 1.;

                    coef[3] = (p2[0] - bezier_surface->second[0])/(bezier_surface->second[2] - bezier_surface->second[0]);
                    coef[4] = (p2[1] - bezier_surface->second[1])/(bezier_surface->second[3] - bezier_surface->second[1]);
                    coef[5] = 1.;

                    dtkAbstractRationalBezierCurve2DData *rational_bezier_curve_2d_data = dtkContinuousGeometry::abstractRationalBezierCurve2DData::pluginFactory().create("dtkRationalBezierCurve2DDataOn");
                    rational_bezier_curve_2d_data->create(2, coef.data());
                    dtkRationalBezierCurve2D* curve = new dtkRationalBezierCurve2D(rational_bezier_curve_2d_data);
                    m_located_rational_bezier_curves.push_back(std::make_pair(curve, bezier_surface->first));

                    dtkAbstractRationalBezierCurve2DData *rational_bezier_curve_2d_data_copy = dtkContinuousGeometry::abstractRationalBezierCurve2DData::pluginFactory().create("dtkRationalBezierCurve2DDataOn");
                    rational_bezier_curve_2d_data_copy->create(2, coef.data());
                    dtkRationalBezierCurve2D* curve_copy = new dtkRationalBezierCurve2D(rational_bezier_curve_2d_data_copy);
                    m_located_rational_bezier_curves_copy.push_back(std::make_pair(curve_copy, bezier_surface->first));
                }
            }
            
            located = true;
            break;
            // ///////////////////////////////////////////////////////////////////
            // Catch issues
            // ///////////////////////////////////////////////////////////////////
            //TODO
        }
        if(located == false) {
            ret = false;
            dtkContinuousGeometryPrimitives::Point_2 p0(0., 0.);
            dtkContinuousGeometryPrimitives::Point_2 p1(0., 0.);
            (*bezier_curve)->evaluatePoint(0., p0.data());
            (*bezier_curve)->evaluatePoint(1., p1.data());
            dtkError() << "The curve starting at :" << qSetRealNumberPrecision(17) << p0 << ", ending at : " << p1 << " could not be located in " << Q_FUNC_INFO;
            dtkError() << "Mid point : " << qSetRealNumberPrecision(17) << p;
            dtkError() << "The bezier surfaces limits in the NURBS parameter space are : (with delta=" << qSetRealNumberPrecision(17) << delta_x << ")";
            auto pos_versus_interval = [](double v, double min, double max) {
              if(v < min) {
                  return "<";
              } else if(v > max) {
                  return ">";
              } else return ">==<";
            };
            auto display_location = [pos_versus_interval](dtkContinuousGeometryPrimitives::Point_2 p, double min_x, double max_x, double min_y, double max_y) {
                dtkError() << "x: [ " << qSetRealNumberPrecision(17) << min_x << " " << max_x << " ] " << pos_versus_interval(p[0], min_x, max_x);
                dtkError() << "y: [ " << qSetRealNumberPrecision(17) << min_y << " " << max_y << " ] " << pos_versus_interval(p[1], min_y, max_y);
            };
            for(auto bezier_surface : p_clipping_rational_bezier_surfaces) {
                display_location(p,
                                 bezier_surface.second[0] - delta_x, bezier_surface.second[2] + delta_x,
                                 bezier_surface.second[1] - delta_y, bezier_surface.second[3] + delta_y);
            }
        }
    }
    return ret;
}

/*! \fn dtkClippedTrim::embed(void)
   Computes the clipped 3D rational Bezier curves of the clipped 2D rational Bezier curves by the clipped rational Bezier surfaces.
 */
bool dtkClippedTrim::embed(void)
{
    bool ret = true;
    dtkRationalBezierCurve2DElevator *embedder = dtkContinuousGeometry::rationalBezierCurve2DElevator::pluginFactory().create("defaultRationalBezierCurve2DElevator");
    if(embedder == nullptr) {
        dtkFatal() << "To use " << Q_FUNC_INFO << " , dtkRationalBezierCurve2DElevator must be compiled under the Blaze plugins";
    }
    for(auto bc_2d = m_located_rational_bezier_curves.begin(); bc_2d != m_located_rational_bezier_curves.end(); ++bc_2d) {
      if (bc_2d->second->uDegree() <= 5 && bc_2d->second->vDegree() <= 5) {
        embedder->setInputRationalBezierCurve2D(bc_2d->first);
        embedder->setInputRationalBezierSurface(bc_2d->second);
        embedder->run();
        dtkRationalBezierCurve *bezier_curve = embedder->rationalBezierCurve();
        if (bezier_curve != nullptr) {
            m_rational_bezier_curves.push_back(bezier_curve);
            m_map_curve3d_to_curve2d[bezier_curve] = *bc_2d;
        } else {
            ret = false;
            dtkWarn() << "The dtkRationalBezierCurve2DElevator could not elevate the trim, perhaps because it was a point.";
        }
      }
      else {
          // if the bezier surface has a very high degree, the 3d curve is approximated as a line segment
          if (bc_2d->first->degree() != 1) {
              dtkFatal() << "The 2d clipped curve must be linear!";
          }
          dtkAbstractRationalBezierCurveData *rational_bezier_curve_data = dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().create("dtkRationalBezierCurveDataOn");
          std::vector<double> cps(8);
          double p2d_1[2], p2d_2[2];
          (bc_2d->first)->controlPoint(0, p2d_1);
          (bc_2d->first)->controlPoint(1, p2d_2);

          double p3d_1[3], p3d_2[3];
          (bc_2d->second)->evaluatePoint(p2d_1[0], p2d_1[1], p3d_1);
          (bc_2d->second)->evaluatePoint(p2d_2[0], p2d_2[1], p3d_2);

          cps[0] = p3d_1[0], cps[1] = p3d_1[1], cps[2] = p3d_1[2], cps[3] = 1.;
          cps[4] = p3d_2[0], cps[5] = p3d_2[1], cps[6] = p3d_2[2], cps[7] = 1.;

          dtkRationalBezierCurve* bezier_curve = new dtkRationalBezierCurve(rational_bezier_curve_data);
          rational_bezier_curve_data->create(2, cps.data());

          m_rational_bezier_curves.push_back(bezier_curve);
          m_map_curve3d_to_curve2d[bezier_curve] = *bc_2d;
      }
/*
        std::cout << "evaluate 2d curve to surface: " << std::endl;
        for (double i = 0.; i < 1.01; i += 0.1) {
            double p2[2];
            (bc_2d->first)->evaluatePoint(i, p2);
            double p3[3];
            (bc_2d->second)->evaluatePoint(p2[0], p2[1], p3);
            std::cout << p3[0] << " " << p3[1] << " " << p3[2] << std::endl;
        }

        std::cout << "3d bezier curve: " << std::endl;
        for (double i = 0.; i < 1.01; i += 0.1) {
            double p[3];
            bezier_curve->evaluatePoint(i, p);
            std::cout << p[0] << " " << p[1] << " " << p[2] << std::endl;
        }
        std::cout << std::endl;

        std::cout << "bezier surface: " << std::endl;
        for (double i = 0.; i < 1.01; i += 0.1) {
            for (double j = 0.; j < 1.01; j += 0.1) {
                double p[3];
                (bc_2d->second)->evaluatePoint(i, j, p);
                std::cout << p[0] << " " << p[1] << " " << p[2] << std::endl;
            }
        }
*/
    }
    delete embedder;
    return ret;
}

/*! \fn dtkClippedTrim::reduce(void)
   Reduces the degree of the clipped 3D rational Bezier curves.
 */
bool  dtkClippedTrim::reduce(void)
{
    bool ret = true;
    dtkRationalBezierCurveLossLessDegreeReductor *reductor = dtkContinuousGeometry::rationalBezierCurveLossLessDegreeReductor::pluginFactory().create("defaultRationalBezierCurveLossLessDegreeReductor");
    if(reductor == nullptr) {
        dtkFatal() << "To use " << Q_FUNC_INFO << " , dtkRationalBezierCurveLossLessDegreeReductor must be compiled under the Default plugins";
    }

    for(auto curve : m_rational_bezier_curves) {
        dtkRationalBezierCurve *temp_curve = new dtkRationalBezierCurve();
        while(temp_curve != nullptr) {
            reductor->setRationalBezierCurve(curve);
            reductor->run();
            temp_curve = reductor->rationalBezierCurve();
            if (temp_curve != nullptr) {
                curve = temp_curve;
            }
        }
        // ///////////////////////////////////////////////////////////////////
        // TODO Need to fix the memory
        // ///////////////////////////////////////////////////////////////////
    }
    return ret;
}

/*! \fn dtkClippedTrim::stitch(void)
   Creates a NURBS curve from the clipped 3D rational Bezier curves.
 */
bool  dtkClippedTrim::stitch(void)
{
    if(m_rational_bezier_curves.size() == 0) {
        dtkError() << "Trying to stich rational Bezier curves in " << Q_FUNC_INFO << "with an empty set of rational Bezier curves cannot work";
        return false;
    }

    std::vector< dtkRationalBezierCurve * > rational_bezier_curves(m_rational_bezier_curves.begin(), m_rational_bezier_curves.end());
    m_nurbs_curve = new dtkNurbsCurve(dtkContinuousGeometry::abstractNurbsCurveData::pluginFactory().create("dtkNurbsCurveDataOn"));
    if(m_nurbs_curve == nullptr) {
        dtkFatal() << "To use " << Q_FUNC_INFO << " , dtkAbstractNurbsCurveData must be compiled under the openNURBS plugins";
    }
    m_nurbs_curve->create(rational_bezier_curves);

    return true;
}


bool dtkClippedTrim::toPolyline3DApprox(const std::vector< std::pair< dtkRationalBezierSurface *, double * > > p_clipping_rational_bezier_surfaces, std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_3D_approximation) const
{
    // ///////////////////////////////////////////////////////////////////
    // Stores the first piece in r_polyline
    // ///////////////////////////////////////////////////////////////////
    dtkContinuousGeometryTools::convexHull3DApproximation(r_polyline, (*m_located_rational_bezier_curves_copy.front().first), (*m_located_rational_bezier_curves_copy.front().second), p_3D_approximation);
    bool found = false;
    std::cerr << "    trim index: " << this->trimIndex() << " p_clipping_rational_bezier_surfaces.size() : " << p_clipping_rational_bezier_surfaces.size()  << std::endl;
    for(auto pair_bn : p_clipping_rational_bezier_surfaces) {
        if (pair_bn.first == m_located_rational_bezier_curves_copy.front().second) {
            found = true;
            for(auto& point : r_polyline) {
                point[0] = pair_bn.second[0] + point[0] * (pair_bn.second[2] - pair_bn.second[0]);
                point[1] = pair_bn.second[1] + point[1] * (pair_bn.second[3] - pair_bn.second[1]);
            }
            break;
        }
    }
    if(!found) {dtkFatal() << "The dtkRationalBezierSurface in which lies the trim part could not be found";}
    if (r_polyline.empty()) { dtkWarn() << "One of the rational bezier cannot be converted to polyline";
        return false;
    }
    if (m_located_rational_bezier_curves_copy.size() > 1) {
        std::list< dtkContinuousGeometryPrimitives::Point_2 > temp;
        for (auto pair = std::next(m_located_rational_bezier_curves_copy.begin()); pair != m_located_rational_bezier_curves_copy.end(); ++pair) {
            dtkContinuousGeometryTools::convexHull3DApproximation(temp, *(pair->first), *(pair)->second, p_3D_approximation);
            // ///////////////////////////////////////////////////////////////////
            // TODO makes this nicer...
            // ///////////////////////////////////////////////////////////////////
            found = false;
            for(auto pair_bn : p_clipping_rational_bezier_surfaces) {
                if (pair_bn.first == pair->second) {
                    for(auto& point : temp) {
                        found = true;
                        point[0] = pair_bn.second[0] + point[0] * (pair_bn.second[2] - pair_bn.second[0]);
                        point[1] = pair_bn.second[1] + point[1] * (pair_bn.second[3] - pair_bn.second[1]);
                        //std::cerr << point[0] << " " << point[1] << std::endl;
                    }
                    break;
                }
            }
            if(!found) {dtkFatal() << "The dtkRationalBezierSurface in which lies the trim part could not be found";}
            if (!temp.empty()) {
                // ///////////////////////////////////////////////////////////////////
                // Clustering
                // ///////////////////////////////////////////////////////////////////
                double distance = dtkContinuousGeometryTools::squaredDistance(*temp.begin(), *std::prev(r_polyline.end()));
                if(distance > 1e-2) {
                    dtkWarn() << qSetRealNumberPrecision(17) << "End point : " << (*std::prev(r_polyline.end()))[0] << " " << (*std::prev(r_polyline.end()))[1];
                    dtkWarn() << qSetRealNumberPrecision(17) << "Start point : " << (*temp.begin())[0] << " " << (*temp.begin())[1];
                    dtkWarn() << qSetRealNumberPrecision(17) << "Squared distance : " << distance;
                    dtkWarn() << "The start point and end point of two consecutives rational bezier curves do not match.";
                    return false;
                }
                temp.erase(temp.begin());
                r_polyline.splice(r_polyline.end(), temp);
            } else {
                dtkWarn() << "The trim is empty, it cannot be converted to polyline";
                return false;
            }
        }
    }
    std::cout << "finish polygon approximation" << std::endl;
    return true;
}
//
// dtkClippedTrim.cpp ends here

// Local Variables:
// c-basic-offset: 4
// End:
