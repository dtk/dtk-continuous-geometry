// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

class dtkRationalBezierCurve;
class dtkNurbsCurve;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkClippedNurbsCurve
{

 public:
    dtkClippedNurbsCurve(const dtkNurbsCurve& p_nurbs_curve);
    virtual ~dtkClippedNurbsCurve(void);

 public:
   std::size_t degree(void) const;

   std::size_t nbCps(void) const;

   std::size_t dim(void) const;

   void controlPoint(std::size_t i, double* r_cp) const;
   void weight(std::size_t i, double* r_w) const;

   void knots(double* r_knots) const;
   double firstKnot(void) const;
   double lastKnot(void) const;

 public:
   void evaluatePoint(double p_u, double* r_point) const;
   void evaluateNormal(double p_u, double* r_normal) const;
   void evaluateNormal(double p_u, double* r_point, double* r_normal) const;

public:
   const dtkNurbsCurve& m_nurbs_curve;
   std::vector< std::pair< dtkRationalBezierCurve *, double * > > m_rational_bezier_curves;
   double first_knot;
   double last_knot;
};


// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkClippedNurbsCurve*)

//
// dtkClippedNurbsCurve.h ends here
