// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

#include "dtkBRepData.h"

class dtkNurbsSurface;
class dtkTopoTrim;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkBRep
{

 public:
    explicit dtkBRep(void) : m_data(nullptr) {;};
    explicit dtkBRep(dtkBRepData *data) : m_data(data) {;};
    dtkBRep(const dtkBRep& other) : m_data(other.m_data->clone()) {;};

 public:
    virtual ~dtkBRep(void);

 public:
    const dtkBRepData* data(void) const { return m_data; };
    dtkBRepData *data(void)       { return m_data;  };
    void setData(dtkBRepData *data) { m_data = data; };

 public:
    const std::vector< dtkNurbsSurface* >& nurbsSurfaces(void) const;
    const std::vector< dtkTopoTrim* >& topoTrims(void) const;

    void aabb(double* r_aabb) const;

 protected:
    dtkBRepData *m_data;
};


// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkBRep*)

//
// dtkBRep.h ends here
