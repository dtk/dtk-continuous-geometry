// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkAbstractRationalBezierSurfaceData.h"

#include "dtkContinuousGeometry.h"

/*!
  \class dtkAbstractRationalBezierSurfaceData
  \inmodule dtkContinuousGeometry
  \brief dtkAbstractRationalBezierSurfaceData is the data container for rational Bezier surface

  It cannot be instanciated on its own.

  It is to be inherited from the classes acting as plugins. For more information : dtkPluginsContinuousGeometry.

  In pratice, the client code will instanciate \l dtkRationalBezierSurface. Check \l dtkRationalBezierSurface for more information.
*/

/*! \fn void dtkAbstractRationalBezierSurfaceData::create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, double* cps) const
  Creates a NURBS surface by providing the required content.

  \a dim : dimension of the space in which lies the surface

  \a nb_cp_u : number of control points along the "u" direction

  \a nb_cp_v : number of control points along the "v" direction

  \a cps : array containing the weighted control points, secified as : [x_00, y_00, z_00, w_00, x_01, y_01, ...]

  \a cps are copied.
*/

/*! \fn void dtkAbstractRationalBezierSurfaceData::create(std::string path) const
  Creates a rational Bezier surface by providing a path to a file storing the required information to create a rational Bezier surface.

  \a path : a valid path to the file containing the information regarding a given rational Bezier surface
*/

/*! \fn dtkAbstractRationalBezierSurfaceData::uDegree(void) const
  Returns the degree of the surface in the "u" direction.
*/

/*! \fn dtkAbstractRationalBezierSurfaceData::vDegree(void) const
  Returns the degree of the surface in the "v" direction.
*/

/*! \fn dtkAbstractRationalBezierSurfaceData::controlPoint(std::size_t i, std::size_t j, double *r_cp) const
  Writes in \a r_cp the \a i th, \a j th weighted control point coordinates[xij, yij, zij].

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_cp : array of size 3 to store the weighted control point coordinates[xij, yij, zij]
 */

/*! \fn dtkAbstractRationalBezierSurfaceData::weight(std::size_t i, std::size_t j, double *r_w) const
  Writes in \a r_w the \a i th, \a j th weighted control point weight wij.

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_w : array of size 1 to store the weighted control point weight wij
*/

/*! \fn dtkAbstractRationalBezierSurfaceData::isDegenerate(void) const
  Returns true if the rational Bezier surface is degenerate : i.e. a line of control points coalesce, else returns false.
*/

/*! \fn dtkAbstractRationalBezierSurfaceData::degenerateLocations(std::list< dtkContinuousGeometryPrimitives::Point_3 >& r_degenerate_locations) const
  Returns true if the rational Bezier surface is degenerate : i.e. a line of control points coalesce and adds in \a r_degenerate_locations the locations at which there are degeneracies, else returns false.
*/

/*! \fn dtkAbstractRationalBezierSurfaceData::evaluatePoint(double p_u, double p_v, double *r_point) const
  Writes in \a r_point the evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation
*/

/*! \fn dtkAbstractRationalBezierSurfaceData::evaluateNormal(double p_u, double p_v, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/

/*! \fn dtkAbstractRationalBezierSurfaceData::evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_point the evaluation, in \a r_u_deriv the first derivative in "u" direction, in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/

/*! \fn dtkAbstractRationalBezierSurfaceData::evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_u_deriv the first derivative in "u" direction and in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/

/*! \fn dtkAbstractRationalBezierSurfaceData::evaluatePointBlossom(const double* p_uis, const double* p_vis, double *r_point, double *r_weight) const
  Stores in \a r_point and \a r_weight the result of the blossom evaluation at \a p_uis, \a p_vis values.

  \a p_uis : "u" values for blossom evaluation

  \a p_vis : "v" values for blossom evaluation

  \a r_point : array of size 3 to slore the point result of the blossom evaluation

  \a r_weight : array of size 1 to slore the weight result of the blossom evaluation
*/

/*! \fn dtkAbstractRationalBezierSurfaceData::split(dtkRationalBezierSurface *r_split_surface, double *p_splitting_parameters) const
  Creates \a r_split_surface as the part of rational Bezier surface lying inside the bounding box of parameters \a p_splitting_parameters.

  \a r_split_surface : pointer that will point to the newly created rationalBezierSurface which is the part of the initial surface restricted to the bouding box of parameters given in \a p_splitting_parameters

  \a p_splitting_parameters : array of size 4, storing the parameters defining the restriction in the parameter space of the rational Bezier surface, given as [u_0, v_0, u_1, v_1]
 */


/*! \fn dtkAbstractRationalBezierSurfaceData::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
 */

/*! \fn dtkAbstractRationalBezierSurfaceData::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/

/*! \fn dtkAbstractRationalBezierSurfaceData::clone(void) const
   Clone.
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkAbstractRationalBezierSurfaceData, abstractRationalBezierSurfaceData, dtkContinuousGeometry);
}

//
// dtkAbstractRationalBezierSurfaceData.cpp ends here
