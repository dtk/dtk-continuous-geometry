#if  __GNUC__ >= 6 || __clang_major__ >= 4
#  pragma GCC diagnostic push
#  pragma GCC diagnostic ignored "-Wunused-parameter"
#  if __GNUC__ >= 9 || __clang_major__ >= 10
#    pragma GCC diagnostic ignored "-Wdeprecated-copy"
#  endif
#endif
#include <blaze/Blaze.h>
#if __GNUC__ >= 6 || __clang_major__ >= 4
#  pragma GCC diagnostic pop
#endif
