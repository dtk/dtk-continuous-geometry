// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

namespace  dtkContinuousGeometryTools {
    template <typename T> int sign(T val)
    {
        return (val < T(0));
    }
}

//
// dtkContinuousGeometryUtils.tpp ends here
