#pragma once

#include <dtkContinuousGeometryCoreExport.h>

#include <dtkCore>

#if dtk_VERSION_MAJOR == 1 // dtk-1.7
namespace dtk {
  namespace LogLevel {
    using dtkLog::Level::Trace;
    using dtkLog::Level::Info;
  }
}
#endif // dtk-1.7

namespace dtkContinuousGeometry
{

    DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkCoreLayerManager& manager(void);

    DTKCONTINUOUSGEOMETRYCORE_EXPORT void initialize(const QString& path = QString());
    DTKCONTINUOUSGEOMETRYCORE_EXPORT void setVerboseLoading(bool b);
    DTKCONTINUOUSGEOMETRYCORE_EXPORT void setAutoLoading(bool auto_loading);
    DTKCONTINUOUSGEOMETRYCORE_EXPORT void uninitialize(void);

}
