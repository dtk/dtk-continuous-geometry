// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkAbstractRationalBezierCurveData.h"

#include "dtkContinuousGeometry.h"

/*!
  \class dtkAbstractRationalBezierCurveData
  \inmodule dtkContinuousGeometry
  \brief dtkAbstractRationalBezierCurveData is the data container for rational Bezier curve

  It cannot be instanciated on its own.

  It is to be inherited from the classes acting as plugins. For more information : dtkPluginsContinuousGeometry.

  In pratice, the client code will instanciate \l dtkRationalBezierCurve. Check \l dtkRationalBezierCurve for more information.
*/

/*! \fn dtkAbstractRationalBezierCurveData::dtkAbstractRationalBezierCurveData(void)
  Instanciate the structure of a dtkAbstractRationalBezierCurveData.

  To add the content, see \l create(std::size_t nb_cp, std::size_t order, double *knots, double *cps).
*/

/*! \fn dtkAbstractRationalBezierCurveData::create(std::size_t order, double *cps) const
  Creates the rational Bezier curve by providing the required content.

 \a order : order

 \a cps : array containing the weighted control points, specified as : [x0, y0, w0, ...]
*/

/*! \fn void dtkAbstractRationalBezierCurveData::create(std::string path) const
  Creates a rational Bezier curve by providing a path to a file storing the required information to create a rational Bezier curve.

  \a path : a valid path to the file containing the information regarding a given rational Bezier curve
*/

/*! \fn  dtkAbstractRationalBezierCurveData::degree(void) const
  Returns the degree of the curve.
*/

/*! \fn  dtkAbstractRationalBezierCurveData::controlPoint(std::size_t i, double *r_cp) const
  Writes in \a r_cp the \a i th weighted control point coordinates[xi, yi].

  \a i : index of the weighted control point

  \a r_cp : array of size 3 to store the weighted control point coordinates[xi, yi, zi]

  The method can be called as follow:
  \code
  dtkContinuousGeometryPrimitives::Point_3 point3(0, 0, 0);
  rational_bezier_curve_data->controlPoint(3, point3.data());
  \endcode
*/

/*! \fn  dtkAbstractRationalBezierCurveData::weight(std::size_t i, double *r_w) const
  Writes in \a r_w the \a i th weighted control point weight wi.

  \a i : index of the weighted control point

  \a r_w : array of size 1 to store the weighted control point weight wi
*/

/*! \fn  dtkAbstractRationalBezierCurveData::setWeightedControlPoint(std::size_t i, double *p_cp)
  Modifies the \a i th weighted control point by \a p_cp : [xi, yi, zi, wi].

  \a i : index of the weighted control point

  \a p_cp : array of size 4 containing the weighted control point coordinates and weight  : [xi, yi, zi, wi]
*/

/*! \fn dtkAbstractRationalBezierCurveData::evaluatePoint(double p_u, double *r_point) const
  Writes in \a r_point the evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 3 to store the result of the evaluation
*/

/*! \fn dtkAbstractRationalBezierCurveData::evaluateNormal(double p_u, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/

/*! \fn dtkAbstractRationalBezierCurveData::split(dtkRationalBezierCurve *r_split_curve_a, dtkRationalBezierCurve *r_split_curve_b, double splitting_parameter) const
  Splits at \a splitting_parameter the rational Bezier curve and stores in \a r_split_curve_a and \a r_split_curve_b the split parts (from 0 to \a splitting_parameter and from \a splitting_parameter to 1 respectively).

  \a r_split_curve_a : a pointer to a valid dtkRationalBezierCurve with data not initialized, in which the first part of the curve (from 0 to to \a splitting_parameter) will be stored

  \a r_split_curve_b : a pointer to a valid dtkRationalBezierCurve with data not initialized, in which the second part of the curve (from \a splitting_parameter to 1) will be stored

  \a splitting_parameter : the parameter at which the curve will be split
*/

/*! \fn dtkAbstractRationalBezierCurveData::split(std::list< dtkRationalBezierCurve * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const
  Splits the rational Bezier curve at each value of \a p_splitting_parameters and stores in \a r_split_curves the split parts.

  \a r_split_curves : a list to which the split rational Bezier curves will be added

  \a p_splitting_parameters : the parameters at which the curve will be split
*/

/*! \fn dtkAbstractRationalBezierCurveData::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
*/

/*! \fn dtkAbstractRationalBezierCurveData::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/

/*! \fn dtkAbstractRationalBezierCurveData::clone(void) const
   Clone
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkAbstractRationalBezierCurveData, abstractRationalBezierCurveData, dtkContinuousGeometry);
}

//
// dtkAbstractRationalBezierCurveData.cpp ends here
