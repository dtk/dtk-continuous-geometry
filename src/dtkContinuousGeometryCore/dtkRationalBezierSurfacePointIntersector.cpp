// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkContinuousGeometry.h"
#include "dtkRationalBezierSurfacePointIntersector.h"

/*!
  \class dtkRationalBezierSurfacePointIntersector
  \inmodule dtkContinuousGeometry
  \brief dtkRationalBezierSurfacePointIntersector is a concept describing the intersection between a point and a rational Bezier surface.

  It cannot be instanciated on its own.

  It is to be inherited from the classes acting as plugins. For more information : dtkPluginsContinuousGeometry.

  The following lines of code show how to instanciate and use the class in pratice :
  \code
  dtkRationalBezierSurfacePointIntersector *intersector = dtkContinuousGeometry::rationalBezierSurfacePointIntersector::pluginFactory().create("sislRationalBezierSurfacePointIntersector");
  intersector->setRationalBezierSurface(...);
  intersector->setPoint(...);
  intersector->run();
  const std::vector< IntersectionObject > & intersection_object = intersector->intersection();
  \endcode
  Check dtkPluginsContinuousGeometry for more information on available plugins for dtkRationalBezierSurfacePointIntersector.
*/

/*! \fn dtkRationalBezierSurfacePointIntersector::setRationalBezierSurface(dtkRationalBezierSurface *surface)
  Provides the rational Bezier surface to the intersector (\a surface).

  \a surface : the rational Bezier surface to intersect
*/

/*! \fn dtkRationalBezierSurfacePointIntersector::setPoint(dtkContinuousGeometryPrimitives::Point_3 *point)
  Provides the \a point to intersect the surface with to the intersector.

  \a point : the point to intersect the rational Bezier surface with
*/

/*! \fn dtkRationalBezierSurfacePointIntersector::run(void)
  Intersects the rational Bezier surface (provided with \l setRationalBezierSurface(dtkRationalBezierSurface *surface)) with the point(provided with \l setPoint(Point_3 *point)), the result can be retrieved with \l intersection(void).
*/

/*! \fn dtkRationalBezierSurfacePointIntersector::intersection(void)
  Returns the intersection objects.

  The caller is responsible for deleting all the IntersectionObject of the returned vector, as well as the vector.
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkRationalBezierSurfacePointIntersector, rationalBezierSurfacePointIntersector, dtkContinuousGeometry);
}
