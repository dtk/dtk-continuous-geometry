// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkAbstractNurbsSurfaceData.h"

#include "dtkContinuousGeometry.h"

#include "dtkTrimLoop"
#include "dtkRationalBezierSurface"

/*!
  \class dtkAbstractNurbsSurfaceData
  \inmodule dtkContinuousGeometry
  \brief dtkAbstractNurbsSurfaceData is the data container for Non Uniform Rational Basis Splines surface

  It cannot be instanciated on its own.

  It is to be inherited from the classes acting as plugins. For more information : dtkPluginsContinuousGeometry.

  In pratice, the client code will instanciate \l dtkNurbsSurface. Check \l dtkNurbsSurface for more information.
*/

/*! \fn void dtkAbstractNurbsSurfaceData::create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, std::size_t order_u, std::size_t order_v, const double *knots_u, const double *knots_v, const double *cps) const
  Creates a NURBS surface by providing the required content.

  \a dim : dimension of the space in which lies the surface

  \a nb_cp_u : number of control points along the "u" direction

  \a nb_cp_v : number of control points along the "v" direction

  \a order_u : order(degree + 1) in the "u" direction

  \a order_v : order(degree + 1) in the "v" direction

  \a knots_u : array containing the knots in the "u" direction

  \a knots_v : array containing the knots in the "v" direction

  \a cps : array containing the weighted control points, secified as : [x_00, y_00, z_00, w_00, x_01, y_01, ...]

  \a knots_u, \a knots_v, and \a cps are copied.
*/

/*! \fn void dtkAbstractNurbsSurfaceData::create(std::string path) const
  Creates a NURBS surface by providing a path to a file storing the required information to create a NURBS surface.

  \a path : a valid path to the file containing the information regarding a given NURBS surface
*/

/*! \fn dtkAbstractNurbsSurfaceData::uDegree(void) const
  Returns the degree of in the "u" direction.
*/

/*! \fn dtkAbstractNurbsSurfaceData::vDegree(void) const
  Returns the degree of in the "v" direction.
*/

/*! \fn dtkAbstractNurbsSurfaceData::uNbCps(void) const
  Returns the number of weighted control points in the "u" direction.
*/

/*! \fn dtkAbstractNurbsSurfaceData::vNbCps(void) const
  Returns the number of weighted control points in the "v" direction.
*/

/*! \fn dtkAbstractNurbsSurfaceData::dim(void) const
  Returns the dimension of the space in which the NURBS surface lies.
*/

/*! \fn dtkAbstractNurbsSurfaceData::controlPoint(std::size_t i, std::size_t j, double *r_cp) const
  Writes in \a r_cp the \a i th, \a j th weighted control point coordinates[xij, yij, zij].

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_cp : array of size 3 to store the weighted control point coordinates[xij, yij, zij]
*/

/*! \fn dtkAbstractNurbsSurfaceData::weight(std::size_t i, std::size_t j, double *r_w) const
  Writes in \a r_w the \a i th, \a j th weighted control point weight wij.

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_w : array of size 1 to store the weighted control point weight wij
*/

/*! \fn dtkAbstractNurbsSurfaceData::uKnots(double *r_u_knots) const
  Writes in \a r_u_knots the knots of the surface along the "u" direction.

  \a r_u_knots : array of size (nb_cp_u + order_u - 2) to store the knots
*/

/*! \fn dtkAbstractNurbsSurfaceData::vKnots(double *r_v_knots) const
  Writes in \a r_v_knots the knots of the surface along the "v" direction.

  \a r_v_knots : array of size (nb_cp_v + order_v - 2) to store the knots
*/

/*! \fn dtkAbstractNurbsSurfaceData::uKnots(void) const
  Returns a pointer to the array storing the knots of the surface along the "u" direction. The array is of size : (nb_cp_u + order_u - 2).
*/

/*! \fn dtkAbstractNurbsSurfaceData::uvBounds(double* uv_bounds) const
  Writes in \a uv_bounds the parametric bounds of the surface: the
  minimal and maximal parameters in the "u" direction, and then in the
  "v" direction.

  \a uv_bounds : array of size 4
*/
void dtkAbstractNurbsSurfaceData::uvBounds(double *uv_bounds) const {
    uv_bounds[0] = uKnots()[0];
    uv_bounds[1] = uKnots()[uNbCps()+uDegree()-2];
    uv_bounds[2] = vKnots()[0];
    uv_bounds[3] = vKnots()[vNbCps()+vDegree()-2];

}
/*! \fn dtkAbstractNurbsSurfaceData::vKnots(void) const
  Returns a pointer to the array storing the knots of the surface along the "v" direction. The array is of size : (nb_cp_v + order_v - 2).
*/

/*! \fn dtkAbstractNurbsSurfaceData::isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const
  Returns true if \a point is culled by the trim loops of the surface, else returns false.
*/

/*! \fn dtkAbstractNurbsSurfaceData::evaluatePoint(double p_u, double p_v, double *r_point) const
  Writes in \a r_point the evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation
*/

/*! \fn dtkAbstractNurbsSurfaceData::evaluateNormal(double p_u, double p_v, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/

/*! \fn dtkAbstractNurbsSurfaceData::evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_point the evaluation, in \a r_u_deriv the first derivative in "u" direction, in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/

/*! \fn dtkAbstractNurbsSurfaceData::evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_u_deriv the first derivative in "u" direction and in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/

/*! \fn dtkAbstractNurbsSurfaceData::decomposeToRationalBezierSurfaces(std::vector< std::pair< dtkRationalBezierSurface *, double * > >& r_rational_bezier_surfaces ) const
  Decomposes the NURBS surface into dtkRationalBezierSurface \a r_rational_bezier_surfaces.

  \a r_rational_bezier_surfaces : vector in which the dtkRationalBezierSurface are stored, along with their limits in the NURBS surface parameter space
*/

/*! \fn dtkAbstractNurbsSurfaceData::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
*/

/*! \fn dtkAbstractNurbsSurfaceData::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/

/*! \fn dtkAbstractNurbsSurfaceData::clone(void) const
  Clone
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkAbstractNurbsSurfaceData, abstractNurbsSurfaceData, dtkContinuousGeometry);
}

//
// dtkAbstractNurbsSurfaceData.cpp ends here

// Local Variables:
// c-basic-offset: 4
// End:
