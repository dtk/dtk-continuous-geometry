// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkContinuousGeometry.h"
#include "dtkNurbsSurfaceRayIntersector.h"

/*!
  \class dtkNurbsSurfaceRayIntersector
  \inmodule dtkContinuousGeometry
  \brief dtkNurbsSurfaceRayIntersector is a concept describing the intersection between a ray and a NURBS surface.

  It cannot be instanciated on its own.

  It is to be inherited from the classes acting as plugins. For more information : dtkPluginsContinuousGeometry.

  The following lines of code show how to instanciate and use the class in pratice :
  \code
  dtkNurbsSurfaceRayIntersector *intersector = dtkContinuousGeometry::nurbsSurfaceRayIntersector::pluginFactory().create("sislNurbsSurfaceRayIntersector");
  intersector->setNurbsSurface(...);
  intersector->setRay(...);
  intersector->run();
  const std::vector< IntersectionObject > & intersection_object = intersector->intersection();
  \endcode
  Check dtkPluginsContinuousGeometry for more information on available plugins for dtkNurbsSurfaceRayIntersector.
*/

/*! \fn dtkNurbsSurfaceRayIntersector::setNurbsSurface(dtkNurbsSurface *surface)
  Provides the NURBS surface to the intersector (\a surface).

  \a surface : the NURBS surface to intersect
*/

/*! \fn dtkNurbsSurfaceRayIntersector::setRay(dtkContinuousGeometryPrimitives::Ray_3 *ray)
  Provides the \a ray to intersect the surface with to the intersector.

  \a ray : the ray to intersect the NURBS surface with
*/

/*! \fn dtkNurbsSurfaceRayIntersector::run(void)
  Intersects the NURBS surface (provided with \l setNurbsSurface(dtkNurbsSurface *surface)) with the ray(provided with \l setRay(Ray_3 *ray)), the result can be retrieved with \l intersection(void).
*/

/*! \fn dtkNurbsSurfaceRayIntersector::intersection(void)
  Returns the intersection objects.

  The caller is responsible for deleting all the IntersectionObject of the returned vector, as well as the vector.
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkNurbsSurfaceRayIntersector, nurbsSurfaceRayIntersector, dtkContinuousGeometry);
}
