#if  __GNUC__ >= 6 || __clang_major__ >= 4
#  pragma GCC diagnostic push
#  pragma GCC diagnostic ignored "-Wignored-qualifiers"
#  pragma GCC diagnostic ignored "-Woverloaded-virtual"
#endif
#include <opennurbs.h>
#if __GNUC__ >= 6 || __clang_major__ >= 4
#  pragma GCC diagnostic pop
#endif
