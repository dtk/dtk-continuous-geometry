// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkRationalBezierCurve2D.h"

#include "dtkContinuousGeometry.h"

/*!
  \class dtkRationalBezierCurve2D
  \inmodule dtkContinuousGeometry
  \brief dtkRationalBezierCurve2D is a container of dtkAbstractRationalBezierCurve2DData

  The following line of code shows how to instanciate the class in pratice :
  \code
  dtkRationalBezierCurve2D *nurbs_curve = new dtkRationalBezierCurve2D(dtkContinuousGeometry::abstractRationalBezierCurve2DData::pluginFactory().create("dtkRationalBezierCurve2DDataDefault");
  nurbs_curve->create(...);
  \endcode
  Check dtkPluginsContinuousGeometry for more information on available plugins for dtkAbstractRationalBezierCurve2DData.
 */

/*! \fn dtkRationalBezierCurve2D::dtkRationalBezierCurve2D(void)
  Instanciates a dtkRationalBezierCurve2D without any data. Any call to any method of the class before setting a valid data will fail.
*/

/*! \fn dtkRationalBezierCurve2D::dtkRationalBezierCurve2D(dtkAbstractRationalBezierCurve2DData *data)
   Instanciates a dtkRationalBezierCurve2D with a pointer to a valid dtkAbstractRationalBezierCurve2DData(\a data).
*/

/*! \fn dtkRationalBezierCurve2D::dtkRationalBezierCurve2D(const dtkRationalBezierCurve2D& other)
   Instanciates a dtkRationalBezierCurve2D by copying \a other content.
*/

/*! \fn dtkRationalBezierCurve2D::~dtkRationalBezierCurve2D(void)
   Destructor : will delete the underlying data, either passed in \l dtkRationalBezierCurve2D(dtkAbstractRationalBezierCurve2DData *data), \l setData(dtkAbstractRationalBezierCurve2DData *data) or dtkRationalBezierCurve2D(const dtkRationalBezierCurve2D& other).
*/
dtkRationalBezierCurve2D::~dtkRationalBezierCurve2D(void) {
    if (m_data) {
        delete m_data;
        m_data = NULL;
    }
};

/*! \fn dtkRationalBezierCurve2D &dtkRationalBezierCurve2D::data(void) const
  Returns a reference to the underlying data.
*/

/*! \fn dtkRationalBezierCurve2D *dtkRationalBezierCurve2D::data(void)
  Returns the underlying data pointer.
*/

/*! \fn void dtkRationalBezierCurve2D::setData(dtkAbstractRationalBezierCurve2DData *data)
  Sets the underlying data pointer to point to \a data.
*/

/*! \fn dtkRationalBezierCurve2D::create(std::size_t order, double *cps) const
  Creates the 2D rational Bezier curve by providing the required content.

 \a order : order

 \a cps : array containing the weighted control points, specified as : [x0, y0, w0, ...]
*/
void dtkRationalBezierCurve2D::create(std::size_t order, double* cps) const
{
    m_data->create(order, cps);
}

/*! \fn  dtkRationalBezierCurve2D::degree(void) const
  Returns the degree of the curve.
*/
std::size_t dtkRationalBezierCurve2D::degree(void) const
{
    return m_data->degree();
}

/*! \fn  dtkRationalBezierCurve2D::controlPoint(std::size_t i, double *r_cp) const
  Writes in \a r_cp the \a i th weighted control point coordinates[xi, yi].

  \a i : index of the weighted control point

  \a r_cp : array of size 2 to store the weighted control point coordinates[xi, yi]

  The method can be called as follow:
  \code
  dtkContinuousGeometryPrimitives::Point_2 point2(0, 0);
  rational_bezier_curve_data->controlPoint(3, point2.data());
  \endcode
*/
void dtkRationalBezierCurve2D::controlPoint(std::size_t i, double* r_cp) const
{
    m_data->controlPoint(i, r_cp);
}

/*! \fn  dtkRationalBezierCurve2D::weight(std::size_t i, double *r_w) const
  Writes in \a r_w the \a i th weighted control point weight wi.

  \a i : index of the weighted control point

  \a r_w : array of size 1 to store the weighted control point weight wi
*/
void dtkRationalBezierCurve2D::weight(std::size_t i, double* r_w) const
{
    m_data->weight(i, r_w);
}

/*! \fn dtkRationalBezierCurve2D::evaluatePoint(double p_u, double *r_point) const
  Writes in \a r_point the evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 2 to store the result of the evaluation
*/
void dtkRationalBezierCurve2D::evaluatePoint(double p_u, double* r_point) const
{
    m_data->evaluatePoint(p_u, r_point);
}

/*! \fn dtkRationalBezierCurve2D::evaluateNormal(double p_u, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_normal : array of size 2 to store the result of the normal evaluation
*/
void dtkRationalBezierCurve2D::evaluateNormal(double p_u, double* r_normal) const
{
    m_data->evaluateNormal(p_u, r_normal);
}

/*! \fn dtkRationalBezierCurve2D::split(dtkRationalBezierCurve2D *r_split_curve_a, dtkRationalBezierCurve2D *r_split_curve_b, double p_splitting_parameter) const
  Splits at \a p_splitting_parameter the rational Bezier curve and stores in \a r_split_curve_a and \a r_split_curve_b the split parts (from 0 to \a p_splitting_parameter and from \a p_splitting_parameter to 1 respectively).

  \a r_split_curve_a : a pointer to a valid dtkRationalBezierCurve2D with data not initialized, in which the first part of the curve (from 0 to to \a p_splitting_parameter) will be stored

  \a r_split_curve_b : a pointer to a valid dtkRationalBezierCurve2D with data not initialized, in which the second part of the curve (from \a p_splitting_parameter to 1) will be stored

  \a p_splitting_parameter : the parameter at which the curve will be split
*/
void dtkRationalBezierCurve2D::split(dtkRationalBezierCurve2D *r_split_curve_a, dtkRationalBezierCurve2D *r_split_curve_b, double splitting_parameter) const
{
    m_data->split(r_split_curve_a, r_split_curve_b, splitting_parameter);
}

/*! \fn dtkRationalBezierCurve2D::split(std::list< dtkRationalBezierCurve2D * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const
  Splits the rational Bezier curve at each value of \a p_splitting_parameters and stores in \a r_split_curves the split parts.

  \a r_split_curves : a list to which the split rational Bezier curves will be added

  \a p_splitting_parameters : the parameters at which the curve will be split
*/
void dtkRationalBezierCurve2D::split(std::list< dtkRationalBezierCurve2D * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const
{
    m_data->split(r_split_curves, p_splitting_parameters);
}

/*! \fn dtkRationalBezierCurve2D::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates.

  \a r_aabb : array of size 4 to store the limits coordinates
*/
void dtkRationalBezierCurve2D::aabb(double* r_aabb) const
{
    m_data->aabb(r_aabb);
}

/*! \fn dtkRationalBezierCurve2D::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 4 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkRationalBezierCurve2D::extendedAabb(double* r_aabb, double factor) const
{
    m_data->extendedAabb(r_aabb, factor);
}

//
// dtkRationalBezierCurve2D.cpp ends here
