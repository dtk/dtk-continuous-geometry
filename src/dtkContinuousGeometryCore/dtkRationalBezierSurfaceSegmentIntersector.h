// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

#include <dtkContinuousGeometryUtils.h>

class dtkRationalBezierSurface;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkRationalBezierSurfaceSegmentIntersector
{
 public:
    virtual ~dtkRationalBezierSurfaceSegmentIntersector(void) {};

 public:
    virtual void setRationalBezierSurface(dtkRationalBezierSurface *surface) = 0;
    virtual void setSegment(dtkContinuousGeometryPrimitives::Segment_3 *segment) = 0;

    virtual bool run(void) = 0;
    virtual void reset() = 0;

    virtual const std::vector< dtkContinuousGeometryPrimitives::IntersectionObject > & intersection(void) = 0;
};

// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkRationalBezierSurfaceSegmentIntersector*)
DTK_DECLARE_PLUGIN(dtkRationalBezierSurfaceSegmentIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkRationalBezierSurfaceSegmentIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkRationalBezierSurfaceSegmentIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkRationalBezierSurfaceSegmentIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT, rationalBezierSurfaceSegmentIntersector);
}

//
// dtkRationalBezierSurfaceSegmentIntersector.h ends here
