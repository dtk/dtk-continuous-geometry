// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkNurbsCurve2D.h"

#include "dtkContinuousGeometry.h"

/*!
  \class dtkNurbsCurve2D
  \inmodule dtkContinuousGeometry
  \brief dtkNurbsCurve2D is a container of dtkAbstractNurbsCurve2DData

  The following lines of code show how to instanciate and use the class in pratice:
  \code
  dtkNurbsCurve2D *nurbs_curve = new dtkNurbsCurve2D(dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataDefault"));
  nurbs_curve->create(...);
  \endcode
  Check dtkPluginsContinuousGeometry for more information on available plugins for dtkAbstractNurbsCurve2DData.
 */

/*! \fn dtkNurbsCurve2D::dtkNurbsCurve2D(void)
   Instanciates a dtkNurbsCurve2D without any data. Any call to any method of the class before setting a valid data will fail.
*/

/*! \fn dtkNurbsCurve2D::dtkNurbsCurve2D(dtkAbstractNurbsCurve2DData *data)
  Instanciates a dtkNurbsCurve2D with a pointer to a valid dtkAbstractNurbsCurveData2D(\a data).
*/

/*! \fn dtkNurbsCurve2D::dtkNurbsCurve2D(const dtkNurbsCurve2D& other)
  Instanciates a dtkNurbsCurve2D by copying \a other content.
*/

/*! \fn dtkNurbsCurve2D::~dtkNurbsCurve2D(void)
   Destructor : will delete the underlying data, either passed in \l dtkNurbsCurve2D(dtkAbstractNurbsCurve2DData *data), \l setData(dtkAbstractNurbsCurve2DData *data) or dtkNurbsCurve2D(const dtkNurbsCurve2D& other).
*/
dtkNurbsCurve2D::~dtkNurbsCurve2D(void) {
    if (m_data) {
        delete m_data;
        m_data = NULL;
    }
}

/*! \fn dtkAbstractNurbsCurve2DData &dtkNurbsCurve2D::data(void) const
   Returns a reference to the underlying data.
*/

/*! \fn dtkAbstractNurbsCurve2DData *dtkNurbsCurve2D::data(void)
   Returns the underlying data pointer.
*/

/*! \fn void dtkNurbsCurve2D::setData(dtkAbstractNurbsCurve2DData *data)
  Sets the underlying data pointer to point to \a data.
*/

/*! \fn dtkNurbsCurve2D::create(std::size_t nb_cp, std::size_t order, double *knots, double *cps) const
  Creates the 2D NURBS curve by providing the required content.

 \a nb_cp : number of control points

 \a order : order

 \a knots : array containing the knots

 \a cps : array containing the weighted control points, specified as : [x0, y0, w0, ...]
 */
void dtkNurbsCurve2D::create(std::size_t nb_cp, std::size_t order, double* knots, double* cps) const
{
    m_data->create(nb_cp, order, knots, cps);
}


/*! \fn dtkNurbsCurve2D::degree(void) const
  Returns the degree of the curve.
 */
std::size_t dtkNurbsCurve2D::degree(void) const
{
    return m_data->degree();
}

/*! \fn dtkNurbsCurve2D::nbCps(void) const
  Returns the number of weighted control points controlling the curve.
 */
std::size_t dtkNurbsCurve2D::nbCps(void) const
{
    return m_data->nbCps();
}

/*! \fn dtkNurbsCurve2D::controlPoint(std::size_t i, double *r_cp) const
  Writes in \a r_cp the \a i th weighted control point coordinates[xi, yi].

  \a i : index of the weighted control point

  \a r_cp : array of size 2 to store the weighted control point coordinates[xi, yi]
*/
void dtkNurbsCurve2D::controlPoint(std::size_t i, double* r_cp) const
{
   m_data->controlPoint(i, r_cp);
}

/*! \fn  dtkNurbsCurve2D::weight(std::size_t i, double *r_w) const
  Writes in \a r_w the \a i th weighted control point weight wi.

  \a i : index of the weighted control point

  \a r_w : array of size 1 to store the weighted control point weight wi
 */
void dtkNurbsCurve2D::weight(std::size_t i, double* r_w) const
{
    m_data->weight(i, r_w);
}

/*! \fn dtkNurbsCurve2D::knots(double *r_knots) const
  Writes in \a r_knots the knots of the curve.

  \a r_knots : array of size (nb_cp + order - 2) to store the knots
*/
void dtkNurbsCurve2D::knots(double* r_knots) const
{
    m_data->knots(r_knots);
}

/*! \fn dtkNurbsCurve2D::knots(void) const
  Returns a pointer to the array storing the knots of the curve. The array is of size : (nb_cp + order - 2).
*/
const double *dtkNurbsCurve2D::knots(void) const
{
    return m_data->knots();
}

/*! \fn dtkNurbsCurve2D::evaluatePoint(double p_u, double *r_point) const
  Writes in \a r_point the evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 2 to store the result of the evaluation
 */
void dtkNurbsCurve2D::evaluatePoint(double p_u, double* r_point) const
{
    m_data->evaluatePoint(p_u, r_point);
}

/*! \fn dtkNurbsCurve2D::evaluateNormal(double p_u, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_normal : array of size 2 to store the result of the normal evaluation
 */
void dtkNurbsCurve2D::evaluateNormal(double p_u, double* r_normal) const
{
    m_data->evaluateNormal(p_u, r_normal);
}

/*! \fn dtkNurbsCurve2D::evaluateNormal(double p_u, double *r_point, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 2 to store the result of the evaluation

  \a r_normal : array of size 2 to store the result of the normal evaluation
 */
void dtkNurbsCurve2D::evaluateNormal(double p_u, double* r_point, double* r_normal) const
{
    m_data->evaluateNormal(p_u, r_point, r_normal);
}

/*! \fn dtkNurbsCurve2D::decomposeToRationalBezierCurve2Ds(std::vector< std::pair< dtkRationalBezierCurve2D *, double * > >& r_rational_bezier_curves ) const
  Decomposes the NURBS curve into dtkRationalBezierCurve2D \a r_rational_bezier_curves.

 \a r_rational_bezier_curves : vector in which the dtkRationalBezierCurve2D are stored, along with their limits in the NURBS curve parameter space
 */
void dtkNurbsCurve2D::decomposeToRationalBezierCurve2Ds(std::vector< std::pair< dtkRationalBezierCurve2D*, double*> >& r_rational_bezier_curves) const
{
    m_data->decomposeToRationalBezierCurve2Ds(r_rational_bezier_curves);
}

/*! \fn dtkNurbsCurve2D::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates of the dtkAbstractNurbsCurve2DData.

  \a r_aabb : array of size 4 to store the limits coordinates
 */
void dtkNurbsCurve2D::aabb(double* r_aabb) const
{
    m_data->aabb(r_aabb);
}

/*! \fn dtkNurbsCurve2D::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates of an extened bounding box of factor \a factor of the dtkAbstractNurbsCurve2DData. The extension factor extends the diagonal.

  \a r_aabb : array of size 4 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
 */
void dtkNurbsCurve2D::extendedAabb(double* r_aabb, double factor) const
{
    m_data->extendedAabb(r_aabb, factor);
}

//
// dtkNurbsCurve2D.cpp ends here
