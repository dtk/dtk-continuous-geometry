// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkClippedNurbsSurface.h"

#include "dtkContinuousGeometryUtils.h"

#include "dtkNurbsSurface.h"
#include "dtkRationalBezierSurface.h"
#include "dtkTrimLoop.h"
#include "dtkTrim.h"
#include "dtkTopoTrim"
#include "dtkClippedTrim.h"
#include "dtkClippedTrimLoop.h"

#include "dtkRationalBezierCurve.h"

#include <unordered_set>

/*!
  \class dtkClippedNurbsSurface
  \inmodule dtkContinuousGeometry
  \brief The dtkClippedNurbsSurface class is a clipped representation of a NURBS surface

  The following line of code shows how to instanciate the class in pratice :
  \code
  dtkNurbsSurface *nurbs_surface = new dtkNurbsSurface(dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().create("dtkNurbsSurfaceDataDefault"));
  nurbs_surface->create(...);
  dtkClippedNurbsSurface *clipped_nurbs_surface = new dtkClippedNurbsSurface(*nurbs_surface);
  \endcode
*/

/*! \fn dtkClippedNurbsSurface::dtkClippedNurbsSurface(const dtkNurbsSurface& p_nurbs_surface)
   Instanciates a dtkClippedNurbsSurface by clipping a NURBS surface (\a p_nurbs_surface).
*/
dtkClippedNurbsSurface::dtkClippedNurbsSurface(const dtkNurbsSurface& nurbs_surface) : m_nurbs_surface(nurbs_surface)
{
    m_nurbs_surface.decomposeToRationalBezierSurfaces(m_rational_bezier_surfaces);

    reversed = m_nurbs_surface.hasReversedOrientation();

    // ///////////////////////////////////////////////////////////////////
    // Recovers the lines along which the decomposition was realized
    // ///////////////////////////////////////////////////////////////////
    std::unordered_set< double > us;
    std::unordered_set< double > vs;
    for(auto it = m_rational_bezier_surfaces.begin(); it != m_rational_bezier_surfaces.end(); ++it) {
        double& u0 = it->second[0];
        double& v0 = it->second[1];
        double& u1 = it->second[2];
        double& v1 = it->second[3];
        if (us.find(u0) == us.end()) {
            m_clipping_lines.push_back(dtkContinuousGeometryPrimitives::Line_2(dtkContinuousGeometryPrimitives::Point_2(u0, v0), dtkContinuousGeometryPrimitives::Vector_2(0., v1 - v0)));
            us.insert(u0);
        }
        if(us.find(u1) == us.end()) {
            m_clipping_lines.push_back(dtkContinuousGeometryPrimitives::Line_2(dtkContinuousGeometryPrimitives::Point_2(u1, v0), dtkContinuousGeometryPrimitives::Vector_2(0., v1 - v0)));
            us.insert(u1);
        }
        if(vs.find(v0) == vs.end()) {
            m_clipping_lines.push_back(dtkContinuousGeometryPrimitives::Line_2(dtkContinuousGeometryPrimitives::Point_2(u0, v0), dtkContinuousGeometryPrimitives::Vector_2(u1 - u0, 0.)));
            vs.insert(v0);
        }
        if(vs.find(v1) == vs.end()) {
            m_clipping_lines.push_back(dtkContinuousGeometryPrimitives::Line_2(dtkContinuousGeometryPrimitives::Point_2(u0, v1), dtkContinuousGeometryPrimitives::Vector_2(u1 - u0, 0.)));
            vs.insert(v1);
        }
    }

#if CGAL_DEBUG_CLIPPED_NURBS_SURFACES
    std::cerr << "surface #" << nurbs_surface.surfaceIndex() << "\n  us: [";
    for(auto u : us) {
      std::cerr << ' ' << u;
    }
    std::cerr << " ]\n  vs: [";
    for(auto v : vs) {
      std::cerr << ' ' << v;
    }
    std::cerr << " ]\n";
#endif // CGAL_DEBUG_CLIPPED_NURBS_SURFACES


    // ///////////////////////////////////////////////////////////////////
    // Discretizes the dtkTrim to dtkClippedNurbsCurve2D
    // ///////////////////////////////////////////////////////////////////
    for (auto& trim_loop : m_nurbs_surface.trimLoops()) {
        dtkClippedTrimLoop *clipped_trim_loop = new dtkClippedTrimLoop();
        clipped_trim_loop->setTrimLoopIndex(trim_loop->trimLoopIndex());
#if CGAL_DEBUG_CLIPPED_NURBS_SURFACES
        std::cerr << "trim loop #" << trim_loop->trimLoopIndex() << '\n';
#endif
        clipped_trim_loop->m_type = trim_loop->type();
        m_clipped_trim_loops.push_back(clipped_trim_loop);
        for(auto& trim : trim_loop->trims()) {
            dtkClippedTrim *clipped_trim = new dtkClippedTrim((*trim));
            clipped_trim->setTrimIndex(trim->topoTrim()->trimIndex());
#if CGAL_DEBUG_CLIPPED_NURBS_SURFACES
            std::cerr << "  trim #" << trim->topoTrim()->trimIndex() << '\n';
#endif
            if(!clipped_trim->split(m_clipping_lines)) continue;
            clipped_trim->locate(m_rational_bezier_surfaces);
            clipped_trim->embed();
            if(!clipped_trim->stitch()) continue;
            //clipped_trim->reduce(); // this operation somehow doesn't update the degree of curves, so it is commented out.
            m_clipped_trim_loops.back()->m_clipped_trims.push_back(clipped_trim);
        }
/*
        // check if the 3d mapping of trimming curves is closed
        std::vector<dtkRationalBezierCurve*> rational_bezier_curves;
        for (auto clipped_trim : clipped_trim_loop->m_clipped_trims) {
          for (auto bezier_curve : clipped_trim->m_rational_bezier_curves) {
            rational_bezier_curves.push_back(bezier_curve);
          }
        }
        dtkContinuousGeometryPrimitives::Point_3 p_b(0., 0., 0.);
        dtkContinuousGeometryPrimitives::Point_3 p_e(0., 0., 0.);
        rational_bezier_curves.front()->evaluatePoint(0., p_b.data());
        rational_bezier_curves.front()->evaluatePoint(1., p_e.data());
        std::cout << p_b[0] << " " << p_b[1] << " " << p_b[2] << std::endl;
        std::cout << p_e[0] << " " << p_e[1] << " " << p_e[2] << std::endl;
        for (std::size_t i = 1; i < rational_bezier_curves.size(); ++i) {
          dtkRationalBezierCurve* bezier_curve = rational_bezier_curves[i];
          dtkContinuousGeometryPrimitives::Point_3 p_b_i(0., 0., 0.);
          dtkContinuousGeometryPrimitives::Point_3 p_e_i(0., 0., 0.);
          bezier_curve->evaluatePoint(0., p_b_i.data());
          bezier_curve->evaluatePoint(1., p_e_i.data());
          std::cout << p_b_i[0] << " " << p_b_i[1] << " " << p_b_i[2] << std::endl;
          std::cout << p_e_i[0] << " " << p_e_i[1] << " " << p_e_i[2] << std::endl;
          if (std::sqrt(dtkContinuousGeometryTools::squaredDistance(p_b_i, p_e)) > 1.e-6) {
            dtkFatal() << "3d mapping of trimming bezier curves do not match at ends!";
          }
          p_e = p_e_i;

          if (i == rational_bezier_curves.size() - 1) {
            if (std::sqrt(dtkContinuousGeometryTools::squaredDistance(p_e_i, p_b)) > 1.e-6) {
              dtkFatal() << "3d mapping of trimming curves do not match at ends!";
            }
          }
        }
*/
    }
}

/*! \fn dtkClippedNurbsSurface::~dtkClippedNurbsSurface(void)
   Destructor.
*/
dtkClippedNurbsSurface::~dtkClippedNurbsSurface(void)
{
    for(auto clipped_trim_loop : m_clipped_trim_loops) {
        for(auto clipped_trim : clipped_trim_loop->m_clipped_trims) {
            delete clipped_trim;
        }
        delete clipped_trim_loop;
    }

    for(auto bezier_surface : m_rational_bezier_surfaces) {
      delete bezier_surface.first;
      delete bezier_surface.second;
    }
}

/*! \fn  dtkClippedNurbsSurface::uDegree(void) const
  Returns the degree of the surface in the "u" direction.
 */
std::size_t dtkClippedNurbsSurface::uDegree(void) const
{
    return m_nurbs_surface.uDegree();
}
/*! \fn  dtkClippedNurbsSurface::vDegree(void) const
  Returns the degree of the surface in the "v" direction.
 */
std::size_t dtkClippedNurbsSurface::vDegree(void) const
{
    return m_nurbs_surface.vDegree();
}

/*! \fn dtkClippedNurbsSurface::uNbCps(void) const
  Returns the number of weighted control points in the "u" direction.
*/
std::size_t dtkClippedNurbsSurface::uNbCps(void) const
{
    return m_nurbs_surface.uNbCps();
}

/*! \fn dtkClippedNurbsSurface::vNbCps(void) const
  Returns the number of weighted control points in the "v" direction.
*/
std::size_t dtkClippedNurbsSurface::vNbCps(void) const
{
    return m_nurbs_surface.vNbCps();
}

/*! \fn dtkClippedNurbsSurface::dim(void) const
  Returns the dimension of the space in which the NURBS surface lies.
*/
std::size_t dtkClippedNurbsSurface::dim(void) const
{
    return m_nurbs_surface.dim();
}

/*! \fn dtkClippedNurbsSurface::controlPoint(std::size_t i, std::size_t j, double *r_cp) const
  Writes in \a r_cp the \a i th, \a j th weighted control point coordinates[xij, yij, zij].

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_cp : array of size 3 to store the weighted control point coordinates[xij, yij, zij]
 */
void dtkClippedNurbsSurface::controlPoint(std::size_t i, std::size_t j, double *r_cp) const
{
    m_nurbs_surface.controlPoint(i, j, r_cp);
}

/*! \fn dtkClippedNurbsSurface::weight(std::size_t i, std::size_t j, double *r_w) const
  Writes in \a r_w the \a i th, \a j th weighted control point weight wij.

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_w : array of size 1 to store the weighted control point weight wij
*/
void dtkClippedNurbsSurface::weight(std::size_t i, std::size_t j, double *r_w) const
{
    m_nurbs_surface.weight(i, j, r_w);
}

/*! \fn dtkClippedNurbsSurface::uKnots(double *r_u_knots) const
  Writes in \a r_u_knots the knots of the curve along the "u" direction.

  \a r_u_knots : array of size (nb_cp_u + order_u - 2) to store the knots
*/
void dtkClippedNurbsSurface::uKnots(double *r_u_knots) const
{
    m_nurbs_surface.uKnots(r_u_knots);
}

/*! \fn dtkClippedNurbsSurface::vKnots(double *r_v_knots) const
  Writes in \a r_v_knots the knots of the curve along the "v" direction.

  \a r_v_knots : array of size (nb_cp_v + order_v - 2) to store the knots
*/

void dtkClippedNurbsSurface::vKnots(double *r_v_knots) const
{
    m_nurbs_surface.vKnots(r_v_knots);
}

/*! \fn dtkClippedNurbsSurface::isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const
  Returns true if \a point is culled by the trim loops of the surface, else returns false.
*/
bool dtkClippedNurbsSurface::isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const
{
    return m_nurbs_surface.isPointCulled(point);
}

/*! \fn dtkClippedNurbsSurface::evaluatePoint(double p_u, double p_v, double *r_point) const
  Writes in \a r_point the evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkClippedNurbsSurface::evaluatePoint(double p_u, double p_v, double *r_point) const
{
    m_nurbs_surface.evaluatePoint(p_u, p_v, r_point);
}

/*! \fn dtkClippedNurbsSurface::evaluateNormal(double p_u, double p_v, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkClippedNurbsSurface::evaluateNormal(double p_u, double p_v, double *r_normal) const
{
    m_nurbs_surface.evaluateNormal(p_u, p_v, r_normal);
}

/*! \fn dtkClippedNurbsSurface::evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double *r_v_deriv) const
    Writes in \a r_point the evaluation, in \a r_u_deriv the first derivative in "u" direction, in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkClippedNurbsSurface::evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double *r_v_deriv) const
{
    m_nurbs_surface.evaluate1stDer(p_u, p_v, r_point, r_u_deriv, r_v_deriv);
}

/*! \fn dtkClippedNurbsSurface::evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_u_deriv the first derivative in "u" direction and in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkClippedNurbsSurface::evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const
{
    m_nurbs_surface.evaluate1stDer(p_u, p_v, r_u_deriv, r_v_deriv);
}

/*! \fn dtkClippedNurbsSurface::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
 */
void dtkClippedNurbsSurface::aabb(double *r_aabb) const
{
    m_nurbs_surface.aabb(r_aabb);
}

/*! \fn dtkClippedNurbsSurface::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkClippedNurbsSurface::extendedAabb(double *r_aabb, double factor) const
{
    m_nurbs_surface.extendedAabb(r_aabb, factor);
}

//
// dtkClippedNurbsSurface.cpp ends here
