// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkAbstractNurbsCurveData.h"

#include "dtkContinuousGeometry.h"

/*!
  \class dtkAbstractNurbsCurveData
  \inmodule dtkContinuousGeometry
  \brief dtkAbstractNurbsCurveData is the data container for Non Uniform Rational Basis Splines curve

  It cannot be instanciated on its own.

  It is to be inherited from the classes acting as plugins. For more information : dtkPluginsContinuousGeometry.

  In pratice, the client code will instanciate \l dtkNurbsCurve. Check \l dtkNurbsCurve for more information.
*/

/*! \fn dtkAbstractNurbsCurveData::dtkAbstractNurbsCurveData(void)
  Instanciates the structure of a dtkAbstractNurbsCurveData.

  To add the content, see \l create(std::size_t nb_cp, std::size_t order, double *knots, double *cps).
*/

/*! \fn dtkAbstractNurbsCurveData::create(std::size_t dim, std::size_t nb_cp, std::size_t order, double *knots, double *cps) const
  Creates a dtkAbstractNurbsCurveData by providing the required content for a nD NURBS curve.

  \a dim : dimension of the space in which lies the curve

  \a nb_cp : number of control points

  \a order : order

  \a knots : array containing the knots

  \a cps : array containing the weighted control points, specified as : [x0, y0, z0, w0, ...]
*/

/*! \fn dtkAbstractNurbsCurveData::create(const std::vector< dtkRationalBezierCurve* >& rational_bezier_curves) const
  Creates the NURBS curve by concatenating connected rational Bezier curves.

  \a rational_bezier_curves : the rational Bezier curves to concatenante

  The \a rational_bezier_curves must be given in order.
*/

/*! \fn dtkAbstractNurbsCurveData::degree(void) const
  Returns the degree of the curve.
*/

/*! \fn dtkAbstractNurbsCurveData::nbCps(void) const
  Returns the number of weighted control points controlling the curve.
*/

/*! \fn dtkAbstractNurbsCurveData::dim(void) const
  Returns the dimension of the space in which the curve lies.
*/

/*! \fn dtkAbstractNurbsCurveData::controlPoint(std::size_t i, double *r_cp) const
  Writes in \a r_cp the \a i th weighted control point coordinates[xi, yi, zi].

  \a i : index of the weighted control point

  \a r_cp : array of size 3 to store the weighted control point coordinates[xi, yi, zi]
*/

/*! \fn dtkAbstractNurbsCurveData::weight(std::size_t i, double *r_w) const
  Writes in \a r_w the \a i th weighted control point weight wi.

  \a i : index of the weighted control point

  \a r_w : array of size 1 to store the weighted control point weight wi
*/

/*! \fn dtkAbstractNurbsCurveData::knots(double *r_knots) const
  Writes in \a r_knots the knots of the curve.

  \a r_knots : array of size (nb_cp + order - 2) to store the knots
*/

/*! \fn dtkAbstractNurbsCurveData::knots(void) const
  Returns a pointer to the array storing the knots of the curve. The array is of size : (nb_cp + order - 2).
*/

/*! \fn dtkAbstractNurbsCurveData::evaluatePoint(double p_u, double *r_point) const
  Writes in \a r_point the evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 3 to store the result of the evaluation
*/

/*! \fn dtkAbstractNurbsCurveData::evaluateNormal(double p_u, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/

/*! \fn dtkAbstractNurbsCurveData::evaluateNormal(double p_u, double *r_point, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/

/*! \fn dtkAbstractNurbsCurveData::decomposeToRationalBezierCurves(std::vector< std::pair< dtkRationalBezierCurve*, double* > >& r_rational_bezier_curves) const
  Decomposes the NURBS curve into dtkRationalBezierCurve \a r_rational_bezier_curves.

  \a r_rational_bezier_curves : vector in which the dtkRationalBezierCurve are stored, along with their limits in the NURBS curve parameter space
*/

/*!  \fn dtkAbstractNurbsCurveData::decomposeToRationalBezierCurves(std::vector< dtkRationalBezierCurve * >& r_rational_bezier_curves) const
  Decomposes the NURBS curve into dtkRationalBezierCurve \a r_rational_bezier_curves.

  \a r_rational_bezier_curves : vector in which the dtkRationalBezierCurve are stored

  To recover the limits of the rational Bezier curves in the NURBS parameter space, check : \l decomposeToRationalBezierCurves(std::vector< std::pair< dtkRationalBezierCurve*, double* > >& r_rational_bezier_curves).
*/

/*!  \fn dtkAbstractNurbsCurveData::clone(void) const
  Clone.
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkAbstractNurbsCurveData, abstractNurbsCurveData, dtkContinuousGeometry);
}

//
// dtkAbstractNurbsCurveData.cpp ends here
