// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkBRep.h"

#include "dtkContinuousGeometry.h"

/*!
  \class dtkBRep
  \inmodule dtkContinuousGeometry
  \brief dtkBRep is a container of dtkBRepData

  The following line of code shows how to instanciate the class in pratice :
  \code
  dtkBRepData *data = new dtkBRepData();
  dtkBRep *brep = new dtkBRep(data);
  \endcode
 */

/*! \fn dtkBRep::dtkBRep(void)
   Instanciates a dtkBRep without any data. Any call to any method of the class before setting a valid data will fail.
*/

/*! \fn dtkBRep::dtkBRep(dtkBRepData *data)
   Instanciates a dtkBRep with a pointer to a valid dtkBRepData(\a data).
*/

/*! \fn dtkBRep::dtkBRep(const dtkBRep& other)
   Instanciates a dtkBRep by copying \a other content.
*/

/*! \fn dtkBRep::~dtkBRep(void)
  Destructor : will delete the underlying data, either passed in \l dtkBRep(dtkBRepData *data), \l setData(dtkBRepData *data) or dtkBRep(const dtkBRep& other).
*/
dtkBRep::~dtkBRep(void) {
    if (m_data)
        delete m_data;
    m_data = NULL;
};

/*! \fn dtkBRepData &dtkBRep::data(void) const
  Returns a reference to the underlying data.
*/

/*! \fn dtkBRepData *dtkBRep::data(void)
  Returns the underlying data pointer.
*/

/*! \fn dtkBRep::setData(dtkBRepData *data)
  Sets the underlying data pointer to point to \a data.
*/

/*! \fn dtkBRep::nurbsSurfaces(void) const
  Returns the NURBS surfaces of the BRep.
 */
const std::vector< dtkNurbsSurface* >& dtkBRep::nurbsSurfaces(void) const
{
    Q_ASSERT(m_data != nullptr);
    return m_data->m_nurbs_surfaces;
}

/*! \fn dtkBRep::topoTrims(void) const
  Return the topological trim of the BRep. See \l dtkTopoTrim for more information.
 */
const std::vector< dtkTopoTrim* >& dtkBRep::topoTrims(void) const
{
    Q_ASSERT(m_data != nullptr);
    return m_data->m_topo_trims;
}

/*! \fn dtkBRep::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates of the dtkBRep.

  \a r_aabb : array of size 6 to store the limits coordinates
 */
void dtkBRep::aabb(double* r_aabb) const
{
    Q_ASSERT(m_data != nullptr);
    for (std::size_t i = 0; i < 6; ++i) {
        r_aabb[i] = m_data->m_aabb[i];
    }
}
