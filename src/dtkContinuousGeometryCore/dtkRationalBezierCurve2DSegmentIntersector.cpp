/*
 * dtkRationalBezierCurve2DSegmentIntersector.cpp
 *
 *  Created on: Dec 19, 2019
 *      Author: xxiao
 */

#include "dtkContinuousGeometry.h"
#include "dtkRationalBezierCurve2DSegmentIntersector.h"

namespace dtkContinuousGeometry {
  DTK_DEFINE_CONCEPT(dtkRationalBezierCurve2DSegmentIntersector, rationalBezierCurve2DSegmentIntersector, dtkContinuousGeometry);
}
