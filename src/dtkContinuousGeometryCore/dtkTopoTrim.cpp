#include "dtkTopoTrim.h"
#include "dtkContinuousGeometryUtils"

void dtkTopoTrim::decomposeToRationalBezierCurves()
{
    m_nurbs_curve_3d->decomposeToRationalBezierCurves(m_rational_bezier_curves);

    const int num_bezier_curves = m_rational_bezier_curves.size();

    // pre-process the connection of bezier curves, ensuring end-to-end connection
    for (int i = 0; i < num_bezier_curves; ++i) {
        dtkRationalBezierCurve* bezier_curve = m_rational_bezier_curves[i];
        bezier_curve->set_not_reversed();

        const int degree = bezier_curve->degree();
        dtkContinuousGeometryPrimitives::Point_3 cp0(0., 0., 0.);
        dtkContinuousGeometryPrimitives::Point_3 cpn(0., 0., 0.);
        bezier_curve->controlPoint(0, cp0.data());
        bezier_curve->controlPoint(degree, cpn.data());

        if (i > 0) {
            dtkRationalBezierCurve* bezier_curve_pre = m_rational_bezier_curves[i - 1];
            int degree_pre = bezier_curve_pre->degree();
            dtkContinuousGeometryPrimitives::Point_3 cp0_pre(0., 0., 0.);
            dtkContinuousGeometryPrimitives::Point_3 cpn_pre(0., 0., 0.);
            bezier_curve_pre->controlPoint(0, cp0_pre.data());
            bezier_curve_pre->controlPoint(degree_pre, cpn_pre.data());

            if ( dtkContinuousGeometryTools::squaredDistance(cp0, cpn_pre) < 1.e-6 ) {
                if (bezier_curve_pre->is_reversed() == true) bezier_curve->reverse();
            }
            else {
                if (bezier_curve_pre->is_reversed() == false) bezier_curve->reverse();
            }
        }
    }

    // remove bezier curves degenerated to a point
    std::vector<dtkRationalBezierCurve*> rational_bezier_curves_tmp = m_rational_bezier_curves;
    for (int i = 0; i < num_bezier_curves; ++i) {
        dtkRationalBezierCurve* bezier_curve = rational_bezier_curves_tmp[i];
        dtkContinuousGeometryPrimitives::Point_3 cp0(0., 0., 0.);
        bezier_curve->controlPoint(0, cp0.data());

        int num_duplicated = 0;
        for (std::size_t j = 1; j < bezier_curve->degree() + 1; ++j) {
            dtkContinuousGeometryPrimitives::Point_3 cpj(0., 0., 0.);
            bezier_curve->controlPoint(j, cpj.data());

            double diff = std::sqrt(dtkContinuousGeometryTools::squaredDistance(cp0, cpj));

            if (diff < 1.e-6) num_duplicated += 1;
        }

        if (num_duplicated == bezier_curve->degree()) {
            auto it = std::find(m_rational_bezier_curves.begin(), m_rational_bezier_curves.end(), bezier_curve);
            m_rational_bezier_curves.erase(it);
        }
    }

}