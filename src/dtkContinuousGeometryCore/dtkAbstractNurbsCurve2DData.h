// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

// ///////////////////////////////////////////////////////////////////
// dtkAbstractNurbsCurve2DData
// ///////////////////////////////////////////////////////////////////
class dtkRationalBezierCurve2D;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkAbstractNurbsCurve2DData
{
public:
    dtkAbstractNurbsCurve2DData(void) {;};
    virtual ~dtkAbstractNurbsCurve2DData(void) {;};

 public:
    virtual void create(std::size_t nb_cp, std::size_t order, double *knots, double *cps) const = 0;

 public:
    virtual std::size_t degree(void) const = 0;
    virtual std::size_t nbCps(void) const = 0;

    virtual void controlPoint(std::size_t i, double *r_cp) const = 0;
    virtual void weight(std::size_t i, double *r_w) const = 0;

    virtual void knots(double *r_knots) const = 0;

    virtual const double *knots(void) const = 0;

 public:
    virtual void evaluatePoint(double p_u, double *r_point) const = 0;
    virtual void evaluateNormal(double p_u, double *r_normal) const = 0;
    virtual void evaluateNormal(double p_u, double *r_point, double *r_normal) const = 0;

 public:
    /// @TODO Fix memory leak: replace `double*` with `std::array<double, 2>` in the API
    virtual void decomposeToRationalBezierCurve2Ds(std::vector< std::pair< dtkRationalBezierCurve2D*, double* > >& r_rational_bezier_curves ) const = 0;

 public:
    virtual void aabb(double *r_aabb) const = 0;
    virtual void extendedAabb(double *r_aabb, double factor) const = 0;

public:
    virtual dtkAbstractNurbsCurve2DData*clone(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////
DTK_DECLARE_OBJECT(dtkAbstractNurbsCurve2DData*)
DTK_DECLARE_PLUGIN(dtkAbstractNurbsCurve2DData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractNurbsCurve2DData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractNurbsCurve2DData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkAbstractNurbsCurve2DData, DTKCONTINUOUSGEOMETRYCORE_EXPORT, abstractNurbsCurve2DData);
}

//
// dtkAbstractNurbsCurve2DData.h ends here
