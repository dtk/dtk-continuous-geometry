// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

class dtkNurbsCurve2D;
namespace dtkContinuousGeometryPrimitives{
    class Circle_2;
    class IntersectionObject_2;
}

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkNurbsCurve2DCircleIntersector
{
 public:
    virtual ~dtkNurbsCurve2DCircleIntersector(void) {};

 public:
    virtual void setNurbsCurve2D(dtkNurbsCurve2D *curve) = 0;
    virtual void setCircle(const dtkContinuousGeometryPrimitives::Circle_2& circle) = 0;

    virtual bool run(void) = 0;

    virtual std::vector< double > intersectionParameters(void) = 0;
};

// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkNurbsCurve2DCircleIntersector*)
DTK_DECLARE_PLUGIN(dtkNurbsCurve2DCircleIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkNurbsCurve2DCircleIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkNurbsCurve2DCircleIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkNurbsCurve2DCircleIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT, nurbsCurve2DCircleIntersector);
}

//
// dtkNurbsCurve2DCircleIntersector.h ends here
