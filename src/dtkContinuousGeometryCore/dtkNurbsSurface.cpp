// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkNurbsSurface.h"

#include "dtkContinuousGeometry.h"

#include "dtkRationalBezierSurface.h"
#include "dtkTrimLoop.h"

#include "dtkNurbsCurve2D.h"
#include "dtkTrim.h"

#include <set>

/*!
  \class dtkNurbsSurface
  \inmodule dtkContinuousGeometry
  \brief dtkNurbsSurface is a container of dtkAbstractNurbsSurfaceData

  The following line of code shows how to instanciate the class in pratice :
  \code
  dtkNurbsSurface *nurbs_surface = new dtkNurbsSurface(dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().create("dtkAbstractNurbsSurfaceDataDefault");
  nurbs_surface->create(...);
  \endcode
  Check dtkPluginsContinuousGeometry for more information on available plugins for dtkAbstractNurbsSurfaceData.
 */

/*! \fn dtkNurbsSurface::dtkNurbsSurface(void)
  Instanciates a dtkNurbsCurve without any data. Any call to any method of the class before setting a valid data will fail.
*/

/*! \fn dtkNurbsSurface::dtkNurbsSurface(dtkAbstractNurbsSurfaceData *data)
  Instanciates a dtkNurbsSurface with a pointer to a valid dtkAbstractNurbsSurfaceData(\a data).
*/

/*! \fn dtkNurbsSurface::dtkNurbsSurface(const dtkNurbsSurface& other)
   Instanciates a dtkNurbsSurface by copying \a other content.
*/

/*! \fn dtkNurbsSurface::~dtkNurbsSurface(void)
  Destructor : will delete the underlying data, either passed in \l dtkNurbsSurface(dtkAbstractNurbsSurfaceData *data), \l setData(dtkAbstractNurbsSurfaceData *data) or dtkNurbsSurface(const dtkNurbsSurface& other).
*/
dtkNurbsSurface::~dtkNurbsSurface(void) {
    if (m_data) {
        delete m_data;
        m_data = NULL;
    }
}

/*! \fn dtkNurbsSurface::data(void) const
  Returns a reference to the underlying data.
*/

/*! \fn dtkNurbsSurface::data(void)
  Returns the underlying data pointer.
*/

/*! \fn dtkNurbsSurface::setData(dtkAbstractNurbsSurfaceData *data)
  Sets the underlying data pointer to point to \a data.
*/

/*! \fn void dtkNurbsSurface::create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, std::size_t order_u, std::size_t order_v, const double *knots_u, const double *knots_v, const double *cps) const
  Creates a NURBS surface by providing the required content.

  \a dim : dimension of the space in which lies the surface

  \a nb_cp_u : number of control points along the "u" direction

  \a nb_cp_v : number of control points along the "v" direction

  \a order_u : order(degree + 1) in the "u" direction

  \a order_v : order(degree + 1) in the "v" direction

  \a knots_u : array containing the knots in the "u" direction

  \a knots_v : array containing the knots in the "v" direction

  \a cps : array containing the weighted control points, secified as : [x_00, y_00, z_00, w_00, x_01, y_01, ...]

  \a knots_u, \a knots_v, and \a cps are copied.
*/
void dtkNurbsSurface::create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, std::size_t order_u, std::size_t order_v, const double *knots_u, const double *knots_v, const double *cps) const
{
    m_data->create(dim, nb_cp_u, nb_cp_v, order_u, order_v, knots_u, knots_v, cps);
}

/*! \fn dtkNurbsSurface::uDegree(void) const
  Returns the degree of in the "u" direction.
*/
std::size_t dtkNurbsSurface::uDegree(void) const
{
    return m_data->uDegree();
}

/*! \fn dtkNurbsSurface::vDegree(void) const
  Returns the degree of in the "v" direction.
*/
std::size_t dtkNurbsSurface::vDegree(void) const
{
    return m_data->vDegree();
}

/*! \fn dtkNurbsSurface::uNbCps(void) const
  Returns the number of weighted control points in the "u" direction.
*/
std::size_t dtkNurbsSurface::uNbCps(void) const
{
    return m_data->uNbCps();
}

/*! \fn dtkNurbsSurface::vNbCps(void) const
  Returns the number of weighted control points in the "v" direction.
*/
std::size_t dtkNurbsSurface::vNbCps(void) const
{
    return m_data->vNbCps();
}

std::size_t dtkNurbsSurface::uNbKnots(void) const
{
    return m_data->uNbKnots();
}

std::size_t dtkNurbsSurface::vNbKnots(void) const
{
    return m_data->vNbKnots();
}

/*! \fn dtkNurbsSurface::dim(void) const
  Returns the dimension of the space in which the NURBS surface lies.
*/
std::size_t dtkNurbsSurface::dim(void) const
{
    return m_data->dim();
}

/*! \fn dtkNurbsSurface::controlPoint(std::size_t i, std::size_t j, double *r_cp) const
  Writes in \a r_cp the \a i th, \a j th weighted control point coordinates[xij, yij, zij].

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_cp : array of size 3 to store the weighted control point coordinates[xij, yij, zij]
 */
void dtkNurbsSurface::controlPoint(std::size_t i, std::size_t j, double *r_cp) const
{
    m_data->controlPoint(i, j, r_cp);
}

/*! \fn dtkNurbsSurface::weight(std::size_t i, std::size_t j, double *r_w) const
  Writes in \a r_w the \a i th, \a j th weighted control point weight wij.

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_w : array of size 1 to store the weighted control point weight wij
*/
void dtkNurbsSurface::weight(std::size_t i, std::size_t j, double *r_w) const
{
    m_data->weight(i, j, r_w);
}

/*! \fn dtkNurbsSurface::uKnots(double *r_u_knots) const
  Writes in \a r_u_knots the knots of the surface along the "u" direction.

  \a r_u_knots : array of size (nb_cp_u + order_u - 2) to store the knots
*/
void dtkNurbsSurface::uKnots(double *r_u_knots) const
{
    m_data->uKnots(r_u_knots);
}

/*! \fn dtkNurbsSurface::vKnots(double *r_v_knots) const
  Writes in \a r_v_knots the knots of the surface along the "v" direction.

  \a r_v_knots : array of size (nb_cp_v + order_v - 2) to store the knots
*/
void dtkNurbsSurface::vKnots(double *r_v_knots) const
{
    m_data->vKnots(r_v_knots);
}

/*! \fn dtkNurbsSurface::uKnots(void) const
  Returns a pointer to the array storing the knots of the surface along the "u" direction. The array is of size : (nb_cp_u + order_u - 2).
*/
const double *dtkNurbsSurface::uKnots(void) const
{
    return m_data->uKnots();
}

/*! \fn dtkNurbsSurface::vKnots(void) const
  Returns a pointer to the array storing the knots of the surface along the "v" direction. The array is of size : (nb_cp_v + order_v - 2).
*/
const double *dtkNurbsSurface::vKnots(void) const
{
    return m_data->vKnots();
}

double dtkNurbsSurface::uPeriod(void) const
{
    if (uPeriodic) return m_data->uPeriod();
    else return 0.;
}

double dtkNurbsSurface::vPeriod(void) const
{
    if (vPeriodic) return m_data->vPeriod();
    else return 0.;
}

/*! \fn dtkNurbsSurface::uvBounds(double *uv_bounds) const
  Returns the parametric bounds of the surface
*/
void dtkNurbsSurface::uvBounds(double *uv_bounds) const {
    return m_data->uvBounds(uv_bounds);
}

/*! \fn dtkNurbsSurface::trimLoops(void) const
  Returns the trim loops.
 */
const std::vector< dtkTrimLoop * >& dtkNurbsSurface::trimLoops(void) const
{
    return m_data->m_trim_loops;
}

/*! \fn dtkNurbsSurface::trimLoops(void)
  Returns the trim loops.
 */
std::vector< dtkTrimLoop * >& dtkNurbsSurface::trimLoops(void)
{
    return m_data->m_trim_loops;
}

/*! \fn dtkNurbsSurface::isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const
  Returns true if \a point is culled by the trim loops of the surface, else returns false.
*/
bool dtkNurbsSurface::isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& p_point) const
{
    return m_data->isPointCulled(p_point);
}

/*! \fn dtkNurbsSurface::evaluatePoint(double p_u, double p_v, double *r_point) const
  Writes in \a r_point the evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkNurbsSurface::evaluatePoint(double p_u, double p_v, double* r_point) const
{
    m_data->evaluatePoint(p_u, p_v, r_point);
}

/*! \fn dtkNurbsSurface::evaluateNormal(double p_u, double p_v, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkNurbsSurface::evaluateNormal(double p_u, double p_v, double* r_normal) const
{
    m_data->evaluateNormal(p_u, p_v, r_normal);
    if (reversed) {
      r_normal[0] = -r_normal[0];
      r_normal[1] = -r_normal[1];
      r_normal[2] = -r_normal[2];
    }
}

/*! \fn dtkNurbsSurface::evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double *r_v_deriv) const
    Writes in \a r_point the evaluation, in \a r_u_deriv the first derivative in "u" direction, in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkNurbsSurface::evaluate1stDer(double p_u, double p_v, double* r_point, double* r_u_deriv, double* r_v_deriv) const
{
    m_data->evaluate1stDer(p_u, p_v, r_point, r_u_deriv, r_v_deriv);
    if (reversed) {
      for (int i = 0; i < 3; ++i) {
        r_u_deriv[i] = -r_u_deriv[i];
        r_v_deriv[i] = -r_v_deriv[i];
      }
    }
}

/*! \fn dtkNurbsSurface::evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const
    Writes in \a r_u_deriv the first derivative in "u" direction and in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkNurbsSurface::evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const
{
    m_data->evaluate1stDer(p_u, p_v, r_u_deriv, r_v_deriv);
    if (reversed) {
      for (int i = 0; i < 3; ++i) {
        r_u_deriv[i] = -r_u_deriv[i];
        r_v_deriv[i] = -r_v_deriv[i];
      }
    }
}

/*! \fn dtkNurbsSurface::decomposeToRationalBezierSurfaces(std::vector< std::pair< dtkRationalBezierSurface *, double * > >& r_rational_bezier_surfaces ) const
  Decomposes the NURBS surface into dtkRationalBezierSurface \a r_rational_bezier_surfaces.

 \a r_rational_bezier_surfaces : vector in which the dtkRationalBezierSurface are stored, along with their limits in the NURBS surface parameter space
 */
void dtkNurbsSurface::decomposeToRationalBezierSurfaces(std::vector< std::pair< dtkRationalBezierSurface *, double *> >& p_rational_bezier_surfaces ) const
{
    m_data->decomposeToRationalBezierSurfaces(p_rational_bezier_surfaces);

    //-----------------------------------------------------------------------------------------------
    // if the nurbs surface is periodic
    if (uPeriodic || vPeriodic) {
      const double uPeriod = this->uPeriod();
      const double vPeriod = this->vPeriod();
      // parametric domain of nurbs surface
      std::set<double> us, vs;
      for (auto bezier_surface : p_rational_bezier_surfaces) {
        us.insert(bezier_surface.second[0]);
        vs.insert(bezier_surface.second[1]);
        us.insert(bezier_surface.second[2]);
        vs.insert(bezier_surface.second[3]); 
      }

      // parametric domain of trimming curves
      double us_min = *std::min_element(us.begin(), us.end());
      double us_max = *std::max_element(us.begin(), us.end());
      double vs_min = *std::min_element(vs.begin(), vs.end());
      double vs_max = *std::max_element(vs.begin(), vs.end());
      double u_min = us_min, u_max = us_max;
      double v_min = vs_min, v_max = vs_max;
      for (auto& trim_loop : this->trimLoops()) {
        for (auto& trim : trim_loop->trims()) {
          const dtkNurbsCurve2D& curve_2d = trim->curve2D();
          for (std::size_t i = 0; i < curve_2d.nbCps(); ++i) {
            std::vector<double> cp(2);
            curve_2d.controlPoint(i, cp.data());
            if (cp[0] < u_min) u_min = cp[0];
            else if (cp[0] > u_max) u_max = cp[0];

            if (cp[1] < v_min) v_min = cp[1];
            else if (cp[1] > v_max) v_max = cp[1];
          }
        }
      }

      // lambda function for adding Bezier surfaces
       // periodicity = 0 (u) or 1 (v)
      auto addBezierSurfaces = [](const std::vector< std::pair< dtkRationalBezierSurface *, double * > >& rational_bezier_surfaces,
                                  const std::set<double>& p,
                                  const double p_min,
                                  const double p_max,
                                  const double period,
                                  const int periodicity)
      {
        std::vector< std::pair< dtkRationalBezierSurface *, double * > > rational_bezier_surfaces_new;
        // extend parametric domain
        int nPeriodMax = 0, nPeriodMin = 0; // number of periodicity needed
        std::set<double> p_new;
        double ps_min = *std::min_element(p.begin(), p.end());
        double ps_max = *std::max_element(p.begin(), p.end());
        int k = 1;
        while (p_max > ps_max) {
          nPeriodMax = k;
          for (auto it = std::next(p.begin()); it != p.end(); ++it) {
            p_new.insert(*it + k*period);
          }
          ps_max = *std::max_element(p_new.begin(), p_new.end());
          k += 1;
        }
        k = 1;
        while (p_min < ps_min) {
          nPeriodMin = k;
          for (auto it = p.begin(); it != std::prev(p.end()); ++it) {
            p_new.insert(*it - k*period);
          }
          ps_min = *std::min_element(p_new.begin(), p_new.end());
          k += 1;
        }

        // new surfaces
        for (auto it = rational_bezier_surfaces.begin(); it != rational_bezier_surfaces.end(); ++it) {
          for (int i = 1; i <= nPeriodMax; ++i) {
            double* knots = new double[4];
            if (periodicity == 0) { // periodicity in u
              knots[0] = it->second[0] + i*period; // u0
              knots[1] = it->second[1]; // v0
              knots[2] = it->second[2] + i*period; // u1
              knots[3] = it->second[3]; // v1
            }
            else { // periodicity in v
              knots[0] = it->second[0]; // u0
              knots[1] = it->second[1] + i*period; // v0
              knots[2] = it->second[2]; // u1
              knots[3] = it->second[3] + i*period; // v1
            }
            rational_bezier_surfaces_new.emplace_back(new dtkRationalBezierSurface(*it->first), knots);
          }
          for (int i = 1; i <= nPeriodMin; ++i) {
            double* knots = new double[4];
            if (periodicity == 0) { // periodicity in u
              knots[0] = it->second[0] - i*period; // u0
              knots[1] = it->second[1]; // v0
              knots[2] = it->second[2] - i*period; // u1
              knots[3] = it->second[3]; // v1
            }
            else { // periodicity in v
              knots[0] = it->second[0]; // u0
              knots[1] = it->second[1] - i*period; // v0
              knots[2] = it->second[2]; // u1
              knots[3] = it->second[3] - i*period; // v1
            }
            rational_bezier_surfaces_new.emplace_back(new dtkRationalBezierSurface(*it->first), knots);
          }
        }

        return rational_bezier_surfaces_new;
      };

      if (uPeriodic) {
        //std::cout << "The nurbs surface is periodic in U direction." << std::endl;
        std::vector< std::pair< dtkRationalBezierSurface *, double * > >
          rational_bezier_surfaces_new = addBezierSurfaces(p_rational_bezier_surfaces, us, u_min, u_max, uPeriod, 0);

        if (rational_bezier_surfaces_new.size() > 0) 
          p_rational_bezier_surfaces.insert(p_rational_bezier_surfaces.end(), rational_bezier_surfaces_new.begin(), rational_bezier_surfaces_new.end());
      }
      
      if (vPeriodic) {
        //std::cout << "The nurbs surface is periodic in V direction." << std::endl;
        std::vector< std::pair< dtkRationalBezierSurface *, double * > >
          rational_bezier_surfaces_new = addBezierSurfaces(p_rational_bezier_surfaces, vs, v_min, v_max, vPeriod, 1);
        
        if (rational_bezier_surfaces_new.size() > 0) 
          p_rational_bezier_surfaces.insert(p_rational_bezier_surfaces.end(), rational_bezier_surfaces_new.begin(), rational_bezier_surfaces_new.end());
      }

    }
}

/*! \fn dtkNurbsSurface::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
 */
void dtkNurbsSurface::aabb(double *r_aabb) const
{
    m_data->aabb(r_aabb);
}

/*! \fn dtkNurbsSurface::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkNurbsSurface::extendedAabb(double *r_aabb, double factor) const
{
    m_data->extendedAabb(r_aabb, factor);
}

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkNurbsSurface, nurbsSurface, dtkContinuousGeometry);
}
