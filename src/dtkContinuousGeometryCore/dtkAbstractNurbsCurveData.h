// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

// ///////////////////////////////////////////////////////////////////
// dtkAbstractNurbsCurveData
// ///////////////////////////////////////////////////////////////////
class dtkRationalBezierCurve;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkAbstractNurbsCurveData
{
public:

public:
    dtkAbstractNurbsCurveData(void) {;};
    virtual ~dtkAbstractNurbsCurveData(void) {;};

 public:
    virtual void create(std::size_t dim, std::size_t nb_cp, std::size_t order, double* knots, double* cps) = 0;
    virtual void create(const std::vector< dtkRationalBezierCurve* >& rational_bezier_curves) = 0;

 public:
    virtual std::size_t degree(void) const = 0;

    virtual std::size_t nbCps(void) const = 0;

    virtual std::size_t dim(void) const = 0;

    virtual void controlPoint(std::size_t i, double* r_cp) const = 0;
    virtual void weight(std::size_t i, double* r_w) const = 0;

    virtual void knots(double* r_knots) const = 0;

    virtual const double *knots(void) const = 0;

 public:
    virtual void evaluatePoint(double p_u, double* r_point) const = 0;
    virtual void evaluateNormal(double p_u, double* r_normal) const = 0;
    virtual void evaluateNormal(double p_u, double* r_point, double* r_normal) const = 0;
    virtual void evaluateCurvature(double p_u, double* r_curvature) const = 0;

 public:
    virtual void decomposeToRationalBezierCurves(std::vector< std::pair< dtkRationalBezierCurve *, double * > >& r_rational_bezier_curves) const = 0;
    virtual void decomposeToRationalBezierCurves(std::vector< dtkRationalBezierCurve * >& r_rational_bezier_curves) const = 0;

public:
    virtual dtkAbstractNurbsCurveData* clone(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////
DTK_DECLARE_OBJECT(dtkAbstractNurbsCurveData*)
DTK_DECLARE_PLUGIN(dtkAbstractNurbsCurveData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractNurbsCurveData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractNurbsCurveData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkAbstractNurbsCurveData, DTKCONTINUOUSGEOMETRYCORE_EXPORT, abstractNurbsCurveData);
}

//
// dtkAbstractNurbsCurveData.h ends here
