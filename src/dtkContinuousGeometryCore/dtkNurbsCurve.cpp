// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkNurbsCurve.h"

#include "dtkContinuousGeometry.h"

/*!
  \class dtkNurbsCurve
  \inmodule dtkContinuousGeometry
  \brief dtkNurbsCurve is a container of dtkAbstractNurbsCurveData

  The following line of code shows how to instanciate the class in pratice :
  \code
  dtkNurbsCurve *nurbs_curve = new dtkNurbsCurve(dtkContinuousGeometry::abstractNurbsCurveData::pluginFactory().create("dtkNurbsCurveDataDefault");
  nurbs_curve->create(...);
  \endcode
  Check dtkPluginsContinuousGeometry for more information on available plugins for dtkAbstractNurbsCurveData.
 */

/*! \fn dtkNurbsCurve::dtkNurbsCurve(void)
   Instanciates a dtkNurbsCurve without any data. Any call to any method of the class before setting a valid data will fail.
*/

/*! \fn dtkNurbsCurve::dtkNurbsCurve(dtkAbstractNurbsCurveData *data)
   Instanciates a dtkNurbsCurve with a pointer to a valid dtkAbstractNurbsCurveData(\a data).
*/

/*! \fn dtkNurbsCurve::dtkNurbsCurve(const dtkNurbsCurve& other)
   Instanciates a dtkNurbsCurve by copying \a other content.
*/

/*! \fn dtkNurbsCurve::~dtkNurbsCurve(void)
   Destructor : will delete the underlying data, either passed in \l dtkNurbsCurve(dtkAbstractNurbsCurveData *data), \l setData(dtkAbstractNurbsCurveData *data) or dtkNurbsCurve(const dtkNurbsCurve& other).
*/
dtkNurbsCurve::~dtkNurbsCurve(void) {
    if (m_data) {
        delete m_data;
        m_data = NULL;
    }
}

/*! \fn dtkNurbsCurve::data(void) const
  Returns a reference to the underlying data.
*/

/*! \fn dtkNurbsCurve::data(void)
  Returns the underlying data pointer.
*/

/*! \fn dtkNurbsCurve::setData(dtkAbstractNurbsCurveData *data)
  Sets the underlying data pointer to point to \a data.
*/

/*! \fn dtkNurbsCurve::create(std::size_t dim, std::size_t nb_cp, std::size_t order, double* knots, double* cps) const
  Creates the NURBS curve by providing the required content for a 2D NURBS curve.

 \a dim : dimension of the space in which lies the curve

 \a nb_cp : number of control points

 \a order : order

 \a knots : array containing the knots

 \a cps : array containing the weighted control points, specified as : [x0, y0, z0, w0, ...]
*/
void dtkNurbsCurve::create(std::size_t dim, std::size_t nb_cp, std::size_t order, double* knots, double* cps) const
{
    m_data->create(dim, nb_cp, order, knots, cps);
}

/*! \fn  dtkNurbsCurve::create(const std::vector< dtkRationalBezierCurve* >& rational_bezier_curves) const
  Creates a the NURBS curve by concatenating connected rational Bezier curves.

  \a rational_bezier_curves : the rational Bezier curves to concatenante

  The \a rational_bezier_curves must be given in order.
 */
void dtkNurbsCurve::create(const std::vector< dtkRationalBezierCurve* >& rational_bezier_curves) const
{
    m_data->create(rational_bezier_curves);
}

/*! \fn  dtkNurbsCurve::degree(void) const
  Returns the degree of the curve.
 */
std::size_t dtkNurbsCurve::degree(void) const
{
    return m_data->degree();
}

/*! \fn  dtkNurbsCurve::nbCps(void) const
  Returns the number of weighted control points controlling the curve.
 */
std::size_t dtkNurbsCurve::nbCps(void) const
{
    return m_data->nbCps();
}

/*! \fn  dtkNurbsCurve::dim(void) const
  Returns the dimension of the space in which the curve lies.
*/
std::size_t dtkNurbsCurve::dim(void) const
{
    return m_data->dim();
}

/*! \fn  dtkNurbsCurve::controlPoint(std::size_t i, double *r_cp) const
  Writes in \a r_cp the \a i th weighted control point coordinates[xi, yi, zi].

  \a i : index of the weighted control point

  \a r_cp : array of size 3 to store the weighted control point coordinates[xi, yi, zi]
*/
void dtkNurbsCurve::controlPoint(std::size_t i, double* r_cp) const
{
   m_data->controlPoint(i, r_cp);
}

/*! \fn  dtkNurbsCurve::weight(std::size_t i, double *r_w) const
  Writes in \a r_w the \a i th weighted control point weight wi.

  \a i : index of the weighted control point

  \a r_w : array of size 1 to store the weighted control point weight wi
*/
void dtkNurbsCurve::weight(std::size_t i, double* r_w) const
{
    m_data->weight(i, r_w);
}

/*! \fn dtkNurbsCurve::knots(double *r_knots) const
  Writes in \a r_knots the knots of the curve.

  \a r_knots : array of size (nb_cp + order - 2) to store the knots
*/
void dtkNurbsCurve::knots(double* r_knots) const
{
    m_data->knots(r_knots);
}

/*! \fn dtkNurbsCurve::knots(void) const
  Returns a pointer to the array storing the knots of the curve. The array is of size : (nb_cp + order - 2).
*/
const double *dtkNurbsCurve::knots(void) const
{
    return m_data->knots();
}

/*! \fn dtkNurbsCurve::evaluatePoint(double p_u, double *r_point) const
  Writes in \a r_point the evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkNurbsCurve::evaluatePoint(double p_u, double* r_point) const
{
    m_data->evaluatePoint(p_u, r_point);
}

/*! \fn dtkNurbsCurve::evaluateNormal(double p_u, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkNurbsCurve::evaluateNormal(double p_u, double* r_normal) const
{
    m_data->evaluateNormal(p_u, r_normal);
}

/*! \fn dtkNurbsCurve::evaluateNormal(double p_u, double *r_point, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkNurbsCurve::evaluateNormal(double p_u, double* r_point, double* r_normal) const
{
    m_data->evaluateNormal(p_u, r_point, r_normal);
}

void dtkNurbsCurve::evaluateCurvature(double p_u, double* r_curvature) const
{
    m_data->evaluateCurvature(p_u, r_curvature);
}

/*! \fn dtkNurbsCurve::decomposeToRationalBezierCurves(std::vector< std::pair< dtkRationalBezierCurve*, double* > >& r_rational_bezier_curves) const
  Decomposes the NURBS curve into dtkRationalBezierCurve \a r_rational_bezier_curves.

 \a r_rational_bezier_curves : vector in which the dtkRationalBezierCurve are stored, along with their limits in the NURBS curve parameter space
*/
void dtkNurbsCurve::decomposeToRationalBezierCurves(std::vector< std::pair< dtkRationalBezierCurve*, double* > >& r_rational_bezier_curves ) const
{
    m_data->decomposeToRationalBezierCurves(r_rational_bezier_curves);
}

/*!  \fn dtkNurbsCurve::decomposeToRationalBezierCurves(std::vector< dtkRationalBezierCurve * >& r_rational_bezier_curves) const
  Decomposes the NURBS curve into dtkRationalBezierCurve \a r_rational_bezier_curves.

 \a r_rational_bezier_curves : vector in which the dtkRationalBezierCurve are stored

 To recover the limits of the rational Bezier curves in the NURBS parameter space, check : \l decomposeToRationalBezierCurves(std::vector< std::pair< dtkRationalBezierCurve*, double* > >& r_rational_bezier_curves).
*/
void dtkNurbsCurve::decomposeToRationalBezierCurves(std::vector< dtkRationalBezierCurve* >& r_rational_bezier_curves) const
{
    m_data->decomposeToRationalBezierCurves(r_rational_bezier_curves);
}

//
// dtkNurbsCurve.cpp ends here
