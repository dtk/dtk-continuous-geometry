// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

// ///////////////////////////////////////////////////////////////////
// dtkAbstractRationalBezierCurve2DData
// ///////////////////////////////////////////////////////////////////
class dtkRationalBezierCurve2D;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkAbstractRationalBezierCurve2DData
{
 public:

 public:
    dtkAbstractRationalBezierCurve2DData(void) {;};
    virtual ~dtkAbstractRationalBezierCurve2DData(void) {;};

 public:
    virtual void create(std::size_t order, double *cps) const = 0;

 public:
    virtual std::size_t degree(void) const = 0;
    virtual void controlPoint(std::size_t i, double *r_cp) const = 0;
    virtual void weight(std::size_t i, double *r_w) const = 0;

 public:
    virtual void evaluatePoint(double p_u, double *r_point) const = 0;
    virtual void evaluateNormal(double p_u, double *r_normal) const = 0;

 public:
   virtual void split(dtkRationalBezierCurve2D *r_split_curve_a, dtkRationalBezierCurve2D *r_split_curve_b, double p_splitting_parameter) const = 0;
   virtual void split(std::list< dtkRationalBezierCurve2D * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const = 0;

 public:
    virtual void aabb(double *r_aabb) const = 0;
    virtual void extendedAabb(double *r_aabb, double factor) const = 0;

 public:
    virtual dtkAbstractRationalBezierCurve2DData* clone(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkAbstractRationalBezierCurve2DData*)
DTK_DECLARE_PLUGIN(dtkAbstractRationalBezierCurve2DData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractRationalBezierCurve2DData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractRationalBezierCurve2DData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkAbstractRationalBezierCurve2DData, DTKCONTINUOUSGEOMETRYCORE_EXPORT, abstractRationalBezierCurve2DData);
}

//
// dtkAbstractRationalBezierCurve2DData.h ends here
