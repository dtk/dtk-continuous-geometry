// Version: $Id:
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkContinuousGeometryCoreExport.h>

#include <QtCore>

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkContinuousGeometrySettings : public QSettings
{
public:
     dtkContinuousGeometrySettings(void);
    ~dtkContinuousGeometrySettings(void);
};

//
// dtkContinuousGeometrySettings.h ends here
