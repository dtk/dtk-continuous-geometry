// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

#include <dtkContinuousGeometryUtils>

// /////////////////////////////////////////////////////////////////
// dtkNurbsPolyhedralSurface
// /////////////////////////////////////////////////////////////////
class dtkNurbsSurface;
class dtkMesh;
class dtkClippedNurbsSurface;

namespace dtkContinuousGeometryPrimitives {
    class Point_2;
    class Point_3;
    class Vector_3;
}

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkNurbsPolyhedralSurface
{
private:
  typedef typename std::list< std::pair< dtkContinuousGeometryEnums::TrimLoopType, std::vector< dtkContinuousGeometryPrimitives::Point_2 > > > Trim_polygon_type;

public:
    dtkNurbsPolyhedralSurface(void);

 public:
    dtkNurbsPolyhedralSurface(const dtkNurbsPolyhedralSurface& other);

public:
    virtual ~dtkNurbsPolyhedralSurface(void);

 public:
    virtual void initialize(dtkNurbsSurface *nurbs_surface, double approximation, int surface_number = -1);

 public:
   virtual bool isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const = 0;

 public:
   virtual void evaluatePoint(double p_u, double p_v, double* r_point) const = 0;
   virtual void evaluateNormal(double p_u, double p_v, double* r_normal) const = 0;
   virtual void evaluate1stDer(double p_u, double p_v, double* r_point, double* r_u_deriv, double* r_v_deriv) const = 0;
   virtual void evaluate1stDer(double p_u, double p_v, double* r_u_deriv, double* r_v_deriv) const = 0;

 public:
   const dtkNurbsSurface& nurbsSurface(void) const;
   const dtkMesh& polyhedralSurface(void) const;
   const dtkClippedNurbsSurface* clippedNurbsSurface(void) const;

 public:
   virtual void pointsAndTriangles(std::vector< dtkContinuousGeometryPrimitives::Point_3 >& r_points, std::vector< std::size_t >& r_triangles) const = 0;
   virtual void pointsTrianglesAndNormals(std::vector< dtkContinuousGeometryPrimitives::Point_3 >& r_points, std::vector< std::size_t >& r_triangles, std::vector< dtkContinuousGeometryPrimitives::Vector_3 >& r_normals) const = 0;

 public:
   virtual Trim_polygon_type trim_polygon_2d(void) const = 0;

 public:
   virtual void aabb(double *r_aabb) const = 0;
   virtual void extendedAabb(double *r_aabb, double factor) const = 0;

 public:
   virtual dtkNurbsPolyhedralSurface *clone(void) const = 0;

 protected:
   dtkNurbsSurface *m_nurbs_surface;
   dtkMesh *m_polyhedral_surface;
   dtkClippedNurbsSurface* m_clipped_surface;
};

// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkNurbsPolyhedralSurface*)
DTK_DECLARE_PLUGIN(dtkNurbsPolyhedralSurface, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkNurbsPolyhedralSurface, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkNurbsPolyhedralSurface, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkNurbsPolyhedralSurface, DTKCONTINUOUSGEOMETRYCORE_EXPORT, nurbsPolyhedralSurface);
}

//
// dtkNurbsPolyhedralSurface.h ends here
