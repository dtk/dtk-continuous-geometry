// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>
#include <dtkTrimLoop>

class dtkClippedTrim;
class dtkRationalBezierSurface;
namespace dtkContinuousGeometryPrimitives {
    class Point_2;
}

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkClippedTrimLoop
{
 public:
    dtkClippedTrimLoop(void) {;};
    ~dtkClippedTrimLoop(void) {;};

    bool toPolyline3DApprox(const std::vector< std::pair< dtkRationalBezierSurface *, double * > > p_clipping_rational_bezier_surfaces, std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_3D_approximation);
    void setTrimLoopIndex(int id) { index = id; }
    int trimLoopIndex() const { return index; }
 public:
     std::vector< dtkClippedTrim * > m_clipped_trims;
     dtkContinuousGeometryEnums::TrimLoopType m_type;
     int index = -1;
};

// ///////////////////////////////////////////////////////////////////
DTK_DECLARE_OBJECT(dtkClippedTrimLoop*)
// ///////////////////////////////////////////////////////////////////

//
// dtkClippedTrimLoop.h ends here
