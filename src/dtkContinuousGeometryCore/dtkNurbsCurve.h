// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

#include "dtkAbstractNurbsCurveData.h"

// /////////////////////////////////////////////////////////////////
// dtkNurbsCurve
// /////////////////////////////////////////////////////////////////
class dtkRationalBezierCurve;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkNurbsCurve
{

public:
    explicit dtkNurbsCurve(void) : m_data(nullptr) {;};
    explicit dtkNurbsCurve(dtkAbstractNurbsCurveData *engine) : m_data(engine) {;};
    dtkNurbsCurve(const dtkNurbsCurve& other) :m_data(other.m_data->clone()) {;};

public:
    virtual ~dtkNurbsCurve(void);

 public :
   void create(std::size_t dim, std::size_t nb_cp, std::size_t order, double *knots, double *cps) const;
   void create(const std::vector< dtkRationalBezierCurve * >& rational_bezier_curves) const;

 public:
   std::size_t degree(void) const;

   std::size_t nbCps(void) const;

   std::size_t dim(void) const;

   void controlPoint(std::size_t i, double *r_cp) const;
   void weight(std::size_t i, double *r_w) const;

   void knots(double *r_knots) const;

   const double *knots(void) const;

 public:
   void evaluatePoint(double p_u, double *r_point) const;
   void evaluateNormal(double p_u, double *r_normal) const;
   void evaluateNormal(double p_u, double *r_point, double *r_normal) const;
   void evaluateCurvature(double p_u, double* r_curvature) const;

 public:
   void decomposeToRationalBezierCurves(std::vector< std::pair< dtkRationalBezierCurve*, double* > >& r_rational_bezier_curves ) const;
   void decomposeToRationalBezierCurves(std::vector< dtkRationalBezierCurve* >& r_rational_bezier_curves) const;

public:
    const dtkAbstractNurbsCurveData* data(void) const { return m_data; };
    dtkAbstractNurbsCurveData *data(void)       { return m_data;  };
    void setData(dtkAbstractNurbsCurveData* data) { m_data = data; };

protected:
    dtkAbstractNurbsCurveData *m_data;
};


// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkNurbsCurve*)

//
// dtkNurbsCurve.h ends here
