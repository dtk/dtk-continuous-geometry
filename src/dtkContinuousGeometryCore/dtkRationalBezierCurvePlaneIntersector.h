/*
 * dtkRationalBezierCurvePlaneIntersector.h
 *
 *  Created on: Apr 1, 2020
 *      Author: xxiao
 */

#ifndef SRC_DTKCONTINUOUSGEOMETRYCORE_DTKRATIONALBEZIERCURVEPLANEINTERSECTOR_H_
#define SRC_DTKCONTINUOUSGEOMETRYCORE_DTKRATIONALBEZIERCURVEPLANEINTERSECTOR_H_

#include <QtCore>

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

#include <vector>

class dtkRationalBezierCurve;

namespace dtkContinuousGeometryPrimitives {
  class Plane_3;
}

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkRationalBezierCurvePlaneIntersector
{
public:
  virtual ~dtkRationalBezierCurvePlaneIntersector(void) {};

public:
  virtual void setRationalBezierCurve(const dtkRationalBezierCurve* curve) = 0;
  virtual void setPlane(const dtkContinuousGeometryPrimitives::Plane_3& plane) = 0;

  virtual bool run(void) = 0;
  virtual void reset() = 0;

  virtual std::vector<double> intersectionParameters(void) = 0;
};

DTK_DECLARE_OBJECT(dtkRationalBezierCurvePlaneIntersector*)
DTK_DECLARE_PLUGIN(dtkRationalBezierCurvePlaneIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkRationalBezierCurvePlaneIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkRationalBezierCurvePlaneIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkRationalBezierCurvePlaneIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT, rationalBezierCurvePlaneIntersector);
}

#endif /* SRC_DTKCONTINUOUSGEOMETRYCORE_DTKRATIONALBEZIERCURVEPLANEINTERSECTOR_H_ */
