// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

#include "dtkAbstractTrimLoopData.h"

// /////////////////////////////////////////////////////////////////
// dtkTrimLoop
// /////////////////////////////////////////////////////////////////
class dtkTrim;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkTrimLoop
{
 public:
    explicit dtkTrimLoop(void) : m_data(nullptr) {;};
    explicit dtkTrimLoop(dtkAbstractTrimLoopData* data) : m_data(data) {;};
             dtkTrimLoop(const dtkTrimLoop& other) : m_data(other.m_data->clone()) {;};

public:
     virtual ~dtkTrimLoop(void);

public:
    const dtkAbstractTrimLoopData* data(void) const { return m_data; };
    dtkAbstractTrimLoopData *data(void)       { return m_data;  };
    void setData(dtkAbstractTrimLoopData* data) { m_data = data; };

 public:
    void setType(dtkContinuousGeometryEnums::TrimLoopType p_type) const;

    dtkContinuousGeometryEnums::TrimLoopType type(void) const;

    const std::vector< dtkTrim * >& trims(void) const;

    std::vector< dtkTrim * >& trims(void);

    bool isPointCulled(dtkContinuousGeometryPrimitives::Point_2 p_point) const;

    bool toPolyline(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_discretisation_length) const;
    bool toPolylineApprox(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_approximation) const;

    bool isValid(void) const;

    void setTrimLoopIndex(int id) { index = id; }
    int trimLoopIndex() const { return index; }

 public:
   void aabb(double* r_aabb) const;

 private :
    dtkAbstractTrimLoopData* m_data = nullptr;
    int index = -1;
};

//
// dtkTrimLoop.h ends here
