// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkContinuousGeometry.h"
#include "dtkRationalBezierCurveLossLessDegreeReductor.h"

/*!
  \class dtkRationalBezierCurveLossLessDegreeReductor
  \inmodule dtkContinuousGeometry
  \brief dtkRationalBezierCurveLossLessDegreeReductor is a concept describing the degree reduction of a curve of degree higher than its actual geometrical representation.

  It cannot be instanciated on its own.

  It is to be inherited from the classes acting as plugins. For more information : dtkPluginsContinuousGeometry.

  The following lines of code show how to instanciate and use the class in pratice :
  \code
  dtkRationalBezierCurveLossLessDegreeReductor *reductor = dtkContinuousGeometry::rationalBezierCurveLossLessDegreeReductor::pluginFactory().create("blazeRationalBezierCurveLossLessDegreeReductor");
  reductor->setRationalBezierCurve(...);
  reductor->run();
  dtkRationalBezierCurve *rational_bezier_curve = reductor->rationalBezierCurve();
  \endcode
  Check dtkPluginsContinuousGeometry for more information on available plugins for dtkRationalBezierCurveLossLessDegreeReductor.
*/

/*! \fn dtkRationalBezierCurveLossLessDegreeReductor::setRationalBezierCurve(dtkRationalBezierCurve *curve)
  Provides the rational Bezier curve to the reductor (\a curve).

  \a curve : the rational Bezier curve which degree is to try to reduce
*/

/*! \fn dtkRationalBezierCurveLossLessDegreeReductor::run(void)
  Tries to reduce rational Bezier curve degree provided with \l setRationalBezierCurve(dtkRationalBezierCurve *curve) without changing the geometry.
*/

/*! \fn dtkRationalBezierCurveLossLessDegreeReductor::rationalBezierCurve(void)
  Returns the degree-reduced rational Bezier curve computed in the \l run(void) method.
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkRationalBezierCurveLossLessDegreeReductor, rationalBezierCurveLossLessDegreeReductor, dtkContinuousGeometry);
}
