//
// Version:
//
//

// Commentary:
//
//

// Change log:
//
//

#include "dtkContinuousGeometry.h"

#include "dtkBRepData.h"
#include "dtkNurbsSurface"
#include "dtkNurbsCurve2D.h"
#include "dtkTopoTrim"

/*!
  \class dtkBRepData
  \inmodule dtkContinuousGeometry
  \brief dtkBRepData is the data container of brep model informations


 */

/*!
   Constructor.
*/
dtkBRepData::dtkBRepData() : m_aabb(new double[6]) {}

/*!
   Destructor.
*/
dtkBRepData::~dtkBRepData(void)
{
    // ///////////////////////////////////////////////////////////////////
    // Delete all the nurbs surfaces of the brep model
    // ///////////////////////////////////////////////////////////////////
    for (auto it = m_nurbs_surfaces.begin(); it != m_nurbs_surfaces.end(); ++it) {
        delete (*it);
    }

    // ///////////////////////////////////////////////////////////////////
    // Delete all the nurbs curves 2d of the brep model
    // ///////////////////////////////////////////////////////////////////
    for (dtkNurbsCurve2D* curve_2d : m_nurbs_curves_2d) {
        delete curve_2d;
    }

    // ///////////////////////////////////////////////////////////////////
    // Deletes all the topological trims of the brep model
    // ///////////////////////////////////////////////////////////////////
    for (auto it = m_topo_trims.begin(); it != m_topo_trims.end(); ++it) {
        delete (*it);
    }
    delete[] m_aabb;
}

/*!
   Clone
*/
dtkBRepData* dtkBRepData::clone(void) const
{
    qDebug() << "default implementation of" << Q_FUNC_INFO << "at " << __FILE__ << ":" << __LINE__;
    return nullptr;
}

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkBRepData, bRepData, dtkContinuousGeometry);
}
