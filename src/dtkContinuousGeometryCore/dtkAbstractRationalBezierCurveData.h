// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

class dtkRationalBezierCurve;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkAbstractRationalBezierCurveData
{
 public:
    dtkAbstractRationalBezierCurveData(void) {;};
    virtual ~dtkAbstractRationalBezierCurveData(void) {;};

 public:
    virtual void create(std::size_t order, double* cps) const = 0;
    virtual void create(std::string path) const = 0;

 public :
    virtual std::size_t degree(void) const = 0;
    virtual void controlPoint(std::size_t i, double* r_cp) const = 0;
    virtual void weight(std::size_t i, double* r_w) const = 0;

 public :
    virtual void setWeightedControlPoint(std::size_t i, double *p_cp) = 0;

 public:
   virtual void evaluatePoint(double p_u, double* r_point) const = 0;
   virtual void evaluateNormal(double p_u, double* r_normal) const = 0;
   virtual void evaluateDerivative(double p_u, double* r_derivative) const = 0;
   virtual void evaluateCurvature(double p_u, double* r_curvature) const = 0;

 public:
   virtual void split(dtkRationalBezierCurve *r_split_curve_a, dtkRationalBezierCurve *r_split_curve_b, double splitting_parameter) const = 0;
   virtual void split(std::list< dtkRationalBezierCurve * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const = 0;

 public:
    virtual void aabb(double* r_aabb) const = 0;
    virtual void extendedAabb(double* r_aabb, double factor) const = 0;

 public:
    virtual dtkAbstractRationalBezierCurveData* clone(void) const = 0;

 public:
    virtual void print(std::ostream& stream) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkAbstractRationalBezierCurveData*)
DTK_DECLARE_PLUGIN(dtkAbstractRationalBezierCurveData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractRationalBezierCurveData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractRationalBezierCurveData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkAbstractRationalBezierCurveData, DTKCONTINUOUSGEOMETRYCORE_EXPORT, abstractRationalBezierCurveData);
}

//
// dtkAbstractRationalBezierCurveData.h ends here
