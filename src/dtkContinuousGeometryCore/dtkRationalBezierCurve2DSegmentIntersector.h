/*
 * dtkRationalBezierCurveSegmentIntersector.h
 *
 *  Created on: Dec 19, 2019
 *      Author: xxiao
 */

#ifndef SRC_DTKCONTINUOUSGEOMETRYCORE_DTKRATIONALBEZIERCURVE2DSEGMENTINTERSECTOR_H_
#define SRC_DTKCONTINUOUSGEOMETRYCORE_DTKRATIONALBEZIERCURVE2DSEGMENTINTERSECTOR_H_

#include <QtCore>

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>
#include <dtkContinuousGeometryUtils.h>

#include <vector>

class dtkRationalBezierCurve2D;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkRationalBezierCurve2DSegmentIntersector
{
public:
  virtual ~dtkRationalBezierCurve2DSegmentIntersector(void) {};

public:
  virtual void setRationalBezierCurve2D(dtkRationalBezierCurve2D* bezier_curve) = 0;
  virtual void setSegment(const dtkContinuousGeometryPrimitives::Segment_2& segment) = 0;

  virtual bool run(void) = 0;
  virtual void reset() = 0;

  virtual const std::vector< dtkContinuousGeometryPrimitives::IntersectionObject_2 >& intersection(void) = 0;
};

DTK_DECLARE_OBJECT(dtkRationalBezierCurve2DSegmentIntersector*)
DTK_DECLARE_PLUGIN(dtkRationalBezierCurve2DSegmentIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkRationalBezierCurve2DSegmentIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkRationalBezierCurve2DSegmentIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkRationalBezierCurve2DSegmentIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT, rationalBezierCurve2DSegmentIntersector);
}

#endif /* SRC_DTKCONTINUOUSGEOMETRYCORE_DTKRATIONALBEZIERCURVE2DSEGMENTINTERSECTOR_H_ */
