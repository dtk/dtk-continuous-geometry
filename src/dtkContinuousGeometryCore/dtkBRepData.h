// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkContinuousGeometryCoreExport.h>

#include <vector>

// ///////////////////////////////////////////////////////////////////
// dtkBRepData
// ///////////////////////////////////////////////////////////////////
class dtkNurbsSurface;
class dtkTopoTrim;
class dtkNurbsCurve2D;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkBRepData
{
 public:

 public:
    dtkBRepData(void);
    dtkBRepData(const dtkBRepData&) {;};
    virtual ~dtkBRepData(void);

 public:
    dtkBRepData *clone(void) const;

 public:
    std::vector< dtkNurbsSurface * > m_nurbs_surfaces;
    std::vector< dtkNurbsCurve2D*> m_nurbs_curves_2d;
    std::vector< dtkTopoTrim * > m_topo_trims;

    double *m_aabb;
};

// ///////////////////////////////////////////////////////////////////
DTK_DECLARE_OBJECT(dtkBRepData *)
DTK_DECLARE_PLUGIN(dtkBRepData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkBRepData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkBRepData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkBRepData, DTKCONTINUOUSGEOMETRYCORE_EXPORT, bRepData);
}

//
// dtkBRepData.h ends here
