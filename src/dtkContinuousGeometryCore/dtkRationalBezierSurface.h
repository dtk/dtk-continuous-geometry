// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

#include "dtkAbstractRationalBezierSurfaceData.h"

#include <iostream>
#include <fstream>
// /////////////////////////////////////////////////////////////////
// dtkRationalBezierSurface
// /////////////////////////////////////////////////////////////////

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkRationalBezierSurface
{

public:
    explicit dtkRationalBezierSurface(void) : m_data(nullptr) {;};
    explicit dtkRationalBezierSurface(dtkAbstractRationalBezierSurfaceData *data) : m_data(data) {;};
             dtkRationalBezierSurface(const dtkRationalBezierSurface& other) :m_data(other.m_data->clone()) {;};

public:
    virtual ~dtkRationalBezierSurface(void);

public:
    const dtkAbstractRationalBezierSurfaceData* data(void) const { return m_data; };
    dtkAbstractRationalBezierSurfaceData *data(void)       { return m_data;  };
    void setData(dtkAbstractRationalBezierSurfaceData* data) { m_data = data; };

 public:
    void create(std::size_t dim, std::size_t order_u, std::size_t order_v, double* cps) const;

 public:
   std::size_t uDegree(void) const;
   std::size_t vDegree(void) const;
   void controlPoint(std::size_t i, std::size_t j, double* r_cp) const;
   void weight(std::size_t i, std::size_t j, double* r_w) const;

   bool isDegenerate(void) const;
   bool degenerateLocations(std::list< dtkContinuousGeometryPrimitives::Point_3 >& r_degenerate_locations) const;

 public:
   void evaluatePoint(double p_u, double p_v, double* r_point) const;
   void evaluateNormal(double p_u, double p_v, double* r_normal) const;
   void evaluate1stDer(double p_u, double p_v, double* r_point, double* r_u_deriv, double* r_v_deriv) const;
   void evaluate1stDer(double p_u, double p_v, double* r_u_deriv, double* r_v_deriv) const;

 public:
   void evaluatePointBlossom(const double* p_uis, const double* p_vis, double* r_point, double* r_weight) const;

 public:
   void split(dtkRationalBezierSurface *r_split_surface, double *p_splitting_parameters) const;

 public:
   void aabb(double* r_aabb) const;
   void extendedAabb(double* r_aabb, double factor) const;

protected:
    dtkAbstractRationalBezierSurfaceData *m_data;
};

DTKCONTINUOUSGEOMETRYCORE_EXPORT std::ostream& operator<<(std::ostream& stream, const dtkRationalBezierSurface& rational_bezier_surface);

// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkRationalBezierSurface*)

//
// dtkRationalBezierSurface.h ends here
