// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

#include <dtkContinuousGeometryUtils.h>

class dtkRationalBezierSurface;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkRationalBezierSurfaceRayIntersector
{
 public:
    virtual ~dtkRationalBezierSurfaceRayIntersector(void) {};

 public:
    virtual void setRationalBezierSurface(dtkRationalBezierSurface *surface) = 0;
    virtual void setRay(dtkContinuousGeometryPrimitives::Ray_3 *ray) = 0;

    virtual bool run(void) = 0;
    virtual void reset() = 0;
    virtual const std::vector< dtkContinuousGeometryPrimitives::IntersectionObject > & intersection(void) = 0;
};

// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkRationalBezierSurfaceRayIntersector*)
DTK_DECLARE_PLUGIN(dtkRationalBezierSurfaceRayIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkRationalBezierSurfaceRayIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkRationalBezierSurfaceRayIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkRationalBezierSurfaceRayIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT, rationalBezierSurfaceRayIntersector);
}

//
// dtkRationalBezierSurfaceRayIntersector.h ends here
