// Version: $Id:
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkContinuousGeometrySettings.h"

/*!
 * \brief dtkContinuousGeometrySettings::dtkContinuousGeometrySettings
 *
 * Constructs settings containing a path to plugins
 */
dtkContinuousGeometrySettings::dtkContinuousGeometrySettings(void) : QSettings(QSettings::IniFormat, QSettings::UserScope, "inria", "dtk-continuous-geometry-core")
{
    this->beginGroup("continuous-geometry-core");
    if(!this->allKeys().contains("plugins"))
        this->setValue("plugins", QString());
    this->sync();
    this->endGroup();
}

dtkContinuousGeometrySettings::~dtkContinuousGeometrySettings(void)
{

}

//
// dtkContinuousGeometrySettings.cpp ends here
