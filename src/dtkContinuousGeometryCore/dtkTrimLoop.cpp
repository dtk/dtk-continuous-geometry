// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkTrimLoop.h"

#include "dtkContinuousGeometryUtils"
#include "dtkTrim"

/*!
  \class dtkTrimLoop
  \inmodule dtkContinuousGeometry
  \brief dtkTrimLoop is a container of dtkAbstractTrimLoopData

  The following line of code shows how to instanciate the class in pratice :
  \code
  dtkTrimLoop *trim_loop = new dtkTrimLoop(dtkContinuousGeometry::abstractTrimLoopData::pluginFactory().create("dtkTrimLoopDataCgal");
  \endcode
  Check dtkPluginsContinuousGeometry for more information on available plugins for dtkAbstractTrimLoopData.
*/


/*! \fn dtkTrimLoop::dtkTrimLoop(void)
   Instanciates a dtkTrimLoop without any data. Any call to any method of the class before setting a valid data will fail.
*/

/*! \fn dtkTrimLoop::dtkTrimLoop(dtkAbstractTrimLoopData *data)
  Instanciates a dtkNurbsCurve with a pointer to a valid dtkAbstractNurbsCurveData(\a data).
*/

/*! \fn dtkTrimLoop::dtkTrimLoop(const dtkTrimLoop& other)
   Instanciates a dtkNurbsCurve by copying \a other content.
*/

/*! \fn dtkTrimLoop::~dtkTrimLoop(void)
   Destructor : will delete the underlying data, either passed in \l dtkTrimLoop(dtkAbstractTrimLoopData *data), \l setData(dtkAbstractTrimLoopData *data) or dtkTrimLoop(const dtkTrimLoop& other).
*/
dtkTrimLoop::~dtkTrimLoop(void) {
    if (m_data) {
        delete m_data;
        m_data= NULL;
    }
}

/*! \fn dtkTrimLoop::data(void) const
  Returns a reference to the underlying data.
*/

/*! \fn dtkTrimLoop::data(void)
  Returns the underlying data pointer.
*/

/*! \fn dtkTrimLoop::setData(dtkAbstractTrimLoopData *data)
  Sets the underlying data pointer to point to \a data.
*/


/*! \fn dtkTrimLoop::type(void) const
  Returns the type of the trim loop, it can either be dtkContinuousGeometryEnums::TrimLoopType::outer, dtkContinuousGeometryEnums::TrimLoopType::inner or dtkContinuousGeometryEnums::TrimLoopType::unknown
 */
dtkContinuousGeometryEnums::TrimLoopType dtkTrimLoop::type(void) const
{
    return m_data->type();
}

/*! \fn dtkTrimLoop::setType(dtkContinuousGeometryEnums::TrimLoopType p_type) const
  Sets the type of the trim loop.

  \a p_type : type of the trim loop, it can be dtkContinuousGeometryEnums::TrimLoopType::outer, dtkContinuousGeometryEnums::TrimLoopType::inner or dtkContinuousGeometryEnums::TrimLoopType::unknown
*/
void dtkTrimLoop::setType(dtkContinuousGeometryEnums::TrimLoopType p_type) const
{
    m_data->setType(p_type);
}

/*! \fn dtkTrimLoop::trims(void) const
  Returns the trims forming the trim loop.
 */
const std::vector< dtkTrim * >& dtkTrimLoop::trims(void) const
{
    return m_data->m_trims;
}

/*! \fn dtkTrimLoop::trims(void)
  Returns the trims forming the trim loop.
 */
std::vector< dtkTrim * >& dtkTrimLoop::trims(void)
{
    return m_data->m_trims;
}

/*! \fn dtkTrimLoop::isPointCulled(dtkContinuousGeometryPrimitives::Point_2 p_point) const
  Returns true if \a p_point is culled by the trim loop.

  \a p_point : point to check if culled or not
*/
bool dtkTrimLoop::isPointCulled(dtkContinuousGeometryPrimitives::Point_2 p_point) const
{
    return m_data->isPointCulled(p_point);
}

/*! \fn dtkTrimLoop::toPolyline(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_discretisation_length) const
  Stores in \a r_polyline a polyline representation of the trim loop with regard to the length of discretisation (\a p_discretisation_length), returns true if successfull, else returns false.

  \a r_polyline : list in which the dtkContinuousGeometryPrimitives::Point_2 will be added as polyline representation of the curve
  \a p_discretisation_length : length in parameter space between two samples
 */
bool dtkTrimLoop::toPolyline(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_discretisation_length) const
{
    return m_data->toPolyline(r_polyline, p_discretisation_length);
}

/*! \fn dtkTrimLoop::toPolylineApprox(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_approximation) const
  Stores in \a r_polyline a polyline representation of the trim loop with regard to an approximation distance to the curve(\a p_approximation), returns true if successfull, else returns false.

  \a r_polyline : list in which the dtkContinuousGeometryPrimitives::Point_2 will be added as polyline representation of the curve
  \a p_approximation : maximal tolerated distance from the polygonalization to the trim loop
 */
bool dtkTrimLoop::toPolylineApprox(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_approximation) const
{
    return m_data->toPolylineApprox(r_polyline, p_approximation);
}

/*! \fn dtkTrimLoop::isValid(void) const
  Returns true if the all the trims of the trim loop are connected, else returns false.
 */
bool dtkTrimLoop::isValid(void) const
{
    return m_data->isValid();
}


/*! \fn dtkTrimLoop::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
*/
void dtkTrimLoop::aabb(double* r_aabb) const
{
    m_data->aabb(r_aabb);
}

//
// dtkTrimLoop.cpp ends here
