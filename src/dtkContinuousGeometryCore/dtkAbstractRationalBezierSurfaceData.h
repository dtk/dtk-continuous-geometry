// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

namespace dtkContinuousGeometryPrimitives {
    class Point_3;
}

class dtkRationalBezierSurface;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkAbstractRationalBezierSurfaceData
{
 public:
    dtkAbstractRationalBezierSurfaceData(void) {;};
    virtual ~dtkAbstractRationalBezierSurfaceData(void) {;};

 public:
    virtual void create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, double* cps) const = 0;
    virtual void create(std::string path) const = 0;

 public:
    virtual std::size_t uDegree(void) const = 0;
    virtual std::size_t vDegree(void) const = 0;
    virtual void controlPoint(std::size_t i, std::size_t j, double* r_cp) const = 0;
    virtual void weight(std::size_t i, std::size_t j, double* r_w) const = 0;

    virtual bool isDegenerate(void) const = 0;
    virtual bool degenerateLocations(std::list< dtkContinuousGeometryPrimitives::Point_3 >& r_degenerate_locations) const = 0;

 public:
    virtual void evaluatePoint(double p_u, double p_v, double* r_point) const = 0;
    virtual void evaluateNormal(double p_u, double p_v, double* r_normal) const = 0;
    virtual void evaluate1stDer(double p_u, double p_v, double* r_point, double* r_u_deriv, double* r_v_deriv) const = 0;
    virtual void evaluate1stDer(double p_u, double p_v, double* r_u_deriv, double* r_v_deriv) const = 0;

 public:
    virtual void evaluatePointBlossom(const double* p_uis, const double* p_vis, double *r_point, double *r_weight) const = 0;

 public:
    virtual void split(dtkRationalBezierSurface *r_split_surface, double *p_splitting_parameters) const = 0;

 public:
    virtual void aabb(double* r_aabb) const = 0;
    virtual void extendedAabb(double* r_aabb, double factor) const = 0;

 public:
    virtual dtkAbstractRationalBezierSurfaceData* clone(void) const = 0;

 public:
    virtual void print(std::ostream& stream) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkAbstractRationalBezierSurfaceData*)
DTK_DECLARE_PLUGIN(dtkAbstractRationalBezierSurfaceData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractRationalBezierSurfaceData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractRationalBezierSurfaceData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkAbstractRationalBezierSurfaceData, DTKCONTINUOUSGEOMETRYCORE_EXPORT, abstractRationalBezierSurfaceData);
}

//
// dtkAbstractRationalBezierSurfaceData.h ends here
