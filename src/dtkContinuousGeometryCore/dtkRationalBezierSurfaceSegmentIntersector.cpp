// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkContinuousGeometry.h"
#include "dtkRationalBezierSurfaceSegmentIntersector.h"

/*!
  \class dtkRationalBezierSurfaceSegmentIntersector
  \inmodule dtkContinuousGeometry
  \brief dtkRationalBezierSurfaceSegmentIntersector is a concept describing the intersection between a segment and a rational Bezier surface.

  It cannot be instanciated on its own.

  It is to be inherited from the classes acting as plugins. For more information : dtkPluginsContinuousGeometry.

  The following lines of code show how to instanciate and use the class in pratice :
  \code
  dtkRationalBezierSurfaceSegmentIntersector *intersector = dtkContinuousGeometry::rationalBezierSurfaceSegmentIntersector::pluginFactory().create("sislRationalBezierSurfaceSegmentIntersector");
  intersector->setRationalBezierSurface(...);
  intersector->setSegment(...);
  intersector->run();
  const std::vector< IntersectionObject > & intersection_object = intersector->intersection();
  \endcode
  Check dtkPluginsContinuousGeometry for more information on available plugins for dtkRationalBezierSurfaceSegmentIntersector.
*/

/*! \fn dtkRationalBezierSurfaceSegmentIntersector::setRationalBezierSurface(dtkRationalBezierSurface *surface)
  Provides the rational Bezier surface to the intersector (\a surface).

  \a surface : the rational Bezier surface to intersect
*/

/*! \fn dtkRationalBezierSurfaceSegmentIntersector::setSegment(dtkContinuousGeometryPrimitives::Segment_3 *segment)
  Provides the \a segment to intersect the surface with to the intersector.

  \a segment : the segment to intersect the rational Bezier surface with
*/

/*! \fn dtkRationalBezierSurfaceSegmentIntersector::run(void)
  Intersects the rational Bezier surface (provided with \l setRationalBezierSurface(dtkRationalBezierSurface *surface)) with the segment(provided with \l setSegment(Segment_3 *segment)), the result can be retrieved with \l intersection(void).
*/

/*! \fn dtkRationalBezierSurfaceSegmentIntersector::intersection(void)
  Returns the intersection objects.

  The caller is responsible for deleting all the IntersectionObject of the returned vector, as well as the vector.
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkRationalBezierSurfaceSegmentIntersector, rationalBezierSurfaceSegmentIntersector, dtkContinuousGeometry);
}
