// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

#include <unordered_map>

// /////////////////////////////////////////////////////////////////
// dtkClippedTrim
// /////////////////////////////////////////////////////////////////
class dtkNurbsCurve;
class dtkRationalBezierCurve2D;
class dtkRationalBezierCurve;
class dtkTrim;
class dtkRationalBezierSurface;

namespace dtkContinuousGeometryPrimitives {
    class Line_2;
    class Point_2;
}

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkClippedTrim
{

public:
    dtkClippedTrim(const dtkTrim& p_trim);
    virtual ~dtkClippedTrim(void);

 public:
   bool split(const std::vector< dtkContinuousGeometryPrimitives::Line_2 >& p_clipping_lines);
   bool locate(const std::vector< std::pair< dtkRationalBezierSurface *, double * > >& p_clipping_rational_bezier_surfaces);
   bool embed(void);
   bool reduce(void);
   bool stitch(void);

 public:
   bool toPolyline3DApprox(const std::vector< std::pair< dtkRationalBezierSurface *, double * > > p_clipping_rational_bezier_surfaces, std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_3D_approximation) const;
   void setTrimIndex(int id) { index = id; }
   int trimIndex() const { return index; }
public:
   const dtkTrim& m_trim;

   std::list< dtkRationalBezierCurve2D * > m_rational_bezier_curves_2d;
   std::list< std::pair< dtkRationalBezierCurve2D*, dtkRationalBezierSurface * > > m_located_rational_bezier_curves;
   std::list< std::pair< dtkRationalBezierCurve2D*, dtkRationalBezierSurface * > > m_located_rational_bezier_curves_copy;
   std::vector< dtkRationalBezierCurve * > m_rational_bezier_curves;
   dtkNurbsCurve *m_nurbs_curve;

   std::unordered_map<dtkRationalBezierCurve*, std::pair< dtkRationalBezierCurve2D*, dtkRationalBezierSurface* > > m_map_curve3d_to_curve2d;
   std::list<dtkContinuousGeometryPrimitives::Point_2> m_trim_polygon;
   int index = -1;
};


// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkClippedTrim*)

//
// dtkClippedTrim.h ends here
