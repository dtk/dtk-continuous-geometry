// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkContinuousGeometry.h"
#include "dtkContinuousGeometrySettings.h"

#include <QtCore>


// ///////////////////////////////////////////////////////////////////
// dtkContinuousGeometry factories and managers
// ///////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry
{

#define DTKCONTINUOUSGEOMETRY_XSTR(x) #x
#define DTKCONTINUOUSGEOMETRY_STR(x) DTKCONTINUOUSGEOMETRY_XSTR(x)

DTK_DEFINE_LAYER_MANAGER;

    void initialize(const QString& path)
    {
        QString realpath = path;
        QStringList pathslist;
        if (path.isEmpty()) {
#ifdef DTK_CONTINUOUS_GEOMETRY_PLUGINS_PATH
            QDir dir = QCoreApplication::applicationDirPath();
            if(dir.cdUp() && dir.cd("plugins") && dir.cd("dtkContinuousGeometry")) {
                realpath = dir.absolutePath();
            } else {
                realpath = DTKCONTINUOUSGEOMETRY_STR(DTK_CONTINUOUS_GEOMETRY_PLUGINS_PATH);
            }
            dtkDebug() << "Load dtkContinuousGeometry plugins from: " << realpath ;
#else
            dtkContinuousGeometrySettings continuous_geometry_settings;
            continuous_geometry_settings.beginGroup("continuous-geometry-core");
            realpath = continuous_geometry_settings.value("plugins").toString();
            continuous_geometry_settings.endGroup();
            //if (realpath.isEmpty()) {
            //    realpath = QDir(DTK_INSTALL_PREFIX).filePath("plugins/dtkContinuousGeometry");
            //    dtkDebug() << "no plugin path configured for dtkContinuousGeometry, use default:" << realpath ;
            //}
#endif
            pathslist = realpath.split(":");
        }
        else {
            pathslist = realpath.split(":");
        }
        foreach (const QString &v_path, pathslist) {
            manager().initialize(v_path);
        }
    }
    void setVerboseLoading(bool b)
    {
        manager().setVerboseLoading(b);
    }

    void setAutoLoading(bool auto_load)
    {
        manager().setAutoLoading(auto_load);
    }

    void uninitialize(void)
    {
        manager().uninitialize();
    }
}
