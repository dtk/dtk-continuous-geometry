// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkContinuousGeometry.h"
#include "dtkRationalBezierSurfaceLineIntersector.h"

/*!
  \class dtkRationalBezierSurfaceLineIntersector
  \inmodule dtkContinuousGeometry
  \brief dtkRationalBezierSurfaceLineIntersector is a concept describing the intersection between a line and a rational Bezier surface.

  It cannot be instanciated on its own.

  It is to be inherited from the classes acting as plugins. For more information : dtkPluginsContinuousGeometry.

  The following lines of code show how to instanciate and use the class in pratice :
  \code
  dtkRationalBezierSurfaceLineIntersector *intersector = dtkContinuousGeometry::rationalBezierSurfaceLineIntersector::pluginFactory().create("sislRationalBezierSurfaceLineIntersector");
  intersector->setRationalBezierSurface(...);
  intersector->setLine(...);
  intersector->run();
  const std::vector< IntersectionObject > & intersection_object = intersector->intersection();
  \endcode
  Check dtkPluginsContinuousGeometry for more information on available plugins for dtkRationalBezierSurfaceLineIntersector.
*/

/*! \fn dtkRationalBezierSurfaceLineIntersector::setRationalBezierSurface(dtkRationalBezierSurface *surface)
  Provides the rational Bezier surface to the intersector (\a surface).

  \a surface : the rational Bezier surface to intersect
*/

/*! \fn dtkRationalBezierSurfaceLineIntersector::setLine(dtkContinuousGeometryPrimitives::Line_3 *line)
  Provides the \a line to intersect the surface with to the intersector.

  \a line : the line to intersect the rational Bezier surface with
*/

/*! \fn dtkRationalBezierSurfaceLineIntersector::run(void)
  Intersects the rational Bezier surface (provided with \l setRationalBezierSurface(dtkRationalBezierSurface *surface)) with the line(provided with \l setLine(Line_3 *line)), the result can be retrieved with \l intersection(void).
*/

/*! \fn dtkRationalBezierSurfaceLineIntersector::intersection(void)
  Returns the intersection objects.

  The caller is responsible for deleting all the IntersectionObject of the returned vector, as well as the vector.
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkRationalBezierSurfaceLineIntersector, rationalBezierSurfaceLineIntersector, dtkContinuousGeometry);
}
