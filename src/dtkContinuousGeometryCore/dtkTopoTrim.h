// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include "dtkContinuousGeometryCoreExport.h"

#include "dtkNurbsCurve"
#include "dtkRationalBezierCurve"

// /////////////////////////////////////////////////////////////////
// dtkTopoTrim
// /////////////////////////////////////////////////////////////////

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkTopoTrim
{
 public:
    ~dtkTopoTrim()
        {
            delete m_nurbs_curve_3d;
        };
    void setTrimIndex(int id) { index = id; }
    int trimIndex() const { return index; }

    void decomposeToRationalBezierCurves();

 public:
    dtkNurbsCurve *m_nurbs_curve_3d = nullptr;
    std::vector<dtkRationalBezierCurve*> m_rational_bezier_curves;
    int index = -1;
};

//
// dtkTropoTrim.h ends here
