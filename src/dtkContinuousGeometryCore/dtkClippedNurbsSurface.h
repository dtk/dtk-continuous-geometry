// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

class dtkClippedTrimLoop;
class dtkRationalBezierSurface;
class dtkNurbsSurface;
namespace dtkContinuousGeometryPrimitives {
    class Point_2;
    class Line_2;
}

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkClippedNurbsSurface
{

public:
    dtkClippedNurbsSurface(const dtkNurbsSurface& p_nurbs_surface);
    virtual ~dtkClippedNurbsSurface(void);

 public:
     std::size_t uDegree(void) const;
     std::size_t vDegree(void) const;

     std::size_t uNbCps(void) const;
     std::size_t vNbCps(void) const;

     std::size_t dim(void) const;

     void controlPoint(std::size_t i, std::size_t j, double *r_cp) const;
     void weight(std::size_t i, std::size_t j, double *r_w) const;

     void uKnots(double *r_u_knots) const;
     void vKnots(double *r_v_knots) const;

     bool isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const;

     bool hasReversedOrientation() const { return reversed; }

 public:
     void evaluatePoint(double p_u, double p_v, double *r_point) const;
     void evaluateNormal(double p_u, double p_v, double *r_normal) const;
     void evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double *r_v_deriv) const;
     void evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const;

 public:
     void aabb(double *r_aabb) const;
     void extendedAabb(double *r_aabb, double factor) const;

 public:
     const dtkNurbsSurface& m_nurbs_surface;

    std::vector< std::pair< dtkRationalBezierSurface *, double * > > m_rational_bezier_surfaces;
    std::vector< dtkContinuousGeometryPrimitives::Line_2 > m_clipping_lines;
    std::vector< dtkClippedTrimLoop * > m_clipped_trim_loops;

protected:
    bool reversed = false;

};

// ///////////////////////////////////////////////////////////////////
DTK_DECLARE_OBJECT(dtkClippedNurbsSurface*)
// ///////////////////////////////////////////////////////////////////

//
// dtkClippedNurbsSurface.h ends here
