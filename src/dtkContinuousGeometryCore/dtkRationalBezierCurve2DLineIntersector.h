// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

// /////////////////////////////////////////////////////////////////
// dtkRationalBezierCurve2DLineIntersector
// /////////////////////////////////////////////////////////////////
class dtkRationalBezierCurve2D;
namespace dtkContinuousGeometryPrimitives {
    class Line_2;
}

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkRationalBezierCurve2DLineIntersector
{
public:
    virtual ~dtkRationalBezierCurve2DLineIntersector() {}
    virtual void setRationalBezierCurve2D(dtkRationalBezierCurve2D *bezier_curve) = 0;
    virtual void setLine(const dtkContinuousGeometryPrimitives::Line_2& line) = 0;

    virtual void run(void) = 0;

    virtual std::vector< double > intersectionsParameters(void) = 0;
};

// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkRationalBezierCurve2DLineIntersector*)
DTK_DECLARE_PLUGIN(dtkRationalBezierCurve2DLineIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkRationalBezierCurve2DLineIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkRationalBezierCurve2DLineIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkRationalBezierCurve2DLineIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT, rationalBezierCurve2DLineIntersector);
}

//
// dtkRationalBezierCurve2DLineIntersector.h ends here
