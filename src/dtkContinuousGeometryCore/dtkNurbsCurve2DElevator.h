// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

// /////////////////////////////////////////////////////////////////
// dtkNurbsCurve2DElevator
// /////////////////////////////////////////////////////////////////
class dtkNurbsCurve2D;
class dtkNurbsSurface;
class dtkNurbsCurve;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkNurbsCurve2DElevator
{
public:
    virtual void setInputNurbsCurve2D(dtkNurbsCurve2D *) = 0;
    virtual void setInputNurbsSurface(dtkNurbsSurface *) = 0;

    virtual void run(void) = 0;

    virtual dtkNurbsCurve *nurbsCurve(void) = 0;
};

// ///////////c////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkNurbsCurve2DElevator*)
DTK_DECLARE_PLUGIN(dtkNurbsCurve2DElevator, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkNurbsCurve2DElevator, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkNurbsCurve2DElevator, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkNurbsCurve2DElevator, DTKCONTINUOUSGEOMETRYCORE_EXPORT, nurbsCurve2DElevator);
}

//
// dtkNurbsCurve2DElevator.h ends here
