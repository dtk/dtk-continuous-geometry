#ifndef SRC_DTKCONTINUOUSGEOMETRYCORE_DTKRATIONALBEZIERSURFACECIRCLEINTERSECTOR_H_
#define SRC_DTKCONTINUOUSGEOMETRYCORE_DTKRATIONALBEZIERSURFACECIRCLEINTERSECTOR_H_

#include <QtCore>

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

#include <dtkContinuousGeometryUtils.h>

class dtkRationalBezierSurface;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkRationalBezierSurfaceCircleIntersector
{
 public:
    virtual ~dtkRationalBezierSurfaceCircleIntersector(void) {};

 public:
    virtual void setRationalBezierSurface(dtkRationalBezierSurface* surface) = 0;
    virtual void setCircle(dtkContinuousGeometryPrimitives::Circle_3* circle) = 0;

    virtual bool run(void) = 0;
    virtual void reset() = 0;

    virtual const std::vector< dtkContinuousGeometryPrimitives::IntersectionObject > & intersection(void) = 0;
};

// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkRationalBezierSurfaceCircleIntersector*)
DTK_DECLARE_PLUGIN(dtkRationalBezierSurfaceCircleIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkRationalBezierSurfaceCircleIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkRationalBezierSurfaceCircleIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkRationalBezierSurfaceCircleIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT, rationalBezierSurfaceCircleIntersector);
}

#endif /* SRC_DTKCONTINUOUSGEOMETRYCORE_DTKRATIONALBEZIERSURFACECIRCLEINTERSECTOR_H_ */
