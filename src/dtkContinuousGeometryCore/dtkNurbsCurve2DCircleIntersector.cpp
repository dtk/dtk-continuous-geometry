// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkContinuousGeometry.h"
#include "dtkNurbsCurve2DCircleIntersector.h"

/*!
  \class dtkNurbsCurve2DCircleIntersector
  \inmodule dtkContinuousGeometry
  \brief dtkNurbsCurve2DCircleIntersector is a concept describing the intersection between a circle and a 2D rational Bezier curve.

  It cannot be instanciated on its own.

  It is to be inherited from the classes acting as plugins. For more information : dtkPluginsContinuousGeometry.

  The following lines of code show how to instanciate and use the class in pratice :
  \code
  dtkNurbsCurve2DCircleIntersector *intersector = dtkContinuousGeometry::nurbsCurve2DCircleIntersector::pluginFactory().create("npNurbsCurve2DCircleIntersector");
  intersector->setNurbsCurve2D(...);
  intersector->setCircle(...);
  intersector->run();
  const std::vector< IntersectionObject > & intersection_object = intersector->intersection();
  \endcode
  Check dtkPluginsContinuousGeometry for more information on available plugins for dtkNurbsCurve2DCircleIntersector.
*/

/*! \fn dtkNurbsCurve2DCircleIntersector::setNurbsCurve2D(dtkNurbsCurve2D *curve)
  Provides the 2D NURBS curve to the intersector (\a curve).

  \a curve : the 2D NURBS curve to intersect
*/

/*! \fn dtkNurbsCurve2DCircleIntersector::setCircle(const dtkContinuousGeometryPrimitives::Circle_2& circle)
  Provides the \a circle to intersect the 2D NURBS curve with to the intersector.

  \a circle : the circle to intersect the 2D NURBS curve with
*/

/*! \fn dtkNurbsCurve2DCircleIntersector::run(void)
  Intersects the 2D NURBS curve (provided with \l setNurbsCurve2D(dtkNurbsCurve2D *curve)) with the circle(provided with \l setCircle(const Circle_2& circle)), the result can be retrieved with \l intersection(void).
*/

/*! \fn dtkNurbsCurve2DCircleIntersector::intersectionParameters(void)
  Returns the intersection parameters in the dtkNurbsCurve2D space of paramters.
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkNurbsCurve2DCircleIntersector, nurbsCurve2DCircleIntersector, dtkContinuousGeometry);
}
