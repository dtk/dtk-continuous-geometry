// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

#include "dtkAbstractNurbsSurfaceData.h"

// /////////////////////////////////////////////////////////////////
// dtkNurbsSurface
// /////////////////////////////////////////////////////////////////
class dtkRationalBezierSurface;
class dtkTrimLoop;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkNurbsSurface
{
public:
    explicit dtkNurbsSurface(void) : m_data(nullptr) {;};
    explicit dtkNurbsSurface(dtkAbstractNurbsSurfaceData *data) : m_data(data) {;};
             dtkNurbsSurface(const dtkNurbsSurface& other) : m_data(other.m_data->clone()) {;};

public:
   virtual ~dtkNurbsSurface(void);

public:
    const dtkAbstractNurbsSurfaceData* data(void) const { return m_data; };
    dtkAbstractNurbsSurfaceData *data(void)       { return m_data;  };
    void setData(dtkAbstractNurbsSurfaceData *data) { m_data = data; };
    bool hasReversedOrientation() const { return reversed; }
    void setReversedOrientation(bool r = true) { reversed = r; }
    bool isDegenerated() const { return degenerated; }
    void setDegenerated(bool d = true) { degenerated = d; }
    void setSurfaceIndex(int id) { surface_id = id; }
    int surfaceIndex() const { return surface_id; }
    void setUPeriodic(bool p = true) { uPeriodic = p; }
    bool isUPeriodic() const { return uPeriodic; }
    void setVPeriodic(bool p = true) { vPeriodic = p; }
    bool isVPeriodic() const { return vPeriodic; }

 public:
   void create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, std::size_t order_u, std::size_t order_v, const double *knots_u, const double *knots_v, const double *cps) const;

 public:
   std::size_t uDegree(void) const;
   std::size_t vDegree(void) const;

   std::size_t uNbCps(void) const;
   std::size_t vNbCps(void) const;

   std::size_t uNbKnots(void) const;
   std::size_t vNbKnots(void) const;

   std::size_t dim(void) const;

   void controlPoint(std::size_t i, std::size_t j, double *r_cp) const;
   void weight(std::size_t i, std::size_t j, double *r_w) const;

   void uKnots(double *r_u_knots) const;
   void vKnots(double *r_v_knots) const;

   const double *uKnots(void) const;
   const double *vKnots(void) const;

   double uPeriod(void) const;
   double vPeriod(void) const;

   void uvBounds(double *uv_bounds) const;

   const std::vector< dtkTrimLoop * >& trimLoops(void) const;

   std::vector< dtkTrimLoop * >& trimLoops(void);

   bool isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const;

 public:
   void evaluatePoint(double p_u, double p_v, double* r_point) const;
   void evaluateNormal(double p_u, double p_v, double* r_normal) const;
   void evaluate1stDer(double p_u, double p_v, double* r_point, double* r_u_deriv, double* r_v_deriv) const;
   void evaluate1stDer(double p_u, double p_v, double* r_u_deriv, double* r_v_deriv) const;

 public:
   void decomposeToRationalBezierSurfaces(std::vector< std::pair< dtkRationalBezierSurface*, double*> >& p_rational_bezier_surfaces) const;

 public:
   void aabb(double *r_aabb) const;
   void extendedAabb(double *r_aabb, double factor) const;

protected:
    dtkAbstractNurbsSurfaceData *m_data = nullptr;
    int surface_id = -1;
    bool reversed = false;
    bool degenerated = false;
    bool uPeriodic = false;
    bool vPeriodic = false;
};

// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkNurbsSurface*)
DTK_DECLARE_PLUGIN(dtkNurbsSurface, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkNurbsSurface, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkNurbsSurface, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkNurbsSurface, DTKCONTINUOUSGEOMETRYCORE_EXPORT, nurbsSurface);
}

//
// dtkNurbsSurface.h ends here
