// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

#include <dtkContinuousGeometryUtils.h>

class dtkRationalBezierSurface;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkRationalBezierSurfaceLineIntersector
{
 public:
    virtual ~dtkRationalBezierSurfaceLineIntersector(void) {};

 public:
    virtual void setRationalBezierSurface(dtkRationalBezierSurface *surface) = 0;
    virtual void setLine(dtkContinuousGeometryPrimitives::Line_3 *line) = 0;

    virtual bool run(void) = 0;
    virtual void reset() = 0;

    virtual const std::vector< dtkContinuousGeometryPrimitives::IntersectionObject > & intersection(void) = 0;
};

// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkRationalBezierSurfaceLineIntersector*)
DTK_DECLARE_PLUGIN(dtkRationalBezierSurfaceLineIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkRationalBezierSurfaceLineIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkRationalBezierSurfaceLineIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkRationalBezierSurfaceLineIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT, rationalBezierSurfaceLineIntersector);
}

//
// dtkRationalBezierSurfaceLineIntersector.h ends here
