// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

class dtkTopoTrim;
class dtkNurbsSurface;
class dtkNurbsCurve2D;
namespace dtkContinuousGeometryPrimitives {
    class Point_2;
}

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkTrim
{

 public:
    dtkTrim(const dtkNurbsSurface& nurbs_surface, const dtkNurbsCurve2D& nurbs_curve_2d, const dtkTopoTrim* topo_trim, bool is_seam = false);

 public:
    const dtkNurbsCurve2D& curve2D(void) const;
    const dtkNurbsSurface& nurbsSurface(void) const;
    const dtkTopoTrim* topoTrim(void) const;

 public:
    void setIsSeam(bool is_seam) const;
    bool isSeam(void) const;

 public:
    void toPolyline(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_discretisation_length) const;
    void toPolyline(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_discretisation_length, double p_0, double p_1) const;
    void toPolylineApprox(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_approximation) const;
    void toPolylineApprox(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_discretisation_length, double p_0, double p_1) const;

 private:
    const dtkNurbsCurve2D& m_nurbs_curve_2d;
    const dtkNurbsSurface&  m_nurbs_surface;
    const dtkTopoTrim* m_topo_trim;
    mutable bool m_is_seam;
};

//
// dtkTrim.h ends here
