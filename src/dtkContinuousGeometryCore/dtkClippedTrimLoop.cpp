//
// Version:
//
//

// Commentary:
//
//

// Change log:
//
//

#include "dtkClippedTrimLoop"
#include "dtkRationalBezierSurface"
#include "dtkRationalBezierCurve2D"
#include "dtkClippedTrim"
#include "dtkContinuousGeometryUtils"
#include <fstream>

bool dtkClippedTrimLoop::toPolyline3DApprox(const std::vector< std::pair< dtkRationalBezierSurface *, double * > > p_clipping_rational_bezier_surfaces, std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_3D_approximation)
{
    bool is_trim_loop_ok = true;
    if (m_clipped_trims.empty()) { dtkWarn() << "The trim loop is empty, it cannot be converted to polyline";
        return false;
    }
    // ///////////////////////////////////////////////////////////////////
    // Check that the last control point of the last trim matches the first control point of the first trim
    // ///////////////////////////////////////////////////////////////////
    // dtkContinuousGeometryPrimitives::Point_2 first_point(0., 0.);
    // m_clipped_trims.front()->curve2D().controlPoint(0, first_point.data());
    // dtkContinuousGeometryPrimitives::Point_2 last_point(0., 0.);
    // m_clipped_trims.back()->curve2D().controlPoint(m_trims.back()->curve2D().nbCps() - 1, last_point.data());
    // double distance = dtkContinuousGeometryTools::squaredDistance(first_point, last_point);

    // ///////////////////////////////////////////////////////////////////
    // Stores the first trim polyline in r_polyline
    // ///////////////////////////////////////////////////////////////////
    m_clipped_trims.front()->toPolyline3DApprox(p_clipping_rational_bezier_surfaces, r_polyline, p_3D_approximation);
    m_clipped_trims.front()->m_trim_polygon = r_polyline;
    if (r_polyline.empty()) { dtkWarn() << "One of the trims is empty, it cannot be converted to polyline";
        return false;
    }
    if (m_clipped_trims.size() > 1) {
        std::list< dtkContinuousGeometryPrimitives::Point_2 > temp;
        for (auto it = std::next(m_clipped_trims.begin()); it !=m_clipped_trims.end(); ++it) {
            (*it)->toPolyline3DApprox(p_clipping_rational_bezier_surfaces, temp, p_3D_approximation);
            if (!temp.empty()) {
                (*it)->m_trim_polygon = temp;
                // ///////////////////////////////////////////////////////////////////
                // Clustering
                // ///////////////////////////////////////////////////////////////////
                double distance = dtkContinuousGeometryTools::squaredDistance(*temp.begin(), *std::prev(r_polyline.end()));
                if(distance > 1e-2) {
                    dtkWarn() << "End point : " << (*std::prev(r_polyline.end()))[0] << " " << (*std::prev(r_polyline.end()))[1];
                    dtkWarn() << "Start point : " << (*temp.begin())[0] << " " << (*temp.begin())[1];
                    dtkWarn() << "Squared distance : " << distance;
                    dtkWarn() << "The start point and end point of two consecutive trims do not match.";
                    is_trim_loop_ok = false;
                }
                temp.erase(temp.begin());
                r_polyline.splice(r_polyline.end(), temp);
            } else {
                dtkWarn() << "The trim loop is empty, it cannot be converted to polyline";
                is_trim_loop_ok = false;
            }
        }
    }
    double distance = dtkContinuousGeometryTools::squaredDistance(*r_polyline.begin(), *std::prev(r_polyline.end()));
    if ( distance > 1e-2) {
        dtkWarn() << qSetRealNumberPrecision(17) << "End point : " << (*r_polyline.begin())[0] << " " << (*r_polyline.begin())[1];
        dtkWarn() << qSetRealNumberPrecision(17) << "Start point : " <<  (*std::prev(r_polyline.end()))[0] << " " <<  (*std::prev(r_polyline.end()))[1];
        dtkWarn() << qSetRealNumberPrecision(17) << "Squared distance : " << distance;
        dtkWarn() << qSetRealNumberPrecision(17) << "The start point and end point of the first and last trims do not match, it does not form a loop";
        is_trim_loop_ok = false;
    }
    if(!is_trim_loop_ok) {
        std::ofstream trim_loop("trim_loop_2d.polylines.txt");
        trim_loop.precision(17);

        dtkContinuousGeometryPrimitives::Point_2 p(0., 0.);
        std::size_t cntr_pts = 0;
        for(auto c_trim : m_clipped_trims) {
            for(auto curve : c_trim->m_rational_bezier_curves_2d) {
                (void)curve;
                for(double i = 0; i < 1.; i += 0.01) {
                    ++cntr_pts;
                }
            }
        }
        trim_loop << cntr_pts << " ";
        for(auto c_trim : m_clipped_trims) {
            for(auto curve : c_trim->m_rational_bezier_curves_2d) {
                for(double i = 0; i < 1.; i += 0.01) {
                    curve->evaluatePoint(i, p.data());
                    trim_loop << p << " 0 ";
                }
            }
        }
        trim_loop.close();
        std::ofstream polyline("trim_loop_polyline_2d.polylines.txt");
        polyline.precision(17);
        dtkInfo() << "Polyline size : " << r_polyline.size();
        polyline << r_polyline.size() << " ";
        for(auto point : r_polyline) {
            polyline << point << " " << "0 ";
        }
        polyline.close();
    }

    return is_trim_loop_ok;
}
