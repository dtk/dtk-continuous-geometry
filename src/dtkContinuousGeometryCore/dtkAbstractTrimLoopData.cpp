// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkAbstractTrimLoopData.h"

#include "dtkContinuousGeometry.h"

/*!
  \class dtkAbstractTrimLoopData
  \inmodule dtkContinuousGeometry
  \brief dtkAbstractTrimLoopData is the data container for trim loop

  It cannot be instanciated on its own.

  It is to be inherited from the classes acting as plugins. For more information : dtkPluginsContinuousGeometry.

  In pratice, the client code will instanciate \l dtkTrimLoop. Check \l dtkTrimLoop for more information.
*/

/*! \fn dtkAbstractTrimLoopData::type(void) const
  Returns the type of the trim loop, it can either be dtkContinuousGeometryEnums::TrimLoopType::outer, dtkContinuousGeometryEnums::TrimLoopType::inner or dtkContinuousGeometryEnums::TrimLoopType::unknown.
*/

/*! \fn dtkAbstractTrimLoopData::setType(dtkContinuousGeometryEnums::TrimLoopType p_type) const
  Sets the type of the trim loop.

  \a p_type : type of the trim loop, it can be dtkContinuousGeometryEnums::TrimLoopType::outer, dtkContinuousGeometryEnums::TrimLoopType::inner or dtkContinuousGeometryEnums::TrimLoopType::unknown
*/

/*! \fn dtkAbstractTrimLoopData::isPointCulled(dtkContinuousGeometryPrimitives::Point_2 p_point) const
  Returns true if \a p_point is culled by the trim loop.

  \a p_point : point to check if culled or not
*/

/*! \fn dtkAbstractTrimLoopData::toPolyline(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_discretisation_length) const
  Stores in \a r_polyline a polyline representation of the trim loop with regard to the length of discretisation (\a p_discretisation_length), returns true if successfull, else returns false.

  \a r_polyline : list in which the dtkContinuousGeometryPrimitives::Point_2 will be added as polyline representation of the curve
  \a p_discretisation_length : length in parameter space between two samples
*/

/*! \fn dtkAbstractTrimLoopData::toPolylineApprox(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_approximation) const
  Stores in \a r_polyline a polyline representation of the trim loop with regard to an approximation distance to the curve(\a p_approximation), returns true if successfull, else returns false.

  \a r_polyline : list in which the dtkContinuousGeometryPrimitives::Point_2 will be added as polyline representation of the curve
  \a p_approximation : maximal tolerated distance from the polygonalization to the trim loop
*/

/*! \fn dtkAbstractTrimLoopData::isValid(void) const
  Returns true if the all the trims of the trim loop are connected, else returns false.
*/

/*! \fn dtkAbstractTrimLoopData::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
*/

/*! \fn dtkAbstractTrimLoopData::clone(void) const
  Clone
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkAbstractTrimLoopData, abstractTrimLoopData, dtkContinuousGeometry);
}

//
// dtkAbstractTrimLoopData.cpp ends here
