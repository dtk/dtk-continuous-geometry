// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkContinuousGeometryCoreExport.h>

#include <QRunnable>
#include <QString>

#include <dtkCore>

class dtkBRep;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkBRepReader : public QRunnable
{
public:
    virtual void setInputBRepFilePath(const QString&) = 0;
    virtual void setInputDumpFilePath(const QString&) = 0;

    virtual void run(void) = 0;

    virtual dtkBRep* outputDtkBRep(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkBRepReader*);
DTK_DECLARE_PLUGIN(dtkBRepReader, DTKCONTINUOUSGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_FACTORY(dtkBRepReader, DTKCONTINUOUSGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_MANAGER(dtkBRepReader, DTKCONTINUOUSGEOMETRYCORE_EXPORT);

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkBRepReader, DTKCONTINUOUSGEOMETRYCORE_EXPORT, bRepReader);
}

//
// dtkBRepReader.h ends here
