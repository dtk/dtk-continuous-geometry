// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

class dtkRationalBezierCurve;
namespace dtkContinuousGeometryPrimitives{
    class Sphere_3;
}

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkRationalBezierCurveSphereIntersector
{
 public:
    virtual ~dtkRationalBezierCurveSphereIntersector(void) {};

 public:
    virtual void setRationalBezierCurve(const dtkRationalBezierCurve *curve) = 0;
    virtual void setSphere(const dtkContinuousGeometryPrimitives::Sphere_3& sphere) = 0;

    virtual bool run(void) = 0;
    virtual void reset() = 0;

    virtual std::vector< double > intersectionParameters(void) = 0;
};

// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkRationalBezierCurveSphereIntersector*)
DTK_DECLARE_PLUGIN(dtkRationalBezierCurveSphereIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkRationalBezierCurveSphereIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkRationalBezierCurveSphereIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkRationalBezierCurveSphereIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT, rationalBezierCurveSphereIntersector);
}

//
// dtkRationalBezierCurveSphereIntersector.h ends here
