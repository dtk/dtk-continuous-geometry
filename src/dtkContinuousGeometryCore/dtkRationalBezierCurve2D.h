// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

#include "dtkAbstractRationalBezierCurve2DData.h"

// /////////////////////////////////////////////////////////////////
// dtkRationalBezierCurve2D
// /////////////////////////////////////////////////////////////////

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkRationalBezierCurve2D
{
 public:
    explicit dtkRationalBezierCurve2D(void) : m_data(nullptr) {;};
    explicit dtkRationalBezierCurve2D(dtkAbstractRationalBezierCurve2DData *data) : m_data(data) {;};
    dtkRationalBezierCurve2D(const dtkRationalBezierCurve2D& other) : m_data(other.m_data->clone()) {;};

 public:
    virtual ~dtkRationalBezierCurve2D(void);

 public:
    const dtkAbstractRationalBezierCurve2DData* data(void) const { return m_data; };
    dtkAbstractRationalBezierCurve2DData *data(void)       { return m_data;  };
    void setData(dtkAbstractRationalBezierCurve2DData* data) { m_data = data; };

 public:
    void create(std::size_t order, double* cps) const;

 public :
    std::size_t degree(void) const;
    void controlPoint(std::size_t i, double* r_cp) const;
    void weight(std::size_t i, double* r_w) const;

 public:
    void evaluatePoint(double p_u, double* r_point) const;
    void evaluateNormal(double p_u, double* r_normal) const;

 public:
    void split(dtkRationalBezierCurve2D *r_split_curve_a, dtkRationalBezierCurve2D *r_split_curve_b, double p_splitting_parameter) const;
    void split(std::list< dtkRationalBezierCurve2D * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const;

 public:
    void aabb(double* r_aabb) const;
    void extendedAabb(double* r_aabb, double factor) const;

 protected:
    dtkAbstractRationalBezierCurve2DData *m_data;
};


// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkRationalBezierCurve2D*)

//
// dtkRationalBezierCurve2D.h ends here
