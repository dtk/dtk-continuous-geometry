// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkContinuousGeometryCoreExport.h>

#include <QRunnable>
#include <QString>

#include <dtkCore>

class dtkBRep;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkBRepViewer : public QRunnable
{
public:
    virtual void setInputBRep(dtkBRep* ) = 0;

    virtual void run(void) = 0;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkBRepViewer*);
DTK_DECLARE_PLUGIN(dtkBRepViewer, DTKCONTINUOUSGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_FACTORY(dtkBRepViewer, DTKCONTINUOUSGEOMETRYCORE_EXPORT);
DTK_DECLARE_PLUGIN_MANAGER(dtkBRepViewer, DTKCONTINUOUSGEOMETRYCORE_EXPORT);

// /////////////////////////////////////////////////////////////////
// Register to dtkDiscretegeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkBRepViewer, DTKCONTINUOUSGEOMETRYCORE_EXPORT, bRepViewer);
}

//
// dtkBRepViewer.h ends here
