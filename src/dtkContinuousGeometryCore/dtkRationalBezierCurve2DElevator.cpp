// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkContinuousGeometry.h"
#include "dtkRationalBezierCurve2DElevator.h"

/*!
  \class dtkRationalBezierCurve2DElevator
  \inmodule dtkContinuousGeometry
  \brief dtkRationalBezierCurve2DElevator is a concept describing the embedding of a 2D rational Bezier curve with a rational Bezier surface into 3D.

  It cannot be instanciated on its own.

  It is to be inherited from the classes acting as plugins. For more information : dtkPluginsContinuousGeometry.

  The following lines of code show how to instanciate and use the class in pratice :
  \code
  dtkRationalBezierCurve2DElevator *embedder = dtkContinuousGeometry::rationalBezierCurve2DElevator::pluginFactory().create("blazeRationalBezierCurve2DElevator");
  embedder->setInputRationalBezierCurve2D(...);
  embedder->setInputRationalBezierSurface(...);
  embedder->run();
  dtkRationalBezierCurve *rational_bezier_curve = embedder->rationalBezierCurve();
  \endcode
  Check dtkPluginsContinuousGeometry for more information on available plugins for dtkRationalBezierCurve2DElevator.
*/

/*! \fn dtkRationalBezierCurve2DElevator::setInputRationalBezierCurve2D(dtkRationalBezierCurve2D *curve)
  Provides the 2D rational Bezier curve to the embedder (\a curve).

  \a curve : the 2D rational Bezier curve to embed in 3D

  The curve must lie in the rational Bezier surface parameter space.
*/

/*! \fn dtkRationalBezierCurve2DElevator::setInputRationalBezierSurface(dtkRationalBezierSurface *surface)
  Provides the rational Bezier surface to the embedder (\a surface).

  \a surface : the rational Bezier surface used to embed the curve
*/

/*! \fn dtkRationalBezierCurve2DElevator::run(void)
  Embeds into 3D the 2D rational Bezier curve provided with \l setInputRationalBezierCurve2D(dtkRationalBezierCurve2D *curve) with the rational Bezier surface provided with \l setInputRationalBezierSurface(dtkRationalBezierSurface *surface).
*/

/*! \fn void dtkRationalBezierCurve2DElevator::rationalBezierCurve(void)
  Returns the 3D rational Bezier curve computed in the \l run(void) method.
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkRationalBezierCurve2DElevator, rationalBezierCurve2DElevator, dtkContinuousGeometry);
}
