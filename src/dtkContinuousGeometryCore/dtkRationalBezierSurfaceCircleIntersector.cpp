#include "dtkContinuousGeometry.h"
#include "dtkRationalBezierSurfaceCircleIntersector.h"

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkRationalBezierSurfaceCircleIntersector, rationalBezierSurfaceCircleIntersector, dtkContinuousGeometry);
}
