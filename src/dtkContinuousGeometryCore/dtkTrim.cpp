// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkTrim.h"

#include "dtkContinuousGeometryUtils.h"

#include "dtkTopoTrim.h"
#include "dtkNurbsCurve2D.h"
#include "dtkNurbsSurface.h"

/*!
  \class dtkTrim
  \inmodule dtkDiscreteGeometryCore
  \brief The dtkTrim class represent a trim of a \l dtkBrep model. It is geometrically defined by its algebric representation in the 2D parameters space of the \l dtkNurbsSurface it trims.
*/

/*! \fn dtkTrim::dtkTrim(const dtkNurbsSurface& nurbs_surface, const dtkNurbsCurve2D& nurbs_curve_2d, const dtkTopoTrim* topo_trim, bool is_seam = false)
  Instanciates a dtkTrim.

  \a nurbs_surface : the trimmed NURBS surface

  \a nurbs_curve_2d : the trimming NURBS curve in the parameter domain of the trimmed NURBS surface (\a nurbs_surface)

  \a topo_trim : the topological trim

  \a is_seam : true if the trim is a seam, else false
*/
dtkTrim::dtkTrim(const dtkNurbsSurface& nurbs_surface, const dtkNurbsCurve2D& nurbs_curve_2d, const dtkTopoTrim* topo_trim, bool is_seam) : m_nurbs_curve_2d(nurbs_curve_2d), m_nurbs_surface(nurbs_surface), m_topo_trim(topo_trim), m_is_seam(is_seam){}

/*! \fn dtkTrim::curve2D(void) const
  Returns the trimming curve.
*/
const dtkNurbsCurve2D& dtkTrim::curve2D(void) const
{
    return m_nurbs_curve_2d;
}

/*! \fn dtkTrim::nurbsSurface(void) const
  Returns the trimmed NURBS surface.
*/
const dtkNurbsSurface& dtkTrim::nurbsSurface(void) const
{
    return m_nurbs_surface;
}

/*! \fn dtkTrim::topoTrim(void) const
  Returns the topological trim.
*/
const dtkTopoTrim* dtkTrim::topoTrim(void) const
{
    return m_topo_trim;
}

/*! \fn dtkTrim::setIsSeam(bool is_seam) const
  If \a is_seam is true, the trim is a seam, if \a is_seam is false, the trim is not a seam
 */
void dtkTrim::setIsSeam(bool is_seam) const
{
    if(is_seam) {
        m_is_seam = true;
    } else {
        m_is_seam = false;
    }
}

/*! \fn dtkTrim::isSeam(void) const
  Returns true if the trim is a seam, else returns false.
*/
bool dtkTrim::isSeam(void) const
{
    return m_is_seam;
}

/*! \fn dtkTrim::toPolyline(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_discretisation_length, double p_0, double p_1) const
  Stores in \a r_polyline a polyline representation of the trim with regard to the length of discretisation (\a p_discretisation_length), returns true if successfull, else returns false.

  \a r_polyline : list in which the dtkContinuousGeometryPrimitives::Point_2 will be added as polyline representation of the trimming curve
  \a p_discretisation_length : length between two samples in the trimming curve parameter space
  \a p_0 : must be within [knots[0], knots[nb_cps + degree - 1][, parameter at which starts the polylinization process
  \a p_1 : must be within ]knots[0], knots[nb_cps + degree - 1]], parameter at which ends the polylinization process
*/
void dtkTrim::toPolyline(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_discretisation_length, double p_0, double p_1) const
{
    std::size_t nb_of_segments = (p_1 - p_0) / p_discretisation_length;
    dtkContinuousGeometryPrimitives::Point_2 point(0., 0.);
    if (nb_of_segments == 0) {
        m_nurbs_curve_2d.evaluatePoint(p_0, point.data());
        r_polyline.push_back(point);
        m_nurbs_curve_2d.evaluatePoint(p_1, point.data());
        r_polyline.push_back(point);
    } else {
        double segment_length = (p_1 - p_0) / nb_of_segments;
        for (double i = 0; i <= nb_of_segments; ++i) {
            m_nurbs_curve_2d.evaluatePoint(p_0 +  segment_length * i, point.data());
            r_polyline.push_back(point);
        }
    }
}

/*! \fn dtkTrim::toPolyline(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_discretisation_length) const
  Stores in \a r_polyline a polyline representation of the trim with regard to the length of discretisation (\a p_discretisation_length), returns true if successfull, else returns false.

  \a r_polyline : list in which the dtkContinuousGeometryPrimitives::Point_2 will be added as polyline representation of the trimming curve
  \a p_discretisation_length : length between two samples in the trimming curve parameter space
*/
void dtkTrim::toPolyline(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_discretisation_length) const
{
    std::size_t nb_of_knots = m_nurbs_curve_2d.nbCps() + m_nurbs_curve_2d.degree() - 1;
    std::vector< double> knots(nb_of_knots);
    m_nurbs_curve_2d.knots(knots.data());

    this->toPolyline(r_polyline, p_discretisation_length, knots[0], knots[nb_of_knots - 1]);
}

/*! \fn dtkTrim::toPolylineApprox(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_approximation, double p_0, double p_1) const
  Stores in \a r_polyline a polyline representation of the trim with regard to an approximation distance to the trimming curve(\a p_approximation), returns true if successfull, else returns false.

  \a r_polyline : list in which the dtkContinuousGeometryPrimitives::Point_2 will be added as polyline representation of the curve
  \a p_approximation : maximal tolerated distance from the polygonalization to the trim loop
  \a p_0 : defaults to 0, must be within [0, 1[, parameter at which starts the polylinization process
  \a p_1 : defaults to 1, must be within ]0, 1], parameter at which ends the polylinization process
 */
void dtkTrim::toPolylineApprox(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_approximation) const
{
    dtkContinuousGeometryTools::convexHull2DApproximation(r_polyline, m_nurbs_curve_2d, p_approximation);
}

//
// dtkTrim.cpp ends here
