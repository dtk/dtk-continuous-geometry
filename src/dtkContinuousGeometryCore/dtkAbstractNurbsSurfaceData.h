// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

// ///////////////////////////////////////////////////////////////////
// dtkAbstractNurbsSurfaceData
// ///////////////////////////////////////////////////////////////////
class dtkTrimLoop;
class dtkRationalBezierSurface;
namespace dtkContinuousGeometryPrimitives {
    class Point_2;
}

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkAbstractNurbsSurfaceData
{

public:
    dtkAbstractNurbsSurfaceData(void) {;};
    virtual ~dtkAbstractNurbsSurfaceData(void) {;};

 public:
    virtual void create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, std::size_t order_u, std::size_t order_v, const double *knots_u, const double *knots_v, const double *cps) const = 0;
    virtual void create(std::string path) const = 0;

 public:
    virtual std::size_t uDegree(void) const = 0;
    virtual std::size_t vDegree(void) const = 0;

    virtual std::size_t uNbCps(void) const = 0;
    virtual std::size_t vNbCps(void) const = 0;

    virtual std::size_t uNbKnots(void) const = 0;
    virtual std::size_t vNbKnots(void) const = 0;

    virtual std::size_t dim(void) const = 0;

    virtual void controlPoint(std::size_t i, std::size_t j, double *r_cp) const = 0;
    virtual void weight(std::size_t i, std::size_t j, double *r_w) const = 0;

    virtual void uKnots(double *r_u_knots) const = 0;
    virtual void vKnots(double *r_v_knots) const = 0;

    virtual const double *uKnots(void) const = 0;
    virtual const double *vKnots(void) const = 0;

    virtual double uPeriod(void) const = 0;
    virtual double vPeriod(void) const = 0;

    virtual void uvBounds(double *uv_bounds) const; // has a default implementation

    virtual bool isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const = 0;

 public:
    virtual void evaluatePoint(double p_u, double p_v, double *r_point) const = 0;
    virtual void evaluateNormal(double p_u, double p_v, double *r_normal) const = 0;
    virtual void evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double *r_v_deriv) const = 0;
    virtual void evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const = 0;

 public:
    virtual void decomposeToRationalBezierSurfaces(std::vector< std::pair< dtkRationalBezierSurface *, double * > >& r_rational_bezier_surfaces ) const = 0;

 public:
    virtual void aabb(double *r_aabb) const = 0;
    virtual void extendedAabb(double *r_aabb, double factor) const = 0;

public:
    virtual dtkAbstractNurbsSurfaceData *clone(void) const = 0;

 public:
    virtual void print(std::ostream& stream) const = 0;

 public:
    std::vector< dtkTrimLoop * > m_trim_loops;
};

// ///////////////////////////////////////////////////////////////////
DTK_DECLARE_OBJECT(dtkAbstractNurbsSurfaceData*)
DTK_DECLARE_PLUGIN(dtkAbstractNurbsSurfaceData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractNurbsSurfaceData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractNurbsSurfaceData, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkAbstractNurbsSurfaceData, DTKCONTINUOUSGEOMETRYCORE_EXPORT, abstractNurbsSurfaceData);
}

//
// dtkAbstractNurbsSurfaceData.h ends here
