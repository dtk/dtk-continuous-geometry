// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

class dtkRationalBezierCurve2D;
namespace dtkContinuousGeometryPrimitives{
    class Circle_2;
    class IntersectionObject_2;
}

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkRationalBezierCurve2DCircleIntersector
{
 public:
    virtual ~dtkRationalBezierCurve2DCircleIntersector(void) {};

 public:
    virtual void setRationalBezierCurve2D(dtkRationalBezierCurve2D *curve) = 0;
    virtual void setCircle(const dtkContinuousGeometryPrimitives::Circle_2& circle) = 0;

    virtual bool run(void) = 0;

    virtual std::vector< double > intersectionParamters(void) = 0;
};

// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkRationalBezierCurve2DCircleIntersector*)
DTK_DECLARE_PLUGIN(dtkRationalBezierCurve2DCircleIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkRationalBezierCurve2DCircleIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkRationalBezierCurve2DCircleIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkRationalBezierCurve2DCircleIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT, rationalBezierCurve2DCircleIntersector);
}

//
// dtkRationalBezierCurve2DCircleIntersector.h ends here
