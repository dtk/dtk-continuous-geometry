// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

#include <dtkContinuousGeometryUtils.h>

class dtkRationalBezierSurface;

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkRationalBezierSurfacePointIntersector
{
 public:
    virtual ~dtkRationalBezierSurfacePointIntersector(void) {};

 public:
    virtual void setRationalBezierSurface(dtkRationalBezierSurface *surface) = 0;
    virtual void setPoint(dtkContinuousGeometryPrimitives::Point_3 *segment) = 0;

    virtual bool run(void) = 0;
    virtual void reset() = 0;

    virtual const std::vector< dtkContinuousGeometryPrimitives::IntersectionObject > & intersection(void) = 0;
};

// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkRationalBezierSurfacePointIntersector*)
DTK_DECLARE_PLUGIN(dtkRationalBezierSurfacePointIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkRationalBezierSurfacePointIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkRationalBezierSurfacePointIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DECLARE_CONCEPT(dtkRationalBezierSurfacePointIntersector, DTKCONTINUOUSGEOMETRYCORE_EXPORT, rationalBezierSurfacePointIntersector);
}

//
// dtkRationalBezierSurfacePointIntersector.h ends here
