// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkContinuousGeometry.h"
#include "dtkRationalBezierCurveSphereIntersector.h"

/*!
  \class dtkRationalBezierCurveSphereIntersector
  \inmodule dtkContinuousGeometry
  \brief dtkRationalBezierCurveSphereIntersector is a concept describing the intersection between a sphere and a rational Bezier curve.

  It cannot be instanciated on its own.

  It is to be inherited from the classes acting as plugins. For more information : dtkPluginsContinuousGeometry.

  The following lines of code show how to instanciate and use the class in pratice :
  \code
  dtkRationalBezierCurveSphereIntersector *intersector = dtkContinuousGeometry::rationalBezierCurveSphereIntersector::pluginFactory().create("npRationalBezierCurveSphereIntersector");
  intersector->setRationalBezierCurve(...);
  intersector->setSphere(...);
  intersector->run();
  std::vector< double > *intersection_parameters = intersector->intersectionParameters();
  \endcode
  Check dtkPluginsContinuousGeometry for more information on available plugins for dtkRationalBezierCurveSphereIntersector.
*/

/*! \fn dtkRationalBezierCurveSphereIntersector::setRationalBezierCurve(const dtkRationalBezierCurve *curve)
  Provides the rational Bezier curve to the intersector (\a curve).

  \a curve : the rational Bezier curve to intersect
*/

/*! \fn dtkRationalBezierCurveSphereIntersector::setSphere(const dtkContinuousGeometryPrimitives::Sphere_3& sphere)
  Provides the \a sphere to intersect the rational Bezier curve with to the intersector.

  \a sphere : the sphere to intersect the rational Bezier curve with
*/

/*! \fn dtkRationalBezierCurveSphereIntersector::run(void)
  Intersects the rational Bezier curve (provided with \l setRationalBezierCurve(dtkRationalBezierCurve *curve)) with the sphere(provided with \l setSphere(const Sphere_3& sphere)), the result can be retrieved with \l intersectionParameters(void).
*/

/*! \fn dtkRationalBezierCurveSphereIntersector::intersectionParameters(void)
  Returns the intersection parameters in the dtkRationalBezierCurve space of paramters.
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkRationalBezierCurveSphereIntersector, rationalBezierCurveSphereIntersector, dtkContinuousGeometry);
}
