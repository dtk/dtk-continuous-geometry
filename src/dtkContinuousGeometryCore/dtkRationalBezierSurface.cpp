// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkRationalBezierSurface.h"

#include "dtkContinuousGeometry.h"

/*!
  \class dtkRationalBezierSurface
  \inmodule dtkContinuousGeometry
  \brief dtkRationalBezierSurface is a container of dtkAbstractRationalBezierSurfaceData

  The following lines of code show how to instanciate the class in pratice :
  \code
  dtkRationalBezierSurface *rational_bezier_surface = new dtkRationalBezierSurface(dtkContinuousGeometry::abstractRationalBezierSurfaceData::pluginFactory().create("dtkAbstractRationalBezierSurfaceDataDefault");
  rational_bezier_surface->create(...);
  \endcode
  Check dtkPluginsContinuousGeometry for more information on available plugins for dtkAbstractRationalBezierSurfaceData.
 */

/*! \fn dtkRationalBezierSurface::dtkRationalBezierSurface(void)
  Instanciates a dtkRationalBezierCurve without any data. Any call to any method of the class before setting a valid data will fail.
*/

/*! \fn dtkRationalBezierSurface::dtkRationalBezierSurface(dtkAbstractRationalBezierSurfaceData *data)
  Instanciates a dtkNurbsSurface with a pointer to a valid dtkAbstractNurbsSurfaceData(\a data).
*/

/*! \fn dtkRationalBezierSurface::dtkRationalBezierSurface(const dtkRationalBezierSurface& other)
  Instanciates a dtkNurbsSurface by copying \a other content.
*/

/*! \fn dtkRationalBezierSurface::~dtkRationalBezierSurface(void)
  Destructor : will delete the underlying data, either passed in \l dtkRationalBezierSurface(dtkAbstractRationalBezierSurfaceData *data), \l setData(dtkAbstractRationalBezierSurfaceData *data) or dtkRationalBezierSurface(const dtkRationalBezierSurface& other).
*/
dtkRationalBezierSurface::~dtkRationalBezierSurface(void) {
    if (m_data) {
        delete m_data;
        m_data = NULL;
    }
}

/*! \fn dtkRationalBezierSurface::data(void) const
  Returns a reference to the underlying data.
*/

/*! \fn dtkRationalBezierSurface::data(void)
  Returns the underlying data pointer.
*/

/*! \fn dtkRationalBezierSurface::setData(dtkAbstractRationalBezierSurfaceData *data)
  Sets the underlying data pointer to point to \a data.
*/

/*! \fn void dtkRationalBezierSurface::create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, double *cps) const
  Creates a NURBS surface by providing the required content.

  \a dim : dimension of the space in which lies the surface

  \a nb_cp_u : number of control points along the "u" direction

  \a nb_cp_v : number of control points along the "v" direction

  \a cps : array containing the weighted control points, secified as : [x_00, y_00, z_00, w_00, x_01, y_01, ...]

  \a cps are copied.
*/
void dtkRationalBezierSurface::create(std::size_t dim, std::size_t order_u, std::size_t order_v, double* cps) const
{
    m_data->create(dim, order_u, order_v, cps);
}

/*! \fn dtkRationalBezierSurface::uDegree(void) const
  Returns the degree of the surface in the "u" direction.
*/
std::size_t dtkRationalBezierSurface::uDegree(void) const
{
    return m_data->uDegree();
}


/*! \fn dtkRationalBezierSurface::vDegree(void) const
  Returns the degree of the surface in the "v" direction.
*/
std::size_t dtkRationalBezierSurface::vDegree(void) const
{
    return m_data->vDegree();
}

/*! \fn dtkRationalBezierSurface::controlPoint(std::size_t i, std::size_t j, double *r_cp) const
  Writes in \a r_cp the \a i th, \a j th weighted control point coordinates[xij, yij, zij].

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_cp : array of size 3 to store the weighted control point coordinates[xij, yij, zij]
 */
void dtkRationalBezierSurface::controlPoint(std::size_t i, std::size_t j, double* r_cp) const
{
    m_data->controlPoint(i, j, r_cp);
}

/*! \fn dtkRationalBezierSurface::weight(std::size_t i, std::size_t j, double *r_w) const
  Writes in \a r_w the \a i th, \a j th weighted control point weight wij.

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_w : array of size 1 to store the weighted control point weight wij
*/
void dtkRationalBezierSurface::weight(std::size_t i, std::size_t j, double* r_w) const
{
    m_data->weight(i, j, r_w);
}

/*! \fn dtkRationalBezierSurface::isDegenerate(void) const
  Returns true if the rational Bezier surface is degenerate : i.e. a line of control points coalesce, else returns false.
*/
bool dtkRationalBezierSurface::isDegenerate(void) const
{
    return m_data->isDegenerate();
}

/*! \fn dtkRationalBezierSurface::degenerateLocations(std::list< dtkContinuousGeometryPrimitives::Point_3 >& r_degenerate_locations) const
  Returns true if the rational Bezier surface is degenerate : i.e. a line of control points coalesce and adds in \a r_degenerate_locations the locations at which there are degeneracies, else returns false.
*/
bool dtkRationalBezierSurface::degenerateLocations(std::list< dtkContinuousGeometryPrimitives::Point_3 >& r_degenerate_locations) const
{
    return m_data->degenerateLocations(r_degenerate_locations);
}

/*! \fn dtkRationalBezierSurface::evaluatePoint(double p_u, double p_v, double *r_point) const
  Writes in \a r_point the evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkRationalBezierSurface::evaluatePoint(double p_u, double p_v, double* r_point) const
{
    m_data->evaluatePoint(p_u, p_v, r_point);
}

/*! \fn dtkRationalBezierSurface::evaluateNormal(double p_u, double p_v, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkRationalBezierSurface::evaluateNormal(double p_u, double p_v, double* r_normal) const
{
    m_data->evaluateNormal(p_u, p_v, r_normal);
}


/*! \fn dtkRationalBezierSurface::evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_point the evaluation, in \a r_u_deriv the first derivative in "u" direction, in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkRationalBezierSurface::evaluate1stDer(double p_u, double p_v, double* r_point, double* r_u_deriv, double* r_v_deriv) const
{
    m_data->evaluate1stDer(p_u, p_v, r_point, r_u_deriv, r_v_deriv);
}

/*! \fn dtkRationalBezierSurface::evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_u_deriv the first derivative in "u" direction and in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkRationalBezierSurface::evaluate1stDer(double p_u, double p_v, double* r_u_deriv, double* r_v_deriv) const
{
    m_data->evaluate1stDer(p_u, p_v, r_u_deriv, r_v_deriv);
}

/*! \fn dtkRationalBezierSurface::evaluatePointBlossom(const double* p_uis, const double* p_vis, double *r_point, double *r_weight) const
  Stores in \a r_point and \a r_weight the result of the blossom evaluation at \a p_uis, \a p_vis values.

  \a p_uis : "u" values for blossom evaluation

  \a p_vis : "v" values for blossom evaluation

  \a r_point : array of size 3 to slore the point result of the blossom evaluation

  \a r_weight : array of size 1 to slore the weight result of the blossom evaluation
*/
void dtkRationalBezierSurface::evaluatePointBlossom(const double* p_uis, const double* p_vis, double* r_point, double* r_weight) const
{
    m_data->evaluatePointBlossom(p_uis, p_vis, r_point, r_weight);
}

/*! \fn dtkRationalBezierSurface::split(dtkRationalBezierSurface *r_split_surface, double *p_splitting_parameters) const
  Creates \a r_split_surface as the part of rational Bezier surface lying inside the bounding box of parameters \a p_splitting_parameters.

  \a r_split_surface : pointer that will point to the newly created rationalBezierSurface which is the part of the initial surface restricted to the bouding box of parameters given in \a p_splitting_parameters

  \a p_splitting_parameters : array of size 4, storing the parameters defining the restriction in the parameter space of the rational Bezier surface, given as [u_0, v_0, u_1, v_1]
 */
void dtkRationalBezierSurface::split(dtkRationalBezierSurface *r_split_surface, double *p_splitting_parameters) const
{
    m_data->split(r_split_surface, p_splitting_parameters);
}

/*! \fn dtkRationalBezierSurface::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
 */
void dtkRationalBezierSurface::aabb(double* r_aabb) const
{
    m_data->aabb(r_aabb);
}

/*! \fn dtkRationalBezierSurface::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkRationalBezierSurface::extendedAabb(double* r_aabb, double factor) const
{
    m_data->extendedAabb(r_aabb, factor);
}

std::ostream& operator<<(std::ostream& stream, const dtkRationalBezierSurface& rational_bezier_surface)
{
    rational_bezier_surface.data()->print(stream);
    return stream;
}
