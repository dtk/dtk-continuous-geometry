// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkContinuousGeometryCoreExport.h>

#include "dtkAbstractRationalBezierCurveData.h"

#include <iostream>

// /////////////////////////////////////////////////////////////////
// dtkRationalBezierCurve
// /////////////////////////////////////////////////////////////////

class DTKCONTINUOUSGEOMETRYCORE_EXPORT dtkRationalBezierCurve
{

 public:
    explicit dtkRationalBezierCurve(void) : m_data(nullptr), m_reversed(false) {;};
    explicit dtkRationalBezierCurve(dtkAbstractRationalBezierCurveData *data) : m_data(data), m_reversed(false) {;};
             dtkRationalBezierCurve(const dtkRationalBezierCurve& other) :m_data(other.m_data->clone()), m_reversed(false) {;};

 public:
             virtual ~dtkRationalBezierCurve(void);

public:
    dtkRationalBezierCurve& operator=(const dtkRationalBezierCurve& other)
    {
        if(m_data != other.m_data) {
            if(m_data != nullptr) {
                delete m_data;
            }
            if(other.m_data != nullptr) {
                m_data = other.m_data->clone();
            } else {
                m_data = nullptr;
            }
        }
        m_reversed = false;
        return *this;
    }

 public:
    const dtkAbstractRationalBezierCurveData *data(void) const { return m_data; };
    dtkAbstractRationalBezierCurveData *data(void)       { return m_data;  };
    void setData(dtkAbstractRationalBezierCurveData* data) { m_data = data; };

 public:
    void create(std::size_t order, double *cps) const;

 public :
    std::size_t degree(void) const;
    void controlPoint(std::size_t i, double *r_cp) const;
    void weight(std::size_t i, double *r_w) const;

 public :
    void setWeightedControlPoint(std::size_t i, double *p_cp);

 public:
    void evaluatePoint(double p_u, double *r_point) const;
    void evaluateNormal(double p_u, double *r_normal) const;
    void evaluateDerivative(double p_u, double* r_derivative) const;
    void evaluateCurvature(double p_u, double* r_curvature) const;

 public:
    void split(dtkRationalBezierCurve *r_split_curve_a, dtkRationalBezierCurve *r_split_curve_b, double splitting_parameter) const;
    void split(std::list< dtkRationalBezierCurve * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const;

 public:
    void aabb(double *r_aabb) const;
    void extendedAabb(double *r_aabb, double factor) const;

 public:
    void set_not_reversed() { m_reversed = false; }
    void reverse() { m_reversed = true; }
    bool is_reversed() const { return m_reversed; };

 protected:
    dtkAbstractRationalBezierCurveData *m_data;

    bool m_reversed; // if the bezier curve is reversed w.r.t. its nurbs correspondence.
};

DTKCONTINUOUSGEOMETRYCORE_EXPORT std::ostream& operator<<(std::ostream& stream, const dtkRationalBezierCurve& rational_bezier_curve);

// ///////////////////////////////////////////////////////////////////
// Plugin system and declaration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkRationalBezierCurve*)

//
// dtkRationalBezierCurve.h ends here
