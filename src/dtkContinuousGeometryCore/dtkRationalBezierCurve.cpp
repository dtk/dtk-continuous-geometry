// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkRationalBezierCurve.h"

#include "dtkContinuousGeometry.h"

/*!
  \class dtkRationalBezierCurve
  \inmodule dtkContinuousGeometry
  \brief dtkRationalBezierCurve is a container of dtkAbstractRationalBezierCurveData

  The following line of code shows how to instanciate the class in pratice :
  \code
  dtkRationalBezierCurve *rational_bezier_curve = new dtkRationalBezierCurve(dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().create("dtkRationalBezierCurveDataDefault");
  rational_bezier_curve->create(...);
  \endcode
  Check dtkPluginsContinuousGeometry for more information on available plugins for dtkAbstractRationalBezierCurveData.
 */

/*! \fn dtkRationalBezierCurve::dtkRationalBezierCurve(void)
  Instanciates a dtkRationalBezierCurve without any data. Any call to any method of the class before setting a valid data will fail.
*/

/*! \fn dtkRationalBezierCurve::dtkRationalBezierCurve(dtkAbstractRationalBezierCurveData *data)
   Instanciates a dtkRationalBezierCurve with a pointer to a valid dtkAbstractRationalBezierCurveData(\a data).
*/

/*! \fn dtkRationalBezierCurve::dtkRationalBezierCurve(const dtkRationalBezierCurve& other)
   Instanciates a dtkRationalBezierCurve by copying \a other content.
*/

/*! \fn dtkRationalBezierCurve::~dtkRationalBezierCurve(void)
  Destructor : will delete the underlying data, either passed in \l dtkRationalBezierCurve(dtkAbstractRationalBezierCurveData *data), \l setData(dtkAbstractRationalBezierCurveData *data) or dtkRationalBezierCurve(const dtkRationalBezierCurve& other).
*/
dtkRationalBezierCurve::~dtkRationalBezierCurve(void) {
    if (m_data) {
        delete m_data;
        m_data = NULL;
    }
}

/*! \fn dtkRationalBezierCurve::data(void) const
  Returns a reference to the underlying data.
*/

/*! \fn dtkRationalBezierCurve::data(void)
  Returns the underlying data pointer.
*/

/*! \fn dtkRationalBezierCurve::setData(dtkAbstractRationalBezierCurveData *data)
  Sets the underlying data pointer to point to \a data.
*/

/*! \fn dtkRationalBezierCurve::create(std::size_t order, double* cps) const
  Creates the rational Bezier curve by providing the required content for a rational Bezier curve.

 \a order : order

 \a cps : array containing the weighted control points, specified as : [x0, y0, z0, w0, ...]
*/
void dtkRationalBezierCurve::create(std::size_t order, double *cps) const
{
    m_data->create(order, cps);
}

/*! \fn  dtkRationalBezierCurve::degree(void) const
  Returns the degree of the curve.
*/
std::size_t dtkRationalBezierCurve::degree(void) const
{
    return m_data->degree();
}

/*! \fn  dtkRationalBezierCurve::controlPoint(std::size_t i, double *r_cp) const
  Writes in \a r_cp the \a i th weighted control point coordinates[xi, yi, zi].

  \a i : index of the weighted control point

  \a r_cp : array of size 3 to store the weighted control point coordinates[xi, yi, zi]
*/
void dtkRationalBezierCurve::controlPoint(std::size_t i, double *r_cp) const
{
    m_data->controlPoint(i, r_cp);
}

/*! \fn  dtkRationalBezierCurve::weight(std::size_t i, double *r_w) const
  Writes in \a r_w the \a i th weighted control point weight wi.

  \a i : index of the weighted control point

  \a r_w : array of size 1 to store the weighted control point weight wi
*/
void dtkRationalBezierCurve::weight(std::size_t i, double *r_w) const
{
    m_data->weight(i, r_w);
}

/*! \fn  dtkRationalBezierCurve::setWeightedControlPoint(std::size_t i, double *p_cp)
   Modifies the \a i th weighted control point by \a p_cp : [xi, yi, zi, wi].

  \a i : index of the weighted control point

  \a p_cp : array of size 4 containing the weighted control point coordinates and weight  : [xi, yi, zi, wi]
*/
void dtkRationalBezierCurve::setWeightedControlPoint(std::size_t i, double *p_cp)
{
    m_data->setWeightedControlPoint(i, p_cp);
}

/*! \fn dtkRationalBezierCurve::evaluatePoint(double p_u, double *r_point) const
  Writes in \a r_point the evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkRationalBezierCurve::evaluatePoint(double p_u, double *r_point) const
{
    m_data->evaluatePoint(p_u, r_point);
}

/*! \fn dtkRationalBezierCurve::evaluateNormal(double p_u, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkRationalBezierCurve::evaluateNormal(double p_u, double *r_normal) const
{
    m_data->evaluateNormal(p_u, r_normal);
}

void dtkRationalBezierCurve::evaluateDerivative(double p_u, double* r_derivative) const
{
    m_data->evaluateDerivative(p_u, r_derivative);
}

void dtkRationalBezierCurve::evaluateCurvature(double p_u, double* r_curvature) const
{
    m_data->evaluateCurvature(p_u, r_curvature);
}

/*! \fn dtkRationalBezierCurve::split(dtkRationalBezierCurve *r_split_curve_a, dtkRationalBezierCurve *r_split_curve_b, double splitting_parameter) const
  Splits at \a splitting_parameter the rational Bezier curve and stores in \a r_split_curve_a and \a r_split_curve_b the split parts (from 0 to \a splitting_parameter and from \a splitting_parameter to 1 respectively).

  \a r_split_curve_a : a pointer to a valid dtkRationalBezierCurve with data not initialized, in which the first part of the curve (from 0 to to \a splitting_parameter) will be stored

  \a r_split_curve_b : a pointer to a valid dtkRationalBezierCurve with data not initialized, in which the second part of the curve (from \a splitting_parameter to 1) will be stored

  \a splitting_parameter : the parameter at which the curve will be split
*/
void dtkRationalBezierCurve::split(dtkRationalBezierCurve *r_split_curve_a, dtkRationalBezierCurve *r_split_curve_b, double splitting_parameter) const
{
    m_data->split(r_split_curve_a, r_split_curve_b, splitting_parameter);
}


/*! \fn dtkRationalBezierCurve::split(std::list< dtkRationalBezierCurve * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const
  Splits the rational Bezier curve at each value of \a p_splitting_parameters and stores in \a r_split_curves the split parts.

  \a r_split_curves : a list to which the split rational Bezier curves will be added

  \a p_splitting_parameters : the parameters at which the curve will be split
*/
void dtkRationalBezierCurve::split(std::list< dtkRationalBezierCurve * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const
{
    m_data->split(r_split_curves, p_splitting_parameters);
}

/*! \fn dtkRationalBezierCurve::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates of the dtkRationalBezierCurve.

  \a r_aabb : array of size 6 to store the limits coordinates
*/
void dtkRationalBezierCurve::aabb(double *r_aabb) const
{
    m_data->aabb(r_aabb);
}

/*! \fn dtkRationalBezierCurve::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates of an extened bounding box of factor \a factor of the dtkRationalBezierCurve. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkRationalBezierCurve::extendedAabb(double *r_aabb, double factor) const
{
    m_data->extendedAabb(r_aabb, factor);
}

std::ostream& operator<<(std::ostream& stream, const dtkRationalBezierCurve& rational_bezier_curve)
{
    rational_bezier_curve.data()->print(stream);
    return stream;
}

//
// dtkRationalBezierCurve.cpp ends here
