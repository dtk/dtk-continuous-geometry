// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkAbstractRationalBezierCurve2DData.h"

#include "dtkContinuousGeometry.h"

/*!
  \class dtkAbstractRationalBezierCurve2DData
  \inmodule dtkContinuousGeometry
  \brief dtkAbstractRationalBezierCurve2DData is the data container for 2D rational Bezier curve

  It cannot be instanciated on its own.

  It is to be inherited from the classes acting as plugins. For more information : dtkPluginsContinuousGeometry.

  In pratice, the client code will instanciate \l dtkRationalBezierCurve2D. Check \l dtkRationalBezierCurve2D for more information.
*/

/*! \fn dtkAbstractRationalBezierCurve2DData::dtkAbstractRationalBezierCurve2DData(void)
  Instanciate the structure of a dtkAbstractRationalBezierCurve2DData.

  To add the content, see \l create(std::size_t order, double *cps) const.
*/

/*! \fn dtkAbstractRationalBezierCurve2DData::create(std::size_t order, double *cps) const
  Creates the 2D rational Bezier curve by providing the required content.

 \a order : order

 \a cps : array containing the weighted control points, specified as : [x0, y0, w0, ...]
*/

/*! \fn  dtkAbstractRationalBezierCurve2DData::degree(void) const
  Returns the degree of the curve.
*/

/*! \fn  dtkAbstractRationalBezierCurve2DData::controlPoint(std::size_t i, double *r_cp) const
  Writes in \a r_cp the \a i th weighted control point coordinates[xi, yi].

  \a i : index of the weighted control point

  \a r_cp : array of size 2 to store the weighted control point coordinates[xi, yi]

  The method can be called as follow:
  \code
  dtkContinuousGeometryPrimitives::Point_2 point2(0, 0);
  rational_bezier_curve_data->controlPoint(3, point2.data());
  \endcode
*/

/*! \fn  dtkAbstractRationalBezierCurve2DData::weight(std::size_t i, double *r_w) const
  Writes in \a r_w the \a i th weighted control point weight wi.

  \a i : index of the weighted control point

  \a r_w : array of size 1 to store the weighted control point weight wi
*/

/*! \fn dtkAbstractRationalBezierCurve2DData::evaluatePoint(double p_u, double *r_point) const
  Writes in \a r_point the evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 2 to store the result of the evaluation
*/

/*! \fn dtkAbstractRationalBezierCurve2DData::evaluateNormal(double p_u, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_normal : array of size 2 to store the result of the normal evaluation
*/

/*! \fn dtkAbstractRationalBezierCurve2DData::split(dtkRationalBezierCurve2D *r_split_curve_a, dtkRationalBezierCurve2D *r_split_curve_b, double p_splitting_parameter) const
  Splits at \a p_splitting_parameter the rational Bezier curve and stores in \a r_split_curve_a and \a r_split_curve_b the split parts (from 0 to \a p_splitting_parameter and from \a p_splitting_parameter to 1 respectively).

  \a r_split_curve_a : a pointer to a valid dtkRationalBezierCurve2D with data not initialized, in which the first part of the curve (from 0 to to \a p_splitting_parameter) will be stored

  \a r_split_curve_b : a pointer to a valid dtkRationalBezierCurve2D with data not initialized, in which the second part of the curve (from \a p_splitting_parameter to 1) will be stored

  \a p_splitting_parameter : the parameter at which the curve will be split
*/

/*! \fn dtkAbstractRationalBezierCurve2DData::split(std::list< dtkRationalBezierCurve2D * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const
  Splits the rational Bezier curve at each value of \a p_splitting_parameters and stores in \a r_split_curves the split parts.

  \a r_split_curves : a list to which the split rational Bezier curves will be added

  \a p_splitting_parameters : the parameters at which the curve will be split
*/

/*! \fn dtkAbstractRationalBezierCurve2DData::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates.

  \a r_aabb : array of size 4 to store the limits coordinates
*/

/*! \fn dtkAbstractRationalBezierCurve2DData::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 4 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/

/*! \fn dtkAbstractRationalBezierCurve2DData::clone(void) const
   Clone
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkAbstractRationalBezierCurve2DData, abstractRationalBezierCurve2DData, dtkContinuousGeometry);
}

//
// dtkAbstractRationalBezierCurve2DData.cpp ends here
