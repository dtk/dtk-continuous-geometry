// Version: $Id: $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkContinuousGeometry.h"
#include "dtkRationalBezierCurve2DCircleIntersector.h"

/*!
  \class dtkRationalBezierCurve2DCircleIntersector
  \inmodule dtkContinuousGeometry
  \brief dtkRationalBezierCurve2DCircleIntersector is a concept describing the intersection between a circle and a 2D rational Bezier curve.

  It cannot be instanciated on its own.

  It is to be inherited from the classes acting as plugins. For more information : dtkPluginsContinuousGeometry.

  The following lines of code show how to instanciate and use the class in pratice :
  \code
  dtkRationalBezierCurve2DCircleIntersector *intersector = dtkContinuousGeometry::rationalBezierCurve2DCircleIntersector::pluginFactory().create("npRationalBezierCurve2DCircleIntersector");
  intersector->setRationalBezierCurve2D(...);
  intersector->setCircle(...);
  intersector->run();
  std::vector< double > *intersection_parameters = intersector->intersectionParameters();
  \endcode
  Check dtkPluginsContinuousGeometry for more information on available plugins for dtkRationalBezierCurve2DCircleIntersector.
*/

/*! \fn dtkRationalBezierCurve2DCircleIntersector::setRationalBezierCurve2D(dtkRationalBezierCurve2D *curve)
  Provides the 2D rational Bezier curve to the intersector (\a curve).

  \a curve : the 2D rational Bezier curve to intersect
*/

/*! \fn dtkRationalBezierCurve2DCircleIntersector::setCircle(const dtkContinuousGeometryPrimitives::Circle_2& circle)
  Provides the \a circle to intersect the 2D rational Bezier curve with to the intersector.

  \a circle : the circle to intersect the 2D rational Bezier curve with
*/

/*! \fn dtkRationalBezierCurve2DCircleIntersector::run(void)
  Intersects the 2D rational Bezier curve  (provided with \l setRationalBezierCurve2D(dtkRationalBezierCurve2D *curve)) with the circle(provided with \l setCircle(const Circle_2& circle)), the result can be retrieved with \l intersectionParameters(void).
*/

/*! \fn dtkRationalBezierCurve2DCircleIntersector::intersectionParameters(void)
  Returns the intersection parameters in the dtkRationalBezierCurve2D space of paramters.
*/

// /////////////////////////////////////////////////////////////////
// Register to dtkContinuousGeometry layer
// /////////////////////////////////////////////////////////////////

namespace dtkContinuousGeometry {
    DTK_DEFINE_CONCEPT(dtkRationalBezierCurve2DCircleIntersector, rationalBezierCurve2DCircleIntersector, dtkContinuousGeometry);
}
