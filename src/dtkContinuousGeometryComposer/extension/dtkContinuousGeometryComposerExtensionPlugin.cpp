/* dtkContinuousGeometryComposerExtension.cpp ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) Inria.
 */
//
//


// Code:

#include "dtkContinuousGeometryComposerExtensionPlugin.h"
#include "dtkContinuousGeometryComposerExtension.h"
#include "dtkContinuousGeometry.h"

#include <dtkComposer>
#include <dtkLog>

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

dtkComposerExtension* dtkContinuousGeometryComposerCreator(void);

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////


void dtkContinuousGeometryComposerExtensionPlugin::initialize(void)
{
    dtkComposer::extension::pluginFactory().record("dtkContinuousGeometry", dtkContinuousGeometryComposerCreator);
    dtkComposerExtension *extension = dtkComposer::extension::pluginFactory().create("dtkContinuousGeometry");
    bool verbose = dtkComposer::extension::pluginManager().verboseLoading();
    dtkContinuousGeometry::setVerboseLoading(verbose);
    extension->extend(&(dtkComposer::node::factory()));
    dtkContinuousGeometry::initialize();
}

void dtkContinuousGeometryComposerExtensionPlugin::uninitialize(void)
{

}


// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkComposerExtension)

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

dtkComposerExtension *dtkContinuousGeometryComposerCreator(void)
{
    return new dtkContinuousGeometryComposerExtension();
}
