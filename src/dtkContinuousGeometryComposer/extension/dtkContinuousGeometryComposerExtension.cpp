// Version: $Id: 1e79f4fa799a123ea455879ae03c89d8ac844a2f $
//
//


// Code:

#include "dtkContinuousGeometryComposerExtension.h"

#include "dtkComposerNodeBRepReader.h"
#include "dtkComposerNodeBRepViewer.h"
#include "dtkComposerNodeBRepWriter.h"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkContinuousGeometryComposerExtension::dtkContinuousGeometryComposerExtension(void) : dtkComposerExtension()
{

}

dtkContinuousGeometryComposerExtension::~dtkContinuousGeometryComposerExtension(void)
{

}

void dtkContinuousGeometryComposerExtension::extend(dtkComposerNodeFactory *factory)
{//record the new nodes
    if (!factory) {
        dtkError() << "No composer factory, can't extend it with dtkContinuousGeometry nodes ";
        return;
    }
    
    factory->record(":dtkComposer/dtkComposerNodeBRepReader.json", dtkComposerNodeCreator< dtkComposerNodeBRepReader >);
	factory->record(":dtkComposer/dtkComposerNodeBRepViewer.json", dtkComposerNodeCreator< dtkComposerNodeBRepViewer >);
	factory->record(":dtkComposer/dtkComposerNodeBRepWriter.json", dtkComposerNodeCreator< dtkComposerNodeBRepWriter >);
}

//
// dtkContinuousGeometryComposerExtension.cpp ends here
