/* dtkContinuousGeometryComposerExtensionPlugin.h ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) Inria.
 */

#pragma once

#include <dtkComposer/dtkComposerExtension.h>

#include <QtCore>

class dtkContinuousGeometryComposerExtensionPlugin : public dtkComposerExtensionPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkComposerExtensionPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkContinuousGeometryComposerExtensionPlugin" FILE "dtkContinuousGeometryComposerExtensionPlugin.json")

public:
     dtkContinuousGeometryComposerExtensionPlugin(void) {}
    ~dtkContinuousGeometryComposerExtensionPlugin(void) {}

public:
    void   initialize(void);
    void uninitialize(void);
};
