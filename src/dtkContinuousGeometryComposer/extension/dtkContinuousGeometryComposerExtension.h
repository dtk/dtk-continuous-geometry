// Version: $Id: d0c58a963562b8af0add85fe1984c3f775982dd8 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkContinuousGeometryComposerExtensionExport.h>

#include <dtkComposer/dtkComposerExtension.h>

class dtkComposerNodeFactory;

class DTKCONTINUOUSGEOMETRYCOMPOSEREXTENSION_EXPORT dtkContinuousGeometryComposerExtension : public dtkComposerExtension
{
public:
     dtkContinuousGeometryComposerExtension(void);
    ~dtkContinuousGeometryComposerExtension(void);

public:
    void extend(dtkComposerNodeFactory *factory);
};

//
// dtkContinuousGeometryComposerFactoryExtension.h ends here
