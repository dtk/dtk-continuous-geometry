// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkComposer>

#include <dtkContinuousGeometryComposerExport.h>
#include "dtkBRepReader.h"

class dtkComposerNodeBRepReaderPrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKCONTINUOUSGEOMETRYCOMPOSER_EXPORT dtkComposerNodeBRepReader : public dtkComposerNodeObject< dtkBRepReader >
{
public:
    dtkComposerNodeBRepReader(void);
    ~dtkComposerNodeBRepReader(void);

public:
    void run(void);

private:
    dtkComposerNodeBRepReaderPrivate *d;
};

//
// dtkComposerNodeBRepReader.h ends here
