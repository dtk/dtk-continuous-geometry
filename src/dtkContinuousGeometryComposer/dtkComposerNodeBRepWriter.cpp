// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkContinuousGeometry.h"

#include "dtkComposerNodeBRepWriter.h"

#include <dtkBRep>

class dtkComposerNodeBRepWriterPrivate
{
public:
    dtkComposerTransmitterReceiver<dtkBRep*> rcv_in_brep;
    dtkComposerTransmitterReceiver<QString> rcv_out_file;
	
public:
    dtkComposerTransmitterEmitter<QString> emt_out_file;
};

dtkComposerNodeBRepWriter::dtkComposerNodeBRepWriter(void) : dtkComposerNodeObject< dtkBRepWriter >(), d(new dtkComposerNodeBRepWriterPrivate())
{
    this->setFactory(dtkContinuousGeometry::bRepWriter::pluginFactory());

    this->appendReceiver(&d->rcv_in_brep);
    this->appendReceiver(&d->rcv_out_file);

    this->appendEmitter(&d->emt_out_file);
}

dtkComposerNodeBRepWriter::~dtkComposerNodeBRepWriter(void)
{
    delete d;

    d = 0;
}

void dtkComposerNodeBRepWriter::run(void)
{
    if (!d->rcv_in_brep.isEmpty()) {
        if (!this->object()) {
            dtkWarn() << Q_FUNC_INFO << "No bRepWriter instantiated, abort:" << this->currentImplementation();
            return;
        }

        dtkBRep* brep = d->rcv_in_brep.constData();
        dtkBRepWriter *process = this->object();
        process->setInputBRep(brep);
        process->setOutputBRepFile(d->rcv_out_file.constData());

        process->run();
		
        d->emt_out_file.setData(process->outputBRepFile());

    } else {
        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        d->emt_out_file.clearData();
        return;

    }
}

//
// dtkComposerNodeBRepWriter.cpp ends here
