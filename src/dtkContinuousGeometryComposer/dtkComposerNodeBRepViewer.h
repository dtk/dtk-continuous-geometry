// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkComposer>

#include <dtkContinuousGeometryComposerExport.h>
#include "dtkBRepViewer.h"

class dtkComposerNodeBRepViewerPrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKCONTINUOUSGEOMETRYCOMPOSER_EXPORT dtkComposerNodeBRepViewer : public dtkComposerNodeObject< dtkBRepViewer >
{
public:
    dtkComposerNodeBRepViewer(void);
    ~dtkComposerNodeBRepViewer(void);

public:
    void run(void);

private:
    dtkComposerNodeBRepViewerPrivate *d;
};

//
// dtkComposerNodeBRepViewer.h ends here
