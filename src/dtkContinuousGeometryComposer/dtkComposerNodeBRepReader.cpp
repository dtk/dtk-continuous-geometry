// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkContinuousGeometry.h"

#include "dtkComposerNodeBRepReader.h"

#include <dtkBRep>

class dtkComposerNodeBRepReaderPrivate
{
public:
    dtkComposerTransmitterReceiver<QString> rcv_in_brep_file_path;
    dtkComposerTransmitterReceiver<QString> rcv_in_dump_file_path;

public:
    dtkComposerTransmitterEmitter<dtkBRep*> emt_out_brep;
};

dtkComposerNodeBRepReader::dtkComposerNodeBRepReader(void) : dtkComposerNodeObject< dtkBRepReader >(), d(new dtkComposerNodeBRepReaderPrivate())
{
    this->setFactory(dtkContinuousGeometry::bRepReader::pluginFactory());

    this->appendReceiver(&d->rcv_in_brep_file_path);
    this->appendReceiver(&d->rcv_in_dump_file_path);

    this->appendEmitter(&d->emt_out_brep);
}

dtkComposerNodeBRepReader::~dtkComposerNodeBRepReader(void)
{
    delete d;

    d = 0;
}

void dtkComposerNodeBRepReader::run(void)
{
    if (!d->rcv_in_brep_file_path.isEmpty()) {
        if (!this->object()) {
            dtkWarn() << Q_FUNC_INFO << "No bRepReader instantiated, abort:" << this->currentImplementation();
            d->emt_out_brep.clearData();
            return;
        }
    if (!d->rcv_in_dump_file_path.isEmpty()) {
            dtkWarn() << "No dump file was provided" << this->currentImplementation();
	}
        const QString brep_file_path = d->rcv_in_brep_file_path.constData();
        const QString dump_file_path = d->rcv_in_dump_file_path.constData();
        dtkBRepReader *process = this->object();
        process->setInputBRepFilePath(brep_file_path);
		process->setInputDumpFilePath(dump_file_path);
        process->run();
        d->emt_out_brep.setData(process->outputDtkBRep());

    } else {
        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        d->emt_out_brep.clearData();
        return;

    }
}

//
// dtkComposerNodeBRepReader.cpp ends here
