// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkComposer>

#include <dtkContinuousGeometryComposerExport.h>
#include "dtkBRepWriter.h"

class dtkComposerNodeBRepWriterPrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKCONTINUOUSGEOMETRYCOMPOSER_EXPORT dtkComposerNodeBRepWriter : public dtkComposerNodeObject< dtkBRepWriter >
{
public:
    dtkComposerNodeBRepWriter(void);
    ~dtkComposerNodeBRepWriter(void);

public:
    void run(void);

private:
    dtkComposerNodeBRepWriterPrivate *d;
};

//
// dtkComposerNodeBRepWriter.h ends here
