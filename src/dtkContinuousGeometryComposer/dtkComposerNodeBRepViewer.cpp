// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkBRepViewer.h"

#include "dtkComposerNodeBRepViewer.h"

#include <dtkBRep>

class dtkComposerNodeBRepViewerPrivate
{
public:
    dtkComposerTransmitterReceiver<dtkBRep*> rcv_in_brep;
};

dtkComposerNodeBRepViewer::dtkComposerNodeBRepViewer(void) : dtkComposerNodeObject< dtkBRepViewer >(), d(new dtkComposerNodeBRepViewerPrivate())
{
    this->setFactory(dtkContinuousGeometry::bRepViewer::pluginFactory());

    this->appendReceiver(&d->rcv_in_brep);
}

dtkComposerNodeBRepViewer::~dtkComposerNodeBRepViewer(void)
{
    delete d;

    d = 0;
}

void dtkComposerNodeBRepViewer::run(void)
{
    if (!d->rcv_in_brep.isEmpty()) {
        if (!this->object()) {
            dtkWarn() << Q_FUNC_INFO << "No bRepViewer instantiated, abort:" << this->currentImplementation();
            return;
        }

        dtkBRep* brep = d->rcv_in_brep.constData();
        dtkBRepViewer *process = this->object();
        process->setInputBRep(brep);

        process->run();

    } else {
        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        return;

    }
}

//
// dtkComposerNodeBRepViewer.cpp ends here
