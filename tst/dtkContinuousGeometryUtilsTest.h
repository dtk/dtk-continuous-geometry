// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

class dtkContinuousGeometryUtilsTestCase : public QObject
{
    Q_OBJECT

public:
             dtkContinuousGeometryUtilsTestCase(void);
    virtual ~dtkContinuousGeometryUtilsTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testArray_2(void);
    void testPoint_2(void);
    void testVector_2(void);
    void testArray_3(void);
    void testPoint_3(void);
    void testVector_3(void);
    void testSegment_3(void);
    void testRay_3(void);
    void testLine_3(void);
    void testIntersect(void);
    void testIsInCircle(void);
    void testBoundingSphere(void);
    void testNormalize(void);
    void testConvexHullApproximationError(void);
    void testConvexHull3DApproximation(void);
    void testConvexHullApproximation(void);
    void testConvexHull2DApproximation(void);
    void testIsAPlane(void);
    void testDecomposeToSplitRationalBezierCurves(void);
    void testRecomposeNurbsCurve(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private :
    double m_eps;
};


//
// dtkContinuousGeometryUtilsTest.h ends here
