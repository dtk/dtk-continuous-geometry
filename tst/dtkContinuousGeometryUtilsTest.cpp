// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkContinuousGeometryUtilsTest.h"

#include <dtkContinuousGeometryUtils>

#include <dtkContinuousGeometry>
#include <dtkAbstractRationalBezierCurveData>
#include <dtkAbstractRationalBezierCurve2DData>
#include <dtkRationalBezierCurve>
#include <dtkRationalBezierCurve2D>
#include <dtkAbstractNurbsCurveData>
#include <dtkNurbsCurve>
#include <dtkAbstractNurbsCurve2DData>
#include <dtkNurbsCurve2D>
#include <dtkAbstractNurbsSurfaceData>
#include <dtkAbstractRationalBezierSurfaceData>
#include <dtkNurbsSurface>
#include <dtkRationalBezierSurface>
#include <dtkClippedNurbsSurface>

#include <fstream>

//For debuging purpose only
#include <iostream>

#include <QtCore>
#include <QTest>

//#include <dtkTest>
#include <dtkLog>

#include <type_traits>
static_assert(std::is_trivial<dtkContinuousGeometryPrimitives::Array_2>::value, "");
static_assert(std::is_trivial<dtkContinuousGeometryPrimitives::Point_2>::value, "");
static_assert(std::is_trivial<dtkContinuousGeometryPrimitives::Vector_2>::value, "");
static_assert(std::is_trivial<dtkContinuousGeometryPrimitives::Array_3>::value, "");
static_assert(std::is_trivial<dtkContinuousGeometryPrimitives::Point_3>::value, "");
static_assert(std::is_trivial<dtkContinuousGeometryPrimitives::Vector_3>::value, "");
static_assert(std::is_trivial<dtkContinuousGeometryPrimitives::Segment_3>::value, "");
static_assert(std::is_trivial<dtkContinuousGeometryPrimitives::Ray_3>::value, "");
static_assert(std::is_trivial<dtkContinuousGeometryPrimitives::Line_3>::value, "");

dtkContinuousGeometryUtilsTestCase::dtkContinuousGeometryUtilsTestCase(void)
{
    dtkLogger::instance().attachConsole();
#if dtk_VERSION_MAJOR == 1
    dtkLogger::instance().setLevel(dtkLog::Trace);
#else // dtk version 2.x
    dtkLogger::instance().setLevel(dtk::LogLevel::Trace);
#endif

    dtkContinuousGeometrySettings settings;
    settings.beginGroup("continuous-geometry");
    dtkContinuousGeometry::initialize(settings.value("plugins").toString());
    settings.endGroup();
}

dtkContinuousGeometryUtilsTestCase::~dtkContinuousGeometryUtilsTestCase(void)
{

}

void dtkContinuousGeometryUtilsTestCase::initTestCase(void)
{
    m_eps = 1e-10;
}

void dtkContinuousGeometryUtilsTestCase::init(void)
{
}

void dtkContinuousGeometryUtilsTestCase::testArray_2(void)
{
    dtkContinuousGeometryPrimitives::Array_2 array_2(1.23, 2.36);
    QVERIFY(array_2[0] == 1.23);
    QVERIFY(array_2[1] == 2.36);

    array_2[0] = 3.7;
    QVERIFY(array_2[0] == 3.7);

    array_2[1] = array_2[0] + array_2[1];
    QVERIFY(array_2[1] > 6.06 - m_eps && array_2[1] < 6.06 + m_eps);

    double* data = array_2.data();
    QVERIFY(data[1] > 6.06 - m_eps && data[1] < 6.06 + m_eps);
}

void dtkContinuousGeometryUtilsTestCase::testPoint_2(void)
{
    dtkContinuousGeometryPrimitives::Point_2 point_2(3.64, 4.67);
    QVERIFY(point_2[0] == 3.64);
    QVERIFY(point_2[1] == 4.67);

    point_2[0] = 2.45;
    QVERIFY(point_2[0] == 2.45);

    point_2[1] = point_2[0] + point_2[1];
    QVERIFY(point_2[1] > 7.12 - m_eps && point_2[1] < 7.12 + m_eps);

    double* data = point_2.data();
    QVERIFY(data[1] > 7.12 - m_eps && data[1] < 7.12 + m_eps);
}

void dtkContinuousGeometryUtilsTestCase::testVector_2(void)
{
    dtkContinuousGeometryPrimitives::Vector_2 vector_2(4.47, 6.35);
    QVERIFY(vector_2[0] == 4.47);
    QVERIFY(vector_2[1] == 6.35);

    vector_2[0] = 3.21;
    QVERIFY(vector_2[0] == 3.21);

    vector_2[1] = vector_2[0] + vector_2[1];
    QVERIFY(vector_2[1] > 9.56 - m_eps && vector_2[1] < 9.56 + m_eps);
}

void dtkContinuousGeometryUtilsTestCase::testArray_3(void)
{
    dtkContinuousGeometryPrimitives::Array_3 array_3_1(0., 0., 0.);

    dtkContinuousGeometryPrimitives::Array_3 array_3_2(1.55, 7.165, 9.345);
    QVERIFY(array_3_2[0] == 1.55);
    QVERIFY(array_3_2[1] == 7.165);
    QVERIFY(array_3_2[2] == 9.345);
}

void dtkContinuousGeometryUtilsTestCase::testPoint_3(void)
{
    dtkContinuousGeometryPrimitives::Point_3 point_3(1.55, 7.165, 9.345);
    QVERIFY(point_3[0] == 1.55);
    QVERIFY(point_3[1] == 7.165);
    QVERIFY(point_3[2] == 9.345);
}

void dtkContinuousGeometryUtilsTestCase::testVector_3(void)
{
    dtkContinuousGeometryPrimitives::Vector_3 vector_3(1.55, 7.165, 9.345);
    QVERIFY(vector_3[0] == 1.55);
    QVERIFY(vector_3[1] == 7.165);
    QVERIFY(vector_3[2] == 9.345);
}

void dtkContinuousGeometryUtilsTestCase::testSegment_3(void)
{
    dtkContinuousGeometryPrimitives::Point_3 point_3_1(1., 2., 3.5);
    dtkContinuousGeometryPrimitives::Point_3 point_3_2(2., -5.3, 4.);

    dtkContinuousGeometryPrimitives::Segment_3 segment_3(point_3_1, point_3_2);
    QVERIFY(segment_3[0][0] == 1.);
    QVERIFY(segment_3[0][1] == 2.);
    QVERIFY(segment_3[0][2] == 3.5);

    QVERIFY(segment_3[1][0] == 2.);
    QVERIFY(segment_3[1][1] == -5.3);
    QVERIFY(segment_3[1][2] == 4.);
}

void dtkContinuousGeometryUtilsTestCase::testRay_3(void)
{
    dtkContinuousGeometryPrimitives::Point_3 point_3_1(1.2, 3.6, 0.);
    dtkContinuousGeometryPrimitives::Vector_3 vector_3_2(2., -5.3, -7.4);

    dtkContinuousGeometryPrimitives::Ray_3 ray_3(point_3_1, vector_3_2);
    QVERIFY(ray_3.source()[0] == 1.2);
    QVERIFY(ray_3.source()[1] == 3.6);
    QVERIFY(ray_3.source()[2] == 0.);

    QVERIFY(ray_3.direction()[0] == 2.);
    QVERIFY(ray_3.direction()[1] == -5.3);
    QVERIFY(ray_3.direction()[2] == -7.4);
}


void dtkContinuousGeometryUtilsTestCase::testLine_3(void)
{
    dtkContinuousGeometryPrimitives::Line_3 line_3_2(dtkContinuousGeometryPrimitives::Point_3(1.55, 7.165, 9.345), dtkContinuousGeometryPrimitives::Vector_3(1., 0., .0));
    QVERIFY(line_3_2.point()[0] == 1.55);
    QVERIFY(line_3_2.point()[1] == 7.165);
    QVERIFY(line_3_2.point()[2] == 9.345);
}

void dtkContinuousGeometryUtilsTestCase::testIntersect(void)
{
    dtkContinuousGeometryPrimitives::Sphere_3 sphere_a(dtkContinuousGeometryPrimitives::Point_3(0., 0., 0.), 1.);
    dtkContinuousGeometryPrimitives::Sphere_3 sphere_b(dtkContinuousGeometryPrimitives::Point_3(1.5, 0., 0.), 1.);

    QVERIFY(dtkContinuousGeometryTools::intersect(sphere_a, sphere_b));
}

void dtkContinuousGeometryUtilsTestCase::testIsInCircle(void)
{
    dtkContinuousGeometryPrimitives::Circle_2 circle_0(dtkContinuousGeometryPrimitives::Point_2(0.5, 0.5), 1.);
    dtkContinuousGeometryPrimitives::Point_2 point_0_0(0.5, 0.5);
    dtkContinuousGeometryPrimitives::Point_2 point_0_1(1.5, 0.5);
    dtkContinuousGeometryPrimitives::Point_2 point_0_2(1.6, 1.6);
    dtkContinuousGeometryPrimitives::Point_2 point_0_3(1e14, 1e14);


    QVERIFY(dtkContinuousGeometryTools::isInCircle(circle_0, point_0_0) == true);
    QVERIFY(dtkContinuousGeometryTools::isInCircle(circle_0, point_0_1) == true);
    QVERIFY(dtkContinuousGeometryTools::isInCircle(circle_0, point_0_2) == false);
    QVERIFY(dtkContinuousGeometryTools::isInCircle(circle_0, point_0_3) == false);
}

void dtkContinuousGeometryUtilsTestCase::testNormalize(void)
{
    dtkContinuousGeometryPrimitives::Vector_3 vec(2., 2., 2.);
    dtkContinuousGeometryTools::normalize(vec);

    double eps = 1e-5;

    QVERIFY( vec[0] >= 0.57735 - eps && vec[0] <= 0.57735 + eps);
    QVERIFY( vec[1] >= 0.57735 - eps && vec[1] <= 0.57735 + eps);
    QVERIFY( vec[2] >= 0.57735 - eps && vec[2] <= 0.57735 + eps);
}

void dtkContinuousGeometryUtilsTestCase::testBoundingSphere(void)
{
    dtkContinuousGeometryPrimitives::Sphere_3 sphere_a_0(dtkContinuousGeometryPrimitives::Point_3(-1., 0., 0.), 1);
    dtkContinuousGeometryPrimitives::Sphere_3 sphere_b_0(dtkContinuousGeometryPrimitives::Point_3(1., 0., 0.), 1);
    dtkContinuousGeometryPrimitives::Sphere_3 r_sphere_0(dtkContinuousGeometryPrimitives::Point_3(0, 0., 0.), 0);

    dtkContinuousGeometryPrimitives::Sphere_3 test_sphere_0(dtkContinuousGeometryPrimitives::Point_3(0, 0., 0.), 2);

    dtkContinuousGeometryTools::boundingSphere(r_sphere_0, sphere_a_0, sphere_b_0);

    QVERIFY(r_sphere_0.center() == test_sphere_0.center());
    QVERIFY(r_sphere_0.radius() == test_sphere_0.radius());

    dtkContinuousGeometryPrimitives::Sphere_3 sphere_a_1(dtkContinuousGeometryPrimitives::Point_3(1., 0., 0.), 5);
    dtkContinuousGeometryPrimitives::Sphere_3 sphere_b_1(dtkContinuousGeometryPrimitives::Point_3(2., 0., 0.), 1);
    dtkContinuousGeometryPrimitives::Sphere_3 r_sphere_1(dtkContinuousGeometryPrimitives::Point_3(0, 0., 0.), 0);

    dtkContinuousGeometryPrimitives::Sphere_3 test_sphere_1(dtkContinuousGeometryPrimitives::Point_3(1, 0., 0.), 5);

    dtkContinuousGeometryTools::boundingSphere(r_sphere_1, sphere_a_1, sphere_b_1);

    QVERIFY(r_sphere_1.center() == test_sphere_1.center());
    QVERIFY(r_sphere_1.radius() == test_sphere_1.radius());

    dtkContinuousGeometryPrimitives::Sphere_3 sphere_a_2(dtkContinuousGeometryPrimitives::Point_3(1., 0., 0.), 3.);
    dtkContinuousGeometryPrimitives::Sphere_3 sphere_b_2(dtkContinuousGeometryPrimitives::Point_3(4., 0., 0.), 1.);
    dtkContinuousGeometryPrimitives::Sphere_3 r_sphere_2(dtkContinuousGeometryPrimitives::Point_3(0, 0., 0.), 0);

    dtkContinuousGeometryPrimitives::Sphere_3 test_sphere_2(dtkContinuousGeometryPrimitives::Point_3(1.5, 0., 0.), 3.5);

    dtkContinuousGeometryTools::boundingSphere(r_sphere_2, sphere_a_2, sphere_b_2);

    QVERIFY(r_sphere_2.center() == test_sphere_2.center());
    QVERIFY(r_sphere_2.radius() == test_sphere_2.radius());

    dtkContinuousGeometryPrimitives::Sphere_3 sphere_a_3(dtkContinuousGeometryPrimitives::Point_3(1., 15., -6.), 7.);
    dtkContinuousGeometryPrimitives::Sphere_3 sphere_b_3(dtkContinuousGeometryPrimitives::Point_3(1., 15., -6.), 7.);
    dtkContinuousGeometryPrimitives::Sphere_3 r_sphere_3(dtkContinuousGeometryPrimitives::Point_3(0, 0., 0.), 0);

    dtkContinuousGeometryPrimitives::Sphere_3 test_sphere_3(dtkContinuousGeometryPrimitives::Point_3(1., 15., -6.), 7.);

    dtkContinuousGeometryTools::boundingSphere(r_sphere_3, sphere_a_3, sphere_b_3);

    QVERIFY(r_sphere_3.center()[0] > test_sphere_3.center()[0] - m_eps && r_sphere_3.center()[0] < test_sphere_3.center()[0] + m_eps);
    QVERIFY(r_sphere_3.center()[1] > test_sphere_3.center()[1] - m_eps && r_sphere_3.center()[1] < test_sphere_3.center()[1] + m_eps);
    QVERIFY(r_sphere_3.center()[2] > test_sphere_3.center()[2] - m_eps && r_sphere_3.center()[2] < test_sphere_3.center()[2] + m_eps);
    QVERIFY(r_sphere_3.radius() > test_sphere_3.radius() - m_eps && r_sphere_3.radius() < test_sphere_3.radius() + m_eps);

    dtkContinuousGeometryPrimitives::Sphere_3 sphere_a_4(dtkContinuousGeometryPrimitives::Point_3(7., 12., 3.5), 5.);
    dtkContinuousGeometryPrimitives::Sphere_3 sphere_b_4(dtkContinuousGeometryPrimitives::Point_3(-12., 21., 3.), 3.);
    dtkContinuousGeometryPrimitives::Sphere_3 r_sphere_4(dtkContinuousGeometryPrimitives::Point_3(0, 0., 0.), 0);

    dtkContinuousGeometryPrimitives::Sphere_3 test_sphere_4(dtkContinuousGeometryPrimitives::Point_3(-1.59651763471, 16.07203466907, 3.27377585172), 14.51487042241);

    dtkContinuousGeometryTools::boundingSphere(r_sphere_4, sphere_a_4, sphere_b_4);

    QVERIFY(r_sphere_4.center()[0] > test_sphere_4.center()[0] - m_eps && r_sphere_4.center()[0] < test_sphere_4.center()[0] + m_eps);
    QVERIFY(r_sphere_4.center()[1] > test_sphere_4.center()[1] - m_eps && r_sphere_4.center()[1] < test_sphere_4.center()[1] + m_eps);
    QVERIFY(r_sphere_4.center()[2] > test_sphere_4.center()[2] - m_eps && r_sphere_4.center()[2] < test_sphere_4.center()[2] + m_eps);
    QVERIFY(r_sphere_4.radius() > test_sphere_4.radius() - m_eps && r_sphere_4.radius() < test_sphere_4.radius() + m_eps);
}

void dtkContinuousGeometryUtilsTestCase::testConvexHullApproximationError(void)
{
    dtkAbstractRationalBezierCurveData *curve_data = dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().create("dtkRationalBezierCurveDataOn");
    if(curve_data == nullptr) {
        dtkWarn() << "This test requires that the openNURBS implementation of dtkAbstractRationalBezierCurveData is available";
        return;
    } else {
        dtkRationalBezierCurve *curve = new dtkRationalBezierCurve(curve_data);

        std::size_t order = 3;
        double* cps = new double[(3 + 1) * order];
        cps[0] = 0.;  cps[1] = 0.;  cps[2] = 0.; cps[3] = 1.;
        cps[4] = 1.;  cps[5] = 1.;  cps[6] = 0.; cps[7] = 1.;
        cps[8] = 2.;  cps[9] = 0.;  cps[10] = 0.; cps[11] = 1.;
        curve_data->create(order, cps);
        dtkContinuousGeometryPrimitives::Point_3 mid(0., 0., 0.);
        curve->evaluatePoint(0.5, mid.data());

        double error = dtkContinuousGeometryTools::convexHullApproximationError(*curve);

        QVERIFY(error >  mid[1]);
        delete curve;
        return;
    }
}

void dtkContinuousGeometryUtilsTestCase::testConvexHull3DApproximation(void)
{
    dtkAbstractRationalBezierCurve2DData *curve_2D_data = dtkContinuousGeometry::abstractRationalBezierCurve2DData::pluginFactory().create("dtkRationalBezierCurve2DDataOn");
    dtkAbstractRationalBezierSurfaceData *surface_data = dtkContinuousGeometry::abstractRationalBezierSurfaceData::pluginFactory().create("dtkRationalBezierSurfaceDataOn");
    if(curve_2D_data == nullptr) {
        dtkWarn() << "This test requires that the openNURBS implementation of dtkAbstractRationalBezierCurve2DData is available";
        return;
    } else if(surface_data == nullptr) {
        dtkWarn() << "This test requires that the openNURBS implementation of dtkAbstractRationalBezierSurfaceData is available";
        return;
    } else {
        dtkRationalBezierCurve2D *curve = new dtkRationalBezierCurve2D(curve_2D_data);
        dtkRationalBezierSurface *surface = new dtkRationalBezierSurface(surface_data);
        //bi-degree 1,2
        std::size_t dim = 3;
        std::size_t nb_cp_u = 4;
        std::size_t nb_cp_v = 4;

        double* s_cps = new double[(dim + 1) * nb_cp_u * nb_cp_v];
        s_cps[0] =  0.;   s_cps[1] = 0.; s_cps[2] = 1.;  s_cps[3] = 1.;
        s_cps[4] =  0.;   s_cps[5] = 1.; s_cps[6] = 2.;  s_cps[7] = 1.;
        s_cps[8] =  0.;   s_cps[9] = 2.; s_cps[10] = 3.; s_cps[11] = 1.;
        s_cps[12] = 0.;   s_cps[13] = 3.; s_cps[14] = 4.; s_cps[15] = 1.;
        s_cps[16] = 1.;   s_cps[17] = 0.; s_cps[18] = 4.; s_cps[19] = 1.;
        s_cps[20] = 1.;   s_cps[21] = 1.; s_cps[22] = 3.; s_cps[23] = 1.;
        s_cps[24] = 1.;   s_cps[25] = 2.; s_cps[26] = 2.; s_cps[27] = 1.;
        s_cps[28] = 1.;   s_cps[29] = 3.; s_cps[30] = 1.; s_cps[31] = 1.;
        s_cps[32] = 2.;   s_cps[33] = 0.; s_cps[34] = 1.; s_cps[35] = 1.;
        s_cps[36] = 2.;   s_cps[37] = 1.; s_cps[38] = 2.; s_cps[39] = 1.;
        s_cps[40] = 2.;   s_cps[41] = 2.; s_cps[42] = 3.; s_cps[43] = 1.;
        s_cps[44] = 2.;   s_cps[45] = 3.; s_cps[46] = 4.; s_cps[47] = 1.;
        s_cps[48] = 3.;   s_cps[49] = 0.; s_cps[50] = 4.; s_cps[51] = 1.;
        s_cps[52] = 3.;   s_cps[53] = 1.; s_cps[54] = 3.; s_cps[55] = 1.;
        s_cps[56] = 3.;   s_cps[57] = 2.; s_cps[58] = 2.; s_cps[59] = 1.;
        s_cps[60] = 3.;   s_cps[61] = 3.; s_cps[62] = 1.; s_cps[63] = 1.;

        surface_data->create(dim, nb_cp_u, nb_cp_v, s_cps);

        std::ofstream bezier_surface_file("rational_bezier_surface.xyz");
        bezier_surface_file << (*surface);

        std::size_t order = 4;
        double* cps = new double[(2 + 1) * order];
        cps[0] = 0.;  cps[1] = 0.;  cps[2] = 1.;
        cps[3] = 0.1;  cps[4] = 1.; cps[5] = 1.;
        cps[6] = 1.;  cps[7] = 1.;  cps[8] = 1.;
        cps[9] = 0.; cps[10] = 1.; cps[11] = 1.;
        curve_2D_data->create(order, cps);

        dtkContinuousGeometryPrimitives::Point_2 p(0., 0.);
        dtkContinuousGeometryPrimitives::Point_3 p_3(0., 0., 0.);
        std::ofstream file("curve_3d.xyz");
        for(double i = 0.; i < 1.; i += 0.01) {
            curve->evaluatePoint(i,p.data());
            surface->evaluatePoint(p[0], p[1], p_3.data());
            file << p_3 << std::endl;
        }

        std::list< dtkContinuousGeometryPrimitives::Point_2 > polyline;
        dtkContinuousGeometryTools::convexHull3DApproximation(polyline, *curve, *surface, 0.0025);
        std::ofstream polyline_file("polyline.xyz");

        for(auto point : polyline) {
            surface->evaluatePoint(point[0], point[1], p_3.data());
            polyline_file << p_3 << std::endl;
        }

        // delete curve;
        delete surface;
        return;
    }
}

void dtkContinuousGeometryUtilsTestCase::testConvexHullApproximation(void)
{
    dtkAbstractRationalBezierCurveData *curve_data = dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().create("dtkRationalBezierCurveDataOn");

    if(curve_data == nullptr) {
        dtkWarn() << "This test requires that the openNURBS implementation of dtkAbstractRationalBezierCurveData is available";
        return;
    }  else {
        dtkRationalBezierCurve *curve = new dtkRationalBezierCurve(curve_data);

        std::size_t order = 4;
        double* cps = new double[(3 + 1) * order];
        cps[0] = 0.;  cps[1] = 0.;  cps[2] = 0.; cps[3] = 1.;
        cps[4] = 1.;  cps[5] = 10.;  cps[6] = 0.; cps[7] = 1.;
        cps[8] = 2.;  cps[9] = 5.;  cps[10] = 0.; cps[11] = 1.;
        cps[12] = 3.; cps[13] = 0.; cps[14] = 0.; cps[15] = 1.;
        curve_data->create(order, cps);
        std::list< dtkContinuousGeometryPrimitives::Point_3 > polyline;
        dtkContinuousGeometryTools::convexHullApproximation(polyline, *curve, 0.01);

        delete curve;
        return;
    }
}

void dtkContinuousGeometryUtilsTestCase::testConvexHull2DApproximation(void)
{
    dtkAbstractRationalBezierCurve2DData *curve_data = dtkContinuousGeometry::abstractRationalBezierCurve2DData::pluginFactory().create("dtkRationalBezierCurve2DDataOn");
    dtkAbstractRationalBezierCurve2DData *curve_data_2 = dtkContinuousGeometry::abstractRationalBezierCurve2DData::pluginFactory().create("dtkRationalBezierCurve2DDataOn");

    if(curve_data == nullptr) {
        dtkWarn() << "This test requires that the openNURBS implementation of dtkAbstractRationalBezierCurve2DData is available";
        return;
    }  else {
        dtkRationalBezierCurve2D *curve = new dtkRationalBezierCurve2D(curve_data);

        std::size_t order = 4;
        std::vector<double> cps((2 + 1) * order);
        cps[0] = 0.;  cps[1] = 0.;  cps[2] = 1.;
        cps[3] = 1.;  cps[4] = 10.; cps[5] = 1.;
        cps[6] = 2.;  cps[7] = 5.;  cps[8] = 1.;
        cps[9] = 3.; cps[10] = 0.; cps[11] = 1.;
        curve_data->create(order, cps.data());

        std::list< dtkContinuousGeometryPrimitives::Point_2 > polyline;
        polyline.clear();

        dtkRationalBezierCurve2D *curve_2 = new dtkRationalBezierCurve2D(curve_data_2);
        curve_data_2->create(order, cps.data());

        dtkContinuousGeometryTools::convexHull2DApproximation(polyline, *curve, 0.01, 0.3, 0.6);
        dtkContinuousGeometryTools::convexHull2DApproximation(polyline, *curve, 0.01);

        delete curve;
        delete curve_2;
        return;
    }
}

void dtkContinuousGeometryUtilsTestCase::testDecomposeToSplitRationalBezierCurves(void)
{
    dtkAbstractNurbsCurve2DData *curve_data = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOn");
    dtkAbstractNurbsSurfaceData *surface_data = dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().create("dtkNurbsSurfaceDataOn");
    if(curve_data == nullptr) {
        dtkWarn() << "This test requires that the openNURBS implementation of dtkAbstractRationalBezierCurve2DData is available";
        return;
    } else if(surface_data == nullptr){
        dtkWarn() << "This test requires that the openNURBS implementation of dtkAbstractNurbsSurfaceData is available";
        return;
    } else {
        dtkNurbsCurve2D curve(curve_data);
        std::size_t order = 3;
        double* knotsc = new double[2 * order - 2];
        /* 0. */ knotsc[0] = 0.; knotsc[1] = 0; knotsc[2] = 2.; knotsc[3] = 2.; /* 2. */

        double* cpc = new double[(2 + 1) * order];
        cpc[0] = 0.;  cpc[1] = 0.;  cpc[2] = 1.;
        cpc[3] = 1.;  cpc[4] = 1.;  cpc[5] = 1.;
        cpc[6] = 2.;  cpc[7] = 0.;  cpc[8] = 1.;
        curve_data->create(order, order, knotsc, cpc);

        dtkNurbsSurface surface = dtkNurbsSurface(surface_data);
        //bi-degree 1,2
        std::size_t dim = 3;
        std::size_t nb_cp_u = 4;
        std::size_t nb_cp_v = 5;
        std::size_t order_u = 2;
        std::size_t order_v = 3;
        /* memory leak */
        double* knots_u = new double[nb_cp_u + order_u - 2];
        /* 0. */ knots_u[0] = 0.; knots_u[1] = 1.2; knots_u[2] = 2.7; knots_u[3] = 3.; /* 3. */
        double* knots_v = new double[nb_cp_v + order_v - 2];
        /* 0. */ knots_v[0] = 0.; knots_v[1] = 0.; knots_v[2] = 0.66; knots_v[3] = 1.33; knots_v[4] = 2.; knots_v[5] = 2.; /* 2. */

        double* cps = new double[(dim + 1) * nb_cp_u * nb_cp_v];
        cps[0] = 0.;   cps[1] = 0.; cps[2] = 0.;  cps[3] = 1.;
        cps[4] = 1.;   cps[5] = 0.; cps[6] = 1.;  cps[7] = 1.;
        cps[8] = 2.;   cps[9] = 0.; cps[10] = 2.; cps[11] = 1.;
        cps[12] = 3.;  cps[13] = 0.;  cps[14] = 3.; cps[15] = 1.;
        cps[16] = 4.;  cps[17] = 0.;  cps[18] = 3.; cps[19] = 1.;
        cps[20] = 0.;  cps[21] = 1.; cps[22] = 0.;  cps[23] = 1.;
        cps[24] = 1.;  cps[25] = 1.; cps[26] = 0.;  cps[27] = 1.;
        cps[28] = 2.;  cps[29] = 1.; cps[30] = 0.; cps[31] = 1.;
        cps[32] = 3.;  cps[33] = 1.;  cps[34] = 0.; cps[35] = 1.;
        cps[36] = 4.;  cps[37] = 2.; cps[38] = 0.;  cps[39] = 1.;
        cps[40] = 0.;  cps[41] = 2.; cps[42] = 0.;  cps[43] = 1.;
        cps[44] = 1.;  cps[45] = 2.; cps[46] = 0.;  cps[47] = 1.;
        cps[48] = 2.;  cps[49] = 2.; cps[50] = 0.; cps[51] = 1.;
        cps[52] = 3.;  cps[53] = 2.;  cps[54] = 0.; cps[55] = 1.;
        cps[56] = 4.;  cps[57] = 2.;  cps[58] = 0.; cps[59] = 1.;
        cps[60] = 0.;  cps[61] = 3.; cps[62] = 0.;  cps[63] = 1.;
        cps[64] = 1.;  cps[65] = 3.; cps[66] = 0.;  cps[67] = 1.;
        cps[68] = 2.;  cps[69] = 3.; cps[70] = 0.; cps[71] = 1.;
        cps[72] = 3.;  cps[73] = 3.;  cps[74] = 0.; cps[75] = 1.;
        cps[76] = 4.;  cps[77] = 3.;  cps[78] = 0.; cps[79] = 1.;
        surface_data->create(dim, nb_cp_u, nb_cp_v, order_u, order_v, knots_u, knots_v, cps);

        dtkClippedNurbsSurface clipped_nurbs_surface(surface);
        std::vector< std::tuple< dtkRationalBezierCurve2D *, double *, const dtkRationalBezierSurface *> > splits;
        dtkContinuousGeometryTools::decomposeToSplitRationalBezierCurves(splits, curve, clipped_nurbs_surface);

        return;
    }
}

void dtkContinuousGeometryUtilsTestCase::testRecomposeNurbsCurve(void)
{
    dtkAbstractNurbsCurveData *curve_data = dtkContinuousGeometry::abstractNurbsCurveData::pluginFactory().create("dtkNurbsCurveDataOn");
    if(curve_data == nullptr) {
        dtkWarn() << "This test requires that the openNURBS implementation of dtkAbstractRationalBezierCurveData is available";
        return;
    } else {
        dtkNurbsCurve curve(curve_data);
        std::size_t order = 3;
        double* knots = new double[5];
        /* -1. */ knots[0] = -1.; knots[1] = -1; knots[2] = 0.; knots[3] = 1.; knots[4] = 1.; /* 1. */

        double* cps = new double[(3 + 1) * 4];
        cps[0] = 0.;   cps[1] = 0.; cps[2] = 0.;  cps[3] = 1.;
        cps[4] = 1.;   cps[5] = 0.; cps[6] = 1.;  cps[7] = 1.;
        cps[8] = 2.;   cps[9] = 0.; cps[10] = -1.; cps[11] = 1.;
        cps[12] = 3.;  cps[13] = 0.;  cps[14] = 0.; cps[15] = 1.;
        curve_data->create(3, 4, order, knots, cps);

        std::vector< dtkRationalBezierCurve * > bezier_curves;
        curve.decomposeToRationalBezierCurves(bezier_curves);
        QVERIFY(bezier_curves.size() == 2);

        dtkNurbsCurve nurbs_curve;
        dtkContinuousGeometryTools::recomposeNurbsCurve(nurbs_curve, bezier_curves);
        QVERIFY(nurbs_curve.degree() == 2);
        QVERIFY(nurbs_curve.nbCps() == 5);
        for(auto bc = bezier_curves.begin(); bc != bezier_curves.end(); ++bc) {
            delete (*bc);
        }
    }

}

void dtkContinuousGeometryUtilsTestCase::testIsAPlane(void)
{
    dtkAbstractRationalBezierSurfaceData *rb_surface_data_0 = dtkContinuousGeometry::abstractRationalBezierSurfaceData::pluginFactory().create("dtkRationalBezierSurfaceDataOn");
    dtkAbstractRationalBezierSurfaceData *rb_surface_data_1 = dtkContinuousGeometry::abstractRationalBezierSurfaceData::pluginFactory().create("dtkRationalBezierSurfaceDataOn");
    dtkAbstractNurbsSurfaceData *nurbs_surface_data_0 = dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().create("dtkNurbsSurfaceDataOn");
    dtkAbstractNurbsSurfaceData *nurbs_surface_data_1 = dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().create("dtkNurbsSurfaceDataOn");
    if(rb_surface_data_0 == nullptr || rb_surface_data_1 == nullptr) {
        dtkWarn() << "This test requires that the openNURBS implementation of dtkAbstractRationalBezierSurfaceData is available";
        return;
    }
    std::size_t dim = 3;
    //bi-degree 1,1
    std::size_t order_u = 2;
    std::size_t order_v = 2;

    dtkRationalBezierSurface *rb_surface_0 = new dtkRationalBezierSurface(rb_surface_data_0);

    std::vector< double > cps_0((dim + 1) * order_u * order_v);
    cps_0[0] = 0.;   cps_0[1] = 0.; cps_0[2] = 0.;  cps_0[3] = 1.;
    cps_0[4] = 0.;   cps_0[5] = 2.; cps_0[6] = 0.;  cps_0[7] = 1.;
    cps_0[8] = 2.;   cps_0[9] = 0.; cps_0[10] = 1.; cps_0[11] = 1.;
    cps_0[12] = 2.;   cps_0[13] = 2.; cps_0[14] = 0.; cps_0[15] = 1.;
    rb_surface_0->create(dim, order_u, order_v, cps_0.data());

    QVERIFY(dtkContinuousGeometryTools::isAPlane(*rb_surface_0) == false);
    delete rb_surface_0;

    dtkRationalBezierSurface *rb_surface_1 = new dtkRationalBezierSurface(rb_surface_data_1);
    std::vector< double > cps_1((dim + 1) * order_u * order_v);
    cps_1[0] = 0.;   cps_1[1] = 0.; cps_1[2] = 0.;  cps_1[3] = 1.;
    cps_1[4] = 0.;   cps_1[5] = 2.; cps_1[6] = 0.;  cps_1[7] = 1.;
    cps_1[8] = 2.;   cps_1[9] = 0.; cps_1[10] = 0.; cps_1[11] = 1.;
    cps_1[12] = 2.;   cps_1[13] = 2.; cps_1[14] = 0.; cps_1[15] = 1.;
    rb_surface_1->create(dim, order_u, order_v, cps_1.data());

    QVERIFY(dtkContinuousGeometryTools::isAPlane(*rb_surface_1) == true);
    delete rb_surface_1;

    if(nurbs_surface_data_0 == nullptr || nurbs_surface_data_1 == nullptr) {
        dtkWarn() << "This test requires that the openNURBS implementation of dtkAbstractNurbsSurfaceData is available";
        return;
    }

    std::size_t nb_cp_u = 2;
    std::size_t nb_cp_v = 2;

    dtkNurbsSurface *nurbs_surface_0 = new dtkNurbsSurface(nurbs_surface_data_0);

    std::vector< double > knots_u(nb_cp_u + order_u - 2);
    /* -2. */ knots_u[0] = -2.; knots_u[1] = 3.; /* 3. */
    std::vector< double > knots_v(nb_cp_v + order_v - 2);
    /* -1. */ knots_v[0] = -1.; knots_v[1] = 2.; /* 2. */

    nurbs_surface_0->create(dim, nb_cp_u, nb_cp_v, order_u, order_v, knots_u.data(), knots_v.data(), cps_0.data());

    QVERIFY(dtkContinuousGeometryTools::isAPlane(*nurbs_surface_0) == false);
    delete nurbs_surface_0;

    dtkNurbsSurface *nurbs_surface_1 = new dtkNurbsSurface(nurbs_surface_data_1);

    nurbs_surface_1->create(dim, nb_cp_u, nb_cp_v, order_u, order_v, knots_u.data(), knots_v.data(), cps_1.data());

    QVERIFY(dtkContinuousGeometryTools::isAPlane(*nurbs_surface_1) == true);
    delete nurbs_surface_1;
    return;
}

void dtkContinuousGeometryUtilsTestCase::cleanup(void) {}

void dtkContinuousGeometryUtilsTestCase::cleanupTestCase(void) {}

#define DTKTEST_MAIN_NOGUI(TestMain, TestObject)    \
    int TestMain(int argc, char *argv[])            \
    {                                               \
        QCoreApplication app(argc, argv);           \
        TestObject tc;                              \
        return QTest::qExec(&tc, argc, argv);       \
    }


DTKTEST_MAIN_NOGUI(dtkContinuousGeometryUtilsTest, dtkContinuousGeometryUtilsTestCase)

//
// dtkContinuousGeometryUtilsTest.cpp ends here
