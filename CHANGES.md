# ChangeLog
## 0.2.2 - 2018-03-14
- fix RPATH target property for composer extension
## 0.2.1 - 2018-03-09
- fix target project for composer extension
## 0.2.0 - 2018-03-08
- depends on dtk 1.4.0
- numerous API changes
